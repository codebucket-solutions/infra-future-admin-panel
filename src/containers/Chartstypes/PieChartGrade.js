import React , {Component } from 'react';
import {Pie} from 'react-chartjs-2';
import SimpleSpinner from '../../components/Spinner/simpleSpinner';
import equal from 'fast-deep-equal';


class RadarChart extends Component{

	constructor(props){
		super(props);
		this.state = {
			
			labels: [],
			datasets: [{
				data: [],
				backgroundColor: [
				'#FF6384',
				'#36A2EB',
				'#FFCE56'
				],
				hoverBackgroundColor: [
				'#FF6384',
				'#36A2EB',
				'#FFCE56'
				]
			}]
			
		}
	}

	componentDidMount(){
		if(this.props.data && this.props.data.length > 0){
			this.updateGraph()
		}
	}

	componentDidUpdate(prevProps) {
		if(!equal(this.props.data, prevProps.data)){
			if(this.props.data && this.props.data.length > 0){
				this.updateGraph()
			}
			else{
				this.setState({labels:[]})
			}
			
		}
	}

	updateGraph =() =>{
        let title = this.props.title.split('-')[1];
        console.log(title)
        let move = 0;
        let modern = 0;
        this.setState({labels:['Move As Is', 'Modernize']})
        if(title == 'DISPOVERSUIT'){
            this.props.data.forEach(d =>{
                // console.log(d)
                // console.log(d["MoveAsIs-Value"], d['Modernize-Value'])
				let moveAsIs = d['Move As Is Global Value'] ? d['Move As Is Global Value'] : d['MoveAsIs-Value'];
				let modernize = d['Modernize Global Value'] ? d['Modernize Global Value'] : d['Modernize-Value'];
				// console.log(moveAsIs, modernize);
                if(moveAsIs >= modernize){
                    move++;
                }
                else{
                    modern++;
                }
            })
        }
        else if(title == 'DISPTECHFIT'){
            this.props.data.forEach(d =>{
                // console.log(d)
				let moveAsIs = d['MoveAsIs Global Value'] ? d['MoveAsIs Global Value'] : d['MoveAsIs-Global'];
				let modernize = d['Modernize Global Value'] ? d['Modernize Global Value'] : d['Modernize-Global'];
				// console.log(moveAsIs, modernize);
                if(moveAsIs >= modernize){
                    move++;
                }
                else{
                    modern++;
                }
            })
        }
		

		let {datasets} = this.state;
		datasets[0].data =[move, modern]

		console.log(datasets)

		this.setState({
			datasets: datasets
		})
	}

	render() {
			const { dropDown } =this.state;
			//console.log(this.state)
			return (
				<>
				{
					this.props.loading ? <SimpleSpinner /> :<Pie data={this.state} />
				}
				</>	
			);
	}
};

 
export default RadarChart;