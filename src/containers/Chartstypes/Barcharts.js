
import React, { Component } from 'react';
import { connect } from 'react-redux';
import SimpleSpinner from '../../components/Spinner/simpleSpinner';
import equal from 'fast-deep-equal';
import { generateGraph } from '../../shared/utility';

import { BarChart, Bar, XAxis, YAxis, Tooltip, ResponsiveContainer } from 'recharts';

class BarCharts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            selectedData: [],
            dataKey: 'Apps',
        }
    }

    componentDidMount() {
        // console.log(this.props.data);
        this.updateGraph();

    }

    componentDidUpdate(prevProps) {
        if (!equal(this.props.data, prevProps.data) || !equal(this.props.selectedApps, prevProps.selectedApps)) {

            // console.log(this.props.data)
            this.updateGraph();
        }
    }

    updateGraph = () => {
        let _data = generateGraph(this.props.data, this.props.selectedApps)
        this.setState({ selectedData: _data.selectedData, dataKey: _data.dataKey, data: _data.data});
    }
    
    render() {
        console.log(this.state);

        return (
            <div style={{ overflowX: 'scroll', overflowY: 'hidden' }}>
                {
                    this.props.loading ? <SimpleSpinner /> :
                        <ResponsiveContainer width={'100%'} height={300}>
                            <BarChart data={this.state.selectedData} maxBarSize={10} >
                                <XAxis padding={{ left: 20, right: 0, }} dataKey={this.state.dataKey} />
                                <YAxis />

                                <Tooltip />
                                <Bar dataKey="technicalFitness" fill="#7A6FBE" />
                                <Bar dataKey="businessValue" fill="#28BBE3" />
                                <Bar dataKey="risk" fill="#787878" />
                            </BarChart>
                        </ResponsiveContainer>

                }
            </div>
        );
    }
}

const mapStatetoProps = state => {
    return {
        tiny_data: state.tiny_red.tinydata
    };
}
export default connect(mapStatetoProps, null)(BarCharts);