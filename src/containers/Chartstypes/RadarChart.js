import React, { Component } from 'react';
import { Radar } from 'react-chartjs-2';
import SimpleSpinner from '../../components/Spinner/simpleSpinner';
import equal from 'fast-deep-equal';

const color = [
    {
        backgroundColor: 'rgba(179,181,198,0.2)',
        borderColor: 'rgba(179,181,198,1)',
        pointBackgroundColor: 'rgba(179,181,198,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(179,181,198,1)',
    },
    {
        backgroundColor: 'rgba(255,99,132,0.2)',
        borderColor: 'rgba(255,99,132,1)',
        pointBackgroundColor: 'rgba(255,99,132,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(255,99,132,1)',
    },
    {
        backgroundColor: 'rgba(248,98,131,0.2)',
        borderColor: 'rgba(248,98,131,1)',
        pointBackgroundColor: 'rgba(248,98,131,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(248,98,131,1)',
    },
    {
        backgroundColor: 'rgba(54,162,235,0.2)',
        borderColor: 'rgba(54,162,235,1)',
        pointBackgroundColor: 'rgba(54,162,235,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(54,162,235,1)',
    },
    {
        backgroundColor: 'rgba(88,219,131,0.2)',
        borderColor: 'rgba(88,219,131,1)',
        pointBackgroundColor: 'rgba(88,219,131,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(88,219,131,1)',
    }
]

class RadarChart extends Component {

    constructor(props) {
        super(props);
        this.state = {
            labels: [],
            datasets: [],
            data: [],
        }
    }

    componentDidMount() {
        console.log(this.props);
        this.updateGraph(this.props.data, this.props.selectedApps)
    }

    componentDidUpdate(prevProps) {
        if (!equal(this.props.data, prevProps.data) || !equal(this.props.selectedApps, prevProps.selectedApps)) {
            //console.log(this.props.data)
            console.log(this.props.selectedApps);

            this.updateGraph(this.props.data, this.props.selectedApps)
        }
    }

    updateGraph = (data , selectedApps) => {
        if (data && data.length > 0) {
            let apps = [];
            let dataSet = [];
    
            let newData = { ...data[0] };
            let keys = Object.keys(newData);
            let allKeys = [...keys];
            keys.shift();
            let radar = keys.map(m => m.toUpperCase())
    
            radar.pop();
            if (selectedApps && selectedApps.length > 0) {
                data.forEach((d, i) => {
                    let apps = d.APPS ? d.APPS : (d.Apps ? d.Apps : (d.apps ? d.apps : null))
                    let selected = selectedApps.filter(app => app === apps)
                    if (selected.length > 0) {
                        newData = { ...d };
                        delete newData[allKeys[0]];
                        dataSet.push({
                            label: d[allKeys[0]],
                            ...color[i%5],
                            data: Object.values(newData)
                        })
                    }
                    
                })
            }
            else{
                data.forEach((d, i) => {
                    if (i < 5) {
                        newData = { ...d };
                        delete newData[allKeys[0]];
                        dataSet.push({
                            label: d[allKeys[0]],
                            ...color[i],
                            data: Object.values(newData)
                        })
                    }
                })
            }
        
            // console.log(radar, dataSet)
            this.setState({ labels: radar, datasets: dataSet, data:data })
        }
        else {
            this.setState({ data: [], labels: [], datasets: [] })
        }   
    }

    render() {
        return (
            <>
                {
                    this.props.loading ? <SimpleSpinner /> : <Radar data={this.state} />
                }
            </>
        );
    }
};


export default RadarChart;