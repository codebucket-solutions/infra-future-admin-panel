import React , {Component } from 'react';
import {Pie} from 'react-chartjs-2';
import SimpleSpinner from '../../components/Spinner/simpleSpinner';

const data = {
	labels: [
		'Red',
		'Blue',
		'Yellow'
	],
	datasets: [{
		data: [300, 50, 100],
		backgroundColor: [
		'#FF6384',
		'#36A2EB',
		'#FFCE56'
		],
		hoverBackgroundColor: [
		'#FF6384',
		'#36A2EB',
		'#FFCE56'
		]
	}]
};

class RadarChart extends Component{

	constructor(props){
		super(props);
		this.state = {
			dropDown :[]
		}
	}

	componentDidMount(){
		if(this.props.data){
			this.updateGraph()
		}
	}

	updateGraph =() =>{
		let keys = Object.keys(this.props.data[0]);
		let _temp_keys = [...keys];
		_temp_keys.shift(); 
		_temp_keys.pop(); 
		console.log(keys, _temp_keys)
		this.setState({dropDown: _temp_keys})
	}

  render() {
	  const { dropDown } =this.state;
    return (
        
			<div>
                <div className="col-xl-6">
                    <div className="card m-b-20">
                        <div className="card-body">
							<div className="row">
								<div className="col-md-9">
									<h4 className="mt-0 header-title">{this.props.title}</h4>
								</div>
								<div className="col-md-3">
									<select 
									required
									className="form-control" 
									onChange={(event) => this.inputChangeHandler(event)}
									value={this.state.platform}>
									{dropDown.map((c,i) =>{
										return (<option key={i} value={c}>{c}</option>)
									})}
									</select>
								</div>
							</div>
                            
                            <div id="morris-donut-example" className="">
                            {
                                this.props.loading ? <SimpleSpinner /> :<Pie data={data} />
                            }
                            </div>
                        </div>
                    </div>
                </div>
			    
			</div>
		);
  }
};

 
export default RadarChart;