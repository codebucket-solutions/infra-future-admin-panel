import React , {Component }from "react";
import { render } from "react-dom";
import { Chart } from "react-google-charts";
import SimpleSpinner from '../../components/Spinner/simpleSpinner';
import equal from 'fast-deep-equal';


class BubbleChart extends Component{

	constructor(props){
		super(props);
		this.state = {
			
			data :[]
		}
	}

	componentDidMount(){
		// console.log(this.props.data);
		if(this.props.data && this.props.data.length > 0){
			this.updateGraph(this.props.data ? this.props.data : [], this.props.selectedApps)
		}
	}

	componentDidUpdate(prevProps) {
        //console.log(this.props.data)
		if(!equal(this.props.data, prevProps.data) || !equal(this.props.selectedApps, prevProps.selectedApps)){
			this.updateGraph(this.props.data ? this.props.data : [], this.props.selectedApps)
		}
	}

	updateGraph =(data, selectedApps) =>{
		let _data = [];
		if(data && data.length > 0){
			let _label = Object.keys(data[0]);
			_label.splice(4, 0, "aRisk");
			_data.push([_label[0], _label[1], _label[2], _label[3], _label[4]])
			if (selectedApps && selectedApps.length > 0) {
				data.forEach((d,i) => {
					let apps = d.APPS ? d.APPS : (d.Apps ? d.Apps : (d.apps ? d.apps : null))
					let selected = selectedApps.filter(app => app === apps)
					if (selected.length > 0) {
						let a = {
							...d,
							'aRisk':d['risk'] == 0 ? 10 : 1/parseFloat(d['risk'])
						}
						// console.log(a);
						_data.push([a[_label[0]],a[_label[1]],a[_label[2]],a[_label[3]],a[_label[4]]])
					}
				})
			}
			else{
				data.forEach((d,i) => {
					if(i<5){
						let a = {
							...d,
							'aRisk':d['risk'] == 0 ? 10 : 1/parseFloat(d['risk'])
						}
						// console.log(a);
						_data.push([a[_label[0]],a[_label[1]],a[_label[2]],a[_label[3]],a[_label[4]]])
					}
				})
			}
			
			// console.log(data)
			this.setState({
				data: _data
			})
		}
		else{
			this.setState({data:[]})
		}
        
	}


	render() {
			const { dropDown } =this.state;
			//console.log(this.state)
			return (
				<>
				{
                    this.props.loading ? <SimpleSpinner /> :
                    <Chart
                            chartType="BubbleChart"
                            data={this.state.data}
                            height={'280px'}
                            width={'100%'}
                            options={{
                                hAxis: { title: "technicalFitness" },
                                vAxis: { title: "businessValue" },
								bubble: { textStyle: { fontSize: 0.001 } },
								chartArea: { left: '8%', top: '10%',bottom: '10%',width: "100%", height: "100%"}
                            }}
  
                    />
				}
				</>	
			);
	}
};

 
export default BubbleChart;