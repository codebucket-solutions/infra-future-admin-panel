import React , {Component } from 'react';
import {Pie} from 'react-chartjs-2';
import SimpleSpinner from '../../components/Spinner/simpleSpinner';
import equal from 'fast-deep-equal';


class RadarChart extends Component{

	constructor(props){
		super(props);
		this.state = {
			
			labels: [],
			datasets: [{
				data: [],
				backgroundColor: [
				'#FF6384',
				'#36A2EB',
				'#FFCE56'
				],
				hoverBackgroundColor: [
				'#FF6384',
				'#36A2EB',
				'#FFCE56'
				]
			}]
			
		}
	}

	componentDidMount(){
		if(this.props.data){
			this.updateGraph()
		}
	}

	componentDidUpdate(prevProps) {
		if(!equal(this.props.data, prevProps.data)){
			this.updateGraph()
		}
	}

	updateGraph =() =>{

		if(this.props.data && this.props.data.length > 0){
			let keys = Object.keys(this.props.data[0]);
			let _temp_keys = [...keys];
			_temp_keys.shift(); 
			_temp_keys.pop(); 
			//console.log(keys, _temp_keys)
			this.setState({labels: _temp_keys})
	
			let count = [];
			_temp_keys.forEach((t, i)=>{
				count[i] = 0;
			})
			//console.log(count)
	
			this.props.data.forEach(d =>{
				let _data = [...Object.values(d)];
				_data.shift(); 
				_data.pop(); 
				let i = _data.indexOf(Math.max(..._data));
				count[i] = count[i]+1
			})
	
			//console.log(count);
	
			let {datasets} = this.state;
			datasets[0].data =count

	
			this.setState({
				datasets: datasets
			})
		}

		
	}

	render() {
			const { dropDown } =this.state;
			//console.log(this.state)
			return (
				<>
				{
					this.props.loading ? <SimpleSpinner /> :<Pie data={this.state} />
				}
				</>	
			);
	}
};

 
export default RadarChart;