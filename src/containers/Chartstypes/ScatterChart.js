import React , {Component } from 'react';
import {Scatter} from 'react-chartjs-2';
import SimpleSpinner from '../../components/Spinner/simpleSpinner';

const data = {
    labels: ['Scatter'],
    datasets: [
      {
        label: 'My First dataset',
        fill: false,
        showLine: true,  //!\\ Add this line
        backgroundColor: 'rgba(75,192,192,0.4)',
        pointBorderColor: 'rgba(75,192,192,1)',
        pointBackgroundColor: '#fff',
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: 'rgba(75,192,192,1)',
        pointHoverBorderColor: 'rgba(220,220,220,1)',
        pointHoverBorderWidth: 2,
        pointRadius: 1,
        pointHitRadius: 10,
        data: [
          { x: 65, y: 75 },
          { x: 59, y: 49 },
          { x: 80, y: 90 },
          { x: 81, y: 29 },
          { x: 56, y: 36 },
          { x: 55, y: 25 },
          { x: 40, y: 18 },
        ]
      }
    ],
    chartOptions : {
        maintainAspectRatio: false,
        showLine: true,
        scales: {
            xAxes: [{
                display: true,
                labelString: "Frequency (Hz)"
            }],
            yAxes: [{
                display: true,
                labelString: "Frequency (Hz)"
            }]
        }
    }
};
class Donut extends Component{
    constructor(props) {
        super(props);
    }
	state={
		labels: [
			'one',
			'two',
			'three'
		],
		datasets: [{
			data: [300, 500 , 200],
			backgroundColor: [
				'#7a6fbe',
				'#29bbe3',
				'#f4c63d',
				'#58db83',
				'#65727f',
			],
			hoverBackgroundColor: [
				'#7a6fbe',
				'#29bbe3',
				'#f4c63d',
				'#58db83',
				'#65727f',
			]
		}]
		
	} 

	componentDidMount(){
		// let levels=[];
		// let data =[];
		// // console.log(this.props.datas);
		// Object.keys(this.props.datas).forEach(item => {
		// 	levels.push(item);
		// 	data.push(this.props.datas[item]);
		// })
		// let {datasets} = this.state;
		// datasets=[{
		// 	...datasets,
		// 	data
		// }]

		// this.setState({labels: levels, datasets: datasets});
	}
			 
	render() {
		//console.log(this.state);
		return (
			<div>
                <div className="col-xl-12">
                    <div className="card m-b-20">
                        <div className="card-body">
                            <h4 className="mt-0 header-title">{this.props.title}</h4>
                            <div id="morris-donut-example" className="">
                            {
                                this.props.loading ? <SimpleSpinner /> : <Scatter data={data} />
                            }
                            </div>
                        </div>
                    </div>
                </div>
			    
			</div>
		);
		}
	}

 
export default Donut;