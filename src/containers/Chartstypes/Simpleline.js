import React , {Component } from 'react';
import { connect } from 'react-redux';
import {Bar} from 'react-chartjs-2';
import equal from 'fast-deep-equal';

          
class Simpleline extends Component{

    constructor(props) {
        super(props);
        this.state ={
          labels: [],
          datasets: [
            {
                label: '',
                fill: false,
                lineTension: 0.1,
                backgroundColor: 'rgba(75,195,192,0.4)',
                borderColor: 'rgba(75,196,192,1)',
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'round',
                pointBorderColor: 'rgba(75,192,192,1)',
                pointBackgroundColor: '#fff',
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                pointHoverBorderColor: 'rgba(220,220,220,1)',
                pointHoverBorderWidth: 2,
                pointRadius: 3,
                pointHitRadius: 50,
                data: [46, 55, 75, 55, 70,65]
            }
          ],
          fontLength:12
         }
  }

  put = () =>{
    let levels=[];
      let data =[];
      
      // console.log(this.props.datas);
      if(this.props.datas){
        Object.keys(this.props.datas).forEach(item => {
          levels.push(item.slice(0, this.props.fontLength));
          data.push(parseInt(this.props.datas[item]));
        })
        let {datasets} = this.state;
        datasets=[{
          ...datasets[0],
          label:this.props.name,
          data
        }]
        // console.log(datasets);
        this.setState({labels: levels, datasets: datasets, fontLength: this.props.fontLength});
      }
      
  }

  componentDidMount(){
    this.put();
  }

  componentDidUpdate(prevProps) {
    if(!equal(this.props.datas, prevProps.datas)){
        //console.log(this.props.data)
        this.put();
    }
  }
    

	render() {
      //console.log(this.props.fontLength);
      return (
                <div>

                  {this.props.side !=='small' ? 
                  <Bar
                  data={this.state}
                  
                  
                  
              />
              :
              <Bar
              data={this.state}
              
              
              
          />
            }
                      

            </div>
      );
      }
	}

const mapStatetoProps = state =>{
    return {
        tiny_data :state.tiny_red.tinydata
    };
  }
 
  
    export default connect(mapStatetoProps,null)(Simpleline);