import React , {Component } from 'react';
import {Doughnut} from 'react-chartjs-2';
import equal from 'fast-deep-equal';

class Donut extends Component{
	state={
		labels: [],
		datasets: [{
			data: [],
			backgroundColor: [
				'#7a6fbe',
				'#29bbe3',
				'#f4c63d',
				'#58db83',
				'#65727f',
			],
			hoverBackgroundColor: [
				'#7a6fbe',
				'#29bbe3',
				'#f4c63d',
				'#58db83',
				'#65727f',
			]
		}]
		
	} 

	componentDidMount(){
		if(this.props.datas){
			this.put();
		}
		
	}

	put = () =>{
		let levels=[];
		let data =[];
		console.log(this.props.datas);
		if(this.props.datas){
			Object.keys(this.props.datas).forEach(item => {
				levels.push(item);
				data.push(this.props.datas[item]);
			})
			console.log(data);
			let {datasets} = this.state;
			datasets[0].data = data;
			
			this.setState({labels: levels, datasets: datasets});
			console.log(datasets, levels);
		}
	}
	componentDidUpdate(prevProps) {
		if(!equal(this.props.datas, prevProps.datas)){
			//console.log(this.props.data)
			if(this.props.datas){
				this.put();
			}
		}
	}

	render() {
		//console.log(this.state);
		return (
			<div>
				<Doughnut   data={this.state} />
			</div>
		);
		}
	}

 
export default Donut;