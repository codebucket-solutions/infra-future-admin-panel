import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from 'react-redux';

import AUX from '../../hoc/Aux_';
import axios from '../../axios-details';

import Spinner from '../../components/Spinner/simpleSpinner';
import Alert from '../../components/Alert/common';
import Star from '../../components/Required/star';

class ModalCSV extends Component {

    constructor(props) {
        super(props);
        this.state={
          type:'',
          client:'',
          file:'',
          clientOptions:[''],
          profileOptions:[''],
          loading:false,
          message:null,
          error:null,
          update:false,
          data:0,
          error_data:{},
        };
    }

    fileChangeHandler = (event, controlName) =>{
      this.setState({[controlName]:event.target.files[0]});
    }

    closeModal = () =>{
      setTimeout(
        () => this.props.toggle(this.state.update)
        ,2000);
    }

    submitHandler = (e) =>{
      e.preventDefault();
      //console.log(this.state);
      this.setState({loading:true, error:null, message: null, error_data:{}});

      let formData = new FormData();     
      formData.append('csv_file', this.state.file);

      const config = {     
        headers: { 'content-type': 'multipart/form-data',Authorization: `Bearer ${this.props.token}` }
      }
      if(this.props.type === "1" ){
        // upload response of multiple apps of a user
        axios.post('/strategy_schema/import', formData, config)
            .then(res=>{
              //console.log(res.data.data);
                if(res.data.status){
                    this.setState({loading:false,update: true, message:res.data.message});
                    this.closeModal();
                }
                else
                    this.setState({loading:false,update: true, error:res.data.message});
                // this.closeModal();
            })
            .catch(err =>{
                //console.log(err);
                this.setState({loading:false, error:"Something went wrong.Please try again"})
                // this.closeModal();
            })
      }
      else if( this.props.type === "2" ){
        // upload apps of a user
        axios.post('/platform/import', formData, config)
          .then(res=>{
                //console.log(res.data);
                if(res.data.status){
                    this.setState({loading:false,update: true, message:res.data.message});
                    this.closeModal();
                }
                else
                    this.setState({loading:false,update: true, error:res.data.message});
                // this.closeModal();
          })
          .catch(err =>{
              //console.log(err);
              this.setState({loading:false, error:"Something went wrong.Please try again"})
              // this.closeModal();
          })
      }
    }
    
    
    
  
    render() {
        let toShow ='';
        if(this.props.type === '2'){
          toShow = (    
            <AUX>
              <div className="col-md-12 text-center" style={{fontSize: '16px',fontWeight: 700}}>
                <a href='files/Platform.csv' style={{color: '#e66161'}} download><u>Download CSV Format to upload platform</u></a>
              </div>
            </AUX>
            )
        }
        else if(this.props.type === '1'){
          toShow = (    
            <div className="col-md-12 text-center" style={{fontSize: '16px',fontWeight: 700}}>
              <a href='files/Strategy_Schema.csv' style={{color: '#e66161'}} download><u>Download CSV Format to upload strategy</u></a>
            </div>
            )
        }

        let body = (
        <div className="card-body">
            <form onSubmit={this.submitHandler}>
                {this.state.message ? <Alert classes={"alert-success"} msg={this.state.message} />: null}
                {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} />: null}
                {this.state.loading ?  <Spinner /> : null }

                <div className="row">
                    <div className="col-sm-6 col-md-6 col-lg-6">
                        <div className="form-group">
                            <label htmlFor="name">Upload CSV<Star /></label>
                            <input type="file" required accept=".csv" onChange={(event) => this.fileChangeHandler(event,"file" )} name="file" id="file" className="" />
                        </div>
                    </div>
                    <div className="col-sm-6 col-md-6 col-lg-6">
                      <div className="form-group">
                          <br />
                          <button type="submit" className="btn btn-primary waves-effect waves-light">Upload</button>
                      </div>
                  </div>
                </div>
                <div className="row">
                    {toShow}
                </div>
            </form>
        </div>
    );
    
    return (
      <div>
        <Modal size="lg" isOpen={this.props.modal} toggle={() => this.props.toggle(this.state.update)} >
          <ModalHeader toggle={() => this.props.toggle(this.state.update)}>Upload CSV</ModalHeader>
          <ModalBody>
            {this.state.loading ?   <Spinner /> : body }
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={() => this.props.toggle(this.state.update)}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStatetoProps = state =>{
  return {
      token: state.auth.token,
      name:state.auth.name,
  };
}

export default connect(mapStatetoProps)(ModalCSV);
