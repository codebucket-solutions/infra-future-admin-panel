import React , {Component } from 'react';
import { MDBDataTable } from 'mdbreact';
import { connect } from 'react-redux';
import { CSVLink } from "react-csv";

import { strategySchemaMapping } from '../../shared/utility'
import axios from '../../axios-details';
import AUX from '../../hoc/Aux_';
import * as actions from '../../store/actions/index';
import { strategyHeader } from '../../shared/questions';

import Header from '../../components/Header';
import Spinner from '../../components/Spinner/simpleSpinner';
import ConfirmModal from '../../components/Modal/modalConfirm.js';
import ModalStrategy from './modalAddStrategy';
import ModalUpload from './modalUploadStrategy';
import Alert from '../../components/Alert/common';
import '../response.css';

let selectedStrategy = new Set();

class Strategy extends Component{


    constructor(props){
        super(props);
        this.state = {
            columns: [
                {
                    label:  <label  className="container mb-4">
                            <input                                 
                                type="checkbox" 
                                value={"all"}
                                onClick={(event) => this.handleCheckboxAll(event, "all")}
                                checked={() =>this.checkAllChech()}
                                />
                                <span className="checkmark"></span>
                            </label>,
                    field: 'sro',
                    width: 150
                },
                {
                    label: 'Strategy Name',
                    field: 'name',
                    sort: 'asc',
                   
                },
                {
                    label: 'Strategy Input Type',
                    field: 'type',
                    sort: 'asc',
                   
                },
                {
                    label: 'Strategy Type',
                    field: 'stype',
                    sort: 'asc',
                   
                },
                {
                    label: 'AE Header',
                    field: 'ae_header',
                    sort: 'asc',
                   
                },
                {
                    label: 'AE Header Dynamic',
                    field: 'ae_header_dynamic',
                    sort: 'asc',
                   
                },
                {
                    label: 'Order Number',
                    field: 'ae_order_id',
                    sort: 'asc',
                   
                },
                {
                    label: 'Value',
                    field: 'value',
                    sort: 'asc',
                    
                },
                {
                    label: 'Action',
                    field: 'action',
                    sort: 'asc',
                    
                },
            ],
            rows: [],
            data:[],
            toggle:false,
            loading:false,
            title:null,
            adminId:null,
            error:null,
            message:null,
            strategyId:null,
            status:null,
            toggleEdit:false,
            modalUpload:false,
            csvData: [],
            allStrategies:[],
        }
    }

    componentDidMount(){
        this.fetchStrategies();
    }

    handleCheckboxAll = (event) =>{
        let {allStrategies} = this.state

        if(event.target.checked){
            allStrategies.map((a) =>{
                selectedStrategy.add(a.strategy_id.toString());
            })
        }
        else{
            selectedStrategy.clear();
        }
        this.createTable(this.state.allStrategies);
    }

    checkAllChech = () =>{
        if(this.state.allStrategies.length === selectedStrategy.size)
            return true;
        else
            return false;
    }

    handleCheckbox = (event, strategy_id) =>{
        //console.log(event.target, quest_id)

        if(event.target.checked){
            selectedStrategy.add(strategy_id.toString());
        }
        else{
            selectedStrategy.delete(event.target.value)
        }
        
        this.createTable(this.state.allStrategies);
    }

    changeCheckBox = () =>{
        let { columns } = this.state;
        columns[0].label = (
            <label  className="container mb-4">
                <input                                 
                    type="checkbox" 
                    value={"all"}
                    onClick={(event) => this.handleCheckboxAll(event, "all")}
                    checked={this.checkAllChech()}
                    />
                    <span className="checkmark"></span>
            </label>
        );
        this.setState({columns: columns});
    }

    fetchStrategies = () =>{
        this.setState({ loading:true });
        const config = {
            headers: { Authorization: `Bearer ${this.props.token}` }
        };
        
        axios.get('/strategy_schema', config)
            .then(res => {
                
                let { strategy_schema } = res.data.data;
                console.log(res.data.data);
               
                if(res.data.status){
                    // if(strategy_schema.length <= 0){
                    //     this.setState({error: "There is no strategy schema present.", loading:false,})
                    // }
                    // else{
                    //     this.createTable(strategy_schema)
                    // }
                    this.createTable(strategy_schema)
                    this.setState({ loading:false, error: null})
                }
            })
            .catch( err => {
                console.error(err);
                this.setState({loading:false, error: "Something went wrong."})
            });    
    }

    createTable = (strategy_schema) =>{
        let strategies = [];
        let csvData = [];
        csvData.push(strategyHeader);
        for(let key in strategy_schema){
            csvData.push([strategy_schema[key].strategy_name, strategySchemaMapping(strategy_schema[key].strategy_type), strategy_schema[key].strategy_value, strategy_schema[key].tabs, strategy_schema[key].ae_header,strategy_schema[key].ae_header_dynamic, strategy_schema[key].ae_order_id])
            
            strategies.push({
                sro : 
                <AUX>
                    <label key={key} className="container">
                        <input                                 
                        type="checkbox" 
                        value={strategy_schema[key].strategy_id}
                        onClick={(event) => this.handleCheckbox(event, strategy_schema[key].strategy_id)}
                        checked={ selectedStrategy ? selectedStrategy.has(strategy_schema[key].strategy_id.toString()) ? true : false : false}
                        />
                    <span className="checkmark"></span>
                </label>
                </AUX>,
                name:strategy_schema[key].strategy_name,
                type:strategySchemaMapping(strategy_schema[key].strategy_type),
                stype: strategy_schema[key].tabs,
                ae_header:strategy_schema[key].ae_header,
                ae_header_dynamic:strategy_schema[key].ae_header_dynamic,
                ae_order_id:strategy_schema[key].ae_order_id,
                value:strategy_schema[key].strategy_value,
                action: 
                <AUX>
                    <button onClick={() => this.openUpdateModal(strategy_schema[key].strategy_id)}
                        className="btn btn-success">Edit</button>
                    
                </AUX>,
            })
        }

        this.setState({ rows:strategies, csvData: csvData, allStrategies:strategy_schema})

        this.changeCheckBox()
    }

    handleToggleModal =() =>{
        this.setState( prevState => {
            return ({toggle: !prevState.toggle, content:"Deleting strategy will effect the gold rules and project ruels.Do you want to delete this strategy?", title:"Delete strategy"})
        })
    }

    handleSubmit = () =>{
        this.handleToggleModal(null);
        this.setState({ loading:true, error:null, message: null});
        const token = {
            token: this.props.token,
            strategy_id: [...selectedStrategy]
        };
        // console.log(token);
        this.setState({loading:true});
        axios.post("/strategy_schema/delete/", token)
        .then(res => {
            // console.log(res.data);
            if(res.data.status){
                
                this.fetchStrategies();
                selectedStrategy.clear();
                this.setState({ loading:false, message: res.data.message});
            }
            else{
                this.setState({ loading:false, error: res.data.message});
            }
            
        })
        .catch(error =>{
            this.setState({loading:false, error:"Something went wrong. Please try again."});
        });
        
        
    }

    openUpdateModal = (strategyId) =>{
        let title =  strategyId ?  'Edit Strategy' : 'Add Strategy';
        this.setState( prevState  => {
          return {toggleEdit: !prevState.toggleEdit,title: title, strategyId: strategyId ? strategyId : null  };
        })
    }

    closeUpdateModal = (status) =>{
        if(status){
            this.fetchStrategies();
        }
        this.setState(prevState =>{
            return {toggleEdit: !prevState.toggleEdit, strategyId:null, title:null};
        })
    }

    openUploadModal = (update) =>{
        if(update){
            this.fetchStrategies();
        }
        this.setState( prevState  => {
          return {modalUpload: !prevState.modalUpload};
        })
    }

    

    render(){

        let filename = "strategy"+Date.now()+".csv";
        return(
            <AUX>
                
                <Header title="Strategies" />

                <div className="row">
                    <div className="col-lg-12">
                        <div className="card m-b-20">
                            <div className="card-body">
                                <h4 className="mt-0 header-title text-right">

                                <button className="btn btn-primary mb-3" onClick={() => this.openUpdateModal()}>Add Strategy</button>&nbsp;
                                <button className="btn btn-secondary mb-3" onClick={() => this.openUploadModal()}>Import</button>
                                &nbsp;
                                <CSVLink
                                    className="btn btn-success mb-3"
                                    filename={filename} 
                                    data={this.state.csvData}
                                >

                                    Export All
                                </CSVLink>
                                &nbsp;
                                <button className="btn btn-danger mb-3" onClick={() =>this.handleToggleModal()}>Delete</button>

                                </h4>
                                {this.state.loading ?  <AUX><Spinner /><br /></AUX> : null }
                                {this.state.message ? <Alert classes={"alert-success"} msg={this.state.message} />: null}
                                {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} />: null}
                                <div style={{overflow:'auto'}}>
                                  <MDBDataTable
                                      bordered
                                      hover
                                      data={{ columns: this.state.columns, rows:this.state.rows }}
                                      info={this.state.rows.length > 0 ? true : false}
                                    />
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                {
                    this.state.toggle
                    ? <ConfirmModal modal={this.state.toggle} toggle={() =>this.handleToggleModal(null)} click={this.handleSubmit} content={this.state.content}  title={this.state.title} />
                    : false
                }
                {
                    this.state.toggleEdit
                    ? <ModalStrategy modal={this.state.toggleEdit}  toggle={this.closeUpdateModal}  title={this.state.title} strategyId={this.state.strategyId} />
                    : false
                }
                {
                    this.state.modalUpload
                    ? <ModalUpload modal={this.state.modalUpload} type="1"  toggle={() =>this.openUploadModal(true)}  />
                    : false
                }
            </AUX>
        );
    }
}

const mapStatetoProps = state =>{
    return {
        token: state.auth.token,
    };
  }
  
  const mapDispatchToProps = dispatch =>{
    return {
        doMapping: (mappingId) => dispatch(actions.doMapping(mappingId)),
    }
  }

export default connect(mapStatetoProps, mapDispatchToProps)(Strategy);