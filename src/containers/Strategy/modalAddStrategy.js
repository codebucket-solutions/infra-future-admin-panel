import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from 'react-redux';


import axios from '../../axios-details';
import Spinner from '../../components/Spinner/simpleSpinner';
import Alert from '../../components/Alert/common';
import Star from '../../components/Required/star';


class ModalStrategySchema extends Component {

    constructor(props) {
        super(props);
        this.state={
            strategy_name:'',
            strategy_type:'score',
            strategy_value:'',
            tabs:'Basic',
            ae_header:'',
            ae_header_dynamic:'',
            ae_order_id:0,
            
            message:null,
            error:null,
            update:false,
        };
    }

    fetchStrategy = () =>{
        this.setState({ loading:true, error: null, message: null});
        const config = {
            headers: { Authorization: `Bearer ${this.props.token}` }
        };
        axios.get('/strategy_schema/'+this.props.strategyId, config)
        .then(res => {
            // console.log(res.data)
            if(res.data.status){
                let { strategy } = res.data.data;
               
                this.setState({ 
                    loading:false, 
                    strategy_name:strategy.strategy_name,
                    strategy_type:strategy.strategy_type, 
                    strategy_value:strategy.strategy_value ? strategy.strategy_value : '' , 
                    tabs:strategy.tabs, 
                    ae_header:strategy.ae_header,
                    ae_header_dynamic:strategy.ae_header_dynamic,
                    ae_order_id:strategy.ae_order_id
                })
            }
        })
        .catch( err => {
            this.setState({loading:false})
        });    
    }
        
    componentDidMount(){
        // console.log(this.props.strategyId);
        if(this.props.strategyId)
            this.fetchStrategy();
    }

    closeModal = () =>{
        setTimeout(
            () => this.props.toggle(this.state.update)
            ,700);
    }

    inputChangeHandler = (event, controlName) =>{
        this.setState({[controlName]:event.target.value});

        if(controlName === "strategy_type" && event.target.value === 'score'){
            this.setState({strategy_value:''})
        }
    }

  
    submitHandler = (e) =>{
        e.preventDefault();
        let ae_order_id = 0
        if(this.state.ae_order_id){
            ae_order_id = parseInt(this.state.ae_order_id);
        }
        // console.log(ae_order_id);
      
        if(ae_order_id>=4 || ae_order_id === 0){
            this.setState({loading:true, error:null, message:null});
            let strategy = {
                token: this.props.token,
                strategy_name: this.state.strategy_name,
                strategy_value: this.state.strategy_value,
                strategy_type: this.state.strategy_type,
                tabs: this.state.tabs,
                ae_header:this.state.ae_header,
                ae_header_dynamic:this.state.ae_header_dynamic,
                ae_order_id:ae_order_id
            }
            //console.log(strategy);
            if(this.props.strategyId){
                strategy.strategy_id = this.props.strategyId;
                axios.put('/strategy_schema/edit', strategy)
                .then(res=>{
                    // console.log(res.data);
                    if(res.data.status)
                        this.setState({loading:false, update:true, message:res.data.message});
                    else {
                        this.setState({loading:false, error:res.data.message});
                    } 
                        this.closeModal();
                }).catch(err =>{
                    this.setState({loading:false, error:"Something went wrong Please try again"});
                    this.closeModal();
                })
            }
            else{
            
                //console.log(strategy);
                axios.post('/strategy_schema/add', strategy)
                .then(res=>{
                    //console.log(res.data);
                    if(res.data.status)
                        this.setState({loading:false, update:true, message:res.data.message});
                    else {
                        this.setState({loading:false, error:res.data.message});
                    }
                    this.closeModal();
                        
                }).catch(err =>{
                    this.setState({loading:false, error:"Something went wrong Please try again"});
                    this.closeModal();
                })
            }
        }
        else{
            this.setState({error:"Order shoud be greater than 3."});
        }
      
    }
    
    render() {
        let dropdownValue = this.state.strategy_type === 'dropdown' ? 
        (
        <div className="form-group">
            <label htmlFor="desc">Strategy Value<Star /></label>
            <input 
                type="text"
                required 
                className="form-control" 
                onChange={(event) => this.inputChangeHandler(event,"strategy_value" )}
                value={this.state.strategy_value}
                placeholder="Enter values separated by comma. " />
            </div>) : '';

        let body = (
        <div className="card-body">
            <form onSubmit={this.submitHandler}>
                {this.state.message ? <Alert classes={"alert-success"} msg={this.state.message} />: null}
                {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} />: null}
                {this.state.loading ?  <Spinner /> : null }
                <div className="row">
                    <div className="col-sm-12">
                        <div className="form-group">
                            <label htmlFor="name">Strategy Name<Star /></label>
                            <input 
                                type="text" 
                                className="form-control" 
                                required 
                                onChange={(event) => this.inputChangeHandler(event,"strategy_name" )}
                                value={this.state.strategy_name}
                                placeholder="Strategy Name" />
                        </div>
                        
                        
                        <div className="form-group">
                            <label htmlFor="l3">Strategy Input Type</label>
                                <select 
                                required
                                className="form-control" 
                                onChange={(event) => this.inputChangeHandler(event,"strategy_type" )}
                                value={this.state.strategy_type}>
                                    <option value="score">Score</option>
                                    <option value="dropdown">Dropdown</option>
                                </select>
                        </div>


                        {dropdownValue}

                        <div className="form-group">
                            <label htmlFor="l3">Strategy Type</label>
                                <select 
                                required
                                className="form-control" 
                                onChange={(event) => this.inputChangeHandler(event,"tabs" )}
                                value={this.state.tabs}>
                                    <option value="Basic">Basic</option>
                                    <option value="Advanced">Advanced</option>
                                </select>
                        </div>

                        <div className="form-group">
                            <label htmlFor="name">AE Header</label>
                            <input 
                                type="text" 
                                className="form-control"  
                                onChange={(event) => this.inputChangeHandler(event,"ae_header" )}
                                value={this.state.ae_header}
                                placeholder="AE Header" />
                        </div>

                        <div className="form-group">
                            <label htmlFor="name">AE Header Dynamic</label>
                            <input 
                                type="text" 
                                className="form-control"  
                                onChange={(event) => this.inputChangeHandler(event,"ae_header_dynamic" )}
                                value={this.state.ae_header_dynamic}
                                placeholder="AE Header Dynamic" />
                        </div>

                        <div className="form-group">
                            <label htmlFor="name">Order Number</label>
                            <input 
                                type="number" 
                                className="form-control"  
                                onChange={(event) => this.inputChangeHandler(event,"ae_order_id" )}
                                value={this.state.ae_order_id}
                                placeholder="Strategy Name" />
                        </div>
                       
                        <button type="submit" className="btn btn-primary waves-effect waves-light">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    );
    
    return (
      <div>
        <Modal isOpen={this.props.modal} toggle={() => this.props.toggle(this.state.update)} >
          <ModalHeader toggle={() => this.props.toggle(this.state.update)}>{this.props.title}</ModalHeader>
          <ModalBody>
            {this.state.loading ? <Spinner /> : body }
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={() => this.props.toggle(this.state.update)}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state =>{
  return {
      token: state.auth.token,
  };
}

export default connect(mapStateToProps)(ModalStrategySchema);
