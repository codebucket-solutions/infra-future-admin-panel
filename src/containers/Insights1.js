import React , {Component } from 'react';
import makeAnimated from "react-select/animated";
import fileDownload from 'js-file-download';
import Alert  from '../components/Alert/common';
import { Link } from 'react-router-dom';
import AUX from '../hoc/Aux_';
import MySelect from '../components/ReactSelect';
import { handleKeyValueClient,objectToArray, handleKeyValue } from '../shared/utility';

import axios from '../axios-details';
import { connect } from 'react-redux';
import AssessmentTable from '../components/AssessmentTable';
import * as actions from '../store/actions/index.js';
import Spinner from '../components/Spinner/simpleSpinner';
import AdvancedInsights from './advancedInsights';


import Donut from './Insight/Donut';
import LineGraph from './Insight/LineGraph';
import CountBox from './Insight/CountBox';
import ModalSelectInsight from '../components/Modal/modalSelectInsight';


// import { CSVLink } from "react-csv";
 
let mRefs = []
const animatedComponents = makeAnimated();
class Insights extends Component{
    constructor(props) {
        super(props);
        mRefs = [];
        this.a1 = React.createRef();
        this.basic = React.createRef();
        this.state={
            height:300,
            width:450,
            APPARCHITECTURE:[],
            APPTYPE:[],
            DBTECHNOLOGY:[],
            PROGRAMMINGLANG:[],
            BIZCRITICALITY:[],
            APPROADMAP:[],
            COMPLIANCEREG:[],
            APPCATEGORY:[],
            HWPLATFORM:[],
            OSWEBTIER:[],
            OSDBTIER:[],
            OSAPPTIER:[],
            no_app:0,
            completed:0,
            profile:0,
            loading:true,
            img:'',
            dataF:[],
            toggle:false,
            suitability:[],
            allClients:[],
            selectedClients:[],
            json1:{},
            assessmentCount1 : 0,
            json2:{},
            assessmentCount2 : 0,

            allBasicFilter:[],
            allA1Filter:[],
            allA2Filter:[],
            selectedBasicFilter:[],
            selectedA1Filter:[],
            selectedA2Filter:[],

            notStarted:0,
            buttonMessage1:"Refresh Assessment",
            buttonMessage2:"Refresh Assessment",
            fixedFiler:[
                'APPTYPE','APPARCHITECTURE','DBTECHNOLOGY','PROGRAMMINGLANG','BIZCRITICALITY','APPROADMAP','COMPLIANCEREG','APPCATEGORY','HWPLATFORM','OSWEBTIER','OSDBTIER','OSAPPTIER'
            ],
            alertClass:null,
            error:null,
            toggle:false,

            lineGraphSize: 6,
            selectedInsightTags:null,  
            insightToggle: false,

            simpleUrl: null,
            exportTitle: "Export All",
            
        }
        this.print = React.createRef();
    }

    componentDidMount(){
        this.setState({selectedBasicFilter: this.props.selectedBasicFilter, selectedA1Filter: this.props.selectedA1Filter})
        this.fetchClient();
        if(this.props.selectedClients.length > 0){
            this.fetchAPI(this.props.selectedClients);
        }
        else{
            this.setState({toggle:true});
        }
    }

    fetchClient = () =>{
        const config = {
            headers: { Authorization: `Bearer ${this.props.token}` }
        };
        //console.log(config)
        axios.get('/user/userclient', config)
        .then(res => {
            //console.log(res.data)
            let clients = handleKeyValueClient(res.data.data.clients);
            //console.log(clients);
            this.setState({ allClients:clients})
        })
        .catch( err => {
            this.setState({error: "Something went wrong."})
        });    
    }
        
    selectTowChange= (selectedClients) => {
        if(selectedClients && selectedClients.length >0){
            this.fetchAPI(selectedClients);
        }
        else{
            this.setState({dataF:[],no_app:0,completed:0,notStarted:0,profile:0, selectedFilter:[]})
        }
        
        this.props.updateAssessmentFilter(this.props.selectedBasicFilter,this.props.selectedA1Filter,this.props.assessmentA2Filter, selectedClients);
    }

    selectTowChangeFilter= (selectedFilter, type) => {
        if(type === "basic"){
            //console.log(selectedFilter);
            this.setState({selectedBasicFilter: selectedFilter});
            this.props.updateAssessmentFilter(selectedFilter,this.state.selectedA1Filter,[], this.props.selectedClients);
        }
        else if(type === "a1"){
            //console.log(selectedFilter);
            this.setState({selectedA1Filter: selectedFilter});
            this.props.updateAssessmentFilter(this.state.selectedBasicFilter,selectedFilter,[], this.props.selectedClients);
        }
        // else if(type === "a2"){
        //     //console.log(selectedFilter);
        //     this.setState({selectedA2Filter: selectedFilter});
        //     this.props.updateAssessmentFilter(this.props.selectedBasicFilter, this.props.selectedA1Filter,selectedFilter);
        // }  
    }
    

    fetchAPI(selectedClients) {
        this.setState({loading:true,});
        const token = {
            token: this.props.token,
            clients: objectToArray(selectedClients),
        };
        console.log(token);
        axios.post('/insight', token)
            .then(res => {
                let data = res.data.data;
                //console.log(res.data);
                if(res.data.status === true) {
                    let assessmentCount1, assessmentCount2;
                    let assessmentA1Filter = Object.keys(data.assessResult1);
                    // let assessmentA2Filter = Object.keys(data.assessResult2);
                    // console.log(assessmentA1Filter);
                    //console.log(final_filter);
                    assessmentCount1 = Object.keys(data.assessResult1).length;
                    // assessmentCount2 = Object.keys(data.assessResult2).length;
                    //console.log(data.assessResult1);
                    //console.log(data.assessResult);
                    this.setState({
                        loading:false,
                        dataF:data.data,
                        APPARCHITECTURE:data.data.APPARCHITECTURE ? data.data.APPARCHITECTURE : [],
                        APPTYPE:data.data.APPTYPE ? data.data.APPTYPE : [],
                        DBTECHNOLOGY:data.data.DBTECHNOLOGY ? data.data.DBTECHNOLOGY : [],
                        PROGRAMMINGLANG:data.data.PROGRAMMINGLANG ? data.data.PROGRAMMINGLANG : [],
                        
                        BIZCRITICALITY:data.data.BIZCRITICALITY ? data.data.BIZCRITICALITY : [],
                        APPROADMAP:data.data.APPROADMAP ? data.data.APPROADMAP : [],
                        COMPLIANCEREG:data.data.COMPLIANCEREG ? data.data.COMPLIANCEREG : [],
                        APPCATEGORY:data.data.APPCATEGORY ? data.data.APPCATEGORY : [],

                        HWPLATFORM:data.data.HWPLATFORM ? data.data.HWPLATFORM : [],
                        OSWEBTIER:data.data.OSWEBTIER ? data.data.OSWEBTIER : [],
                        OSDBTIER:data.data.OSDBTIER ? data.data.OSDBTIER : [],
                        OSAPPTIER:data.data.OSAPPTIER ? data.data.OSAPPTIER : [],

                        no_app:data.no_app,
                        profile:data.profile,
                        completed:data.completed,
                        notStarted:data.not_started,
                        json1: data.assessResult1,
                        json2: data.assessResult2,
                        assessmentCount1: assessmentCount1,
                        assessmentCount2: assessmentCount2,

                        allBasicFilter: handleKeyValue(this.state.fixedFiler),
                        // selectedBasicFilter: handleKeyValue(this.state.fixedFiler),
                        allA1Filter:handleKeyValue(assessmentA1Filter),
                        selectedA1Filter: (this.props.selectedA1Filter && this.props.selectedA1Filter.length > 0) ? this.props.selectedA1Filter : handleKeyValue(assessmentA1Filter),
                        // allA2Filter:handleKeyValue(assessmentA2Filter),
                        // selectedA2Filter:handleKeyValue(assessmentA2Filter),
                        toggle:true,
                    })
                    //console.log(data.data);
                    
                }
            })
            .catch( err => {
                this.setState({loading:false})
                console.log(err)
            });    
    }

    handleAssessmentAPI  =(string) =>{
        let assessmentType = 1;
        this.setState({loadingMessage:true, ['buttonMessage'+assessmentType]:"Loading...",alertMessage: "Please wait. We are fetching assessment data.",error: null })
        const token = {
            token: this.props.token,
            clients: objectToArray(this.props.selectedClients),
            assessment_tags:string,
        };
        //console.log(token);
        axios.post('/fetchAssessment/', token)
        .then(res => {
            //console.log(res.data);
            if(res.data.status){
                this.setState({loadingMessage:false, ['buttonMessage'+assessmentType]:"Refresh Assessment", error: null,})
                this.fetchAPI(this.props.selectedClients);
            }
            else{
                this.setState({loadingMessage:false, ['buttonMessage'+assessmentType]:"Refresh Assessment", error: res.data.message, })
            }
            
        })
        .catch( err => {
            this.setState({loadingMessage:false, ['buttonMessage'+assessmentType]:"Refresh Assessment", error: "Something went wrong. Please try again"})
         //console.log(err)
        });   
    }


    exportAll = () => {
        mRefs.map((v) => {
            v.exportExcel();
            return v;
        })
    }

    inputChangeHandler = (e) =>{
        this.setState({lineGraphSize: e.target.value})
    } 

    handleSimpleExport = (type) => {
        const token = {
            token: this.props.token,
            clients: objectToArray(this.props.selectedClients),
        };
        if(type === 2){
            this.setState({exportTitle:"Please wait..."})
            //console.log(type)
        }
        let fileName = type === 1 ? 'simple'+Date.now()+'.xlsx' : 'advanced'+Date.now()+'.xlsx';
        
        //console.log(token);
        axios.post(type === 1 ? '/insight/export-simple' : '/insight/export-advance', token, {
            responseType: 'blob',
          })
        .then((response) => {
            fileDownload(response.data, fileName);
            this.setState({exportTitle: "Export All"})
        })
        .catch( err => {
            
         console.log(err)
        });
    }

    // downloadExcel = (url) => {
    //     //console.log(url);
    //     window.open(url);
    // }

    // wild card search function
    handleKeyDown = (e,all, selected,myInput) => {
        let allFields  = this.state[all];
        let selectedFields = this.state[selected] ;
        
        if (e.key === 'Enter') {
            let string = e.target.value.substring(0, e.target.value.length - 1);
            allFields.forEach( a => {
                //console.log(a);
                if(a.value.toLowerCase().includes(string.toLowerCase())){
                    selectedFields.push(a);
                }
            })
            this.setState({[selected]:selectedFields});
            //console.log(selectedFields);
            
            this[myInput].current.blur();
            this[myInput].current.focus();

            this.selectTowChangeFilter(selectedFields,myInput );

        }
    }

    handleInsightModal = (data) => {
        
        this.setState( prev => ({
            selectedInsightTags: data, insightToggle: !prev.insightToggle
        }))
        if(data && data.length > 0){
            this.handleAssessmentAPI(data);
        }
    }
  
    render(){
        const { assessmentCount1, assessmentCount2, json1,json2, toggle,lineGraphSize } = this.state;
       // console.log(this.state.simpleUrl);
        //console.log(this.props.selectedA1Filter);
        //console.log(selectedFilter)
        //console.log(this.state.APPARCHITECTURE);
        //const {BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend} = Recharts;
        let attributes = this.state.loading ? '' : {
            id:"tab_1",
            'data-toggle':"tab",
            'href':"#advance_1",
            role:"tab"
        };
        return(
            <AUX>
                <br />
                <div className="">

                </div>
                <div className="row">
                   
                    <div className="col-md-12">
                        {
                            this.state.loadingMessage ? <Alert classes="alert-danger" msg={this.state.alertMessage} /> : null
                        }
                        {
                            this.state.error ? <Alert classes="alert-danger" msg={this.state.error} /> : null
                        }
                    </div>
                    
                    <div className="col-sm-12 col-lg-1">
                        <div className="page-title-box">
                            <h4 className="page-title">Insights</h4>
                        </div>
                    </div>
                    <div className="col-sm-12 col-lg-11">
                        <div className="page-title-box">
                            <MySelect
                                options={this.state.allClients}
                                isMulti
                                noMin = {true}
                                components={animatedComponents}
                                onChange={this.selectTowChange}
                                allowSelectAll={true}
                                value={this.props.selectedClients}
                                
                            />
                        </div>
                    </div>
                </div>
                 
                <div className="row">
                    <div className="col-md-12">
                        <ul className="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                            <li className="nav-item" >
                                <a className="nav-link active"  id="tab_1" data-toggle="tab" href="#basic" role="tab">Basic</a>
                            </li>
                            <li className="nav-item"  >
                                <a className="nav-link"  {...attributes} >Advanced</a>
                            </li>
                            {/* <li className="nav-item" >
                                <a className="nav-link"  id="tab_1" data-toggle="tab" href="#advance_2" role="tab">Advanced 2</a>
                            </li> */}
                        </ul>
                        <div className="tab-content">
                            <div className="tab-pane pt-3 active" id="basic" role="tabpanel" >
                                <div className="col-lg-12 mb-2 pl-0 pr-0">
                                    <MySelect
                                        options={this.state.allBasicFilter}
                                        isMulti
                                        noMin = {true}
                                        components={animatedComponents}
                                        onChange={(e) => this.selectTowChangeFilter(e,"basic")}
                                        allowSelectAll={true}
                                        value={this.props.selectedBasicFilter}
                                        handleKeyDown={(e) => this.handleKeyDown(e,'allBasicFilter', 'selectedBasicFilter','basic')}
                                        ref={this.basic}
                                    />
                                </div> 
                                <CountBox 
                                    no_app ={this.state.no_app} 
                                    completed = {this.state.completed}
                                    notStarted = {this.state.notStarted}
                                    />
                                {
                                    Object.keys(this.state.dataF).length > 0 ?   
                                
                                (<>
                                <div className="row">
                                    <div className="col-md-12 text-right">
                                <button className="btn btn-lg btn-primary" onClick={() => this.handleSimpleExport(1)}>{this.state.exportTitle}</button>
                                    </div>
                                    
                                    
                                </div>
                                <br />
                                <div className="row" >
                                    { this.props.selectedBasicFilter.some(q => q.label === "APPTYPE") ?
                                        <Donut title="APPTYPE" loading={this.state.loading} data={this.state.APPTYPE} />
                                    : null}
                                    { this.props.selectedBasicFilter.some(q => q.label === "APPARCHITECTURE") ?
                                        <Donut title="APPARCHITECTURE" loading={this.state.loading} data={this.state.APPARCHITECTURE} />
                                    : null}
                                    { this.props.selectedBasicFilter.some(q => q.label === "BIZCRITICALITY") ?
                                        <Donut title="BIZCRITICALITY" loading={this.state.loading} data={this.state.BIZCRITICALITY} />
                                    : null}
                                </div>  
                                {/* <div className="row mb-2">
                                    <div className="col-md-8"></div>
                                    <div className="col-md-4 text-right">
                                    <select 
                                        className="form-control" 
                                        onChange={(event) => this.inputChangeHandler(event)}
                                        value={lineGraphSize}>
                                            <option value={4}>3 in a row</option>
                                            <option value={6}>2 in a row</option>
                                            <option value={12}>1 in a row</option>
                                        </select>
                                    </div>
                                </div> */}
                                <div className="row" >
                                    { this.props.selectedBasicFilter.some(q => q.label === "DBTECHNOLOGY") ?
                                        <LineGraph title="DBTECHNOLOGY" graphSize={lineGraphSize} loading={this.state.loading} data={this.state.DBTECHNOLOGY} />
                                    : null}
                                    { this.props.selectedBasicFilter.some(q => q.label === "PROGRAMMINGLANG") ?
                                        <LineGraph title="PROGRAMMINGLANG" graphSize={lineGraphSize} loading={this.state.loading} data={this.state.PROGRAMMINGLANG} />
                                    : null}
                                    { this.props.selectedBasicFilter.some(q => q.label === "APPROADMAP") ?
                                        <LineGraph title="APPROADMAP" graphSize={lineGraphSize} loading={this.state.loading} data={this.state.APPROADMAP} />
                                    : null}
                                    { this.props.selectedBasicFilter.some(q => q.label === "COMPLIANCEREG") ?
                                        <LineGraph title="COMPLIANCEREG" graphSize={lineGraphSize} loading={this.state.loading} data={this.state.COMPLIANCEREG} />
                                    : null}
                                    { this.props.selectedBasicFilter.some(q => q.label === "APPCATEGORY") ?
                                        <LineGraph title="APPCATEGORY" graphSize={lineGraphSize} loading={this.state.loading} data={this.state.APPCATEGORY} />
                                    : null}
                                    { this.props.selectedBasicFilter.some(q => q.label === "HWPLATFORM") ?
                                        <LineGraph title="HWPLATFORM" graphSize={lineGraphSize} loading={this.state.loading} data={this.state.HWPLATFORM} />
                                    : null}
                                    { this.props.selectedBasicFilter.some(q => q.label === "OSWEBTIER") ?
                                        <LineGraph title="OSWEBTIER" graphSize={lineGraphSize} loading={this.state.loading} data={this.state.OSWEBTIER} />
                                    : null}
                                    { this.props.selectedBasicFilter.some(q => q.label === "OSDBTIER") ?
                                        <LineGraph title="OSDBTIER" graphSize={lineGraphSize} loading={this.state.loading} data={this.state.OSDBTIER} />
                                    : null}
                                    { this.props.selectedBasicFilter.some(q => q.label === "OSAPPTIER") ?
                                        <LineGraph title="OSAPPTIER" graphSize={lineGraphSize} loading={this.state.loading} data={this.state.OSAPPTIER} />
                                    : null}
                                </div>  
                                
                                </>) : null }
                            </div>
                            <div className={!toggle ? "tab-pane pt-3 active" : "tab-pane pt-3"} id="advance_1" role="tabpanel" style={{minHeight:300}} >
                                <div className="col-lg-12 mb-2 pl-0 pr-0">
                                <MySelect
                                    options={this.state.allA1Filter}
                                    isMulti
                                    noMin = {true}
                                    components={animatedComponents}
                                    onChange={(e) => this.selectTowChangeFilter(e,"a1")}
                                    allowSelectAll={true}
                                    value={this.props.selectedA1Filter}
                                    handleKeyDown={(e) => this.handleKeyDown(e,'allA1Filter', 'selectedA1Filter','a1')}
                                    ref={this.a1}
                                />
                                </div>
                                <br />
                                <div className="row ">
                                    <div className="col-md-12  col-sm-12 text-right">
                                        <button  type="button" title="Get new data from Assessment API" className="btn btn-primary btn-lg waves-effect" onClick={() => this.handleInsightModal(null)}>{this.state.buttonMessage1}</button>
                                        &nbsp;&nbsp;
                                        <button className="btn btn-lg btn-primary" onClick={() => this.handleSimpleExport(2)}>{this.state.exportTitle}</button>
                                    </div>
                                </div>
                                {
                                    assessmentCount1 > 0 ? <AdvancedInsights json={json1} /> : null
                                }
                                
                                <br />
                                {
                                    assessmentCount1 > 0 ? Object.keys(json1).map((q, i) =>{
                                        //console.log(json1[q])
                                        return ( this.props.selectedA1Filter.some(t => t.label === q) ?   <AssessmentTable key={i} ref={(ref) => ref && mRefs.push(ref)} title={q} data={json1[q]} /> : null)
                                    }) : null
                                }
                            </div>
                            {/* <div className={!toggle ? "tab-pane pt-3 active" : "tab-pane pt-3"} id="advance_2" role="tabpanel" style={{minHeight:300}} >
                            <div className="col-lg-12 mb-2 pl-0 pr-0">
                                <MySelect
                                    options={this.state.allA2Filter}
                                    isMulti
                                    noMin = {true}
                                    components={animatedComponents}
                                    onChange={(e) => this.selectTowChangeFilter(e,"a2")}
                                    allowSelectAll={true}
                                    value={this.props.selectedA2Filter}
                                />
                                </div>
                                <br />
                                <div className="row ">
                                    <div className="col-md-12  col-sm-12 text-right">
                                        <button  type="button" title="Get new data from Assessment API" className="btn btn-primary btn-lg waves-effect" onClick={() => this.handleAssessmentAPI(2)}>{this.state.buttonMessage2}</button>
                                        &nbsp;&nbsp;
                                        <button className="btn btn-lg btn-primary" onClick={this.exportAll}>Export All</button>
                                    </div>
                                </div>
                                <br />
                                {
                                    assessmentCount2 > 0 ? Object.keys(json2).map((q, i) =>{
                                        //console.log(json[q])
                                        return ( this.props.selectedA2Filter.some(t => t.label === q) ?   <AssessmentTable key={i} ref={(ref) => ref && mRefs.push(ref)} tableId={i} title={q} data={json2[q]} /> : null)
                                    }) : null
                                }
                            </div> */}
                        </div>
                    </div>
                </div>

               
                
                <br />
                {
                    this.state.insightToggle
                    ? <ModalSelectInsight modal={this.state.insightToggle} toggle={this.handleInsightModal}  />
                    : false

                }   
            </AUX>
        );
    }
}

const mapStateToProps = state =>{
    return {
        token : state.auth.token,
        selectedBasicFilter : state.assessment.assessmentBasicFilter,
        selectedA1Filter : state.assessment.assessmentA1Filter,
        selectedA2Filter : state.assessment.assessmentA2Filter,
        selectedClients: state.assessment.customerFilter
    }
}

const mapDispatchToProps = dispatch =>{
    return {
        updateAssessmentFilter : (selectedBasicFilter,assessmentA1Filter,assessmentA2Filter,  selectedClients) => dispatch(actions.assessmentFilter(selectedBasicFilter, assessmentA1Filter,assessmentA2Filter,selectedClients))
    }
    
}

export default connect(mapStateToProps, mapDispatchToProps)(Insights);
  