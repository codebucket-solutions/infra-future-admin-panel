import React, {Component} from 'react';
let style={maxHeight: 300,
    'overflow': 'scroll'}

class SingleBox extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            header:"",
            count:0,
        }
    }

    
    
    render() {
        const {header, count} = this.props

        return (
            <div className="col-xl-3 col-md-6">
                <div className="card mini-stat bg-primary">
                    <div className="card-body mini-stat-img" style={{padding:'0.5rem 1.25rem'}}>
                        <div className="row">
                            
                            
                            <div className="col-md-10">
                                <div className="text-white">
                                    <h6 className="text-uppercase mb-3">{header}</h6>
                                    <h4 className="mb-4">{count}</h4>
                                </div>
                            </div>
                            <div className="col-md-2 text-right">
                                <div className="btn-group ">
                                
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        )
    }
    
}

export default SingleBox