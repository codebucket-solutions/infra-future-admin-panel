import React from 'react';
import Simpleline from '../Chartstypes/Simpleline';
import SimpleSpinner from '../../components/Spinner/simpleSpinner';


const LineGraph = (props) =>{
    let className="col-xl-"+(props.graphSize ? props.graphSize : 4);
    let fontLength = 12;
    // if(props.graphSize){
    //     if(props.graphSize == 6){
    //         fontLength = 16;
    //     }
    //     else if(props.graphSize == 12){
    //         fontLength = 20;
    //     }
    // }
    //console.log(fontLength);

    let graph = (
        <div className={className}>
            <div className="card m-b-20">
                <div className="card-body">
                    <h4 className="mt-0 header-title">{props.title}</h4>
                    <div id="morris-donut-example" className="">
                    {
                        props.loading ? <SimpleSpinner /> :<Simpleline fontLength={fontLength} side="small" datas={props.data} name={props.title} />
                    }
                    </div>
                    
                </div>
            </div>
        </div>
    );
    if(props.group){
        graph = (
            <>
                {
                    props.loading 
                    ? 
                        <SimpleSpinner /> 
                    :
                        props.data ?
                        <Simpleline fontLength={fontLength} side="small" datas={props.data} name={props.title} />
                        : null
                }
            </>
        )
    }
    return (
        <>
            {graph}
        </>
        
    )
}

export default LineGraph;