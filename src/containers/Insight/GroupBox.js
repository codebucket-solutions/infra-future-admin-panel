import React, {Component} from 'react';
import axios from '../../axios-details';
import { connect } from 'react-redux';

import Donut from './Donut';
import LineGraph from './LineGraph';
import GraphLayout from './GraphLayout';
import PieChart from '../Chartstypes/PieChart';
import BarChart from '../Chartstypes/Barcharts';
import Radar from '../Chartstypes/RadarChart';
import PiechartGrade from '../Chartstypes/PieChartGrade';
import BubbleChart from '../Chartstypes/BubbleChart';

const lineGraphSize= 6;

class GroupBox extends Component{
    constructor(props) {
        super(props);
        this.state = {
            allGroup1: [['APPTYPE', 'Donut'],['APPARCHITECTURE', 'Donut'], ['DBTECHNOLOGY', 'Linegraph'],['APPROADMAP', 'Linegraph']],
            selectedGroup1: ['APPTYPE', 'Donut'],
            loading:false,
            
        }
    }

    componentDidMount(){
        //console.log(this.props.dataBasic.BIZCRITICALITY);
        this.fetchGroup();
    }

    fetchGroup  =() =>{
        this.setState({loading:true})
        const token = {
            token: this.props.token,
            group: this.props.groupNumber
        }
        axios.post('/insight/fetch_by_group', token)
        .then(res => {
            //console.log(res.data);
            let platform = process.env.REACT_APP_INSIGHT_PLATFORM.split(',');
            platform.map(p => p.trim());

            let {data} = res.data;
            //console.log(data)
            if(res.data.status){
                let newData = [];
                data.forEach((d) =>{
                    if(d.platform_associated === 1){
                        platform.forEach(p =>{
                            newData.push({
                                name: p.toUpperCase()+'-'+d.name,
                                graph_type:d.graph_type,
                                type:d.type,
                                platform_associated:d.platform_associated,
                                description:d.description ? p.toUpperCase()+'-'+d.description : p.toUpperCase()+'-'+d.name
                            })
                        })
                        
                    }
                    else{
                        newData.push(d)
                    }
                })
                //console.log(newData);

                //
                let selectedGroup1;
                const selected = localStorage.getItem('admin_graph'+this.props.groupNumber)
                //console.log(selected)
                if(selected){
                    selectedGroup1 = JSON.parse(selected)
                }
                else{
                    selectedGroup1 = newData[0];
                    localStorage.setItem('admin_graph'+this.props.groupNumber, JSON.stringify(newData[0]))
                }
                this.setState({loading:false, allGroup1: newData, selectedGroup1: selectedGroup1})
            }
            else{
                this.setState({loading:false})
            }
        })
        .catch( err => {
            console.log(err)
            this.setState({loading:false})
        });   
    }

    showGraph = () =>{
        const {allGroup1, selectedGroup1} = this.state;
        //console.log(selectedGroup1)
        let graph = '';
        if(selectedGroup1.graph_type === "Donut"){
            graph=(
                <Donut title={selectedGroup1.name} loading={this.props.loading} data={this.props.dataBasic[selectedGroup1.name]} group={true} />
            )
        }
        else if(selectedGroup1.graph_type === "Linegraph"){
            graph=(
                <LineGraph title={selectedGroup1.name} graphSize={lineGraphSize} loading={this.props.loading} data={this.props.dataBasic[selectedGroup1.name]} group={true} />
            )
        }
        else if(selectedGroup1.graph_type === "Piechart"){
            //console.log(this.props.dataAdvanced)
            graph=(
                <PieChart data={this.props.dataAdvanced[selectedGroup1.name]} title={'selectedGroup1.name'} loading={this.props.loading} group={true} />
                
            )
        }
        else if(selectedGroup1.graph_type === "Bargraph"){
            // console.log(selectedGroup1)
            // console.log(this.props.dataAdvanced)
            graph=(
                <BarChart 
                    selectedApps={this.props.selectedApps}
                    data={this.props.dataAdvanced[selectedGroup1.name]} title={'selectedGroup1.name'} loading={this.props.loading} group={true} />
            )
        }
        else if(selectedGroup1.graph_type === "Radarchart"){
            // console.log(selectedGroup1)
            //console.log(this.props.dataAdvanced)
            graph=(
                <Radar 
                    selectedApps={this.props.selectedApps}
                    data={this.props.dataAdvanced[selectedGroup1.name]} title={'selectedGroup1.name'} loading={this.props.loading} group={true} />
            )
        }

        else if(selectedGroup1.graph_type === "PiechartGrade"){
            // console.log(selectedGroup1)
            //console.log(this.props.dataAdvanced)
            graph=(
                <PiechartGrade data={this.props.dataAdvanced[selectedGroup1.name]} title={selectedGroup1.name} loading={this.props.loading} group={true} />
            )
        }
        else if(selectedGroup1.graph_type === "BubbleChart"){
            graph=(
                <BubbleChart 
                    selectedApps={this.props.selectedApps}
                    data={this.props.dataAdvanced[selectedGroup1.name]} title={selectedGroup1.name} loading={this.props.loading} group={true}  />
            )
        }

        return (
            <GraphLayout 
                title={selectedGroup1.description ? selectedGroup1.description : selectedGroup1.name} handleGroup={this.handleGroup} allGroup1={allGroup1} 
            >
                {graph}
            </GraphLayout>
        )
    }

    handleGroup = (data) => {
        localStorage.setItem('admin_graph'+this.props.groupNumber, JSON.stringify(data))
        this.setState({selectedGroup1:data});
    }

    render() {
        const {allGroup1, selectedGroup1} = this.state;
        return (
            <>
                {
                    allGroup1 && allGroup1.length > 0 
                    ?
                        <div className="col-lg-6">
                            {this.showGraph()}
                        </div>
                    : null
                }
            </>
        )
    }
}

const mapStateToProps = state =>{
    return {
        token : state.auth.token,
    }
}

export default connect(mapStateToProps)(GroupBox);
