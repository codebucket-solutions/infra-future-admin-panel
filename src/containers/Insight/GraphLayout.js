import React from "react";


let style={maxHeight: 300,
    'overflowY': 'scroll'}
class GraphLayout extends React.Component{

    render() {
        return (
            <div className="card m-b-20" style={{height:'94%'}}>
                <div className="card-body">
                    <div className="row">
                        <div className="col-md-6">
                            <h4 className="mt-0 header-title">{this.props.title}</h4>
                        </div>
                        {
                            this.props.isDropdown 
                            ? 
                            <div className="col-md-5"  >
                                
                            </div>
                            : <div className="col-md-5"  ></div>

                        }
                        <div className="col-md-1 text-right" style={{marginTop:'-2%',paddingRight: 5}}>
                            <div className="btn-group mb-2">
                               
                                <i className="fa fa-ellipsis-h" data-toggle="dropdown" style={{color:'#6d6d6d',cursor: 'pointer'}}></i>
                               
                                <div className="dropdown-menu mo-mb-2" style={style}>
                                    {
                                        this.props.allGroup1 && this.props.allGroup1.map((a,i) =>(
                                            <div key={i} className="dropdown-item" onClick={() => this.props.handleGroup(a)}>{ a.description ? a.description :  a.name}</div>
                                        ))
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="morris-donut-example" className="" >
                        {this.props.children}
                    </div>
                </div>
            </div>
        )
    }
    
}

export default GraphLayout;