import React from 'react';
import Doughnut from '../Chartstypes/Donut';
import SimpleSpinner from '../../components/Spinner/simpleSpinner';


const LineGraph = (props) =>{
    let graph = (
        <div className="col-xl-4">
            <div className="card m-b-20">
                <div className="card-body">
                    <h4 className="mt-0 header-title">{props.title}</h4>
                    <div id="morris-donut-example" className="">
                    {
                        props.loading ? <SimpleSpinner /> : <Doughnut datas={props.data} />
                    }
                    </div>
                </div>
            </div>
        </div>
    );
    if(props.group){
        graph = (
                <>
                    {
                        props.loading ? <SimpleSpinner /> : <Doughnut datas={props.data} />
                    }
                </>
        )
    }
    return (
        <>
            {graph}
        </>
    )
}

export default LineGraph;