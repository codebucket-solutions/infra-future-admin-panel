import React, {Component} from 'react';
let style={maxHeight: 300,
    'overflowY': 'scroll'}

class SingleBox extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            heading:"",
            count:0,
        }
    }

    handleGroup = (data) =>{
        console.log(data)
        let count = 0;
        let name = '';
        this.props.data.map(d =>{
            if(d[0] === data){
                count = d[1]
            }
            name = data;
        })
        this.setState({heading: data , count:count})
        localStorage.setItem('admin_box'+this.props.default, data)
    }

    componentDidMount(){
        //console.log(this.props)
        if(this.props.data  && this.props.data.length > 0){
            this.updateState();
        }
       
    }

    componentDidUpdate(prevProps) {
        if(prevProps.data != this.props.data){
            this.updateState();
        }
    }

    

    updateState(){
        //console.log(this.props.data[this.props.default][0]);
        // console.log(this.props.selected)
        // console.log(this.props.selected ? this.props.selected : this.props.filter[0].name)
        this.handleGroup( this.props.selected ? this.props.selected : this.props.filter[0].name)
    }
    
    render() {
        const {heading, count} = this.state
        //console.log(this.props.filter, this.props.data)
        return (
            <div className="col-xl-3 col-md-6">
                <div className="card mini-stat bg-primary">
                    <div className="card-body mini-stat-img" style={{padding:'0.5rem 1.25rem'}}>
                        <div className="row">
                            
                            
                            <div className="col-md-10">
                                <div className="text-white">
                                    <h6 className="text-uppercase mb-3" style={{minHeight: 40}}>{heading}</h6>
                                    <h4 className="mb-4">{count}</h4>
                                </div>
                            </div>
                            <div className="col-md-2 text-right">
                                <div className="btn-group ">
                                        <i className="fa fa-ellipsis-h" data-toggle="dropdown" style={{color:'white',cursor: 'pointer'}}></i>
                                    <div className="dropdown-menu"  style={style}>
                                        {
                                            this.props.data && this.props.filter.map((d,i) => {
                                                return (
                                                    <div key={i} className="dropdown-item" onClick={() => this.handleGroup(d.name)}>{d.name}</div>
                                                )
                                            })
                                        }
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        )
    }
    
}

export default SingleBox