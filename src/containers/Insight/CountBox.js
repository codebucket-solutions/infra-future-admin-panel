import React, {Component} from 'react';
import SingleBox from './singleBox';
import axios from '../../axios-details';
import { connect } from 'react-redux';

class CountBox extends Component {
    constructor(props){
        super(props);
        this.state={
            dataByGroup: [],
            selectedGroup1: [],
            dataCount:[]
        }
    }

    componentDidMount(){
        //console.log(this.props.dataBasic.BIZCRITICALITY);
        this.fetchGroup();
    }

    fetchGroup  =() =>{
        this.setState({loading:true})
        const token = {
            token: this.props.token,
        }
        axios.post('/insight/fetch_by_box', token)
        .then(res => {
            
            let platform = process.env.REACT_APP_INSIGHT_PLATFORM.split(',');
            platform.map(p => p.trim());

            let {data} = res.data;
            console.log(data);
            if(res.data.status){
                let selectedGroup1 = [0];
                for(let i = 1; i<5; i++){
                    const selected = localStorage.getItem('admin_box'+i)
                    //console.log(selected);
                    if(selected && selected != "undefined"){
                        selectedGroup1[i] = selected;
                        
                    }
                    else{
                        selectedGroup1[i] = data[i][0].name;
                        localStorage.setItem('admin_box'+i, data[i].name)
                    }
                }
                //console.log(selectedGroup1)
                // console.log(dataByGroup)
                this.setState({loading:false, dataByGroup: data, selectedGroup1: selectedGroup1})
            }
            else{
                this.setState({loading:false})
            }
        })
        .catch( err => {
            console.log(err)
            this.setState({loading:false})
        });   
    }
    
    render(){
        // console.log(this.props.dataCount)
        // console.log(this.state, this.props.dataCount)
        return(
            <div className="row">
                {
                    this.state.dataByGroup.length > 0 ?
                        <>
                            <SingleBox 
                                default={1}
                                data={this.props.dataCount}
                                filter={this.state.dataByGroup[1]}
                                selected={this.state.selectedGroup1 && this.state.selectedGroup1.length > 0 ? this.state.selectedGroup1[1] : null}
                            />
                            <SingleBox 
                                default={2}
                                data={this.props.dataCount}
                                filter={this.state.dataByGroup[2]}
                                selected={this.state.selectedGroup1 && this.state.selectedGroup1.length > 0 ? this.state.selectedGroup1[2] : null}
                            />
                            <SingleBox 
                                default={3}
                                data={this.props.dataCount}
                                filter={this.state.dataByGroup[3]}
                                selected={this.state.selectedGroup1 && this.state.selectedGroup1.length > 0 ? this.state.selectedGroup1[3] : null}
                            />
                            <SingleBox 
                                default={4}
                                data={this.props.dataCount}
                                filter={this.state.dataByGroup[4]}
                                selected={this.state.selectedGroup1 && this.state.selectedGroup1.length > 0 ? this.state.selectedGroup1[4] : null}
                            />
                        </>
                    : null
                }
                
            </div>
        )
    }
}

const mapStateToProps = state =>{
    return {
        token : state.auth.token,
    }
}

export default connect(mapStateToProps)(CountBox);
// export default CountBox;