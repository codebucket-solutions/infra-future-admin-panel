import React, { Component } from 'react';
import { MDBDataTable } from 'mdbreact';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import axios from '../axios-details';
import AUX from '../hoc/Aux_';
import * as actions from '../store/actions/index';


import Spinner from '../components/Spinner/simpleSpinner';
import ConfirmModal from '../components/Modal/modalConfirm.js';
import ModalUsers from '../components/Modal/modalAddUsers.js';
import { Content } from 'react-bootstrap/lib/Tab';
import { btnStyle } from '../util';

class User extends Component {


    constructor(props) {
        super(props);
        this.state = {
            columns: [
                {
                    label: 'Sr.No.',
                    field: 'sro',
                    sort: 'asc',
                    width: 150
                },
                {
                    label: 'Name',
                    field: 'name',
                    sort: 'asc',
                    width: 270
                },
                {
                    label: 'Company',
                    field: 'company',
                    sort: 'asc',
                    width: 200
                },
                {
                    label: 'Address',
                    field: 'address',
                    sort: 'asc',
                    width: 100
                },
                {
                    label: 'Email',
                    field: 'email',
                    sort: 'asc',
                    width: 100
                },
                {
                    label: 'Phone',
                    field: 'phone',
                    sort: 'asc',
                    width: 100
                },
                {
                    label: 'Action',
                    field: 'action',
                    sort: 'asc',
                    width: 100
                },
            ],
            rows: [],
            data: [],
            toggle: false,
            loading: false,
            title: null,
            adminId: null,
            error: null,
            message: null,
            clientId: null,
            status: null,
            toggleClient: false
        }
    }

    fetchClient = () => {
        this.setState({ loading: true });
        const config = {
            headers: { Authorization: `Bearer ${this.props.token}` }
        };
        //console.log(config)
        axios.get('/user', config)
            .then(res => {
                let clients = [];
                let client = res.data.data.clients;
                console.log(client);
                let active;
                if (res.data.status) {
                    for (let key in client) {
                        if (client[key].active === '1') {
                            active = (
                                <AUX>
                                    <button className="btn btn-secondary" onClick={() => this.handleToggleModal(client[key].clnt_id, 0)}>Disable</button>&nbsp;
                                    <button className="btn btn-danger" onClick={() => this.handleToggleModal(client[key].clnt_id, 2, client[key].client_id)}>Delete</button>&nbsp;
                                    <button onClick={() => this.openUpdateModal(client[key].clnt_id)}
                                        className="btn btn-success">Edit</button>

                                </AUX>
                            )
                        }
                        else {
                            active = (
                                <AUX>
                                    <button className="btn btn-primary" onClick={() => this.handleToggleModal(client[key].clnt_id, 1)}>Enable</button>&nbsp;
                                    <button className="btn btn-danger" onClick={() => this.handleToggleModal(client[key].clnt_id, 2, client[key].client_id)}>Delete</button>&nbsp;
                                    <button onClick={() => this.openUpdateModal(client[key].clnt_id)}
                                        className="btn btn-success">Edit</button>
                                </AUX>
                            )
                        }
                        clients.push({
                            sro: parseInt(key) + 1,
                            name: client[key].f_name + " " + client[key].l_name,
                            company: client[key].company,
                            address: client[key].address_1,
                            email: client[key].email,
                            phone: client[key].phone,
                            action: active,
                        })
                    }
                }
                this.setState({ loading: false, rows: clients })
            })
            .catch(err => {
                this.setState({ loading: false, error: "Something went wrong." })
            });
    }

    handleToggleModal = (id, key, isClient = 0) => {
        console.log(isClient);
        if (key === 1) {
            this.setState(prevState => {
                return ({ toggle: !prevState.toggle, clientId: id, content: "Do you want to activate this user?", title: "Deactivate user", status: key })
            })
        }
        else if (key === 0) {
            this.setState(prevState => {
                return ({ toggle: !prevState.toggle, clientId: id, content: "Do you want to disable this user?", title: "Activate user", status: key })
            })
        }
        else if (key === 2) {
            let content = "Do you want to delete this user?";
            if (!isClient) {
                content = content + " All the apps will be deleted.";
            }
            else {
                content = content + " All the associated apps will be attached to client."
            }
            this.setState(prevState => {
                return ({ toggle: !prevState.toggle, clientId: id, content: content, title: "Delete user", status: key })
            })
        }

    }

    closeModal = () => {
        this.setState(prevState => {
            return { toggle: !prevState.toggle };
        })
    }

    handleSubmit = () => {
        if (this.state.status === 2) {
            const token = {
                token: this.props.token,
                user_id: this.state.clientId,
            };
            //console.log(token);
            this.setState({ loading: true });
            //console.log(token);
            axios.post("/user/delete", token)
                .then(res => {
                    //console.log(res.data);
                    if (res.data.status) {
                        this.closeModal();
                        this.fetchClient();
                    }
                })
                .catch(error => {
                    this.setState({ loading: false, error: "Something went wrong. Please try again." });
                });
        }
        else {
            const token = {
                token: this.props.token,
                user_id: this.state.clientId,
                active: this.state.status,
            };
            //console.log(token);
            this.setState({ loading: true });
            //console.log(token);
            axios.post("/user/active", token)
                .then(res => {
                    //console.log(res.data);
                    if (res.data.status) {
                        this.closeModal();
                        this.fetchClient();
                    }
                })
                .catch(error => {
                    this.setState({ loading: false, error: "Something went wrong. Please try again." });
                });
        }

    }

    openUpdateModal = (clientId) => {
        let title = clientId ? 'Edit User' : 'Add User';
        this.setState(prevState => {
            return { toggleClient: !prevState.toggleClient, title: title, clientId: clientId ? clientId : null };
        })
    }

    closeUpdateModal = (status) => {
        if (status) {
            this.fetchClient();
        }
        this.setState(prevState => {
            return { toggleClient: !prevState.toggleClient, clientId: null, title: null };
        })
    }

    componentDidMount() {
        this.fetchClient();
    }

    render() {
        return (
            <AUX>
                <div className="row">
                    <div className="col-sm-12">
                        <div className="page-title-box">
                            <h4 className="page-title">Users</h4>
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                                <li className="breadcrumb-item active">Users</li>
                            </ol>
                            {/* <Tinycharts /> */}
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-12">
                        <div className="card m-b-20">
                            <div className="card-body">
                                <h4 className="mt-0 header-title text-right">
                                    <button style={btnStyle
                                    } className="btn btn-primary mb-3" onClick={() => this.openUpdateModal()}>Add Users</button>
                                </h4>
                                {this.state.loading ? <AUX><Spinner /><br /></AUX> : null}
                                <div style={{ overflow: 'auto' }}>
                                    <MDBDataTable
                                        bordered
                                        hover
                                        data={{ columns: this.state.columns, rows: this.state.rows }}
                                        info={this.state.rows.length > 0 ? true : false}
                                    />
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                {
                    this.state.toggle
                        ? <ConfirmModal modal={this.state.toggle} toggle={this.closeModal} click={this.handleSubmit} content={this.state.content} title={this.state.title} />
                        : false
                }
                {
                    this.state.toggleClient
                        ? <ModalUsers modal={this.state.toggleClient} toggle={this.closeUpdateModal} title={this.state.title} clientId={this.state.clientId} />
                        : false
                }
            </AUX>
        );
    }
}

const mapStatetoProps = state => {
    return {
        token: state.auth.token,
    };
}

const mapDispatchToProps = dispatch => {
    return {
        doMapping: (mappingId) => dispatch(actions.doMapping(mappingId)),
    }
}

export default connect(mapStatetoProps, mapDispatchToProps)(User);