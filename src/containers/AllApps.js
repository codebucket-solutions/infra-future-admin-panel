import React, { Component } from 'react';
import { MDBDataTable } from 'mdbreact';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { CSVLink } from "react-csv";
import { Progress } from 'reactstrap';
import fileDownload from 'js-file-download';

import AUX from '../hoc/Aux_';
import axios from '../axios-details';
import * as actions from '../store/actions/index';

import Spinner from '../components/Spinner/simpleSpinner';
import ModalUpload from '../components/Modal/modalUpload';
import Alert from '../components/Alert/common';
import ModalAttachProfile from '../components/Modal/modalAttachProfile';
import ModalApp from '../components/Modal/modalApp';
import ConfirmModal from '../components/Modal/modalConfirm.js';
import ChangeClient from '../components/Modal/modalChangeUser.js';
import ModalAppExport from '../components/Modal/modalAppExport';

import './response.css';
import { btnStyle } from '../util';


const header = ['App Name', 'Question Tag', 'Question', 'Client Response', 'Comment'];
let headerApps = ["App Name", "Business Service", "Business Owner", "App Owner", "Tags"];
let headerAppsPercentage = ["App Name", "Business Service", "Business Owner", "App Owner", "Tags", "Percentage", "Profile"];

let answers = [];
let count = 1;
class AllApps extends Component {

    constructor(props) {
        super(props);
        this.state = {
            columns: [
                {
                    label: <><label className="container mb-4">
                        <input
                            type="checkbox"
                            value={answers}
                            onClick={(event) => this.handleCheckbox(event, "all")}
                            checked={count === answers.length ? true : false} />
                        <span className="checkmark"></span>
                        
                    </label>
                    <span> 0 apps selected </span></>,
                    field: 'export',
                    sort: 'asc',
                    width: 150
                },
                {
                    label: 'App Name',
                    field: 'aname',
                    sort: 'asc',
                    width: 270
                },
                {
                    label: 'Business Service',
                    field: 'bc',
                    sort: 'asc',
                    width: 200
                },
                {
                    label: 'App Owner',
                    field: 'app_owner',
                    sort: 'asc',
                    width: 100
                },
                {
                    label: 'User Name',
                    field: 'cname',
                    sort: 'asc',
                    width: 100
                },
                {
                    label: 'Progress',
                    field: 'progress',
                    sort: 'asc',
                    width: 100
                },
                {
                    label: 'Action',
                    field: 'action',
                    sort: 'asc',
                    width: 100
                },
            ],
            rows: [],
            toggle: false,
            data: [],
            loading: false,
            toggleProfile: false,
            appId: null,
            appName: null,
            apps: null,
            error: null,
            csvData: [],
            csvApps: [],
            csvAppsPer: [],
            confirmModal:false,
            changeClient:false,
            toggleAppUpdate:false,
            toggleAppExport:false
        }
        this.csv = React.createRef();
        this.csv1 = React.createRef();
        this.csv_per = React.createRef();
    }


    Toggle = () => {
        this.setState(prevState => {
            return { toggle: !prevState.toggle };
        })

    }

    AppCancel = () => {
        this.Toggle();
        this.fetchAPI();
    }

    fetchAPI = () => {
        this.setState({ loading: true });
        const config = {
            headers: { Authorization: `Bearer ${this.props.token}` }
        };
        axios.get('/apps', config)
            .then(res => {
                
                //console.log(res.data.data);
                
                if(res.data.status){
                    this.putDataToState(res.data.data.apps);
                    this.setState({ apps: res.data.data.apps });
                    count = res.data.data.apps.length;
                    // console.log(count);
                }
                this.setState({ loading: false })

            })
            .catch(err => {
                this.setState({ loading: false })
            }

            );
    }
    
    putDataToState = (apps) =>{
        let csvApps = [];
        let csvAppsPer = [];
        csvApps.push(headerApps);
        csvAppsPer.push(headerAppsPercentage);
        // console.log(apps);
        let app = [];
        for(let key in apps){
            csvApps.push([ apps[key].app_name, apps[key].b_service, apps[key].b_owner, apps[key].app_owner, apps[key].tags ]);
            csvAppsPer.push([apps[key].app_name, apps[key].b_service, apps[key].b_owner, apps[key].app_owner, apps[key].tags, apps[key].percent, apps[key].profile_name])
            let color= (apps[key].percent === 100) ? 'success' : 'danger';

            app.push({
                export: <AUX>
                    <label key={key} className="container">
                        <input
                            type="checkbox"
                            value={apps[key].app_id}
                            onClick={(event) => this.handleCheckbox(event, apps[key].app_id)}
                            checked={answers ? (answers.includes(apps[key].app_id) ? true : false) : false} />
                        <span className="checkmark"></span>
                    </label>
                </AUX>,
                aname: apps[key].app_name,
                bc: apps[key].b_service,
                app_owner: apps[key].app_owner,
                cname: apps[key].f_name + " " + (apps[key].l_name ? apps[key].l_name : ''),
                progress: <AUX><Progress color={color} value={apps[key].percent} /><label>{apps[key].percent}%</label></AUX>,
                action:
                    <AUX>
                        <button
                            style={{btnStyle}}
                            onClick={() => this.HandleAttachProfileModal(apps[key].app_id, apps[key].app_name)} className="btn btn-primary"
                            title="Attach Profile">Profile
                        </button>&nbsp;
                        <button
                            onClick={() => this.closeAppUpdate(apps[key].app_id)} className="btn btn-success"
                            title="Edit this application">Edit
                        </button>&nbsp;
                        <button
                            title="View response of the application"
                            onClick={() => this.handlePush(apps[key].app_id)}
                            className="btn btn-secondary">Response
                        </button>
                    </AUX>

            })
        }
        this.setState({ loading:false, rows:app, csvApps:csvApps, csvAppsPer: csvAppsPer})
        this.changeExport();
    }

    changeExport = () => {
        let { columns } = this.state;
        columns[0].label = (
            <>
                <label className="container mb-4">
                    <input
                        type="checkbox"
                        value={answers}
                        onClick={(event) => this.handleCheckbox(event, "all")}
                        checked={count === answers.length ? true : false} />
                    <span className="checkmark"></span>
                </label>
                <br />
                {answers.length >0 ? <span className = "mt-2"> {answers.length} selected </span> : null }
            </>
        );
        this.setState({ columns: columns });
    }


    handleCheckbox = (event, app_id) => {
        // console.log(app_id);
        let { apps } = this.state;
        if (app_id === "all") {
            if (event.target.checked) {
                answers = [];
                apps.map(a => answers.push(a.app_id));
            }
            else {
                answers = [];
            }
        }
        else {
            if (event.target.checked) {
                answers.push(app_id);
            }
            else {
                let dataIndex = answers.indexOf(app_id);
                answers.splice(dataIndex, 1);
            }
        }

        this.putDataToState(this.state.apps)
    }

    componentDidMount() {
        answers = [];
        count = 1;
        this.fetchAPI();
    }

    handlePush = (id) => {
        this.props.setResponse(id);
        this.props.history.push('/responses');
    }

    openModal = () => {
        this.setState(prevState => ({
            toggle: !prevState.toggle
        }))
    }

    closeModal = (status) => {
        this.setState({ toggle: false })
        if (status === true) {
            this.fetchAPI();
        }
    }

    HandleAttachProfileModal = (id, name) => {
        //console.log(id);
        this.setState(prevState => {
            return { toggleProfile: !prevState.toggleProfile, appId: id, appName: name }
        })

    }

    // delete profile
    handleAppDelete = (appId) => {
        // console.log(this.state);
        this.state.toggleDelete ? this.closeDeleteModal() : this.closeBulkDeleteModal();
        this.setState({ loading: true, message: null, error: null });
        const data = {
            token: this.props.token,
            app_id: appId
        }
        axios.put('/apps/del', data)
            .then(res => {
                // console.log(res.data);
                if (res.data.status) {
                    this.setState({ loading: false, appId: null, message: res.data.message })
                }
                else {
                    this.setState({ loading: false, appId: null, error: res.data.message })
                }
                this.fetchAPI();
            })
            .catch(err => {
                this.setState({ loading: false, appId: null, error: "Something went wrong. Please try again later." });

            })
    }

    exportCSV = () => {
        // console.log("adasdas");
        let { csvData } = this.state;
        //console.log(this.state.answers);
        csvData = [];
        csvData.push(header);
        
        this.setState({loading:true, error:null});
        let token = {
            token:this.props.token,
            apps: answers,
        }

    // console.log(token);
    axios.post('/response/export', token)
        .then(res => {
            // console.log(res.data);
            let response = res.data.data.responses;
            let arr = [];
            let answer_key,answers,val;
            for(let key in response){
                answer_key = JSON.parse(response[key].response_json);
                answers = response[key].answer;
                arr = [];
                // console.log(answer_key);
                answer_key.map((each_data, index) => {
                    val = each_data.position;
                    // console.log(val);
                    if(answers && answers.includes(val ? val.toString(): '~~')){
                        arr.push(each_data.choice)   
                    }
                    return each_data;
                })
                csvData.push([response[key].app_name,response[key].quest_tag,response[key].question.replace(/"/g, '""'), arr ? arr.join("|") : '', response[key].comment ])
            }
            this.setState({csvData});
            this.setState({loading:false});
            let btn = this.refs.csv;
            btn.link.click();

            let btn1 = this.refs.csv1;
            btn1.link.click();
        })
        .catch( err => {
            this.setState({loading:false})
            // console.log(err)
        });    
       
    }

    exportCSVPer = () =>{
        let btn = this.refs.csv_per;
        btn.link.click();
    }

    //close  Bulk delete Modal
    closeBulkDeleteModal = () => {
        // answers = [];
        this.setState(prevState => {
            return { confirmModal: !prevState.confirmModal };
        })
    }

    handleBulkDelete = () => {
        //console.log(answers.length);
        if (answers.length > 0) {
            this.setState({ confirmModal: true, error: null })
        }
        else {
            this.setState({ error: "Please select at least one application to delete." })
        }
    }

    handleAppDelete = (appId) => {
        // console.log(this.state);
        this.closeBulkDeleteModal();
        this.setState({ loading: true, message: null, error: null });
        const data = {
            token: this.props.token,
            app_id: appId,
            user_name: this.props.name,
        }
        //console.log(data);
        axios.put('/apps/del', data)
            .then(res => {
                // console.log(res.data);
                if (res.data.status) {
                    this.setState({ loading: false, appId: null, message: res.data.message })
                }
                else {
                    this.setState({ loading: false, appId: null, error: res.data.message })
                }
                this.fetchAPI();
            })
            .catch(err => {
                this.setState({ loading: false, appId: null, error: "Something went wrong. Please try again later." });

            })
    }

    handleChangeClient = () => {
        if (answers.length > 0) {
            this.setState({ changeClient: true, error: null })
        }
        else {
            this.setState({ error: "Please select at least one application to delete." })
        }
    }

    //close  change client modal
    closeChangeClient = (update) => {
        if (update) {
            this.fetchAPI();
        }
        // answers = [];
        this.setState(prevState => {
            return { changeClient: !prevState.changeClient };
        })
    }

    //open or close app change modal 
    closeAppUpdate = (appId, update) => {
        if (update) {
            this.fetchAPI();
        }
        this.setState(prevState => {
            return { toggleAppUpdate: !prevState.toggleAppUpdate, appId: appId };
        })
    }

    toggleExport = (id) =>{
        if(id ===1){
            if(answers.length >0){
                this.setState(prev =>{
                    return { toggleAppExport: !prev.toggleAppExport}
                })
            }
            else{
                this.setState({error:"Please select at least one application to export."})
            }
        }
        else{
            // answers = [];
            this.setState(prev =>{
                return { toggleAppExport: !prev.toggleAppExport}
            })
        }
    }

    exportCSVAdvance = () =>{

        const token = {
            token: this.props.token,
            assessment_tags: process.env.REACT_APP_INSIGHT_PLATFORM
        };
        axios.post('/fetchAssessment/export_on_demand', token, {
            responseType: 'blob',
          })
        .then((response) => {
            fileDownload(response.data, 'ae_request_data'+Date.now()+'.xlsx');
        })
        .catch( err => {
            
         console.log(err)
        });
    }

    render(){
        let filename = "assessment"+Date.now()+".csv";
        let filename1 = "apps"+Date.now()+".csv";
        let filename_per = "apps_percentage"+Date.now()+".csv";
        // console.log(this.state.toggleAppExport);
        return(
            <AUX>
                <div className="row">
                    <div className="col-sm-12">
                        <div className="page-title-box">
                            <h4 className="page-title">Apps</h4>
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                                <li className="breadcrumb-item active">Apps</li>
                            </ol>

                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-12">
                        <div className="card m-b-20">
                            <div className="card-body">
                                <h4 className="mt-0 header-title text-right">

                                    <button style={{btnStyle}} className="btn btn-primary mb-3" onClick={this.handleChangeClient}>Change Client</button>&nbsp;
                                    <button
                                        title="Import responses or applications"
                                        className="btn btn-secondary mb-3"
                                        onClick={this.openModal}>Import
                                    </button>&nbsp;
                                    <button className="btn btn-success mb-3" onClick ={() =>this.toggleExport(1)}>Export All</button>&nbsp;
                                    <button 
                                        title="Delete selected application"
                                        onClick={() => this.handleBulkDelete()}
                                        className="btn btn-danger mb-3">Delete
                                    </button>
                                    <CSVLink
                                        ref="csv"
                                        filename={filename}
                                        data={this.state.csvData}
                                        asyncOnClick={true}
                                        style={{ display: 'none' }}
                                    >

                                    </CSVLink>
                                    <CSVLink
                                        ref="csv1"
                                        filename={filename1}
                                        data={this.state.csvApps}
                                        asyncOnClick={true}
                                        style={{ display: 'none' }}
                                    >

                                    </CSVLink>
                                    { this.state.csvAppsPer.length > 0 ?  <CSVLink
                                        ref="csv_per"
                                        filename={filename_per} 
                                        data={this.state.csvAppsPer}
                                        asyncOnClick={true}
                                        style={{display:'none'}}
                                    >
                                        
                                    </CSVLink> : null}
                                    
                                </h4>

                                {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} /> : null}
                                {this.state.loading ? <AUX><Spinner /><br /></AUX> : null}
                                <div style={{ overflow: 'auto' }}>
                                    <MDBDataTable
                                        bordered
                                        hover
                                        data={{ columns: this.state.columns, rows: this.state.rows }}
                                        info={this.state.rows.length > 0 ? true : false}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {
                    this.state.toggle ? <ModalUpload modal={this.state.toggle} toggle={this.closeModal} /> : null
                }
                {
                    this.state.toggleProfile
                        ? <ModalAttachProfile modal={this.state.toggleProfile} toggle={this.HandleAttachProfileModal} appId={this.state.appId} appName={this.state.appName} />
                        : false
                }
                {
                    this.state.confirmModal
                        ? <ConfirmModal modal={this.state.confirmModal} toggle={this.closeBulkDeleteModal} click={() => this.handleAppDelete(answers)} content={"Do you really want to delete this app."} title={"Delete App"} />
                        : false
                }
                {
                    this.state.changeClient
                        ? <ChangeClient modal={this.state.changeClient} app_id={answers} toggle={this.closeChangeClient} />
                        : false
                }
                {
                    this.state.toggleAppUpdate
                        ? <ModalApp modal={this.state.toggleAppUpdate} appId={this.state.appId} toggle={this.closeAppUpdate} title="Edit App" />
                        : false
                }
                {
                    this.state.toggleAppExport
                    ? <ModalAppExport modal={this.state.toggleAppExport} exportCSV={this.exportCSV} exportCSVPer={this.exportCSVPer} exportCSVAdvance={this.exportCSVAdvance}  toggle={this.toggleExport} title="Export Apps"  />
                    : false
                }
            </AUX>
        );
    }
}

const mapStateToProps = state => {
    return {
        token: state.auth.token,
        name: state.auth.name,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setResponse: (id) => dispatch(actions.setResponse(id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AllApps);
