import React , {Component } from 'react';
import Scatter from './Chartstypes/ScatterChart';
import Radar from './Chartstypes/RadarChart';
import PieChart from './Chartstypes/PieChart';
import BarChart from './Chartstypes/Barcharts';
class AdvancedInsights extends Component{

    constructor(props){
        super(props);
        this.state = {
            barGraph : [],
            radarChart:[],
            OVERALLSUITCP:[],
            TECHFITCP:[],
        }
    }

    componentDidMount() {
        let barGraph = []
        let radarChart = []
        let OVERALLSUITCP = []
        let TECHFITCP = []
        let c_bar = 0;
        let c_radar = 0;
        let advancedKey =  Object.keys(this.props.json);
        advancedKey.forEach(a =>{
            let keys = a.split('-');
             
            if(keys[1] && keys[1].trim() === 'SUITABILITY'){
                barGraph[c_bar]= []
                barGraph[c_bar]['value'] = []
                barGraph[c_bar]['title'] = a;
                barGraph[c_bar]['value'].push(...this.props.json[a])
                c_bar++
            }

            else if(keys[1] && keys[1].trim() === 'TECHFIT'){
                radarChart[c_radar]= []
                radarChart[c_radar]['value'] = []
                radarChart[c_radar]['title'] = a;
                radarChart[c_radar]['value'].push(...this.props.json[a])
                c_radar++
            }
            else if(keys[0] && keys[0].trim() === 'OVERALLSUITCP'){
                OVERALLSUITCP.push(...this.props.json[a])
            }
            else if(keys[0] && keys[0].trim() === 'TECHFITCP'){
                TECHFITCP.push(...this.props.json[a])
            }
        })
        //console.log(radarChart);
        this.setState({barGraph:barGraph, radarChart:radarChart, OVERALLSUITCP:OVERALLSUITCP, TECHFITCP:TECHFITCP})
    }

    render() {
        // console.log(this.props.json);
        console.log(this.state.TECHFITCP);
        return(
            <>
            {/* <Scatter /> */}
                <br />
                <div className="row">
                {
                    this.state.radarChart && this.state.radarChart.length > 0 
                    ?
                    this.state.radarChart.map(b => {
                        // console.log(b)
                        return <Radar title={b['title']} data={b['value']}  />
                    })
                    : null
                }
                {
                    this.state.barGraph && this.state.barGraph.length > 0 
                    ?
                    this.state.barGraph.map(b => {
                        // console.log(b)
                        return <BarChart title={b['title']} data={b['value']}  />
                    })
                    : null
                }
                </div>
            </>

        )
        
    }
}

export default AdvancedInsights;