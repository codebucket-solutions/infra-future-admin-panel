import React, { Component } from 'react';
import AUX from '../../hoc/Aux_';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { specialCharacters } from '../../Utils/specialCharater';

import axios from '../../axios-details';
import * as actions from '../../store/actions/index';
import Alert from '../../components/Alert/common';
import SimpleSpinner from '../../components/Spinner/simpleSpinner';
import ModalForgotPassword from '../../components/Modal/modalForgotPassword';
import CommonNav from '../../components/CommonNav';


class Login extends Component 
{
    constructor(props){
        super(props);
        this.state = {
            email: '',
            password: '',
            rePassword:'',
            loading:false,
            isAdmin:false,
            incorrectPassword:false,
        }
    }
    componentDidMount(){
        //console.log(this.props.building, this.props.authRedirect);
        this.checkAdmin();
        if( this.props.authRedirect !=='/')
            this.props.onSetAuthRedirectPath();
    }

    checkAdmin = () =>{
        this.setState({loading:false});
        axios.get('/login')
        .then((res) =>{
            //console.log(res.data)
            if(res.data.status){
                this.setState({isAdmin:true, loading:false});
            }
            else{
                this.setState({loading:false, message:res.data.message});
            }
        })
        .catch(err=>{
            this.setState({loading:false, error:"Something went wrong. Please try again."});
        })
    }

    submitHandler = (event) => {
        event.preventDefault();
        //console.log(this.state);
        if(this.state.isAdmin){
            this.props.onAuth(this.state.email, this.state.password)
        }
        else{
            this.setState({loading:false, incorrectPassword: false, error: null, message: null});
            if(this.state.password.length >= 6 && specialCharacters(this.state.password)){
                if(this.state.password === this.state.rePassword){
                    this.setState({loading:true})
                    const apiData = {
                        email: this.state.email,
                        password: this.state.password
                    }
                    axios.post('/login/create', apiData)
                    .then((res) =>{
                        //console.log(res.data)
                        if(res.data.status){
                            this.setState({isAdmin:true, loading:false, message: res.data.message});
                        }
                        else{
                            this.setState({loading:false, error:res.data.message});
                        }
                    })
                    .catch(err=>{
                        this.setState({loading:false, error:"Something went wrong. Please try again."});
                    })
                }
                else{
                    this.setState({error: "Both passwords are not same."})
                }
            }
            else{
                this.setState({incorrectPassword: true})
            }
        }
        
    }
    
    inputChangeHandler = (event, controlName) =>{
        //console.log(controlName);
        this.setState({[controlName]:event.target.value});
    }

    modalToggle =() =>{
        this.setState(prevState =>{
            return{ toggle: !prevState.toggle}
        })
    }
    render() {
        const {isAdmin}  = this.state;
        let loginForm = (
            <>
                <div className="form-group">
                    <label htmlFor="email">Email</label>
                    <input 
                        required
                        type="email" 
                        className="form-control" 
                        id="email" 
                        onChange={(event) => this.inputChangeHandler(event,"email" )}
                        placeholder="Enter email" />
                </div>

                <div className="form-group">
                    <label htmlFor="userpassword">Password</label>
                    <input 
                        required
                        type="password" 
                        className="form-control" 
                        id="userpassword" 
                        onChange={(event) => this.inputChangeHandler(event,"password" )}
                        placeholder="Enter password" />
                </div>
                <div className="form-group">
                    <div className="col-12 text-center">
                        <button className="btn btn-primary w-md waves-effect waves-light" type="submit">Log In</button>
                    </div>
                </div>
                <div className="form-control text-right" style={{border: 0}}>
                    <p><a href="#" onClick={this.modalToggle} className="text-primary"> Forgot Password ?  </a> </p>
                </div>
            </>
            );
        if(!isAdmin){
            loginForm = (
                <>
                <div className="form-group">
                    <label htmlFor="email">Email</label>
                    <input 
                        required
                        type="email" 
                        className="form-control" 
                        id="email" 
                        onChange={(event) => this.inputChangeHandler(event,"email" )}
                        placeholder="Enter email" />
                </div>

                <div className="form-group">
                    <label htmlFor="userpassword">Password</label>
                    <input 
                        required
                        type="password" 
                        className="form-control" 
                        id="userpassword" 
                        onChange={(event) => this.inputChangeHandler(event,"password" )}
                        placeholder="Enter password" />
                         <span className={this.state.incorrectPassword ? "passwordStyle" : null}>At least 6 characters having one special character.</span>
                </div>
                <div className="form-group">
                    <label htmlFor="userpassword">Retype Password</label>
                    <input 
                        required
                        type="password" 
                        className="form-control" 
                        onChange={(event) => this.inputChangeHandler(event,"rePassword" )}
                        placeholder="Enter password again" />
                </div>
                <div className="form-group">
                    <div className="col-12 text-center">
                        <button className="btn btn-primary w-md waves-effect waves-light" type="submit">Submit</button>
                    </div>
                </div>
            </>
            )
        }
        return (
            <AUX>
                <CommonNav />
                <div className="wrapper-page">
                    <div className="card">
                        <div className="card-body">
                            <h3 className="text-center m-0">
                                
                            </h3>
                            <div className="p-3">
        <h4 className="text-muted font-18 m-b-5 text-center">Welcome Admin</h4>
                                <form className="form-horizontal m-t-30" onSubmit={this.submitHandler}>
                                    {this.props.error ? <Alert msg={this.props.error} classes="alert-danger" /> : null }
                                    {this.state.message ? <Alert msg={this.state.message} classes="alert-success" /> : null }
                                    {this.state.error ? <Alert msg={this.state.error} classes="alert-danger" /> : null }
                                    {this.props.loading ?  <SimpleSpinner /> : loginForm }
                                    
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                {
                    this.state.toggle
                    ? <ModalForgotPassword modal={this.state.toggle} toggle={this.modalToggle} />
                    : false
                }
            </AUX>
        );
    }
}

const mapStateToProps = (state) =>{
    return {
        loading: state.auth.loading,
        error: state.auth.error,
        isAuth : state.auth.token !== null,
    }
  }
  
  const mapDispatchToProps = dispatch =>{
    return {
      onAuth : (email, password) => dispatch(actions.auth(email, password)),
      onSetAuthRedirectPath: () => dispatch(actions.setAuthRedirect('/admin/dashboard'))
    }
  }

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login));