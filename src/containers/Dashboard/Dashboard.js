import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import axios from '../../axios-details';

import CountBox from '../Insight/CountBox';
import DashboardCardComp from '../../components/Dashboard/DashboardCardComp';
import SimpleSpinner from '../../components/Spinner/simpleSpinner';
import { handleKeyValueClient, objectToArray } from '../../shared/utility'
import SingleBoxEmpty from '../Insight/singleBoxEmpty'


class Dashboard extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            dataCount: [],
            allClients:[],
            loading: false
        }
    }

    componentDidMount() {
        this.fetchClient()
    }

    fetchClient = () => {
        this.setState({ loading: true });
        const config = {
            headers: { Authorization: `Bearer ${this.props.token}` }
        };
        console.log(config)
        axios.get('/user/userclient', config)
            .then(res => {
                //console.log(res.data)
                let clients = handleKeyValueClient(res.data.data.clients);
                //console.log(clients);
                this.setState({ allClients: clients, loading: false })
                this.fetchAPI(clients);
            })
            .catch(err => {
                this.setState({ error: "Something went wrong.", loading: false })
            });
    }

    fetchAPI(clients) {
        this.setState({ loading: true });
        const token = {
            token: this.props.token,
            clients: objectToArray(clients),
        };
        console.log(token);
        axios.post('/insight', token)
            .then(res => {
                let data = res.data.data;
                console.log(res.data.data)
                if (res.data.status === true) {
                    this.setState({
                        dataCount: data.countData,
                    })
                }
                this.setState({loading:false});
            })
            .catch(err => {
                this.setState({ loading: false })
                console.log(err)
            });
    }

    render() {

        return <>
            <div className="row">
                <div className="col-sm-12">
                    <div className="page-title-box">
                        <h4 className="page-title">Dashboard</h4>
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                            <li className="breadcrumb-item active"><Link to="/dashboard">Dashboard</Link></li>
                        </ol>
                    </div>
                </div>
            </div> 
            <div className="row">
                <div className="col-lg-12">
                    <div className="card m-b-20">
                        <div className="tab-pane p-3 active">
                            {this.state.loading ? <SimpleSpinner > </SimpleSpinner> : null}
                            {
                                this.state.dataCount && this.state.dataCount.length > 0 ?
                                    <CountBox
                                        dataCount={this.state.dataCount}
                                    />
                                    : 
                                    <>
                                    <div className="row">
                                        <SingleBoxEmpty header={"TOTAL APPS"} count={0} />
                                        <SingleBoxEmpty header={"TOTAL USERS"} count={0} />
                                        <SingleBoxEmpty header={"NEW SURVEYS"} count={0} />
                                        <SingleBoxEmpty header={"TOTAL SURVEYS"} count={0} />
                                    </div>

                                    </>
                            }
                        </div>
                        <div className="tab-pane p-3 active" >
                            <DashboardCardComp />

                        </div>


                    </div>
                </div>
            </div>


        </>
    }
}

const mapStateToProps = state => {
    return {
        token: state.auth.token,
        selectedBasicFilter: state.assessment.assessmentBasicFilter,
    }
}
export default connect(mapStateToProps)(Dashboard);