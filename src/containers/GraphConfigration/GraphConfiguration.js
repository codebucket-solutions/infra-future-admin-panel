import React, { Component } from 'react';
import { connect } from 'react-redux';

import AUX from '../../hoc/Aux_';
import GraphDescription from '../Configuration/GraphDescription'
import GraphConfig from '../Configuration/GraphSetting';
import BoxSettings from '../Configuration/BoxSetting';
import GraphDescriptionAdvanced from '../Configuration/GraphDescriptionAdvanced';

class Configuration extends Component {

    constructor(props) {
        super(props);
        this.state = {

            loading: false,
            error: null,
            message: null,
        }
    }

    render() {
        return (
            <AUX>
                <div className="row">
                    <div className="col-sm-12">
                        <div className="page-title-box">
                            <h4 className="page-title">Graph Configuration</h4>
                        </div>
                    </div>
                </div>
                <GraphConfig />
                <BoxSettings />
                <GraphDescription />
                <GraphDescriptionAdvanced />
            </AUX>
        );
    }
}

const mapStateToProps = state => {
    return {
        token: state.auth.token
    }
}

export default connect(mapStateToProps)(Configuration);
