import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from 'react-redux';
import axios from '../../axios-details';

import Spinner from '../../components/Spinner/simpleSpinner';
import Alert from '../../components/Alert/common';
import Star from '../../components/Required/star';

class modalUploadAHPHierarchy extends Component {

    constructor(props) {
        super(props);
        this.state={
            id:'',
            file:'', 
            loading:false,
            message:null,
            error:null,
        };
    }

    componentDidMount(){
      if(this.props.hierarchy && this.props.hierarchy.length > 0){
        this.setState({id: this.props.hierarchy[0].id});
      }
      else{
        this.setState({error:"Please add a hierarchy first"});
      }
    }
    
    fileChangeHandler = (event, controlName) =>{
      this.setState({[controlName]:event.target.files[0]});
    }

    inputChangeHandler = (event, controlName) =>{
        this.setState({[controlName]:event.target.value});
    }

    closeModal = () =>{
      setTimeout(
        () => this.props.toggle(this.state.update)
        ,2000);
    }

    submitHandler = (e) =>{
      e.preventDefault();
      console.log(this.state);
      this.setState({loading:true, error:null, message: null, error_data:{}});

      let formData = new FormData();     
      formData.append('json_file', this.state.file);
      formData.append('hierarchy_id', this.state.id);

      const config = {     
        headers: { 'content-type': 'multipart/form-data',Authorization: `Bearer ${this.props.token}` }
      }
        // upload response of multiple apps of a user
        axios.post('/hierarchy/import', formData, config)
            .then(res=>{
              console.log(res.data);
                if(res.data.status){
                    this.setState({loading:false,update: true, message:res.data.message});
                    this.closeModal();
                }
                else{
                  let message = res.data.message;
                  this.setState({loading:false,update: true, error:message});
                }
                    
                // this.closeModal();
            })
            .catch(err =>{
                console.log(err);
                this.setState({loading:false, error:"Something went wrong.Please try again"})
                // this.closeModal();
            })
    }
      
    
    
    
    
  
    render() {
        console.log(this.props)
        let body = (
        <div className="card-body">
            <form onSubmit={this.submitHandler}>
                {this.state.message ? <Alert classes={"alert-success"} msg={this.state.message} />: null}
                {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} />: null}
                {this.state.loading ?  <Spinner /> : null }
                <br />
                <div className="row">
                    <div className="col-sm-6 col-md-6 col-lg-6">
                        <div className="form-group">
                            <label htmlFor="name">Select Hierarchy Name<Star /></label>
                            <select 
                                className="form-control" 
                                onChange={(event) => this.inputChangeHandler(event, "id")}
                                value={this.state.name}
                                required
                            >
                              {
                                this.props.hierarchy.map((h,i) =>(
                                    <option value={h.id}>{h.name}</option>
                                ))
                              }
                            </select>
                        </div>
                    </div>
                    <div className="col-sm-6 col-md-6 col-lg-6">
                        <div className="form-group">
                            <label htmlFor="name">Upload JSON<Star /></label>
                            <input type="file" required accept=".json" onChange={(event) => this.fileChangeHandler(event,"file" )} name="file" id="file" className="" />
                        </div>
                    </div>
                    <div className="col-sm-12 col-md-12 col-lg-12 text-center">
                      <div className="form-group">
                          <br />
                          <button type="submit" className="btn btn-primary waves-effect waves-light">Upload</button>
                      </div>
                  </div>
                </div>
            </form>
        </div>
    );
    
    return (
      <div>
        <Modal size="lg" isOpen={this.props.modal} toggle={() => this.props.toggle(this.state.update)} >
          <ModalHeader toggle={() => this.props.toggle(this.state.update)}>Upload JSON</ModalHeader>
          <ModalBody>
            {this.state.loading ?   <Spinner /> : body }
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={() => this.props.toggle(this.state.update)}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStatetoProps = state =>{
  return {
      token: state.auth.token,
      name:state.auth.name,
  };
}

export default connect(mapStatetoProps)(modalUploadAHPHierarchy);
