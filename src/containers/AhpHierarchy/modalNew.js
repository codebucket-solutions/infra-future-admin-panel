import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from 'react-redux';
import axios from '../../axios-details';

import Spinner from '../../components/Spinner/simpleSpinner';
import Alert from '../../components/Alert/common';
import Star from '../../components/Required/star';

import MySelect from '../../components/ReactSelect';
import { handleKeyValue, objectToArray } from '../../shared/utility';
import { postAPI, getAPI } from '../../APICall/index';


class ModalScore extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            message: null,
            error: null,
            name: '',
            selectedCriteria: [],
            allCriteria: [],
            ahp2: [],
            project: 0,
        };
    }

    inputChangeHandler = (event, controlName) => {
        this.setState({ [controlName]: event.target.value });
    }

    componentDidMount() {
        // console.log(this.props);

        this.fetchAPI()
    }

    fetchAPI = () => {
        const config = {
            headers: { Authorization: `Bearer ${this.props.token}` }
        };
        // console.log(config)
        axios.get('/hierarchy/fetch_dropdown', config)
            .then(res => {
                // console.log(res.data)
                if (res.data.status) {
                    // console.log(res.data.data)

                    this.setState({

                        ahp2: res.data.data.ahp_criteria_lev_2,
                        allCriteria: handleKeyValue(res.data.data.ahp_criteria_lev_1)
                    })

                }
            })
            .catch(err => console.error(err))
    }

    selectToChange = (e) => {
        this.setState({ selectedCriteria: e })
    }


    submitHandler = (e) => {
        e.preventDefault();

        this.setState({ loading: true, error: null, message: null });
        const data = {
            token: this.props.token,
            name: this.state.name,
            project_id: this.state.project,
            criteria: objectToArray(this.state.selectedCriteria).toString()
        }
        // console.log(this.state);
        // console.log(data, this.props)
        if (this.props.selectedID) {
            data.selected_id = this.props.selectedID;
            axios.post('/hierarchy/duplicate', data)
                .then(res => {
                    // console.log(res)
                    this.setState({ loading: false, error: null, message: null });
                    this.props.toggle(true, this.props.selectedID)
                })
                .catch(err => {
                    this.setState({ loading: false, error: "Something went wrong." })
                });
        }
        else if (this.state.selectedCriteria && this.state.selectedCriteria.length > 0) {
            // console.log(this.state);

            axios.post('/hierarchy/add', data)
                .then(res => {
                    // console.log(res)
                    this.setState({ loading: false, error: null, message: null });
                    this.props.toggle(true, this.props.selectedID)
                })
                .catch(err => {
                    this.setState({ loading: false, error: "Something went wrong." })
                });
        }
        else {
            this.setState({ loading: false, error: "Please select some AHP criteria to add new graph" })
        }

    }
    render() {
        let body = (
            <div className="card-body">
                <form onSubmit={this.submitHandler}>
                    {this.state.message ? <Alert classes={"alert-success"} msg={this.state.message} /> : null}
                    {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} /> : null}
                    {this.state.loading ? <Spinner /> : null}

                    <div className="row">
                        <div className="col-sm-12 col-md-12 col-lg-12">
                            <div className="form-group">
                                <label htmlFor="name">Name<Star /></label>
                                <input type="text" placeholder="Enter Name" value={this.state.name} required onChange={(event) => this.inputChangeHandler(event, "name")} className="form-control" />
                            </div>
                        </div>

                        {!this.props.selectedID ? <div className="col-sm-12">
                            <div className="form-group">
                                <label htmlFor="name">APH Criteria<Star /></label>
                                <MySelect
                                    value={this.state.selectedCriteria}
                                    onChange={this.selectToChange}
                                    options={this.state.allCriteria}
                                    allowSelectAll={true}
                                    isMulti={true}
                                    required
                                    noMin={true}
                                />
                            </div>
                        </div>
                            : null}
                        <div className="col-sm-12 col-md-12 col-lg-12 text-center">
                            <div className="form-group">
                                <br />
                                <button type="submit" className="btn btn-primary waves-effect waves-light">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        );

        return (
            <div>
                <Modal size="lg" isOpen={this.props.modal} toggle={() => this.props.toggle(null, this.props.selectedID)} >
                    <ModalHeader toggle={() => this.props.toggle(null, this.props.selectedID)}>New Hierarchy</ModalHeader>
                    <ModalBody>
                        {this.state.loading ? <Spinner /> : body}
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={() => this.props.toggle(null, this.props.selectedID)}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

const mapStatetoProps = state => {
    return {
        token: state.auth.token,
        name: state.auth.name,
    };
}

export default connect(mapStatetoProps)(ModalScore);
