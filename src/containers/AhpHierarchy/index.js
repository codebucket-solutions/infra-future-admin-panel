import React, { Component } from 'react';
import { MDBDataTable } from 'mdbreact';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import fileDownload from 'js-file-download';

import axios from '../../axios-details';
import AUX from '../../hoc/Aux_';

import ModalHierarchy from './modalNew';
import ModalUpload from './modalUploadAHPHierarchy';
import Spinner from '../../components/Spinner/simpleSpinner';
import ConfirmModal from '../../components/Modal/modalConfirm.js';

class AhpHierarchy extends Component {

    constructor(props) {
        super(props);
        this.state = {
            columns: [
                {
                    label: 'Sr.No.',
                    field: 'sro',
                    sort: 'asc',
                    width: 150
                },
                {
                    label: 'Name',
                    field: 'name',
                    sort: 'asc',
                    width: 270
                },
                {
                    label: 'Project Name',
                    field: 'pname',
                    sort: 'asc',
                    width: 270
                },
                {
                    label: 'Action',
                    field: 'action',
                    sort: 'asc',
                    width: 100
                },
            ],
            rows: [],
            data: [],
            toggle: false,
            loading: false,
            title: null,
            adminId: null,
            error: null,
            message: null,
            clientId: null,
            status: null,
            modalHierarchy: false,
            userNotMapped: [],
            allHierarchy:[],

            selectedID: null,
            selectedProject:0,
            modalImport: false,

            deleteModal: false,
        }
    }

    fetchHierarchy = () => {
        this.setState({ loading: true });
        const config = {
            headers: { Authorization: `Bearer ${this.props.token}` }
        };
        //console.log(config)
        axios.get('/hierarchy/fetch_all', config)
            .then(res => {
                let _hierarchy = [];
                let hierarchy = res.data.data.hierarchy;
                console.log(res.data.data);
                let active;
                if (res.data.status) {
                    for (let key in hierarchy) {
                            active = (
                                <AUX>
                                    <button onClick={() => this.handleEdit(hierarchy[key].id)}
                                        className="btn btn-success">Edit</button> &nbsp;&nbsp;
                                    <button onClick={() => this.handleDupicate(hierarchy[key].id, hierarchy[key].project_id)}
                                        className="btn btn-primary">Duplicate</button>&nbsp;&nbsp;
                                    <button onClick={() => this.exportCSV(hierarchy[key].id)}
                                        className="btn btn-secondary">Export</button>
                                    &nbsp;&nbsp;
                                    <button onClick={() => this.handleDelete(hierarchy[key].id)}
                                        className="btn btn-danger">Delete</button>
                                </AUX>
                            )
                            _hierarchy.push({
                            sro: parseInt(key) + 1,
                            name: hierarchy[key].name,
                            pname: hierarchy[key].pname ? hierarchy[key].pname : 'Gold',
                            action: active,
                        })
                    }
                }
                this.setState({ loading: false, rows: _hierarchy,allHierarchy: hierarchy })
            })
            .catch(err => {
                this.setState({ loading: false, error: "Something went wrong." })
            });
    }

    handleDelete = (id) =>{
        this.setState({deleteModal: !this.state.deleteModal, selectedID: id})
    }

    handleEdit = (id) =>{
        this.props.history.push({
            pathname:'/hierarchy_tree/'+id,
            state: { id: id }
        })
    }

    componentDidMount() {
        this.fetchHierarchy();
    }

    hadleHierarachyModal = (update, selectedID) =>{
        if(update){
            this.fetchHierarchy();
        }
        if(selectedID){
            // this.setState({selectedID: selectedID})
        }
        console.log(this.state.modalHierarchy)
        this.setState({ modalHierarchy: !this.state.modalHierarchy, selectedID: null, selectedProject:null})
    }   

    handleDupicate = (id, project_id) =>{
        
        this.hadleHierarachyModal()
        this.setState({selectedID : id, selectedProject:project_id })
    }

    toggleImportModal = () =>{
        this.setState({modalImport : !this.state.modalImport})
    }

    exportCSV = (id) =>{

        const token = {
            token: this.props.token,
            hierarchy_id: id
        };
        axios.post('/hierarchy/export', token, {
            responseType: 'blob',
          })
        .then((response) => {
            fileDownload(response.data, 'hierarchy'+Date.now()+'.json');
        })
        .catch( err => {
            
         console.log(err)
        });
    }

    submitDelete = () =>{

        const token = {
            token: this.props.token,
            hierarchy_id: this.state.selectedID
        };

        axios.post('/hierarchy/delete', token)
        .then((res) => {
            this.setState({error: res.data.status})
            this.handleDelete(null);
            this.fetchHierarchy();
        })
        .catch( err => {
            
         console.log(err)
        });
    }

    render() {
        console.log(this.state.modalImport)
        return (
            <AUX>
                <div className="row">
                    <div className="col-sm-12">
                        <div className="page-title-box">
                            <h4 className="page-title">Hierarchy</h4>
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                                <li className="breadcrumb-item active">Hierarchy</li>
                            </ol>
                            {/* <Tinycharts /> */}
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-12">
                        <div className="card m-b-20">
                            <div className="card-body">
                                <h4 className="mt-0 header-title text-right">
                                    <button className="btn btn-success mb-3" onClick={() => this.toggleImportModal()}>Import</button> &nbsp;
                                    <button className="btn btn-primary mb-3" onClick={() => this.hadleHierarachyModal()}>Add Hierarchy</button>
                                </h4>
                                {this.state.loading ? <AUX><Spinner /><br /></AUX> : null}
                                <div style={{ overflow: 'auto' }}>
                                    <MDBDataTable
                                        bordered
                                        hover
                                        data={{ columns: this.state.columns, rows: this.state.rows }}
                                        info={this.state.rows.length > 0 ? true : false}
                                    />
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                {
                    this.state.modalHierarchy
                        ? <ModalHierarchy 
                            selectedProject={this.state.selectedProject}
                            modal={this.state.modalHierarchy} selectedID = {this.state.selectedID} toggle={this.hadleHierarachyModal} title={"Add new Hierarchy"} />
                        : false
                }
                {
                    this.state.modalImport 
                    ?
                    <ModalUpload modal={this.state.modalImport} hierarchy={this.state.allHierarchy} toggle={this.toggleImportModal} /> 
                    : null
                }
                {
                    this.state.deleteModal
                    ? <ConfirmModal 
                        modal={this.state.deleteModal} 
                        toggle={() =>this.handleDelete(null)} 
                        click={() => this.submitDelete()} 
                        content={"Do you really want to delete this hierarchy?"}  
                        title={"Delete hierarchy"} />
                    : false
                }
            </AUX>
        );
    }
}

const mapStatetoProps = state => {
    return {
        token: state.auth.token,
    };
}

export default connect(mapStatetoProps)(AhpHierarchy);