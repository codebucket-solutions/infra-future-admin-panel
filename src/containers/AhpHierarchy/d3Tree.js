import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { DndProvider, DragSource, DropTarget } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import SortableTree from 'react-sortable-tree';
import 'react-sortable-tree/style.css'; // This only needs to be imported once in your app
import axios from '../../axios-details';
import { connect } from 'react-redux';
import Alert from '../../components/Alert/common'

import { changeNodeAtPath,removeNodeAtPath } from '../../shared/tree.js';
import ModalScore from './modalScore';
import LabelComponent from './labelComponet'
import { TreeMapping } from  '../../Utils/tree';
import ConfirmModal from '../../components/Modal/modalConfirm.js';


const difference = (arr1, arr2) =>{
  return arr1.filter(x => !arr2.includes(x));
}
// -------------------------
// Create an drag source component that can be dragged into the tree
// https://react-dnd.github.io/react-dnd/docs-drag-source.html
// -------------------------
// This type must be assigned to the tree via the `dndType` prop as well
const externalNodeType = 'yourNodeType';
const externalNodeSpec = {
  // This needs to return an object with a property `node` in it.
  // Object rest spread is recommended to avoid side effects of
  // referencing the same object in different trees.
  beginDrag: componentProps => ({ node: { ...componentProps.node } }),
};
const externalNodeCollect = (connect /* , monitor */) => ({
  connectDragSource: connect.dragSource(),
  // Add props via react-dnd APIs to enable more visual
  // customization of your component
  // isDragging: monitor.isDragging(),
  // didDrop: monitor.didDrop(),
});

const newStyle={
    display: 'inline-block',
    padding: '7px 10px',
    background: '#7f7fee',
    color: 'white',
    borderRadius: '6px',
    boxShadow: '0 4px 8px 0 rgba(0,0,0,0.2)'
  }
class externalNodeBaseComponent extends Component {

  render() {
    const { connectDragSource, node } = this.props;

    return connectDragSource(
      <div
        style={newStyle}
      >
        {node.title}
      </div>,
      { dropEffect: 'copy' }
    );
  }
}
externalNodeBaseComponent.propTypes = {
  node: PropTypes.shape({ title: PropTypes.string }).isRequired,
  connectDragSource: PropTypes.func.isRequired,
};
const YourExternalNodeComponent = DragSource(
  externalNodeType,
  externalNodeSpec,
  externalNodeCollect
)(externalNodeBaseComponent);

const trashAreaType = 'yourNodeType';
const trashAreaSpec = {
  // The endDrag handler on the tree source will use some of the properties of
  // the source, like node, treeIndex, and path to determine where it was before.
  // The treeId must be changed, or it interprets it as dropping within itself.
  drop: (props, monitor) => ({ ...monitor.getItem(), treeId: 'trash' }),
};
const trashAreaCollect = (connect, monitor) => ({
  connectDropTarget: connect.dropTarget(),
  isOver: monitor.isOver({ shallow: true }),
});

// The component will sit around the tree component and catch
// nodes dragged out
class trashAreaBaseComponent extends Component {
  render() {
    const { connectDropTarget, children, isOver } = this.props;

    return connectDropTarget(
      <div
        style={{
          height: '70vh',
          padding: 50,
          borderRadius:5,
          background: isOver ? 'pink' : '#f7dfdf',
        }}
      >
        {children}
      </div>
    );
  }
}
trashAreaBaseComponent.propTypes = {
  connectDropTarget: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired,
  isOver: PropTypes.bool.isRequired,
};
const TrashAreaComponent = DropTarget(
  trashAreaType,
  trashAreaSpec,
  trashAreaCollect
)(trashAreaBaseComponent);



const parseObject = (object) =>{
  object.map(o1 =>{
    if(o1.children && o1.children.length > 0){
        o1.children.map(o2 =>{
            if(o2.children && o2.children.length > 0){
                o2.children.map(o3 =>{
                    o3['subtitle']  = 0
                })
            }
            o2['subtitle'] = 0
        })
    }
    o1['subtitle'] = 0
  })

  return(object)
}

const createObject = (object) =>{
  object.map(o1 =>{
    if(o1.children && o1.children.length > 0){
        o1.children.map(o2 =>{
            if(o2.children && o2.children.length > 0){
                o2.children.map(o3 =>{
                    o3['subtitle']  = <LabelComponent localScore={o3.localScore} globalScore={o3.globalScore} />
                })
            }
            o2['subtitle'] = <LabelComponent localScore={o2.localScore} globalScore={o2.globalScore} />
        })
    }
    o1['subtitle'] = <LabelComponent localScore={o1.localScore} globalScore={o1.globalScore} />
  })

  return(object)
}
 

class Tree extends Component {
  constructor(props) {
    super(props);

    this.state = {
      treeData: [ { title: 'Suitability' }],
      prevTreeData:[ { title: 'Suitability' }],
      externalNode: { title: 'Select' },
      graphId:null,
      quest_tag:[],
      ahp1:[],
      ahp2:[],
      name:null,
      modal: false,
      details: null,
      path: null,
      selectedAHP1:null,
      selectedAHP2:null,
      selectedTag: null,
      message:null,
      selected_ahp1:[],
      selected_ahp2:[],
      selected_tags:[],
      lastMovePrevPath: null,
      lastMoveNextPath: null,
      lastMoveNode: null,
      deleteModal:false,
      node:null,
      path:null,
    };
  }
  
  componentDidMount() {
    let graphId = this.props.location.state.id;
    this.setState({
      graphId:graphId
    })
    this.fetchGraph(graphId);
  }

  fetchAPI = (projectId) => {
    this.setState({ loading: true });
    const config = {
        headers: { Authorization: `Bearer ${this.props.token}` }
    };
    //console.log(config)
    axios.get('/hierarchy/fetch_dropdown', config)
    .then(res => {
      console.log(res.data)
        if(res.data.status){
          console.log(res.data.data)
          
          this.setState({
            quest_tag: res.data.data.quest_tag,
            ahp1: res.data.data.ahp_criteria_lev_1,
            ahp2: res.data.data.ahp_criteria_lev_2,

          })
        }
    })
    .catch(err => console.error(err))
  }

  fetchGraph = (graphId) => {
    this.setState({ loading: true });
    const config = {
        headers: { Authorization: `Bearer ${this.props.token}` }
    };
    //console.log(config)
    axios.get('/hierarchy/fetch/'+graphId, config)
    .then(res => {
      
      console.log(res.data)
        if(res.data.status){
          if(res.data.data.hierarchy[0].hierarchy){
            this.setState({
              treeData: createObject(JSON.parse(res.data.data.hierarchy[0].hierarchy)), 
              prevTreeData:createObject(JSON.parse(res.data.data.hierarchy[0].hierarchy)), 
              name: res.data.data.hierarchy[0].name,
            })
          }
          else{
            let _tree = [];
            
            selected_ahp1.map((a, i) =>{
              _tree.push({
                title: a,
                children:[],
                localScore:0,
                globalScore:0,
                subtitle: <LabelComponent localScore={0} globalScore={0} />, 
                isTag:0,
                type:"ahp1"
  
              })
            })

            console.log(_tree);
            this.setState({
              name: res.data.data.hierarchy[0].name,
              ahp1: res.data.data.ahp_criteria_lev_1,
              treeData: _tree,
              prevTreeData:_tree,
            })
          }

          let selected_ahp1 = res.data.data.hierarchy[0].selected_ahp1 ? res.data.data.hierarchy[0].selected_ahp1.split(',') : []
          let selected_ahp2  = res.data.data.hierarchy[0].selected_ahp2 ? res.data.data.hierarchy[0].selected_ahp2.split(',') : []
          let selected_tags = res.data.data.hierarchy[0].selected_tags ? res.data.data.hierarchy[0].selected_tags.split(',') : []
          console.log(selected_ahp2);
          console.log(selected_ahp1);
          console.log(selected_tags);

          
          this.setState({
            selected_ahp1: selected_ahp1,
            selected_ahp2:selected_ahp2,
            selected_tags: selected_tags,
          })

          this.fetchAPI(res.data.data.hierarchy[0].project_id);
        }
    })
    .catch(err => console.error(err))
  }


  inputChangeHandler = (e, isTag,type) =>{
    this.setState({externalNode:{
      title:e.target.value,
      children:[],
      localScore:0,
      globalScore:0,
      subtitle: <LabelComponent localScore={0} globalScore={0} />,
      isTag:isTag,
      type:type
    }})

    if(isTag === 0 && type === "ahp1"){
      this.setState({selectedTag:0, selectedAHP1: e.target.value, selectedAHP2: 0})
    }
    else if(isTag === 0 && type === "ahp2"){
      this.setState({selectedTag:0, selectedAHP1:0, selectedAHP2: e.target.value})
    }
    else{
      this.setState({selectedAHP1:0,selectedAHP2: 0, selectedTag: e.target.value})
    }
  }

  openScoreModal = (node, path) =>{

    console.log(node, path)
    // this.setState(state => ({
    //   treeData: addNodeUnderParent({
    //     treeData: state.treeData,
    //     parentKey: path[path.length - 1],
    //     expandParent: true,
    //     getNodeKey,
    //     newNode: {
    //       title: `${getRandomName()}`,
    //     },
    //     addAsFirstChild: state.addAsFirstChild,
    //   }).treeData,
    // }))
    console.log( path[path.length - 1])

    this.setState({ modal: true, details: node, path: path})
  }

  fetchOptions = (all ,selected) =>{
    let toSend = [];
    // console.log(all, this.state[all])
    // console.log(selected, this.state[selected])
    let data = this.arrayDifference(this.state[all], this.state[selected])
    data.forEach((q, i) =>{
      toSend.push(<option key={i} value={q}>{q}</option>); 
    })
    return toSend;
   
  }
  

  handleSave = () =>{
    const data = {
      token: this.props.token,
      hierarchy: JSON.stringify(parseObject(this.state.treeData)),
      ahp1: this.state.selected_ahp1.toString(),
      ahp2: this.state.selected_ahp2.toString(),
      tags: this.state.selected_tags.toString(),
    };

    axios.post('/hierarchy/fetch/'+this.state.graphId, data)
        .then(r =>{
            console.log(r)
            this.setState({
              message: r.data.message
            })
            this.fetchGraph(this.state.graphId);
        })
        .catch(c =>{
          console.log(c);
        })
  }

  closeModal = (data) =>{
    
    let { path } = this.state
    if(data){
      console.log(this.state.details,data, this.state.path)

      let _details = { ...this.state.details};
      _details['localScore'] = data.localScore;
      _details['globalScore'] = data.globalScore;
      _details['subtitle'] = <LabelComponent localScore={data.localScore} globalScore={data.globalScore} />;
      console.log( path[path.length - 1])
      console.log(_details);
      // this.openScoreModal()
      let _treeData = changeNodeAtPath({
        treeData: this.state.treeData,
        path:path,
        
        newNode: {
          ..._details
        },
        getNodeKey:({treeIndex}) => treeIndex,
      })
      console.log(data);

      this.setState({
        treeData: _treeData,
        prevTreeData: _treeData
      })
    

    console.log(this.state.treeData)

    }
    this.setState({ modal: false, details: null})
  }

  handleChange = (treeData) =>{
    // console.log(treeData)
    this.setState({ treeData })
  }

  
  handleOnMoveNode = (args) =>{
    console.log(this.state.treeData, this.state.prevTreeData);
    this.resetFilter()
    // this.recordCall('onMoveNode', args);
    const { prevPath, nextPath, node,nextParentNode,  } = args;
    console.log(args);
    if(nextParentNode){
      if(TreeMapping(nextParentNode, node)){
        this.setState({ prevTreeData: this.state.treeData })
        this.updateDropDown(node, "add")
      }
      else{
        this.setState({ treeData: this.state.prevTreeData })
      }
    }
    else{
      this.updateDropDown(node, "add")  
    }
    
    this.setState({
      lastMovePrevPath: prevPath,
      lastMoveNextPath: nextPath,
      lastMoveNode: node,
    });
  }

  arrayDifference = (all, selected) =>{
    return all.filter(x => !selected.includes(x));
  }

  getNodeKey = ({ treeIndex }) => treeIndex;

  updateArray = (array, element, type) =>{
    console.log(type);
    if(type==="add"){
      array.push(element);
      console.log(array)
    }
    else{
      const index = array.indexOf(element);
      if (index > -1) {
        array.splice(index, 1);
      }
     
    }
    return array;
  }

  handleDeleteModal = (path, node) =>{
    if(node.children && node.children.length > 0){
      this.setState({deleteModal: !this.state.deleteModal, node:node, path:path})
    }
    else{
      this.handleDeleteNode(path, node)
    }
  }

  closeDeleteModal =() =>{
    this.setState({deleteModal: !this.state.deleteModal})
  }

  handleDeleteNode = (path, node) =>{
    console.log(path, node);
    this.resetFilter()
    this.updateDropDown(node, "delete")
    this.setState(state => ({
      treeData: removeNodeAtPath({
        treeData: state.treeData,
        path,
        getNodeKey:this.getNodeKey,
      }),
    }))

    this.setState({deleteModal: false})
  }

  udpateHierarchydelete = (node) =>{
      node.map(n =>{
          console.log(n);
          if(n.children && n.children.length > 0){
              n.children.map(c =>{
                console.log(c);
                this.updateDropDown(c, "update")
              })
          }
          this.updateDropDown(n, "update")
      })
  } 

  updateDropDown = (node, type) =>{
    console.log(node, type);
    if(node.type === "ahp1"){
      let _selected_aph1 = [...this.state.selected_ahp1];
      _selected_aph1 = this.updateArray(_selected_aph1, node.title, type)
      this.setState({
        selected_ahp1 : [..._selected_aph1]
      })
      if(type === "delete" && node.children && node.children.length > 0){
          this.udpateHierarchydelete(node.children)
      }

    }
    else if(node.type === "ahp2"){
      let _selected_aph2 = [...this.state.selected_ahp2];
      _selected_aph2 = this.updateArray(_selected_aph2, node.title, type)
      this.setState({
        selected_ahp2 : [..._selected_aph2]
      })
      console.log(node.children)
      if(type === "delete" && node.children && node.children.length > 0){
        this.udpateHierarchydelete(node.children)
      }
    }
    else if(node.type === "tag"){
      let selected_tags = [...this.state.selected_tags];
      selected_tags = this.updateArray(selected_tags, node.title, type)
      this.setState({
        selected_tags : [...selected_tags]
      })
    }
  }

  handleDrag = (isDragging, draggedNode) =>{
    console.log(isDragging, draggedNode)
  }


  resetFilter = () =>{
    this.setState({
      selectedAHP1:0,
      selectedAHP2:0,
      selectedTag: 0,
      externalNode: { title: 'Select' }
    })
  }



  
  render() {
    // console.log(this.state.selectedAHP2)
    const { externalNode, quest_tag, ahp1, ahp2, modal, details } = this.state;
    
    return (
      <>
      <div className="row">
          <div className="col-sm-12">
              <div className="page-title-box">
                <h4 className="page-title">{this.state.name}</h4>
              </div>
          </div>
      </div>
      {this.state.message ? <Alert classes="alert-success" msg={this.state.message} /> : null}
      <DndProvider backend={HTML5Backend}>
        <br />
        <div className="row">
            <div className="col-md-11">
              <div className="row"> 
                <div className="col-sm-3 mt-4">
                 { externalNode.title === "Select" ? <span style={newStyle}>Select</span> : <YourExternalNodeComponent node={externalNode} />}← drag this
                </div>
                <div className="col-sm-3">
                  <div className="form-group">
                      <label>APH Criteria 1</label>
                      <select 
                        className="form-control" 
                        onChange={(event) => this.inputChangeHandler(event,0, "ahp1")}
                        value={this.state.selectedAHP1}>
                          <option value={0}>Select</option>
                          {
                            ahp1 && ahp1.length>0 && this.fetchOptions("ahp1","selected_ahp1")
                          }
                      </select>
                  </div>
                </div>
                <div className="col-sm-3">
                  <div className="form-group">
                      <label>APH Criteria 2</label>
                      <select 
                        className="form-control" 
                        onChange={(event) => this.inputChangeHandler(event, 0, "ahp2")}
                        value={this.state.selectedAHP2}>
                          <option value={0}>Select</option>
                          {
                              ahp2 && ahp2.length>0 && this.fetchOptions("ahp2","selected_ahp2")
                          }
                      </select>
                  </div>
                </div>
                <div className="col-sm-3">
                  <div className="form-group">
                      <label>Question Tag</label>
                      <select 
                        className="form-control" 
                        onChange={(event) => this.inputChangeHandler(event, 1, "tag")}
                        value={this.state.selectedTag}>
                          <option value={0}>Select</option>
                          {
                            quest_tag && quest_tag.length>0 && this.fetchOptions("quest_tag","selected_tags")

                          }
                      </select>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-1">
              <div className="col-sm-3">
                <label></label>
                <button type="button" onClick={this.handleSave} className="btn btn-primary">Save</button>
              </div>
            </div>
            
        </div>
        
        <br />
        <div className="row">
          <div className="col-md-12">
            <div style={{ height: '70vh'}}>
              <SortableTree
                treeData={this.state.treeData}
                onChange={(treeNode) => this.handleChange(treeNode)}
                dndType={externalNodeType}
                onDragStateChanged={({isDragging,draggedNode}) =>{
                  this.handleDrag(isDragging, draggedNode)
                }}
                generateNodeProps={({ node, path }) => ({
                  buttons: [
                    <button
                      className="btn btn-success btn-sm"
                      style={{backgroundColor: '#a8a3a4',border: '1px solid #a8a3a4'}}
                      onClick={() => this.openScoreModal(node, path)}
                    >
                     <i className="mdi  mdi-pencil"></i>
                    </button>,
                    <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>,
                    
                    <button
                        className="btn btn-danger btn-sm"
                        style={{backgroundColor: '#f097a5',border: '1px solid #f097a5'}}
                        onClick={() =>this.handleDeleteModal(path, node)}
                    >
                    <i className="mdi mdi-minus"></i>
                  </button>,
                  ],
                  
                })}
                onMoveNode={args => this.handleOnMoveNode(args)}
              />
            </div>
          </div>
          {/* <div className="col-md-6">
              <TrashAreaComponent style={{height:'5vh'}}>
              
              </TrashAreaComponent>
          </div> */}
          
        
          
        
        </div>
      </DndProvider>
      {
        modal ?  <ModalScore modal={modal} details={details} toggle={this.closeModal} /> : null
      }
      {
          this.state.deleteModal
          ? <ConfirmModal modal={this.state.deleteModal} toggle={() =>this.closeDeleteModal()} click={() => this.handleDeleteNode(this.state.path, this.state.node)} content={"Do you want to delete this node.There are clild nodes present."}  title={"Delete Node"} />
          : false
      }
      
      </>
    );
  }
}


const mapStatetoProps = state => {
    return {
        token: state.auth.token,
    };
}

export default connect(mapStatetoProps)(Tree);
