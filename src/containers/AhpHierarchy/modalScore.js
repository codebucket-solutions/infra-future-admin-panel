import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from 'react-redux';

import Spinner from '../../components/Spinner/simpleSpinner';
import Alert from '../../components/Alert/common';
import Star from '../../components/Required/star';

class ModalScore extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      message: null,
      error: null,
      update: false,
      localScore:0,
      globalScore:0,
    };
  }

  inputChangeHandler = (event, controlName) => {
    this.setState({ [controlName]: event.target.value });
  }

  componentDidMount() {
      console.log("asdad");
      this.updateScore();
    
  }

  updateScore = () =>{
    console.log(this.props)
      this.setState({
        localScore: this.props.details && this.props.details.localScore ? this.props.details.localScore : 0,
        globalScore: this.props.details && this.props.details.globalScore ? this.props.details.globalScore : 0,
      })
  }


  submitHandler = (e) => {
    e.preventDefault();
    this.props.toggle({
      localScore: this.state.localScore,
      globalScore: this.state.globalScore
    })
  }




  render() {
    
    let body = (
      <div className="card-body">
        <form onSubmit={this.submitHandler}>
          {this.state.message ? <Alert classes={"alert-success"} msg={this.state.message} /> : null}
          {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} /> : null}
          {this.state.loading ? <Spinner /> : null}

          <div className="row">
            <div className="col-sm-6 col-md-6 col-lg-6">
              <div className="form-group">
                <label htmlFor="name">Local Score<Star /></label>
                <input 
                  type="number" 
                  step="0.001" 
                  placeholder="Enter Local Score"  
                  value={this.state.localScore} 
                  required 
                  onChange={(event) => this.inputChangeHandler(event, "localScore")} 
                  className="form-control" 
                />
              </div>
            </div>

            <div className="col-sm-6 col-md-6 col-lg-6">
              <div className="form-group">
                <label htmlFor="name">Global Score<Star /></label>
                <input 
                  type="number" 
                  step="0.001" 
                  placeholder="Enter Global Score" 
                  value={this.state.globalScore} 
                  required 
                  onChange={(event) => this.inputChangeHandler(event, "globalScore")} 
                  className="form-control"
                  disabled={this.props.details && this.props.details.isTag ? false: true}
                />
              </div>
            </div>

            <div className="col-sm-12 col-md-12 col-lg-12 text-center">
              <div className="form-group">
                <br />
                <button type="submit" className="btn btn-primary waves-effect waves-light">Update</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    );

    return (
      <div>
        <Modal size="lg" isOpen={this.props.modal} toggle={() => this.props.toggle()} >
          <ModalHeader toggle={() => this.props.toggle()}>Update Score</ModalHeader>
          <ModalBody>
            {this.state.loading ? <Spinner /> : body}
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={() => this.props.toggle()}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStatetoProps = state => {
  return {
    token: state.auth.token,
    name: state.auth.name,
  };
}

export default connect(mapStatetoProps)(ModalScore);
