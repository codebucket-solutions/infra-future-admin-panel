import React, { Component } from 'react';
import InputField from '../../../components/BSPInput';
import { getAPI } from '../../../APICall/index';
import { FETCH_BUSINESS_CAPABILITIES } from '../../../APICall/urls';
import  { generateInput, handleRadio, handleCheckbox } from '../../../shared/states';

class BusinessCapability extends Component {
    constructor(props) {
        super(props);
        this.state = {
            inputField: [],
            dynamicState: {
                name: '',
                description: '',
            },
        };
    }

    componentDidMount() {
        this.fetchAPI();
    }

    fetchAPI = async () => {
        let result = await getAPI(FETCH_BUSINESS_CAPABILITIES);
        console.log(result);
        if (result.status) {
            let _dynamicState = generateInput(this.state.dynamicState,result.data);
            console.log(_dynamicState)
            this.setState({ inputField: result.data, dynamicState: _dynamicState });
            
        }
    }

    handleInputChange = (e, fieldName, dataType, isDynamic) => {
        let _dynamicState = {
            ...this.state.dynamicState
        }
        if(dataType === 'checkbox') {
            let data = handleCheckbox(e.target.value, _dynamicState[fieldName], e.target.checked)
            _dynamicState[fieldName] =  data;
        }
        else if(dataType === 'radio'){
            let data = handleRadio(e.target.value, _dynamicState[fieldName])
            console.log(data)
            _dynamicState[fieldName] =  data;
        }
        else{
            _dynamicState[fieldName] =  e.target.value
        }
        this.setState({dynamicState: _dynamicState})
    }

    render() {
        // console.log(Input);
        console.log(this.state.dynamicState)
        const { inputField, dynamicState } = this.state;
        return (
            <>
                <div className="row">
                    <div className="col-md-12">
                        <div className="form-group">
                            <label htmlFor="mobile">Name</label>
                            <input
                                value={dynamicState.name}
                                onChange={(e) => this.handleInputChange(e, "name", "text", false)}
                                type="text"
                                className="form-control"
                                placeholder="Enter Name"
                                required
                            />
                        </div>
                    </div>
                    <div className="col-md-12">
                        <div className="form-group">
                            <label htmlFor="mobile">Description</label>
                            <textarea
                                value={dynamicState.description}
                                onChange={(e) => this.handleInputChange(e, "description", "text", false)}
                                className="form-control"
                                placeholder="Enter Description"
                                required
                            />
                        </div>
                    </div>
                    {
                        inputField && inputField.length > 0 && inputField.map((i, j) => {
                            return (
                                <div className="col-md-12" key={i}>
                                    <InputField
                                        data_type={i.data_type}
                                        options={i.options}
                                        isRequired={i.isRequired}
                                        value={dynamicState[i.name]}
                                        name={i.attributes_name}
                                        handleInputChange={this.handleInputChange}
                                    />
                                </div>
                            )
                        })
                    }
                </div>
            </>
        )
    }
}

export default BusinessCapability;