import React, { Component } from 'react';
import { MDBDataTable } from 'mdbreact';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import AUX from '../../../hoc/Aux_';
import * as actions from '../../../store/actions/index';
import Spinner from '../../../components/Spinner/simpleSpinner';
import Alert from '../../../components/Alert/common';
import '../../response.css';
import { btnStyle } from '../../../util';
import ModalLayout from '../../../components/ModalLayout';
import AddBusinessModal from './addBusinessModal';


let answers = [];
let count = 1;


class Capability extends Component {



    constructor(props) {
        super(props);
        this.state = {
            attribute_schema:null,
            columns: [
                {
                    label: <label className="container mb-4">
                        <input
                            type="checkbox"
                            value={answers}
                            onClick={(event) => this.handleCheckbox(event, "all")}
                            checked={count === answers.length ? true : false} />
                        <span className="checkmark"></span>
                    </label>,
                    field: 'export',
                    sort: 'asc',
                    width: 150
                },
                {
                    label: ' Name',
                    field: 'aname',
                    sort: 'asc',
                    width: 270
                },
                {
                    label: 'Description',
                    field: 'bc',
                    sort: 'asc',
                    width: 200
                },
               
                {
                    label: 'Action',
                    field: 'action',
                    sort: 'asc',
                    width: 100
                },
            ],
            rows: [],
            toggle: false,
            data: [],
           
        }
        this.csv = React.createRef();
        this.csv1 = React.createRef();
        this.csv_per = React.createRef();
    }



    
 

    changeExport = () => {
        let { columns } = this.state;
        columns[0].label = (
            <label className="container mb-4">
                <input
                    type="checkbox"
                    value={answers}
                    onClick={(event) => this.handleCheckbox(event, "all")}
                    checked={count === answers.length ? true : false} />
                <span className="checkmark"></span>
            </label>
        );
        this.setState({ columns: columns });
    }



    componentDidMount() {
      
      
    }

    
    state={
        openModal : false
    }

    onClickButton = e =>{
        this.setState({openModal : true})
    }

    onCloseModal = ()=>{
        this.setState({openModal : false})
    }


    handleInputChange = (e, d, type, addBusiness) =>{
        

        let value = e.target.value
        
        d[addBusiness] = value;
        
    }


    render(){
       
       
        return(
            <AUX>
                <div className="row">
                    <div className="col-sm-12">
                        <div className="page-title-box">
                            <h4 className="page-title">Business Capability</h4>
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                               
                            </ol>

                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-12">
                        <div className="card m-b-20">
                            <div className="card-body">
                                <h4 className="mt-0 header-title text-right">
                                <button
                                        title="Add new Business Capability"
                                        className="btn btn-primary mb-3" style={{btnStyle}} onClick={this.onClickButton}>Add Business</button>
                                        <ModalLayout isOpen={this.state.openModal} toggle={this.onCloseModal} >
                                            <AddBusinessModal />
                                            </ModalLayout>
                                            
                                      
                                  &nbsp;
                                    <button
                                        title="Import responses or applications"
                                        className="btn btn-secondary mb-3"
                                        onClick={this.openModal}>Import
                                    </button>&nbsp;
                                    <button className="btn btn-success mb-3" >Export All</button>&nbsp;
                                    <button 
                                        title="Delete selected application"
                                        onClick={() => this.handleBulkDelete()}
                                        className="btn btn-danger mb-3">Delete
                                    </button>
                                  
                                    
                                </h4>

                                {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} /> : null}
                                {this.state.loading ? <AUX><Spinner /><br /></AUX> : null}
                                <div style={{ overflow: 'auto' }}>
                                    <MDBDataTable
                                        bordered
                                        hover
                                        data={{ columns: this.state.columns, rows: this.state.rows }}
                                        info={this.state.rows.length > 0 ? true : false}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              
            </AUX>
        );
    }
}

const mapStateToProps = state => {
    return {
        token: state.auth.token,
        name: state.auth.name,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setResponse: (id) => dispatch(actions.setResponse(id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Capability);
