import React, { Component } from 'react';
import InputField from '../../../components/BSPInput';
import  { getAPI } from '../../../APICall/index';
import  { FETCH_BUSINESS_CAPABILITIES  } from '../../../APICall/urls';


class AddService extends Component{
    constructor(props){
        super(props);
        this.state={
            inputField:[],
        };
    }


    componentDidMount(){
        this.fetchAPI();
    }

    fetchAPI = async() =>{
        let result = await getAPI(FETCH_BUSINESS_CAPABILITIES);
        console.log(result);
        if(result.status){
            this.setState({inputField: result.data})
        }
    }

    handleInputChange = () =>{

    }

    render(){
        // console.log(Input);
        const { inputField} = this.state;
        return (
            <>
                <div className="row">
                {
                    inputField && inputField.length > 0 && inputField.map((i,j) =>{
                        return (
                            <div className="col-md-12" key={i}>
                                <InputField
                                    data_type={i.data_type}
                                    options={i.options}
                                    isRequired={i.isRequired}
                                    value={0}
                                    name={i.attributes_name}
                                    handleInputChange={this.handleInputChange}
                                />
                            </div>
                        )
                    })
                }
                </div>
            </>
        )
    }
}

export default AddService;