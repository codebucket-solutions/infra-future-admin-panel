import React, { Component } from 'react';
import { MDBDataTable } from 'mdbreact';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { CSVLink } from "react-csv";
import { Progress } from 'reactstrap';


import AUX from '../../../hoc/Aux_';
import * as actions from '../../../store/actions/index';

import Spinner from '../../../components/Spinner/simpleSpinner';
import ModalUpload from '../../../components/Modal/modalUpload';
import Alert from '../../../components/Alert/common';
import ModalAttachProfile from '../../../components/Modal/modalAttachProfile';
import ModalApp from '../../../components/Modal/modalApp';
import ConfirmModal from '../../../components/Modal/modalConfirm.js';
import ChangeClient from '../../../components/Modal/modalChangeUser.js';
import ModalAppExport from '../../../components/Modal/modalAppExport';

import '../../response.css';
import { btnStyle } from '../../../util';
import ModalLayout from '../../../components/ModalLayout';
import AddService from './addService';

let answers = [];
let count = 1;


class Services extends Component {

    constructor(props) {
        super(props);
        this.state = {
            columns: [
                {
                    label: <label className="container mb-4">
                        <input
                            type="checkbox"
                            value={answers}
                            onClick={(event) => this.handleCheckbox(event, "all")}
                            checked={count === answers.length ? true : false} />
                        <span className="checkmark"></span>
                    </label>,
                    field: 'export',
                    sort: 'asc',
                    width: 150
                },
                {
                    label: ' Technology',
                    field: 'tech',
                    sort: 'asc',
                    width: 270
                },
                {
                    label: 'Size',
                    field: 'size',
                    sort: 'asc',
                    width: 200
                },
               
                {
                    label: 'Action',
                    field: 'action',
                    sort: 'asc',
                    width: 100
                },
            ],
            rows: [],
            toggle: false,
            data: [],
            loading: false,
            toggleProfile: false,
            appId: null,
            appName: null,
            apps: null,
            error: null,
            csvData: [],
            csvApps: [],
            csvAppsPer: [],
            confirmModal:false,
            changeClient:false,
            toggleAppUpdate:false,
            toggleAppExport:false
        }
        this.csv = React.createRef();
        this.csv1 = React.createRef();
        this.csv_per = React.createRef();
    }



    changeExport = () => {
        let { columns } = this.state;
        columns[0].label = (
            <label className="container mb-4">
                <input
                    type="checkbox"
                    value={answers}
                    onClick={(event) => this.handleCheckbox(event, "all")}
                    checked={count === answers.length ? true : false} />
                <span className="checkmark"></span>
            </label>
        );
        this.setState({ columns: columns });
    }


    handleCheckbox = (event, app_id) => {
        // console.log(app_id);
        let { apps } = this.state;
        if (app_id === "all") {
            if (event.target.checked) {
                answers = [];
                apps.map(a => answers.push(a.app_id));
            }
            else {
                answers = [];
            }
        }
        else {
            if (event.target.checked) {
                answers.push(app_id);
            }
            else {
                let dataIndex = answers.indexOf(app_id);
                answers.splice(dataIndex, 1);
            }
        }

        this.putDataToState(this.state.apps)
    }

    componentDidMount() {
      
      
    }

    state={
        openModal : false
    }

    onClickButton = e =>{
        this.setState({openModal : true})
    }

    onCloseModal = ()=>{
        this.setState({openModal : false})
    }


    handleInputChange = (e, d, type, addBusiness) =>{
        

        let value = e.target.value
        
        d[addBusiness] = value;
        
    }
   
    render(){
        let filename = "assessment"+Date.now()+".csv";
        let filename1 = "apps"+Date.now()+".csv";
        let filename_per = "apps_percentage"+Date.now()+".csv";
        // console.log(this.state.toggleAppExport);
        return(
            <AUX>
                <div className="row">
                    <div className="col-sm-12">
                        <div className="page-title-box">
                            <h4 className="page-title">Services</h4>
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                               
                            </ol>

                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-12">
                        <div className="card m-b-20">
                            <div className="card-body">
                                <h4 className="mt-0 header-title text-right">
                                <button
                                        title="Create a new profile"
                                        className="btn btn-primary mb-3" style={{btnStyle}} onClick={this.onClickButton}>Add Service</button>
                                        <ModalLayout isOpen={this.state.openModal} toggle={this.onCloseModal} >
                                            <AddService />
                                            </ModalLayout>
                                  &nbsp;
                                    <button
                                        title="Import responses or applications"
                                        className="btn btn-secondary mb-3"
                                        onClick={this.openModal}>Import
                                    </button>&nbsp;
                                    <button className="btn btn-success mb-3" >Export All</button>&nbsp;
                                    <button 
                                        title="Delete selected application"
                                        onClick={() => this.handleBulkDelete()}
                                        className="btn btn-danger mb-3">Delete
                                    </button>
                                  
                                    
                                </h4>

                                {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} /> : null}
                                {this.state.loading ? <AUX><Spinner /><br /></AUX> : null}
                                <div style={{ overflow: 'auto' }}>
                                    <MDBDataTable
                                        bordered
                                        hover
                                        data={{ columns: this.state.columns, rows: this.state.rows }}
                                        info={this.state.rows.length > 0 ? true : false}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {
                    this.state.toggle ? <ModalUpload modal={this.state.toggle} toggle={this.closeModal} /> : null
                }
                {
                    this.state.toggleProfile
                        ? <ModalAttachProfile modal={this.state.toggleProfile} toggle={this.HandleAttachProfileModal} appId={this.state.appId} appName={this.state.appName} />
                        : false
                }
                {
                    this.state.confirmModal
                        ? <ConfirmModal modal={this.state.confirmModal} toggle={this.closeBulkDeleteModal} click={() => this.handleAppDelete(answers)} content={"Do you really want to delete this app."} title={"Delete App"} />
                        : false
                }
                {
                    this.state.changeClient
                        ? <ChangeClient modal={this.state.changeClient} app_id={answers} toggle={this.closeChangeClient} />
                        : false
                }
                {
                    this.state.toggleAppUpdate
                        ? <ModalApp modal={this.state.toggleAppUpdate} appId={this.state.appId} toggle={this.closeAppUpdate} title="Edit App" />
                        : false
                }
                {
                    this.state.toggleAppExport
                    ? <ModalAppExport modal={this.state.toggleAppExport} exportCSV={this.exportCSV} exportCSVPer={this.exportCSVPer} exportCSVAdvance={this.exportCSVAdvance}  toggle={this.toggleExport} title="Export Apps"  />
                    : false
                }
            </AUX>
        );
    }
}

const mapStateToProps = state => {
    return {
        token: state.auth.token,
        name: state.auth.name,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setResponse: (id) => dispatch(actions.setResponse(id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Services);
