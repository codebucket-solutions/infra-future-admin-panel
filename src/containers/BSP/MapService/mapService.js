import React, { Component } from 'react';
import { btnStyle } from '../../../util';
import axios from '../../../axios-details';
import { connect } from 'react-redux';
import { data } from 'jquery';
import Spinner from '../../../components/Spinner/simpleSpinner';
import Alert from '../../../components/Alert/common.js';
import { Link } from 'react-router-dom';


class MapService extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            error: null,
            message: null,
            description: [],
            data: [],
            apiData: [],
            tags:[],
        }
    }

    fetchData = () => {
        const config = {
            headers: { Authorization: `Bearer ${this.props.token}` }
        };
        axios.get('/graph_settings/basic', config).then(response => {
            console.log(response.data.data);
            this.setState({ apiData: response && response.data && response.data.data })
        }).catch(err => { console.log(err) })
    }


    fetchTags = () => {
        const config = {
            headers: { Authorization: `Bearer ${this.props.token}` }
        };
        axios.get('/questions/fetch/tags', config).then(response => {
            console.log(response.data.data)
            this.setState({ tags: response.data.data })
        }).catch(err => { console.log(err) })
    }


    componentDidMount() {
        this.fetchData();
        this.fetchTags();


    }

    submitGraphDescriptionData = (e) => {
        this.setState({ loading: true, error: null, message: null })
        window.scrollTo(0, 0)
        e.preventDefault();
        const data = {
            token: this.props.token,
            graphs: this.state.description
        };
        console.log(data)
        axios.post('/graph_settings/edit', data).then(res => {
            if (res.data.status) {
                this.setState({ loading: false, error: null, message: res.data.message })
            }
            else {
                this.setState({ loading: false, error: res.data.message, message: null })
            }
        }).catch(err => { this.setState({ loading: false, error: "Something went wrong. Please try again.", message: null }); console.log(err) })
    }

    handleChange =(e, index, attribute) =>{
        let _tempDescriptionArray = [...this.state.apiData];

        _tempDescriptionArray[index][attribute] = e.target.value;
        this.setState({ description: _tempDescriptionArray })
    }


    render() {
        console.log(this.state.description)
        return <>
        <div className="row">
            <div className="col-sm-12">
                <div className="page-title-box">
                    <h4 className="page-title">All Settings</h4>
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                        <li className="breadcrumb-item active">All Services</li>
                    </ol>

                </div>
            </div>
        </div>
        <div className="row">
            <div className="col-lg-12">
                <div className="card">
                    <div className="card-body">
                        <h6 className="page-title text-left">Map Services</h6>
                        {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} /> : null}
                        {this.state.message ? <Alert classes={"alert-success"} msg={this.state.message} /> : null}
                        {this.state.loading ? <><Spinner /><br /></> : null}
                        <br />
                        <form onSubmit={(e) => this.submitGraphDescriptionData(e)}>

                            {this.state.apiData && this.state.apiData.map((graph, index) => {
                                return <div key={index} className="row">
                                    <div className="col-sm-4 col-lg-4">
                                        <label>Service 1</label>
                                    </div>
                                    <div className="col-sm-4 col-lg-4 mb-3">  
                                        <select 
                                            className="form-control"
                                            value={graph.name}
                                            onChange={(e) => this.handleChange(e, index, "name")}
                                        >
                                            <option>A</option>
                                            <option>B</option>
                                            <option>C</option>
                                            <option>D</option>
                                        </select>
                                    </div>
                                </div>

                            })}
                            <div className="col-12 text-center">
                                <button style={{ btnStyle }} type="submit" className="btn btn-primary text-center">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </>
    }
}
const mapStateToProps = (state) => {
    return { token: state.auth.token }
}

export default connect(mapStateToProps)(MapService);