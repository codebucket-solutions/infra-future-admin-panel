import React, { Component } from 'react';
import makeAnimated from "react-select/animated";
import fileDownload from 'js-file-download';
import Alert from '../components/Alert/common';
import { Link } from 'react-router-dom';
import AUX from '../hoc/Aux_';
import MySelect from '../components/ReactSelect';
import { handleKeyValueClient, objectToArray, handleKeyValue } from '../shared/utility';

import axios from '../axios-details';
import { connect } from 'react-redux';
import AssessmentTable from '../components/AssessmentTable';
import * as actions from '../store/actions/index.js';
import Spinner from '../components/Spinner/simpleSpinner';
import AdvancedInsights from './advancedInsights';


import Donut from './Insight/Donut';
import LineGraph from './Insight/LineGraph';
import CountBox from './Insight/CountBox';
import ModalSelectInsight from '../components/Modal/modalSelectInsight';

import GroupBox from './Insight/GroupBox';
import SingleBoxEmpty from './Insight/singleBoxEmpty'
import { btnStyle } from '../util';
import { Picky } from 'react-picky';
import 'react-picky/dist/picky.css'; 


// import { CSVLink } from "react-csv";

let mRefs = []
const animatedComponents = makeAnimated();
class Insights extends Component {
    constructor(props) {
        super(props);
        mRefs = [];
        this.a1 = React.createRef();
        this.basic = React.createRef();
        this.state = {
            height: 300,
            width: 450,
            no_app: 0,
            completed: 0,
            profile: 0,
            loading: false,
            img: '',
            dataBasic: [],
            dataCount: [],
            toggle: false,
            suitability: [],
            allClients: [],
            selectedClients: [],
            json1: {},
            assessmentCount1: 0,
            json2: {},
            assessmentCount2: 0,

            allBasicFilter: [],
            allA1Filter: [],
            allA2Filter: [],
            selectedBasicFilter: [],
            selectedA1Filter: [],
            selectedA2Filter: [],

            notStarted: 0,
            buttonMessage1: "Refresh Assessment",
            buttonMessage2: "Refresh Assessment",
            fixedFiler: [
                'APPTYPE', 'APPARCHITECTURE', 'DBTECHNOLOGY', 'PROGRAMMINGLANG', 'BIZCRITICALITY', 'APPROADMAP', 'COMPLIANCEREG', 'APPCATEGORY', 'HWPLATFORM', 'OSWEBTIER', 'OSDBTIER', 'OSAPPTIER'
            ],
            alertClass: null,
            error: null,
            toggle: false,
            allApps:[],
            selectedApps:[],

            lineGraphSize: 6,
            selectedInsightTags: null,
            insightToggle: false,

            simpleUrl: null,
            exportTitle: "Export All",


        }
        this.print = React.createRef();
    }

    componentDidMount() {
        this.setState({ selectedBasicFilter: this.props.selectedBasicFilter, selectedA1Filter: this.props.selectedA1Filter })
        this.fetchClient();
        if (this.props.selectedClients.length > 0) {
            this.fetchAPI(this.props.selectedClients);
        }
        else {
            this.setState({ toggle: true });
        }
    }

    fetchClient = () => {
        // this.setState({ loading: true });
        const config = {
            headers: { Authorization: `Bearer ${this.props.token}` }
        };
        //console.log(config)
        axios.get('/user/userclient', config)
            .then(res => {
                //console.log(res.data)
                let clients = handleKeyValueClient(res.data.data.clients);
                //console.log(clients);
                this.setState({ allClients: clients })
            })
            .catch(err => {
                this.setState({ error: "Something went wrong." })
            });
    }

    selectTowChange = (selectedClients) => {
        if (selectedClients && selectedClients.length > 0) {
            this.fetchAPI(selectedClients);
        }
        else {
            this.setState({ dataBasic: [], no_app: 0, completed: 0, notStarted: 0, profile: 0, selectedFilter: [] })
        }

        this.props.updateAssessmentFilter(this.props.selectedBasicFilter, this.props.selectedA1Filter, this.props.assessmentA2Filter, selectedClients);
    }

    selectTowChangeFilter = (selectedFilter, type) => {
        if (type === "basic") {
            //console.log(selectedFilter);
            this.setState({ selectedBasicFilter: selectedFilter });
            this.props.updateAssessmentFilter(selectedFilter, this.state.selectedA1Filter, [], this.props.selectedClients);
        }
        else if (type === "a1") {
            //console.log(selectedFilter);
            this.setState({ selectedA1Filter: selectedFilter });
            this.props.updateAssessmentFilter(this.state.selectedBasicFilter, selectedFilter, [], this.props.selectedClients);
        }
        // else if(type === "a2"){
        //     //console.log(selectedFilter);
        //     this.setState({selectedA2Filter: selectedFilter});
        //     this.props.updateAssessmentFilter(this.props.selectedBasicFilter, this.props.selectedA1Filter,selectedFilter);
        // }  
    }


    fetchAPI(selectedClients) {
        this.setState({ loading: true, });
        const token = {
            token: this.props.token,
            clients: objectToArray(selectedClients),
        };
        console.log(token);
        axios.post('/insight', token)
            .then(res => {
                let data = res.data.data;
                console.log(res.data);
                if (res.data.status === true) {
                    let assessmentCount1, assessmentCount2;
                    let assessmentA1Filter = Object.keys(data.assessResult1);
                    // let assessmentA2Filter = Object.keys(data.assessResult2);
                    // console.limport Alert from '../components/'og(assessmentA1Filter);
                    //console.log(final_filter);
                    assessmentCount1 = Object.keys(data.assessResult1).length;
                    // assessmentCount2 = Object.keys(data.assessResult2).length;
                    //console.log(data.assessResult1);
                    //console.log(data.assessResult);

                    //allApps
                    let selectedApps = localStorage.getItem('adminSelectedApps');
                    console.log(selectedApps);
                    selectedApps = selectedApps ? selectedApps.split(',') : data.allApps.length > 5 ? data.allApps.slice(0, 5) : data.allApps

                    console.log(data.dataBasic)
                    this.setState({
                        loading: false,
                        dataBasic: data.dataBasic,
                        dataCount: data.countData,
                        json1: data.assessResult1,
                        json2: data.assessResult2,
                        assessmentCount1: assessmentCount1,
                        assessmentCount2: assessmentCount2,
                        allApps:data.allApps,
                        selectedApps:selectedApps,

                        allBasicFilter: handleKeyValue(this.state.fixedFiler),
                        // selectedBasicFilter: handleKeyValue(this.state.fixedFiler),
                        allA1Filter: handleKeyValue(assessmentA1Filter),
                        selectedA1Filter: (this.props.selectedA1Filter && this.props.selectedA1Filter.length > 0) ? this.props.selectedA1Filter : handleKeyValue(assessmentA1Filter),
                        // allA2Filter:handleKeyValue(assessmentA2Filter),
                        // selectedA2Filter:handleKeyValue(assessmentA2Filter),
                        toggle: true,
                    })
                    //console.log(data.data);

                }
                else{
                    this.setState({ loading: false })
                }
            })
            .catch(err => {
                this.setState({ loading: false })
                console.log(err)
            });
    }

    handleAssessmentAPI = () => {
        let assessmentType = 1;
        this.setState({ loadingMessage: true, ['buttonMessage' + assessmentType]: "Loading...", alertMessage: "Please wait. We are fetching assessment data.", error: null })
        const token = {
            token: this.props.token,
            clients: objectToArray(this.props.selectedClients),
        };
        //console.log(token);
        axios.post('/fetchAssessment/', token)
            .then(res => {
                //console.log(res.data);
                if (res.data.status) {
                    this.setState({ loadingMessage: false, ['buttonMessage' + assessmentType]: "Refresh Assessment", error: null, })
                    this.fetchAPI(this.props.selectedClients);
                }
                else {
                    this.setState({ loadingMessage: false, ['buttonMessage' + assessmentType]: "Refresh Assessment", error: res.data.message, })
                }

            })
            .catch(err => {
                this.setState({ loadingMessage: false, ['buttonMessage' + assessmentType]: "Refresh Assessment", error: "Something went wrong. Please try again" })
                //console.log(err)
            });
    }


    exportAll = () => {
        mRefs.map((v) => {
            v.exportExcel();
            return v;
        })
    }

    inputChangeHandler = (e) => {
        this.setState({ lineGraphSize: e.target.value })
    }

    handleSimpleExport = (type) => {
        const token = {
            token: this.props.token,
            clients: objectToArray(this.props.selectedClients),
        };
        if (type === 2) {
            this.setState({ exportTitle: "Please wait..." })
            //console.log(type)
        }
        let fileName = type === 1 ? 'simple' + Date.now() + '.xlsx' : 'advanced' + Date.now() + '.xlsx';

        //console.log(token);
        axios.post(type === 1 ? '/insight/export-simple' : '/insight/export-advance', token, {
            responseType: 'blob',
        })
            .then((response) => {
                fileDownload(response.data, fileName);
                this.setState({ exportTitle: "Export All" })
            })
            .catch(err => {

                console.log(err)
            });
    }

    // downloadExcel = (url) => {
    //     //console.log(url);
    //     window.open(url);
    // }

    // wild card search function
    handleKeyDown = (e, all, selected, myInput) => {
        let allFields = this.state[all];
        let selectedFields = this.state[selected];

        if (e.key === 'Enter') {
            let string = e.target.value.substring(0, e.target.value.length - 1);
            allFields.forEach(a => {
                //console.log(a);
                if (a.value.toLowerCase().includes(string.toLowerCase())) {
                    selectedFields.push(a);
                }
            })
            this.setState({ [selected]: selectedFields });
            //console.log(selectedFields);

            this[myInput].current.blur();
            this[myInput].current.focus();

            this.selectTowChangeFilter(selectedFields, myInput);

        }
    }

    // handleInsightModal = (data) => {
    //     this.setState(prev => ({
    //         selectedInsightTags: data, insightToggle: !prev.insightToggle
    //     }))
    //     if (data && data.length > 0) {
    //         this.handleAssessmentAPI(data);
    //     }
    // }

    handleApps = (value) =>{
        localStorage.setItem('adminSelectedApps', value);
        this.setState({selectedApps: value});
    }


    render() {
        const { assessmentCount1, assessmentCount2, json1, json2, toggle, lineGraphSize, selectedGroup1, allGroup1, dataCount } = this.state;
        let attributes = this.state.loading ? '' : {
            id: "tab_1",
            'data-toggle': "tab",
            'href': "#advance_1",
            role: "tab"
        };
        return (

            <AUX>
                <br />
                <div className="">

                </div>
                <div className="row">

                    <div className="col-md-12">
                        {
                            this.state.loadingMessage ? <Alert classes="alert-danger" msg={this.state.alertMessage} /> : null
                        }
                        {
                            this.state.error ? <Alert classes="alert-danger" msg={this.state.error} /> : null
                        }
                    </div>

                    <div className="col-sm-12 col-lg-1">
                        <div className="page-title-box">
                            <h4 className="page-title">Insights</h4>
                        </div>
                    </div>
                    <div className="col-sm-8 col-lg-7">
                        <div className="page-title-box">
                            <MySelect
                                options={this.state.allClients}
                                isMulti
                                noMin={true}
                                components={animatedComponents}
                                onChange={this.selectTowChange}
                                allowSelectAll={true}
                                value={this.props.selectedClients}

                            />
                        </div>
                    </div>
                    <div className="col-sm-4 col-lg-4">
                        <div className="page-title-box" style={{minHeight: 108,display: 'flex',alignItems: 'center',justifyContent: 'center'}}>
                            <Picky
                                id="picky"
                                options={this.state.allApps}
                                value={this.state.selectedApps}
                                multiple={true}
                                includeSelectAll={true}
                                includeFilter={true}
                                onChange={values => this.handleApps(values)}
                                dropdownHeight={600}
                            />
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-12">
                        <ul className="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                            <li className="nav-item" >
                                <a className="nav-link active" id="tab_1" data-toggle="tab" href="#basic" role="tab">Basic</a>
                            </li>
                            <li className="nav-item"  >
                                <a className="nav-link"  {...attributes} >Advanced</a>
                            </li>
                            {/* <li className="nav-item" >
                                <a className="nav-link"  id="tab_1" data-toggle="tab" href="#advance_2" role="tab">Advanced 2</a>
                            </li> */}
                        </ul>
                        <div className="tab-content">
                            <div className="tab-pane pt-3 active" id="basic" role="tabpanel" >
                                {
                                    dataCount.length > 0 ?
                                        <CountBox
                                            dataCount={dataCount}
                                        />
                                        :
                                        <>
                                            <div className="row">
                                                <SingleBoxEmpty header={"TOTAL APPS"} count={0} />
                                                <SingleBoxEmpty header={"TOTAL USER"} count={0} />
                                                <SingleBoxEmpty header={"NEW SURVEYS"} count={0} />
                                                <SingleBoxEmpty header={"TOTAL SURVEYS"} count={0} />
                                            </div>

                                        </>
                                }
                                {this.state.loading ? <Spinner /> : null}

                                {
                                    Object.keys(this.state.dataBasic).length > 0 ?

                                        (<>
                                            {/* <div className="row">
                                    <div className="col-md-12 text-right">
                                        <button className="btn btn-lg btn-primary" onClick={() => this.handleSimpleExport(1)}>{this.state.exportTitle}</button>
                                    </div>
                                </div> */}

                                            <div className="row" >
                                                <GroupBox dataBasic={this.state.dataBasic} dataAdvanced={json1} groupNumber={1} />
                                                <GroupBox dataBasic={this.state.dataBasic} dataAdvanced={json1} groupNumber={2} />
                                                <GroupBox 
                                                    selectedApps={this.state.selectedApps}
                                                    dataBasic={this.state.dataBasic} dataAdvanced={json1} groupNumber={3} />
                                                <GroupBox 
                                                    selectedApps={this.state.selectedApps}
                                                    dataBasic={this.state.dataBasic} dataAdvanced={json1} groupNumber={4} />
                                                <GroupBox 
                                                    selectedApps={this.state.selectedApps}
                                                    dataBasic={this.state.dataBasic} dataAdvanced={json1} groupNumber={5} />
                                                <GroupBox 
                                                    selectedApps={this.state.selectedApps}
                                                    dataBasic={this.state.dataBasic} dataAdvanced={json1} groupNumber={6} />
                                            </div>

                                        </>) : null}
                            </div>
                            <div className={!toggle ? "tab-pane pt-3 active" : "tab-pane pt-3"} id="advance_1" role="tabpanel" style={{ minHeight: 300 }} >
                                <div className="col-lg-12 mb-2 pl-0 pr-0">
                                    <MySelect
                                        options={this.state.allA1Filter}
                                        isMulti
                                        noMin={true}
                                        components={animatedComponents}
                                        onChange={(e) => this.selectTowChangeFilter(e, "a1")}
                                        allowSelectAll={true}
                                        value={this.props.selectedA1Filter}
                                        handleKeyDown={(e) => this.handleKeyDown(e, 'allA1Filter', 'selectedA1Filter', 'a1')}
                                        ref={this.a1}
                                    />
                                </div>
                                <br />
                                <div className="row ">
                                    <div className="col-md-12  col-sm-12 text-right">
                                        <button style={{ btnStyle }} type="button" title="Get new data from Assessment API" className="btn btn-primary btn-lg waves-effect" onClick={() => this.handleAssessmentAPI()}>{this.state.buttonMessage1}</button>
                                        &nbsp;&nbsp;
                                        <button style={{ btnStyle }} className="btn btn-lg btn-primary" onClick={() => this.handleSimpleExport(2)}>{this.state.exportTitle}</button>
                                    </div>
                                </div>
                                {/* {
                                    assessmentCount1 > 0 ? <AdvancedInsights json={json1} /> : null
                                } */}

                                <br />
                                {
                                    assessmentCount1 > 0 ? Object.keys(json1).map((q, i) => {
                                        //console.log(json1[q])
                                        return (this.props.selectedA1Filter.some(t => t.label === q) ? <AssessmentTable key={i} ref={(ref) => ref && mRefs.push(ref)} title={q} data={json1[q]} /> : null)
                                    }) : null
                                }
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                {
                    this.state.insightToggle
                        ? <ModalSelectInsight modal={this.state.insightToggle} toggle={this.handleInsightModal} />
                        : false

                }
            </AUX>
        );
    }
}

const mapStateToProps = state => {
    return {
        token: state.auth.token,
        selectedBasicFilter: state.assessment.assessmentBasicFilter,
        selectedA1Filter: state.assessment.assessmentA1Filter,
        selectedA2Filter: state.assessment.assessmentA2Filter,
        selectedClients: state.assessment.customerFilter
    }
}

const mapDispatchToProps = dispatch => {
    return {
        updateAssessmentFilter: (selectedBasicFilter, assessmentA1Filter, assessmentA2Filter, selectedClients) => dispatch(actions.assessmentFilter(selectedBasicFilter, assessmentA1Filter, assessmentA2Filter, selectedClients))
    }

}

export default connect(mapStateToProps, mapDispatchToProps)(Insights);
