import React, { Component } from 'react';
import { MDBDataTable } from 'mdbreact';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import axios from '../axios-details';
import AUX from '../hoc/Aux_';
import * as actions from '../store/actions/index';

import SubAdminModal from '../components/Modal/modalSubAdmin';
import Alert from '../components/Alert/common';
import Spinner from '../components/Spinner/simpleSpinner';
import ConfirmModal from '../components/Modal/modalConfirm.js';
import { btnStyle } from '../util';


class Profile extends Component {


    constructor(props) {
        super(props);
        this.state = {
            columns: [
                {
                    label: 'Sr.No.',
                    field: 'sro',
                    sort: 'asc',
                    width: 150
                },
                {
                    label: 'Admin Name',
                    field: 'aname',
                    sort: 'asc',
                    width: 270
                },
                {
                    label: 'Admin Type',
                    field: 'atype',
                    sort: 'asc',
                    width: 200
                },
                {
                    label: 'Username',
                    field: 'uname',
                    sort: 'asc',
                    width: 100
                },
                {
                    label: 'Action',
                    field: 'action',
                    sort: 'asc',
                    width: 100
                },
            ],
            rows: [],
            data: [],
            accessLevel: ["Super Admin", "Profile Admin", "Question Admin", "Super Admin"],
            toggle: false,
            loading: false,
            title: null,
            adminId: null,
            error: null,
            message: null,

            content: null,
            deactivateType: null,
        }
    }

    openModal = (adminId) => {
        let title = adminId ? 'Edit Admin' : 'Add Admin';
        this.setState(prevState => {
            return { toggle: !prevState.toggle, title: title, adminId: adminId ? adminId : null };
        })
    }

    handleUpdateModal = (adminId, type) => {
        let title = null;
        let content = null;
        if (type === '0') {
            title = 'Deactivate Admin';
            content = 'Do you want to Deactivate this admin.';
        }
        else if (type === '1') {
            title = 'Activate Admin';
            content = 'Do you want to Activate this admin.';
        }
        else if (type === '2') {
            title = 'Delete Admin';
            content = 'Do you want to delete this admin.';
        }
        this.setState(prevState => {
            return { confirmModal: !prevState.confirmModal, title: title, adminId: adminId, content: content, deactivateType: type };
        })
    }

    fetchAdmin = () => {
        this.setState({ loading: true });
        const config = {
            headers: { Authorization: `Bearer ${this.props.token}` }
        };
        //console.log(config)
        axios.get('/admin', config)
            .then(res => {
                let admins = [];
                let activateButton = '';
                //console.log(res.data)
                if (res.data.status) {
                    for (let key in res.data.data.admins) {
                        if (res.data.data.admins[key].active === '1') {
                            activateButton = (
                                <button
                                    title="Disable admin details"
                                    onClick={() => this.handleUpdateModal(res.data.data.admins[key].admin_id, '0')}
                                    className="btn btn-secondary">Disable</button>
                            );
                        }
                        else {
                            activateButton = (
                                <button
                                    title="Disable admin details"
                                    onClick={() => this.handleUpdateModal(res.data.data.admins[key].admin_id, '1')}
                                    className="btn btn-primary">Enable</button>
                            );
                        }
                        admins.push({
                            sro: parseInt(key) + 1,
                            aname: res.data.data.admins[key].name,
                            atype: this.state.accessLevel[res.data.data.admins[key].access_level],
                            uname: res.data.data.admins[key].username,
                            action:
                                <>
                                    <button
                                        title="Edit admin details"
                                        onClick={() => this.openModal(res.data.data.admins[key].admin_id)}
                                        className="btn btn-success">Edit</button> &nbsp;
                                    {activateButton}&nbsp;
                                    <button
                                        title="Delete admin details"
                                        onClick={() => this.handleUpdateModal(res.data.data.admins[key].admin_id, '2')}
                                        className="btn btn-danger">Delete</button>
                                </>

                        })
                    }
                }
                this.setState({ loading: false, rows: admins })
            })
            .catch(err => {
                //console.log("sdsdccdsc ");
                this.setState({ loading: false, error: "Something went wrong." })
            });
    }

    componentDidMount() {
        this.fetchAdmin();
    }


    closeModal = (update) => {
        if (update) {
            this.fetchAdmin();
        }
        this.openModal();
    }

    handleDisable = () => {
        this.setState({ loading: true });
        let token = {
            token: this.props.token,
            admin_id: this.state.adminId,
        };
        //console.log(token);
        console.log(this.state.deactivateType)
        if (parseInt(this.state.deactivateType) !== 2) {
            token = {
                ...token,
                active: this.state.deactivateType,
            }
            axios.post("/admin/disable", token)
                .then(res => {
                    console.log(res.data);
                    if (res.data.status) {
                        this.fetchAdmin();
                    }
                    else {
                        this.setState({ loading: false, error: res.data.message });
                    }
                    this.handleUpdateModal(null, null);
                })
                .catch(error => {
                    this.setState({ loading: false, error: "Something went wrong. Please try again." });
                    this.handleUpdateModal(null, null);
                });
        }
        else {
            axios.post("/admin/delete", token)
                .then(res => {
                    console.log(res.data);
                    if (res.data.status) {
                        this.fetchAdmin();
                        this.setState({ loading: false });
                    }
                    else {
                        this.setState({ loading: false, error: res.data.message });
                    }
                    this.handleUpdateModal(null, null);
                })
                .catch(error => {
                    this.setState({ loading: false, error: "Something went wrong. Please try again." });
                    this.handleUpdateModal(null, null);
                });
        }
    }

    render() {
        return (
            <AUX>
                <div className="row">
                    <div className="col-sm-12">
                        <div className="page-title-box">
                            <h4 className="page-title">Admins & Roles</h4>
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                                <li className="breadcrumb-item active">Admins & Roles</li>
                            </ol>
                            {/* <Tinycharts /> */}
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-12">
                        <div className="card m-b-20">
                            <div className="card-body">
                                <h4 className="mt-0 header-title text-right">
                                    <button
                                        style={{btnStyle}}
                                        title="Add new user and assign roles"
                                        className="btn btn-primary mb-3" onClick={() => this.openModal()}>Add Admins & Roles</button>
                                </h4>
                                {this.state.loading ? <AUX><Spinner /><br /></AUX> : null}
                                {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} /> : null}
                                <div style={{ overflow: 'auto' }}>
                                    <MDBDataTable
                                        bordered
                                        hover
                                        data={{ columns: this.state.columns, rows: this.state.rows }}
                                        info={this.state.rows.length > 0 ? true : false}
                                    />
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                {
                    this.state.toggle
                        ? <SubAdminModal modal={this.state.toggle} toggle={this.closeModal} title={this.state.title} adminId={this.state.adminId} />
                        : false
                }
                {
                    this.state.confirmModal
                        ? <ConfirmModal modal={this.state.confirmModal} toggle={this.handleUpdateModal} click={this.handleDisable} content={this.state.content} title={this.state.title} />
                        : false
                }
            </AUX>
        );
    }
}

const mapStatetoProps = state => {
    return {
        token: state.auth.token,
    };
}

const mapDispatchToProps = dispatch => {
    return {
        doMapping: (mappingId) => dispatch(actions.doMapping(mappingId)),
    }
}

export default connect(mapStatetoProps, mapDispatchToProps)(Profile);