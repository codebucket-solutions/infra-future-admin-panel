
import React, { useState, useEffect } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import SelectParameterModal from './SelectParameterModal/SelectParametersModal';
import { Link } from 'react-router-dom';
import { MDBDataTable } from 'mdbreact';
import axios from '../../axios-details';
import { connect } from 'react-redux';
import MySelect from '../../components/ReactSelect/index';
import { data } from 'jquery';
import '../../App.css';
import { btnStyle } from '../../util';
import ConfirmModal from '../../components/Modal/modalConfirm.js';

import { objectToArray, handleKeyValueApps } from '../../shared/utility'
import { Progress } from 'reactstrap';


const convertArrayToKeyValue = (arrayToBeConverted) => {
    let _tempObject = [];
    for (let key in arrayToBeConverted) {
        _tempObject.push({
            value: arrayToBeConverted[key].app_id,
            label: arrayToBeConverted[key].tags
        })
    }
    return _tempObject;
}
const convertArrayToKey = (arrayToBeConverted) => {
    let _tempObject = [];
    for (let key in arrayToBeConverted) {
        _tempObject.push({
            value: arrayToBeConverted[key],
            label: arrayToBeConverted[key]
        })
    }
    return _tempObject;
}


class PresignedURL extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedUser: [],
            selectedUserInput:[],
            selectedDomain: [],
            selectedApplication: [],
            allApplicationList: [],
            newDiv: [''],
            loading: false,
            message: null,
            error: null,
            userList: [],
            domainList: [],
            data: {
                user: [],
                app: [],
                domain: []
            },
            confirmModal:false,
            isModalOpen: false,
            urlId:null,
            column: [

                {
                    label: 'Sr. No.',
                    field: 'sno',
                    sort: 'asc',
                    width: 270
                },
                {
                    label: 'User Name',
                    field: 'uname',
                    sort: 'asc',
                    width: 200
                },
                {
                    label: 'Apps',
                    field: 'apps',
                    sort: 'asc',
                    width: 100
                },
                {
                    label: 'Progress',
                    field: 'progress',
                    sort: 'asc',
                    width: 100
                },
                {
                    label: 'Domain',
                    field: 'domain',
                    sort: 'asc',
                    width: 100
                },
                {
                    label: 'Action',
                    field: 'action',
                    sort: 'asc',
                    width: 100
                },
            ],
            rows: []
        }

    }

    fetchAPI = () => {
        this.setState({ loading: true, message: null, error: null });
        const config = {
            headers: { Authorization: `Bearer ${this.props.token}` }
        };

        axios.get('/user/userclient', config)
            
            .then(response => {
                // console.log(response)
                this.setState({ userList: response && response.data && response.data.data && response.data.data.clients, loading: false })
            }).catch(err => {
            });
        
        axios.get('/questions/filter/get', config).then((response) => {
            this.setState({ domainList: convertArrayToKey(response && response.data && response.data.data && response.data.data.domains) });
        }).catch();

        
    }

    fetchApps = (user_id, index) => {
        const config = {
            headers: { Authorization: `Bearer ${this.props.token}` }
        };
        //console.log(user_id)
        axios.get('/apps/apps/' + user_id, config)
        .then(res => {
            // console.log(res.data);
            let allApps = handleKeyValueApps(res.data.data.apps);
            // console.log(allApps);
            let prevApp = [...this.state.allApplicationList];
            prevApp[index] = allApps;
            // console.log(prevApp)
            this.setState({ allApplicationList: prevApp })
        })
        .catch(err => {
    
        });
    }

    fetchAllURL() {
        const config = {
            headers: { Authorization: `Bearer ${this.props.token}` }
        };
        axios.get('/presigned_url', config)
        .then((res) => {
            if(res.data.status){
                let { data } = res.data;
                let _row = [];
                console.log(data);
                data.forEach((d,i) =>{
                    
                    _row.push({
                        sno: i+1,
                        uname: d.f_name ? d.f_name + ' ' + (d.l_name ? d.l_name : '') : d.user_id,
                        apps: d.app_name,
                        progress: <><Progress color={d.percent === 100 ? 'success' : 'danger'} value={d.percent} /><label>{d.percent}%</label></>,
                        domain: d.app_domain,
                        action:
                            <button 
                                title="Delete selected presigned URL"
                                onClick={() => this.handleDeleteModal(d.id, false)}
                                className="btn btn-danger mb-3">Delete
                            </button>
                    })
                })

                this.setState({rows: _row});
            }
            // console.log(res);
            
        }).catch(e =>{
            console.log(e);
        });
    }
    componentDidMount() {
        this.fetchAPI();
        this.fetchAllURL();
    }

    inputChangeHandler = (event, name, index) => {
        
        let _tempList = [...this.state.selectedUser]
        let _tempInputUser = [...this.state.selectedUserInput]

        if(name === "UsersInput"){
            _tempInputUser[index] = event.target.value;
        }
        else{
            if(event.target.value === "Other"){
                _tempInputUser[index] = '';
            }
            else{
                _tempInputUser[index] = event.target.value;
            }
            
            _tempList[index] = event.target.value;
        }
       
        this.setState({ selectedUser: _tempList,selectedUserInput: _tempInputUser })
        this.fetchApps(event.target.value === "Other"  ? 0 : event.target.value, index)

    }

    onSelectionChange = (selectedValue, index, type) => {
        if (type === "Application") {
            let _tempAppList = [];
            _tempAppList = [...this.state.selectedApplication];
            _tempAppList[index] = selectedValue;
            this.setState({ selectedApplication: _tempAppList });
        } else if (type === "Domain") {
            let _tempDomainList = [...this.state.selectedDomain];
            _tempDomainList[index] = selectedValue;
            this.setState({ selectedDomain: _tempDomainList });
        }
    }
    addSelectDiv = () => {
        let _tempDivArray = [...this.state.newDiv];
        _tempDivArray.push('');
        this.setState(this.state.newDiv = _tempDivArray);
    }

    removeSelectDiv = (index) => {

        let _tempDivArray = [...this.state.newDiv];
        _tempDivArray.splice(index, 1);
        this.setState({ newDiv: _tempDivArray });

        let _tempUserArray = [...this.state.selectedUser];
        _tempUserArray.splice(index, 1);
        this.setState({ selectedUser: _tempUserArray });

        let _tempDomainList = [...this.state.selectedDomain];
        _tempDomainList.splice(index, 1);
        this.setState({ selectedDomain: _tempDomainList });

        let _tempAppList = [...this.state.selectedApplication];
        _tempAppList.splice(index, 1);
        this.setState({ selectedApplication: _tempAppList });

    }

    openModal = () => {
        this.setState({ isModalOpen: true });
    }

    onSubmitButtonClicked = (e) => {
        e.preventDefault();
        let _tempApp = [];
        let _tempDomainList = [];
        this.state.selectedApplication.forEach((s) => _tempApp.push(objectToArray(s)));
        this.state.selectedDomain.forEach((s) => _tempDomainList.push(objectToArray(s)));
        // console.log(this.state.selectedUser, _tempDomainList, _tempApp);

        let data = {
            token: this.props.token,
            user: this.state.selectedUserInput,
            apps: _tempApp,
            domain: _tempDomainList,
            host: process.env.REACT_APP_CLIENT_URL
        }
        console.log(data);
        axios.post('/presigned_url/add', data).then(res => {
            if(res.data.status) {
                console.log(res.data)
                // this.setState({ allApplicationList: convertArrayToKeyValue(response && response.data && response.data.data && response.data.data.apps) });
                this.setState({isModalOpen: false, selectedApplication:[], selectedDomain:[], selectedUser:[], selectedUserInput:[]})
                this.fetchAllURL();
            }
            
        }).catch((err) => {

        })
    }

    handleDeleteModal = (urlId, status) =>{
        console.log(urlId, this.state.confirmModal)
        if(status){
            this.handleDelete(this.state.urlId);
        }
        this.setState({ urlId: urlId, confirmModal: !this.state.confirmModal})

    }

    handleDelete = (urlId) => {
        // console.log(this.state);
        this.setState({ loading: true, message: null, error: null });
        const data = {
            token: this.props.token,
            url_id:urlId,
        }
        console.log(data);
        axios.post('/presigned_url/delete_data_by_id', data)
            .then(res => {
                console.log(res.data);
                if (res.data.status) {
                    this.setState({ loading: false, appId: null, message: res.data.message })
                }
                else {
                    this.setState({ loading: false, appId: null, error: res.data.message })
                }
                this.fetchAPI();
                this.fetchAllURL();
            })
            .catch(err => {
                console.log(err);
                this.setState({ loading: false, appId: null, error: "Something went wrong. Please try again later." });

            })
    }


    toggle = () => this.setState({ isModalOpen: !this.state.isModalOpen })
    
    render() {
        // console.log(this.state.userList);
        return <>
            <div className="row">
                <div className="col-sm-12">
                    <div className="page-title-box">
                        <h4 className="page-title">Presigned URL</h4>
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                            <li className="breadcrumb-item active">Presigned URL</li>
                        </ol>

                    </div>
                </div>
            </div>

            <div className="row">
                <div className="col-lg-12">
                    <div className="card m-b-20">
                        <div className="card-body">
                            <h4 className="mt-0 header-title text-right">
                                <button
                                    style={{btnStyle}}
                                    title="Add a new question"
                                    className=" btn btn-primary mb-3 "
                                    onClick={this.openModal}
                                >Create URL</button>&nbsp;
                        </h4>
                            <div style={{ overflow: 'auto' }}>
                                <MDBDataTable
                                    bordered
                                    hover
                                    data={{ columns: this.state.column, rows: this.state.rows }}
                                    info={this.state.rows.length > 0 ? true : false}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div>
                <Modal size="xl" isOpen={this.state.isModalOpen} >
                    <ModalHeader toggle={this.toggle}>Select Parameters </ModalHeader>
                    <ModalBody >

                        <div style={{
                            overflowY: "auto",
                            overflowX: "hidden",
                            height: "70vh"
                        }}>
                            {/* <SelectParameterModal onDataSubmit={onDataSubmitted} /> */}
                            <form onSubmit={(e) => this.onSubmitButtonClicked(e)}>
                                <div className="row">
                                    {this.state.newDiv.map((div, index) => {
                                        return <div key={index} className="col-12 select-container">

                                            <div className="col-11">

                                                <div className="select-element-container">
                                                    <div className="col-3 users">
                                                        { this.state.selectedUser[index] && this.state.selectedUser[index] === "Other"  ? 
                                                        <div className="form-group">
                                                            <label htmlFor="Users">Users</label>
                                                            <input
                                                                style={{minHeight:65}}
                                                                placeholder="Enter Email ID"
                                                                required
                                                                type="email"
                                                                className="form-control"
                                                                onChange={(event) => this.inputChangeHandler(event, "UsersInput", index)}
                                                                value={this.state.selectedUserInput[index]}
                                                            />
                                                        </div>
                                                        :
                                                        <div className="form-group">
                                                            <label htmlFor="Users">Users</label>
                                                            <select
                                                                style={{minHeight:65}}
                                                                required
                                                                className="form-control"
                                                                onChange={(event) => this.inputChangeHandler(event, "Users", index)}
                                                                value={this.state.selectedUser[index]}>
                                                                <option>Select</option>
                                                                {this.state.userList.map((user, index) => {
                                                                    return <option key={index} value={user.clnt_id}>{`${user.f_name} ${user.l_name ? user.l_name :''}`}</option>
                                                                })}
                                                                <option value="Other">Other</option>
                                                            </select>
                                                        </div> 
                                                        
                                                        }
                                                    </div>
                                                    <div className="col-5">
                                                        <label>Application</label>
                                                        <MySelect
                                                            required
                                                            value={this.state.selectedApplication[index]}
                                                            onChange={(e) => this.onSelectionChange(e, index, "Application")}
                                                            options={this.state.allApplicationList[index] ? this.state.allApplicationList[index] : []}
                                                            allowSelectAll={true}
                                                            isMulti={true}
                                                            noMin={false}
                                                        />
                                                    </div>
                                                    <div className="col-4">
                                                        <label>Domain</label>
                                                        <MySelect
                                                            required
                                                            value={this.state.selectedDomain[index]}
                                                            onChange={(e) => this.onSelectionChange(e, index, "Domain")}
                                                            options={this.state.domainList}
                                                            allowSelectAll={true}
                                                            isMulti={true}
                                                            noMin={false}
                                                        />
                                                    </div>

                                                </div>
                                            </div>
                                            <div className="col-1 mt-4">
                                                <button className="btn btn-md btn-danger waves-effect waves-light" onClick={(event) => this.removeSelectDiv(index)}>X</button>
                                            </div>
                                        </div>
                                    })}
                                    <div className="col-12 text-right">
                                        <Button color="success" onClick={this.addSelectDiv} >Add</Button>{' '}
                                    </div>
                                    <div className="col-12 text-center">
                                        <br />
                                    </div>
                                    <div className="col-12 text-center">
                                        <Button type="submit" color="primary" >Submit</Button>{' '}
                                    </div>
                                </div>
                            </form>
                        </div>

                    </ModalBody>
                    {/* <ModalFooter>
                        <div className="row text-center">
                            <div className="col-6">
                                <Button type="submit" color="primary" >Submit</Button>{' '}
                            </div>
                        </div>
                    </ModalFooter> */}
                </Modal>
            </div>
            {
                    this.state.confirmModal
                    ? <ConfirmModal modal={this.state.confirmModal} toggle={() =>this.handleDeleteModal(null,false)} click={() => this.handleDeleteModal(null, true)} content={"Do you really want to delete this Presigned URL."} title={"Delete Presigned URL"} />
                    : false
                }
        </>
    }
}
const mapStateToProps = (state) => {
    return { token: state.auth.token }

}
export default connect(mapStateToProps)(PresignedURL);