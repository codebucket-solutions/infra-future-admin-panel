import React, { Component } from 'react';
import AUX from '../../../hoc/Aux_';
import makeAnimated from "react-select/animated";
import { connect } from 'react-redux';

import { MDBDataTable } from 'mdbreact';
import { CSVLink } from "react-csv";
import { Link } from 'react-router-dom';
import { Progress } from 'reactstrap';
import axios from '../../../axios-details';
import MySelect from '../../../components/ReactSelect';
import { handleKeyValue, objectToArray } from '../../../shared/utility';

import '../../response.css';
import SimpleSpinner from '../../../components/Spinner/simpleSpinner';
import ModalComment from '../../../components/Modal/modalComment';

const header = ['App Name', 'Question Tag', 'Question', 'Client Response', 'Comment'];
const animatedComponents = makeAnimated();

class UrlLandingPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            columns: [
                {
                    label: 'Questions',
                    field: 'question',
                    sort: 'asc',
                    width: 270
                },
                {
                    label: 'Description',
                    field: 'description',
                    sort: 'asc',
                    width: 200
                },
                {
                    label: 'Responses',
                    field: 'option',
                    sort: 'asc',
                    width: 100
                },
                {
                    label: 'Comment',
                    field: 'comment',
                    sort: 'asc',
                    width: 100
                },
            ],
            rows: [],
            appName: '',
            answered: '',
            ansCount: 0,
            initialAnsCount: 0,
            totalCount: 0,
            questions: [],
            allCriteria: [],
            selectedCriteria: [],
            allLevels: [],
            selectedLevel: [],
            allDomains: [],
            selectedDomains: [],
            allTopics: [],
            selectedTopics: [],
            loading: false,
            message: null,
            error: null,
            checkRedirect: false,
            csvData: [],
            modalComment: false,
            modalQuestID: null,
            answerType: 0,
            countUnanswered: 0,
            percent: 0,
        }
    }

    fetchAPI() {
        this.setState({ loading: true, message: null, error: null });
        //console.log('/assessment/'+this.props.assessment);
        const config = {
            headers: { Authorization: `Bearer ${this.props.token}` }
        };
        //console.log(this.props.responseId);
        axios.get('/apps/' + this.props.responseId, config)
            .then(response => {
                //console.log(response.data)
                let { data: res } = response
                console.log(response.data);
                if (res.status === true) {
                    let criteria = handleKeyValue(res.data.criteria);
                    let levels = handleKeyValue(res.data.levels);
                    let domains = handleKeyValue(res.data.domains);
                    let topics = handleKeyValue(res.data.topics);
                    //console.log(topics);

                    // get filter from localStorage
                    const local_domain = JSON.parse(localStorage.getItem('admin_response_domain_filter'));
                    const local_criteria = JSON.parse(localStorage.getItem('admin_response_criteria_filter'));
                    const local_level = JSON.parse(localStorage.getItem('admin_response_level_filter'));
                    const local_topic = JSON.parse(localStorage.getItem('admin_response_topic_filter'));
                    //console.log(local_level);
                    //create table
                    this.createTable(
                        res.data.data_obj,
                        local_criteria && local_criteria.length > 0 ? local_criteria : criteria,
                        local_level && local_level.length > 0 ? local_level : levels,
                        local_domain && local_domain.length > 0 ? local_domain : domains,
                        local_topic && local_topic.length > 0 ? local_topic : topics,
                        this.state.answerType
                    );


                    this.setState({
                        appName: res.data.app_name,
                        answered: res.data.answered,
                        ansCount: res.data.ans_count,
                        initialAnsCount: res.data.ans_count,
                        totalCount: res.data.total_qs,
                        questions: res.data.data_obj,
                        percent: res.data.percent,

                        selectedCriteria: (this.state.selectedCriteria.length > 0 ? this.state.selectedCriteria : (local_criteria && local_criteria.length > 0 ? local_criteria : criteria)),
                        allCriteria: criteria,

                        selectedLevel: (this.state.selectedLevel.length > 0 ? this.state.selectedLevel : (local_level && local_level.length > 0 ? local_level : levels)),
                        allLevels: levels,

                        selectedDomains: (this.state.selectedDomains.length > 0 ? this.state.selectedDomains : (local_domain && local_domain.length > 0 ? local_domain : domains)),
                        allDomains: domains,

                        allTopics: topics,
                        selectedTopics: (this.state.selectedTopics.length > 0 ? this.state.selectedTopics : (local_topic && local_topic.length > 0 ? local_topic : topics)),

                        loading: false,
                    })
                    this.exportCSV(res.data.data_obj);
                }
                else {
                    this.setState({ loading: false, error: response.data.message });
                }

            })
            .catch(err => {
                //console.log(err);
                this.setState({ loading: false, error: "Something went wrong. Please try again." });
            });
    }

    createTable = (data_obj, criteria, levels, domains, topics, answerType) => {

        //console.log(levels)
        //set filter data to local storage
        localStorage.setItem('admin_response_domain_filter', JSON.stringify(domains));
        localStorage.setItem('admin_response_criteria_filter', JSON.stringify(criteria));
        localStorage.setItem('admin_response_level_filter', JSON.stringify(levels));
        localStorage.setItem('admin_response_topic_filter', JSON.stringify(topics));

        criteria = objectToArray(criteria);
        levels = objectToArray(levels);
        domains = objectToArray(domains);
        topics = objectToArray(topics);
        let fetchQuestions = [];

        //console.log(data_obj);
        for (let key in data_obj) {

            if ((criteria && criteria.includes(data_obj[key].ahp_criteria_lev_1)) && (levels && levels.includes(data_obj[key].levels)) && (domains && domains.includes(data_obj[key].category)) && (topics && topics.some(r => data_obj[key].topics.indexOf(r) >= 0))) {
                //.log(this.state.answerType)
                let color = "btn-primary";
                if (data_obj[key].comment && data_obj[key].comment.length > 0) {
                    color = "btn-success"
                }
                if ((parseInt(answerType) === 1)) {
                    if ((data_obj[key].response_type === 'mcq-single') && (!data_obj[key].answer)) {
                        fetchQuestions.push({
                            question: data_obj[key].question,
                            description: data_obj[key].description,
                            option: this.getFetchData(data_obj[key].response_json, key, data_obj[key].answer, data_obj[key].response_type, data_obj[key].quest_id),
                            comment: data_obj[key].allow_comments === '1' ? <button type="button" className={"btn btn-sm " + color} onClick={() => this.handleModalComment(data_obj[key].quest_id)}>Comment</button> : null,
                        })
                    }
                    else if ((data_obj[key].response_type === 'mcq-multiple')) {
                        if ((data_obj[key].answer && data_obj[key].answer.length === data_obj[key].response_json.length)) {

                        }
                        else {
                            //console.log(this.state.answerType);
                            fetchQuestions.push({
                                question: data_obj[key].question,
                                description: data_obj[key].description,
                                option: this.getFetchData(data_obj[key].response_json, key, data_obj[key].answer, data_obj[key].response_type, data_obj[key].quest_id),
                                comment: data_obj[key].allow_comments === '1' ? <button type="button" className={"btn btn-sm " + color} onClick={() => this.handleModalComment(data_obj[key].quest_id)}>Comment</button> : null,
                            })
                        }

                    }
                }
                else if (parseInt(answerType) === 0) {
                    fetchQuestions.push({
                        question: data_obj[key].question,
                        description: data_obj[key].description,
                        option: this.getFetchData(data_obj[key].response_json, key, data_obj[key].answer, data_obj[key].response_type, data_obj[key].quest_id),
                        comment: data_obj[key].allow_comments === '1' ? <button type="button" className={"btn btn-sm " + color} onClick={() => this.handleModalComment(data_obj[key].quest_id)}>Comment</button> : null,
                    })
                }
            }
        }
        this.setState({ rows: fetchQuestions });
    }

    getFetchData = (answer_key, unique, answers, response_type, quest_id) => {
        let content = [];
        let val;
        //array = array.JSON.parse();

        if (response_type === 'mcq-single') {
            //console.log(answer_key);
            answer_key.map((data, index) => {
                val = data.position
                // console.log(data.position);
                return content.push(
                    <label key={index} className="container">{data.choice}
                        <input
                            type="radio"
                            disabled
                            value={val}
                            name={quest_id}
                            checked={answers ? (answers.includes(val.toString()) ? true : false) : false} />
                        <span className="radiomark"></span>
                    </label>);
            })

        }
        else if (response_type === 'mcq-multiple') {
            answer_key.map((data, index) => {
                val = data.position
                return content.push(
                    <label key={index} className="container">{data.choice}
                        <input
                            type="checkbox"
                            value={val}
                            disabled
                            name={quest_id}
                            checked={answers ? answers.includes(val.toString()) ? true : false : false}
                        />
                        <span className="checkmark"></span>
                    </label>);
            })
        }

        return content;
    }

    exportCSV = (questions) => {
        let answer_key, answers, val;
        let { csvData } = this.state;
        csvData = [];
        csvData.push(header);


        questions.map((quest, index) => {
            answer_key = quest.response_json;
            answers = quest.answer;
            let arr = [];
            answer_key.map((key, index) => {
                val = key.position;
                if (answers && answers.includes(val.toString())) {
                    arr.push(key.choice)
                }
                return key;
            })
            csvData.push([this.state.appName, quest.quest_tag, quest.question.replace(/"/g, '""'), arr ? arr.join("|") : '', quest.comment]);

            return quest;
        })
        this.setState({ csvData })
    }

    updateProgress = (count) => {
        let { ansCount } = this.state;
        this.setState({ ansCount: ansCount + count })
    }

    componentDidMount() {
        this.fetchAPI();
    }

    selectTowChangeCriteria = (selectedCriteria) => {
        this.setState({ selectedCriteria: selectedCriteria ? selectedCriteria : [] });
        this.createTable(this.state.questions, selectedCriteria ? selectedCriteria : [], this.state.selectedLevel, this.state.selectedDomains, this.state.selectedTopics, this.state.answerType);
    }

    selectTowChangeDomains = (selectedDomains) => {
        this.setState({ selectedDomains: selectedDomains ? selectedDomains : [] });
        this.createTable(this.state.questions, this.state.selectedCriteria, this.state.selectedLevel, selectedDomains ? selectedDomains : [], this.state.selectedTopics, this.state.answerType);
    }

    selectTowChangeLevel = (selectedLevel) => {
        this.setState({ selectedLevel: selectedLevel ? selectedLevel : [] });
        this.createTable(this.state.questions, this.state.selectedCriteria, selectedLevel ? selectedLevel : [], this.state.selectedDomains, this.state.selectedTopics, this.state.answerType);

    }

    selectTowChangeTopics = (selectedTopics) => {
        this.setState({ selectedTopics: selectedTopics ? selectedTopics : [] });
        this.createTable(this.state.questions, this.state.selectedCriteria, this.state.selectedLevel, this.state.selectedDomains, selectedTopics ? selectedTopics : [], this.state.answerType);

    }
    handleFilter = (event, type) => {
        if (type === "level") {
            let { selectedLevel } = this.state;
            //console.log(selectedLevel);
            selectedLevel = [];
            selectedLevel.push(event.target.value);
            this.setState({ selectedLevel })
        }
    }

    handleModalComment = (quest_id) => {
        //console.log(quest_id);
        this.setState(prev => {
            return { modalComment: !prev.modalComment, modalQuestID: quest_id ? quest_id : null }
        })
    }

    inputChangeHandler = (e) => {
        this.setState({ answerType: e.target.value })
        this.createTable(this.state.questions, this.state.selectedCriteria, this.state.selectedLevel, this.state.selectedDomains, this.state.selectedTopics, e.target.value);
    }

    // wild card search function
    handleKeyDown = (e, all, selected, myInput) => {
        let allFields = this.state[all];
        let selectedFields = this.state[selected];

        if (e.key === 'Enter') {
            let string = e.target.value.substring(0, e.target.value.length - 1);
            allFields.map(a => {
                if (a.value.toLowerCase().includes(string.toLowerCase())) {
                    selectedFields.push(a);
                }
                return a;
            })
            this.setState({ [selected]: selectedFields });


            this[myInput].current.blur();
            this[myInput].current.focus();


            this.createTable(this.state.questions, this.state.selectedCriteria, this.state.selectedLevel, this.state.selectedDomains, this.state.selectedTopics, this.state.answerType);
        }
    }

    render() {
        //console.log(this.state.selectedLevel);
        const { modalComment, modalQuestID } = this.state;
        let filename = "response" + Date.now() + ".csv";
        let progressBar = this.state.percent;

        return (
            <AUX>
                <div className="row">
                    <div className="col-sm-8 col-lg-8">
                        <div className="page-title-box">
                            <h4 className="page-title">Responses</h4>
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                                <li className="breadcrumb-item"><Link to="/apps">Apps</Link></li>
                                <li className="breadcrumb-item active">Responses</li>
                            </ol>

                        </div>
                    </div>
                    <div className="col-sm-4 col-lg-4">
                        <div className="page-title-box">
                            <select
                                className="form-control"
                                onChange={(event) => this.inputChangeHandler(event)}
                                value={this.state.answerType}>
                                <option value="0">All </option>
                                <option value="1">Un-answered</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-sm-12 col-lg-4">
                        <div className="page-title-box">
                            <label>Domains</label>
                            <MySelect
                                isDisabled={true}
                                value={this.state.selectedDomains}
                                onChange={this.selectTowChangeDomains}
                                options={this.state.allDomains}
                                components={animatedComponents}
                                allowSelectAll={true}
                                isMulti={true}


                                handleKeyDown={(e) => this.handleKeyDown(e, 'allDomains', 'selectedDomains', 'domain')}
                                ref={this.domain}
                            />
                        </div>
                    </div>
                    <div className="col-sm-12 col-lg-4">
                        <div className="page-title-box">
                            <label>Level</label>
                            <MySelect
                                value={this.state.selectedLevel}
                                onChange={this.selectTowChangeLevel}
                                options={this.state.allLevels}
                                components={animatedComponents}
                                allowSelectAll={true}
                                isMulti={true}

                                handleKeyDown={(e) => this.handleKeyDown(e, 'allLevels', 'selectedLevel', 'level')}
                                ref={this.level}
                            />
                        </div>
                    </div>
                    <div className="col-sm-12 col-lg-4">
                        <div className="page-title-box">
                            <label>Criteria</label>
                            <MySelect
                                value={this.state.selectedCriteria}
                                onChange={this.selectTowChangeCriteria}
                                options={this.state.allCriteria}
                                components={animatedComponents}
                                allowSelectAll={true}
                                isMulti={true}

                                handleKeyDown={(e) => this.handleKeyDown(e, 'allCriteria', 'selectedCriteria', 'criteria')}
                                ref={this.criteria}
                            />
                        </div>
                    </div>
                    <div className="col-sm-12 col-lg-12">
                        <div className="page-title-box">
                            <label>Topics</label>
                            <MySelect
                                value={this.state.selectedTopics}
                                components={animatedComponents}
                                onChange={this.selectTowChangeTopics}
                                options={this.state.allTopics}
                                allowSelectAll={true}
                                isMulti={true}

                                handleKeyDown={(e) => this.handleKeyDown(e, 'allTopics', 'selectedTopics', 'topic')}
                                ref={this.topic}
                            />
                        </div>
                    </div>
                </div>


                <div className="row">
                    <div className="col-lg-12">
                        <div className="card m-b-20">

                            {this.state.loading ? <SimpleSpinner /> : null}
                            <div className="card-body">
                                <h4 className="mt-0 header-title">Assessment | {this.state.appName}

                                    <div className="text-right">

                                        &nbsp;
                                            <CSVLink

                                            className="btn btn-primary btn-lg "
                                            filename={filename}
                                            data={this.state.csvData}
                                            asyncOnClick={true}
                                            title="Export all responses for this application"
                                        >
                                            Export
                                                </CSVLink>
                                    </div>
                                </h4>
                                <div className="tab-content">
                                    <Progress color="success" value={progressBar} />
                                </div>
                                <div style={{ overflow: 'auto' }}>
                                    <MDBDataTable
                                        bordered
                                        hover
                                        searching={false}
                                        data={{ columns: this.state.columns, rows: this.state.rows }}
                                        info={this.state.rows.length > 0 ? true : false}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {

                    modalComment ? <ModalComment app_id={this.props.responseId} quest_id={modalQuestID} toggle={this.handleModalComment} modal={modalComment} /> : null
                }
            </AUX>

        );
    }
}

const mapStateToProps = state => {
    return {
        token: state.auth.token,
        responseId: state.mapping.responseId
    }
}

export default connect(mapStateToProps)(UrlLandingPage);
