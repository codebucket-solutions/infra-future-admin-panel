import React, { Component } from 'react';
import { Button } from 'reactstrap';
import { connect } from 'react-redux';

import MySelect from '../../../components/ReactSelect/index';
import './SelectParameterModal.scss';
import axios from '../../../axios-details';
import { data } from 'jquery';

const options = [
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' },
];
const demoList = [
    "demo1",
    "demo2",
    "demo3"
];

const convertArrayToKeyValue = (arrayToBeConverted) => {
    let _tempObject = [];
    for (let key in arrayToBeConverted) {
        _tempObject.push({
            value: arrayToBeConverted[key].tags,
            label: arrayToBeConverted[key].tags
        })
    }
    return _tempObject;
}
const convertArrayToKey = (arrayToBeConverted) => {
    let _tempObject = [];
    for (let key in arrayToBeConverted) {
        _tempObject.push({
            value: arrayToBeConverted[key],
            label: arrayToBeConverted[key]
        })
    }
    return _tempObject;
}


class SelectParametersModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedUser: [],
            selectedUserInput:[],
            selectedDomain: [],
            selectedApplication: [],
            allApplicationList: [],
            newDiv: [''],
            loading: false,
            message: null,
            error: null,
            userList: [],
            domainList: [],
            data: []
        }
        this.inputChangeHandler = this.inputChangeHandler.bind(this);
        this.addSelectDiv = this.addSelectDiv.bind(this);
        this.removeSelectDiv = this.removeSelectDiv.bind(this);
        this.onSelectionChange = this.onSelectionChange.bind(this)
    }

    fetchAPI = () => {
        this.setState({ loading: true, message: null, error: null });
        const config = {
            headers: { Authorization: `Bearer ${this.props.token}` }
        };

        axios.get('/user', config)
            .then(response => {
                this.setState({ userList: response && response.data && response.data.data && response.data.data.clients, loading: false })
            }).catch(err => {
            });
        axios.get('/apps', config).then(response => {
            this.setState({ allApplicationList: convertArrayToKeyValue(response && response.data && response.data.data && response.data.data.apps) });
        }).catch((err) => {

        })
        axios.get('/questions/filter/get', config).then((response) => {
            this.setState({ domainList: convertArrayToKey(response && response.data && response.data.data && response.data.data.domains) });
        }).catch()
    }

    componentDidMount() {
        this.fetchAPI();
    }

    componentWillUpdate() {
        this.props.onDataSubmit(this.state.selectedUser, this.state.selectedApplication, this.state.selectedDomain)
    }

    inputChangeHandler = (event, name, index) => {
        let _tempList = [...this.state.selectedUser]
        let _tempInputUser = [...this.state.selectedUserInput]
        _tempInputUser[index] = ''
        _tempList[index] = event.target.value
        
        this.setState({ selectedUser: _tempList, selectedUser: _tempInputUser })

    }

    onSelectionChange = (selectedValue, index, type) => {
        if (type === "Application") {
            let _tempAppList = [];
            _tempAppList = [...this.state.selectedApplication];
            _tempAppList[index] = selectedValue;
            this.setState({ selectedApplication: _tempAppList });
        } else if (type === "Domain") {
            let _tempDomainList = [...this.state.selectedDomain];
            _tempDomainList[index] = selectedValue;
            this.setState({ selectedDomain: _tempDomainList });
        }
    }

    addSelectDiv = () => {
        let _tempDivArray = [...this.state.newDiv];
        _tempDivArray.push('');
        this.setState(this.state.newDiv = _tempDivArray);
    }

    removeSelectDiv = (index) => {

        let _tempDivArray = [...this.state.newDiv];
        _tempDivArray.splice(index, 1);
        this.setState({ newDiv: _tempDivArray });

        let _tempUserArray = [...this.state.selectedUser];
        _tempUserArray.splice(index, 1);
        this.setState({ selectedUser: _tempUserArray });

        let _tempDomainList = [...this.state.selectedDomain];
        _tempDomainList.splice(index, 1);
        this.setState({ selectedDomain: _tempDomainList });

        let _tempAppList = [...this.state.selectedApplication];
        _tempAppList.splice(index, 1);
        this.setState({ selectedApplication: _tempAppList });

    }

    render() {
        return (
            <div className="row">
                {this.state.newDiv.map((div, index) => {
                    return <div key={index} className="col-12 select-container">
                        <div className="col-11">
                            <div className="select-element-container">
                                <div className="col-3 users">
                                    { this.state.selectedUser[index] && this.state.selectedUser[index] === "Other" ? 
                                    <div className="form-group">
                                        <label htmlFor="Users">Users</label>
                                        <select
                                            required
                                            className="form-control"
                                            onChange={(event) => this.inputChangeHandler(event, "Users", index)}
                                            value={this.state.selectedUser[index]}>
                                            <option>Select</option>
                                            {this.state.userList.map((user, index) => {
                                                return <option key={index}>{`${user.f_name} ${user.l_name}`}</option>
                                            })}
                                            <option value="Other">Other</option>
                                        </select>
                                    </div> :
                                    <div className="form-group">
                                        <label htmlFor="Users">Users</label>
                                            <input
                                                required
                                                type="email"
                                                className="form-control"
                                                onChange={(event) => this.inputChangeHandler(event, "UsersInput", index)}
                                                value={this.state.selectedUserInput[index]}
                                            />
                                        </div>
                                    }
                                </div>
                                <div className="col-4">
                                    <label>Application</label>
                                    <MySelect
                                        value={this.state.selectedApplication[index]}
                                        onChange={(e) => this.onSelectionChange(e, index, "Application")}
                                        options={this.state.allApplicationList}
                                        allowSelectAll={true}
                                        isMulti={true}
                                        noMin={true}
                                    />
                                </div>
                                <div className="col-4">
                                    <label>Domain</label>
                                    <MySelect
                                        value={this.state.selectedDomain[index]}
                                        onChange={(e) => this.onSelectionChange(e, index, "Domain")}
                                        options={this.state.domainList}
                                        allowSelectAll={true}
                                        isMulti={true}
                                        noMin={true}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="col-1 mt-4">
                            <button className="btn btn-md btn-danger waves-effect waves-light" onClick={(event) => this.removeSelectDiv(index)}>X</button>
                        </div>
                    </div>
                })}
                <div className="col-12 text-right">
                    <Button color="success" onClick={this.addSelectDiv} >Add</Button>{' '}
                </div>

            </div>

        );
    }
}

const mapStateToProps = (state) => {
    return { token: state.auth.token }

}

export default connect(mapStateToProps)(SelectParametersModal);