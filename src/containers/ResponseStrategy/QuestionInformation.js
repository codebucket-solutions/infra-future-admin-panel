import React, { Component } from 'react';

class QuestionInformation extends Component{
   
    render() {
        return (
        <div>
            {this.props.type == 1 ?
                <table className="table table-hover table-striped">
                    <thead className="thead-light">
                        <tr>
                            <th>Tag</th>
                            <th>Question</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{this.props.question.quest_tag}</td>
                            <td>{this.props.question.question}</td>
                            <td>{this.props.question.description}</td>
                        </tr>
                    </tbody>
                </table>
            :

                <table className="table table-borderless">
                    <tbody>
                        <tr>
                            <td>Tag:</td>
                            <td>{this.props.tag}</td>
                        </tr>
                        <tr>
                            <td>Question:</td>
                            <td>{this.props.question}</td>
                        </tr>
                        <tr>
                            <td>Description:</td>
                            <td>{this.props.question}</td>
                        </tr>
                    </tbody>
                </table>
            }

        </div>
        )
    }
}

export default QuestionInformation;