import React, { Component } from 'react';
import { connect } from 'react-redux';

import axios from '../../axios-details';

import Header from '../../components/Header';
import Spinner from '../../components/Spinner/simpleSpinner';
import Alert  from '../../components/Alert/common';
import QuestionInfo from './QuestionInformation';
import StrategyTable from './StrategyTable';


class ResponseStrategy extends Component{

    constructor(props){
        super(props);
        this.state = {
            loading: false,
            error:null,
            message:null,

            questId:null,
            projectId:null,

            question:null,
            platform:[],
            platform_strategy:[],
            strategy_schema:[],
            tabs:[],
            basicStrategy:null,
            advancedStrategy:null
        }
    }

    componentDidMount(){
        let id = this.props.location.pathname.split('/')[2];
        let projectId=this.props.location.state.projectId;
        this.setState({questId:id, projectId:projectId})
        this.fetchStrategy(id, projectId);
    }

    fetchStrategy(questId, projectId){
        const config ={
            token:this.props.token,
            ques_id: questId,
            user_id: projectId ? projectId : 0
        }
        //console.log(config);
        this.setState({loading:true, error:null, message: null});
        axios.post('/strategy/view/', config)
        .then(res => {
            const { data } = res.data;
            if(res.data.status){
                //console.log(data);

                let response = JSON.parse(data.question[0].response_json);
                //console.log(response);
                let _strategy_basic  = [];
                let _strategy_advanced = [];

                data.platform.forEach((p) =>{
                    response.forEach((r) =>{
                        //console.log(p);
                        let _schema_basic = [];
                        let _schema_advanced = [];
                        data.strategy_schema.forEach(s =>{
                            let value = '';
                            data.platform_strategy.forEach((d) =>{
                                if((d.platform.trim() === p.name.trim()) && (d.Value.trim() === r.choice.trim())) {
                                   
                                    value = d[s.strategy_name]
                                }   
                            })

                            if(s.tabs === "Basic"){
                                _schema_basic.push([s.strategy_name, value]);
                            }
                            else{
                                _schema_advanced.push([s.strategy_name, value]);
                            }
                            
                        })
                        _schema_basic.push(["Platform", p.name], ["Value", r.choice]);
                        _schema_advanced.push(["Platform", p.name], ["Value", r.choice]);

                        _strategy_basic.push(Object.fromEntries(_schema_basic));
                        _strategy_advanced.push(Object.fromEntries(_schema_advanced));

                    })
                })

                //console.log(_strategy_advanced, _strategy_basic);

                this.setState({
                        question: data.question[0], 
                        platform: data.platform, 
                        platform_strategy: data.platform_strategy, 
                        strategy_schema:data.strategy_schema, 
                        tabs:data.tabs,
                        basicStrategy: _strategy_basic,
                        advancedStrategy: _strategy_advanced,
                        loading:false, error:null, message: null});
            }
            
        })
        .catch(error =>{
            console.log(error);
            this.setState({loading:false, error:"Something went wrong. Please try again", message: null});
        })
    }

    handleAlert = (status, message) => {
        window.scrollTo(0, 0)
        if(status){
            this.setState({message: message})
        }
        else{
            this.setState({error: message})
        }
    }
    render() {
        const {question, basicStrategy, advancedStrategy, projectId} = this.state;
        
        return (
        <>
            <Header title="Rules" />
            <div className="row">
                <div className="col-lg-12">
                    <div className="card m-b-20">
                        <div className="card-body">
                            <h4 className="mt-0 header-title">
                                {/* APPTYPE */}
                            </h4>
                            {this.state.loading ?  <Spinner /> : null }
                            {this.state.message ? <Alert classes={"alert-success"} msg={this.state.message} />: null}
                            {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} />: null}
                            <br />
                            {
                                question ? <QuestionInfo  type={1} question={question}  />  : null
                            }
                            {
                                advancedStrategy && basicStrategy ? 
                                <StrategyTable 
                                    basicStrategy={basicStrategy} 
                                    advancedStrategy={advancedStrategy} 
                                    strategy_schema = {this.state.strategy_schema}
                                    question = {question}
                                    projectId = {projectId}
                                    handleAlert={this.handleAlert}
                                    />
                                : null
                            }
                        </div>
                    </div>
                </div>
            </div>
        </>
        )
    }
}
const mapStateToProps = state => {
    return {
        token:state.auth.token,
        name : state.auth.name,
    }
}

export default connect(mapStateToProps)(ResponseStrategy);
