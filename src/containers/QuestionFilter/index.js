import React , {Component } from 'react';
import makeAnimated from "react-select/animated";
import MySelect from '../../components/ReactSelect';
import axios from '../../axios-details';
import { connect } from 'react-redux';
import { handleKeyValue } from '../../shared/utility.js'


const animatedComponents = makeAnimated();

class Filter extends Component{

    constructor(props){
        super(props);
        this.state = {
            allCriteria:[],
            selectedCriteria:[],
            allLevels: [],
            selectedLevel:[],
            allDomains:[],
            selectedDomains:[],
            allTopics:[],
            selectedTopics:[],
        }

        this.domain = React.createRef();
        this.level = React.createRef();
        this.criteria = React.createRef();
        this.topic = React.createRef();
    }

  
    fetchFilter = () =>{
        this.setState({ loading:true });
        const config = {
            headers: { Authorization: `Bearer ${this.props.token}` }
        };
        axios.get('/questions/filter/get/', config)
            .then(res => {
                console.log(res.data)
                if(res.data.status){
                    
                    let criteria = handleKeyValue(res.data.data.criteria);
                    let levels = handleKeyValue(res.data.data.levels);
                    let domains = handleKeyValue(res.data.data.domains);
                    let topics = handleKeyValue(res.data.data.topics);
                    console.log(criteria);
                    
                    // get filter from localStorage
                    const local_domain  = JSON.parse(localStorage.getItem(`${this.props.domainFilter}`));
                    const local_criteria  = JSON.parse(localStorage.getItem(`${this.props.criteriaFilter}`));
                    const local_level  = JSON.parse(localStorage.getItem(`${this.props.levelFilter}`));
                    const local_topic  = JSON.parse(localStorage.getItem(`${this.props.topicFilter}`));
                    console.log(local_level);
                    //create table
                    this.props.createTable(
                        this.props.question,
                        local_criteria && local_criteria.length > 0 ? local_criteria :criteria,
                        local_level && local_level.length > 0 ? local_level :levels,
                        local_domain && local_domain.length > 0 ? local_domain :domains,
                        local_topic && local_topic.length > 0 ? local_topic :topics,
                        0
                    );
                    
                    this.setState({
                        
                        selectedCriteria: (this.state.selectedCriteria.length > 0 ? this.state.selectedCriteria :  (local_criteria && local_criteria.length>0 ? local_criteria: criteria)),
                        allCriteria: criteria,

                        selectedLevel: (this.state.selectedLevel.length > 0 ? this.state.selectedLevel : (local_level && local_level.length>0 ? local_level: levels)),
                        allLevels : levels,

                        selectedDomains: (this.state.selectedDomains.length > 0 ? this.state.selectedDomains : (local_domain && local_domain.length>0 ? local_domain: domains)),
                        allDomains: domains,

                        allTopics: topics,
                        selectedTopics: (this.state.selectedTopics.length > 0 ? this.state.selectedTopics : (local_topic && local_topic.length>0 ? local_topic: topics)),
                        
                        
                    })
                }
            })
            .catch( err => {
                console.log(err);
                this.setState({loading:false})
            });    
    }

    componentDidMount() {
        //console.log(this.props.question)
        if(this.props.question.length > 0) {
            this.fetchFilter();
        }
    }

    selectTowChangeCriteria= (selectedCriteria) => {
        this.setState({ selectedCriteria:selectedCriteria ?  selectedCriteria : [] });
       
        this.props.createTable(this.props.question, selectedCriteria ? selectedCriteria : [], this.state.selectedLevel,  this.state.selectedDomains, this.state.selectedTopics, this.props.answerType);
    }

    selectTowChangeDomains= (selectedDomains) => {
        this.setState({ selectedDomains:selectedDomains ? selectedDomains : []  });
        
        this.props.createTable(this.props.question, this.state.selectedCriteria, this.state.selectedLevel, selectedDomains ? selectedDomains : [], this.state.selectedTopics, this.props.answerType);
    }

    selectTowChangeLevel= (selectedLevel) => {
        this.setState({ selectedLevel:selectedLevel ? selectedLevel : [] });
        
        this.props.createTable(this.props.question, this.state.selectedCriteria, selectedLevel ? selectedLevel : [], this.state.selectedDomains, this.state.selectedTopics, this.props.answerType);
        
    }

    selectTowChangeTopics= (selectedTopics) => {
        this.setState({ selectedTopics:selectedTopics ? selectedTopics : [] });
        
        this.props.createTable(this.props.question, this.state.selectedCriteria,this.state.selectedLevel, this.state.selectedDomains, selectedTopics ? selectedTopics : [], this.props.answerType);
        
    }

    // wild card search function
    handleKeyDown = (e,all, selected,myInput) => {
        let allFields  = this.state[all];
        let selectedFields = this.state[selected] ;
        
        if (e.key === 'Enter') {
            let string = e.target.value.substring(0, e.target.value.length - 1);
            allFields.forEach( a => {
                if(a.value.toLowerCase().includes(string.toLowerCase())){
                    selectedFields.push(a);
                }
            })
            this.setState({[selected]:selectedFields});

            
            this[myInput].current.blur();
            this[myInput].current.focus();

            this.props.createTable(this.props.question, this.state.selectedCriteria , this.state.selectedLevel,  this.state.selectedDomains, this.state.selectedTopics, this.props.answerType);
        }
    }

    render(){
        return (
            <div className="row">
                <div className="col-sm-12 col-lg-4">
                    <div className="page-title-box">
                        <label>Domains</label>
                        <MySelect
                            value={this.state.selectedDomains}
                            onChange={this.selectTowChangeDomains}
                            options={this.state.allDomains}
                            components={animatedComponents}
                            allowSelectAll={true}
                            isMulti ={true}

                            handleKeyDown={(e) => this.handleKeyDown(e,'allDomains', 'selectedDomains','domain')}
                            ref={this.domain}
                        />
                    </div>
                </div>
                <div className="col-sm-12 col-lg-4">
                    <div className="page-title-box">
                        <label>Level</label>
                        <MySelect
                            value={this.state.selectedLevel}
                            onChange={this.selectTowChangeLevel}
                            options={this.state.allLevels}
                            components={animatedComponents}
                            allowSelectAll={true}
                            isMulti ={true}

                            handleKeyDown={(e) => this.handleKeyDown(e,'allLevels', 'selectedLevel','level')}
                            ref={this.level}
                        />
                    </div>
                </div>
                <div className="col-sm-12 col-lg-4">
                    <div className="page-title-box">
                        <label>Criteria</label>
                        <MySelect
                            value={this.state.selectedCriteria}
                            onChange={this.selectTowChangeCriteria}
                            options={this.state.allCriteria}
                            components={animatedComponents}
                            allowSelectAll={true}
                            isMulti ={true}

                            handleKeyDown={(e) => this.handleKeyDown(e, 'allCriteria', 'selectedCriteria','criteria')}
                            ref={this.criteria}
                        />
                    </div>
                </div>
                <div className="col-sm-12 col-lg-12">
                    <div className="page-title-box">
                        <label>Topics</label>
                        <MySelect
                            value={this.state.selectedTopics}
                            components={animatedComponents}
                            onChange={this.selectTowChangeTopics}
                            options={this.state.allTopics}
                            allowSelectAll={true}
                            isMulti ={true}

                            handleKeyDown={(e) => this.handleKeyDown(e, 'allTopics', 'selectedTopics','topic')}
                            ref={this.topic}
                        />
                        </div>
                </div>
            </div>
        )
    }
}


const mapStateToProps = state => {
    return {
        token:state.auth.token,
    }
}

export default connect(mapStateToProps)(Filter);
