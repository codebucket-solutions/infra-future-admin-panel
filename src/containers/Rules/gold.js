import React from 'react'
import AllRules from './rules';

class GoldQuestion extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            projectId:0,
            pageTitle:"Rules",
            quest_tag:null
        }
    }
    componentDidMount(){
        
        let quest_tag = localStorage.getItem("goldTags");        
        this.setState({quest_tag: JSON.parse(quest_tag)});
    }
    render() {
        return (
            this.state.projectId === 0 ? <AllRules 
                projectId={this.state.projectId} 
                pageTitle={this.state.pageTitle} 
                questTag={this.state.quest_tag}
                userId={0} 
                {...this.props} header= "Admin"  /> : null
        )
    }
}

export default GoldQuestion