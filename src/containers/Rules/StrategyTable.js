import React, { Component } from 'react';
import { connect } from 'react-redux';
import equal from 'fast-deep-equal';
import axios from '../../axios-details';
import * as actions from '../../store/actions/index';

import StrategyInput from '../../components/UI/Input/strategyInput';

class StrategyTable extends Component{

    constructor(props){
        super(props);
        this.state = {
            basicHeader : null,
            advancedHeader : null,
            basicStrategy: null,
            advancedStrategy: null,
            strategy_schema:null,
            selectedPlatform: 0,
        }
    }

    componentDidMount(){
        this.handleTable()
    }

    componentDidUpdate(prevProps) {
        if(!equal(this.props.basicStrategy, prevProps.basicStrategy)){
            this.handleTable();
        }
    }

    handleTable = () =>{
        if( this.props.basicStrategy && this.props.basicStrategy.length > 0 && this.props.advancedStrategy && this.props.advancedStrategy.length > 0){
            let basicHeader = Object.keys(this.props.basicStrategy[0]);
            let advancedHeader = Object.keys(this.props.advancedStrategy[0]);
            //console.log(basicHeader, advancedHeader);
            console.log(this.props.projectId)
            this.setState({basicHeader, advancedHeader, basicStrategy: this.props.basicStrategy, advancedStrategy: this.props.advancedStrategy, strategy_schema: this.props.strategy_schema});
        }
    }

    generate = (header, data, vatType, selectedPlatform) => {
        let allRows = [];
        //console.log(header, data);
        const { strategy_schema }  = this.state;
        //console.log(strategy_schema);
        allRows = data.map((d, i) =>{
            // let rows = [];
            if(selectedPlatform == 0 || (selectedPlatform != 0 && selectedPlatform.trim() === d.Platform.trim()) ){
                
                    return (<tr>
                        {header.map((h, i) =>{
                           if(h === "Value"){
                               // rows.push(<td className="headcol">{d[h]}</td>)
                               return <td className="headcol">{d[h]}</td>
                           }
                           else if(h === "Platform"){
                               // rows.push(<td className="headcol1">{d[h]}</td>)
                               return <td className="headcol1">{d[h]}</td>
                           }
                           else{
                               let type,value;
                               strategy_schema.forEach((s) =>{
                                   if(s.strategy_name === h){
                                       type = s.strategy_type;
                                       value = s.strategy_value;
                                   }
           
                               })
                               
                               // rows.push(<td><StrategyInput type="input" /></td>)
                               return <td><StrategyInput 
                                   type={type} 
                                   dropdown={value} 
                                   value={d[h]}
                                   disabled
                                   handleInputChange = {(e) => this.handleInputChange (e, d, vatType, h, type)}
                                   /></td>
                           }
                           //console.log(rows);
                       })}
                       </tr>)
                
            }
            
        }
            // allRows.push("<tr>"+rows.join(" ")+"</tr>")
            //console.log("<tr>"+rows.join(" ")+"</tr>");
        )

        return allRows;
        
    }

    handleInputChange = (e, d, type, strategyName, inputType) =>{
        
        const { [type]: strategy } = this.state;
        // console.log(type, strategyName);
        // console.log(strategy);
        //console.log(e.target.value, d);
        let value;
        //console.log(e.target.value)
        if(inputType === 'dropdown'){
            value = e.target.value
        }
        else{
            if(e.target.value === ''){
                value = e.target.value;
            }
            else if(e.target.value > 0 && e.target.value < 10){
                if(Number.isInteger(parseInt(e.target.value))){
                    console.log(parseInt(e.target.value))
                    value = e.target.value;
                }
                else{
                    value = d[strategyName];
                }
            }
            else{
                value = d[strategyName];
            }
        }
        
        d[strategyName] = value;
        this.setState({[strategy]: {
            ...[strategy],
            d
        }})
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const { basicStrategy, advancedStrategy } = this.state;
        let final = basicStrategy.map((b,i) =>{
            let temp = advancedStrategy[i];

            return {
                ...b,
                ...temp,
                'Tag': this.props.question.quest_tag
            }
        })
        //console.log(final)
        let token = {
            token:this.props.token,
            user_id:this.props.projectId,
            rules: final
        }
        console.log(token);
        axios.post('/strategy/edit',token)
        .then(res => {
            
            console.log(res.data);
            if(res.data.status){
                this.props.handleAlert(true, res.data.message);    
            }
            else{
                this.props.handleAlert(false, res.data.message);    
            }
        })
        .catch( err => {
            console.error(err);
            this.props.handleAlert(false, "Something went wrong. Please try again.");  
        }
            
        );    
    }   

    inputChangeHandler = (e) =>{
        // const { advancedStrategy, basicStrategy , basicHeader, advancedHeader} = this.state;
        this.setState({selectedPlatform: e.target.value})
        // this.generate(basicHeader, basicStrategy, "basicStrategy", e.target.value)
        // this.generate(advancedHeader, advancedStrategy, "advancedStrategy", e.target.value)
    }
    render() {
        const { basicHeader, advancedHeader, basicStrategy, advancedStrategy, selectedPlatform} = this.state;
        //console.log(basicHeader);
        let classes;
        return (
        <>
            <form onSubmit={this.handleSubmit} >
                <ul className="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                    <li className="nav-item" >
                        <a className="nav-link active"  id="tab_1" data-toggle="tab" href="#basic" role="tab">Basic</a>
                    </li>
                    <li className="nav-item" >
                        <a className="nav-link"  id="tab_1" data-toggle="tab" href="#advance" role="tab">Advanced</a>
                    </li>
                </ul>
                <div className="tab-content">
                    <div className="tab-pane pt-3 active" id="basic" role="tabpanel" >
                        <div className="tableScroll">
                            <table className="table table-hover table-striped">
                                <thead className="thead-dark">
                                    <tr>
                                        {
                                            basicHeader && basicHeader.map((h,i) =>{
                                                if(h === "Value"){
                                                    classes = "headcol";
                                                }
                                                else if(h === "Platform"){
                                                    classes = "headcol1";
                                                }
                                                else{
                                                    classes = '';
                                                }
                                                return (<th key={i} className={classes}>{h}</th>)
                                            })
                                        }
                                    </tr>
                                </thead>
                                <tbody>
                                    {basicHeader && basicStrategy ?  this.generate(basicHeader, basicStrategy, "basicStrategy", selectedPlatform)
                                        : null}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="tab-pane pt-3" id="advance" role="tabpanel" >
                        <div className="tableScroll">
                            <table className="table table-hover table-striped">
                                <thead className="thead-dark">
                                    <tr>
                                        {
                                            advancedHeader && advancedHeader.map((h,i) =>{
                                                if(h === "Value"){
                                                    classes = "headcol";
                                                }
                                                else if(h === "Platform"){
                                                    classes = "headcol1";
                                                }
                                                else{
                                                    classes = '';
                                                }
                                                return (<th className={classes}>{h}</th>)
                                            })
                                        }
                                    </tr>
                                </thead>
                                <tbody>
                                {advancedHeader && advancedStrategy ?  this.generate(advancedHeader, advancedStrategy, "advancedStrategy", selectedPlatform)
                                        : null}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <br /><br />
                <div className="text-center">
                    <button className="btn btn-md btn-primary">Save Rules</button>
                </div>
            </form>
        </>

        )
    }
}

const mapStateToProps = state => {
    return {
        token:state.auth.token,
        name : state.auth.name,
    }
}

const mapDispatchToProps = dispatch =>{
    return {
        setResponse: (id) => dispatch(actions.setResponse(id))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(StrategyTable);
