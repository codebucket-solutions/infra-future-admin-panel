import React, { Component } from 'react';
import { connect } from 'react-redux';
import { CSVLink } from "react-csv";

import axios from '../../axios-details';

import Header from '../../components/Header';
import Spinner from '../../components/Spinner/simpleSpinner';
import Alert  from '../../components/Alert/common';
import StrategyTable from './StrategyTable';
import RulesFilter from '../RulesFilter';
import ImportModal from '../../components/Modal/modalImportRules';
import QuestionInfo from '../ResponseStrategy/QuestionInformation';
import { handleKeyValue, handleKeyValueresponseBasic } from '../../shared/utility.js'



let csvRules = [];
class ResponseStrategy extends Component{

    constructor(props){
        super(props);
        this.state = {
            loading: false,
            error:null,
            message:null,

            questId:null,
            projectId:null,

            question:null,
            platform:[],
            platform_strategy:[],
            strategy_schema:[],
            tabs:[],
            response:[],
            basicStrategy:null,
            advancedStrategy:null,
            toggleImport:false,
            rulesFilter:true,

        }
        this.fetchStrategy = this.fetchStrategy.bind(this);
    }

    componentDidMount(){
    }

    fetchStrategy(questTag, projectId, platformTags, responses, token, isTagChanged){
        console.log(this.props);
        const config ={
            token:token,
            ques_tag: questTag,
            platform_tags:platformTags,
            user_id: projectId ? projectId : 0
        }
        if(isTagChanged === 0){
            config.responses = responses;
        }
        console.log(config);
        this.setState({loading:true, error:null, message: null});
        axios.post('/strategy/view_by_filter/', config)
        .then(res => {
            const { data } = res.data;
            console.log(res.data);
            if(res.data.status){
                console.log(data);

                // let response = responses ? handleKeyValueresponseBasic(responses) : JSON.parse(data.question[0].response_json);
                // console.log(response);
                console.log(isTagChanged)
                let response;
                if(isTagChanged === 0){
                    response = responses ? handleKeyValueresponseBasic(responses) : JSON.parse(data.question[0].response_json);
                    console.log(response);
                }
                else{
                    
                    response = JSON.parse(data.question[0].response_json)
                    console.log(response);
                }
                let _strategy_basic  = [];
                let _strategy_advanced = [];

                data.platform.forEach((p) =>{
                    response.forEach((r) =>{
                        //console.log(p);
                        let _schema_basic = [];
                        let _schema_advanced = [];
                        data.strategy_schema.forEach(s =>{
                            let value = '';
                            data.platform_strategy.forEach((d) =>{
                                if((d.platform.trim() === p.name.trim()) && (d.Value.trim() === r.choice.trim())) {
                                   
                                    value = d[s.strategy_name]
                                }   
                            })

                            if(s.tabs === "Basic"){
                                _schema_basic.push([s.strategy_name, value]);
                            }
                            else{
                                _schema_advanced.push([s.strategy_name, value]);
                            }
                            
                        })
                        _schema_basic.push(["Platform", p.name], ["Value", r.choice]);
                        _schema_advanced.push(["Platform", p.name], ["Value", r.choice]);

                        _strategy_basic.push(Object.fromEntries(_schema_basic));
                        _strategy_advanced.push(Object.fromEntries(_schema_advanced));

                    })
                })

                //console.log(_strategy_advanced, _strategy_basic);

                this.setState({
                        question: data.question[0], 
                        platform: data.platform, 
                        platform_strategy: data.platform_strategy, 
                        strategy_schema:data.strategy_schema, 
                        tabs:data.tabs,
                        basicStrategy: _strategy_basic,
                        advancedStrategy: _strategy_advanced,
                        loading:false, error:null, message: null,
                        response:response
                    });

            }
            else{
                this.setState({loading:false, error:null})
            }
            
        })
        .catch(error =>{
            console.log(error);
            this.setState({loading:false, error:"Rules are empty. Please sync rules to continue.", message: null});
        })
    }

    handleAlert = (status, message) => {
        window.scrollTo(0, 0)
        if(status){
            this.setState({message: message})
        }
        else{
            this.setState({error: message})
        }
    }

    getRulesHeader = () =>{
        axios.get('/strategy/table_schema/fetch')
        .then(res=>{
            console.log(res.data.data.schema)
            let _schema = res.data.data.schema;
            
            this.exportCSVResponse(_schema)
        })
        .catch(err =>{
          console.log(err)
        })
    }

    exportCSVResponse = (rulesHeader) =>{
        csvRules = [];
        csvRules.push(rulesHeader)
        console.log(rulesHeader)
        const token = {
            token: this.props.token,
            user_id:this.props.projectId,
        };
        console.log(token)
        this.setState({loading:true});
        axios.post("/strategy/export", token).then(res => {
            
            let { platform_strategy } = res.data.data;
            console.log(platform_strategy);
            for(let key in platform_strategy){
                let columnArray = [];
                rulesHeader.map((v, i) => {
                    //console.log(v)
                    
                    columnArray.push(platform_strategy[key][v] ? platform_strategy[key][v].toString().replace(/"/g, '""'): '')
                    
                    return true;
                    //console.log(res.data.data.questions[key][v]);
                })
                csvRules.push(columnArray);
            }
            this.setState({loading:false});
            let btn = this.refs.csv;
            btn.link.click();
        }).catch(error =>{
            console.log(error);
        });
    }

    closeModalImport = (update) =>{
        //console.log(update);
        // if(update){
        //     this.fetchQuestion(this.props.projectId);
        // }
        this.setState( prevState => {
            return ({toggleImport: !prevState.toggleImport})
        })
        
    }

    rulesFilter = (filter) =>{
        this.setState({rulesFilter: filter})
    }

    
    render() {
        console.log(this.state.response)
        const {question, basicStrategy, advancedStrategy, projectId, rulesFilter} = this.state;
        let filenameRules = "rules"+Date.now()+".csv";
        return (
        <>
            {
                this.props.projectId 
                ? 
                    <Header 
                        title={this.props.pageTitle} 
                        path="/projects"
                        main="Project"
                    />
                : 
                    <Header 
                        title={this.props.pageTitle} 
                    />
            }
            <div className="row">
                <div className="col-lg-12">
                    <div className="card m-b-20">
                        <div className="card-body" style={{minHeight:400}}>
                            <h4 className="mt-0 header-title">
                                <div className="row">
                                    <div className="col-md-4">
                                        {this.props.header}
                                    </div>
                                    <div className="col-md-8 text-right">
                                        <button     
                                            title="Add a new question"
                                            className="btn btn-secondary mb-3" 
                                            onClick={this.closeModalImport}>Import</button>&nbsp;
                                        <button     
                                            title="Add a new question"
                                            className="btn btn-success mb-3" 
                                            onClick={this.getRulesHeader}>Export</button>&nbsp;
                                        <CSVLink
                                            ref="csv"
                                            filename={filenameRules} 
                                            data={csvRules}
                                            style={{display:'none'}}
                                        ></CSVLink>
                                    </div>
                                </div>
                                
                            </h4>
                            {this.state.loading ?  <Spinner /> : null }
                            {this.state.message ? <Alert classes={"alert-success"} msg={this.state.message} />: null}
                            {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} />: null}
                                {<RulesFilter 
                                    projectId={this.props.projectId}
                                    platforms="goldPlatform"
                                    tags="goldTags"
                                    response={this.state.response}
                                    fetchStrategy={this.fetchStrategy}
                                    rulesFilter = {this.rulesFilter}
                                /> }
                            <br />
                            {
                                question ? <QuestionInfo  type={1} question={question}  />  : null
                            }
                            {
                                advancedStrategy && basicStrategy ?  
                                <StrategyTable 
                                    basicStrategy={basicStrategy} 
                                    advancedStrategy={advancedStrategy} 
                                    strategy_schema = {this.state.strategy_schema}
                                    question = {question}
                                    projectId = {this.props.projectId}
                                    handleAlert={this.handleAlert}
                                    />
                                : null
                            }
                        </div>
                    </div>
                </div>
            </div>
            {
                this.state.toggleImport
                ? <ImportModal modal={this.state.toggleImport} toggle={() => this.closeModalImport(true)} 
                projectId={this.props.projectId}
                type={2}
                />
                : false
            }
        </>
        )
    }
}
const mapStateToProps = state => {
    return {
        token:state.auth.token,
        name : state.auth.name,
    }
}

export default connect(mapStateToProps)(ResponseStrategy);
