import React , {Component } from 'react';
import makeAnimated from "react-select/animated";
import MySelect from '../../components/ReactSelect';
import axios from '../../axios-details';
import { connect } from 'react-redux';
import { handleKeyValue, objectToArray, handleKeyValueresponse } from '../../shared/utility.js'


const animatedComponents = makeAnimated();

class Filter extends Component{

    constructor(props){
        super(props);
        this.state = {
            allTags:[],
            selectedTags:[],
            allPlatform: [],
            selectedPlatform:[],
            allResponse: [],
            selectedResponse:[],
        }

        this.domain = React.createRef();
        this.level = React.createRef();
    }

  
    fetchFilter = () =>{
        this.setState({ loading:true });
        const config = {
            headers: { Authorization: `Bearer ${this.props.token}` }
        };
        console.log(this.props.projectId);
        axios.get('/master_qs/rules_filter/get/'+this.props.projectId, config)
            .then(res => {
                console.log(res.data)
                if(res.data.status){
                    
                    let platforms = handleKeyValue(res.data.data.platforms);
                    let tags = handleKeyValue(res.data.data.qs);
                    //console.log(criteria);
                    
                    // get filter from localStorage
                    
                    const local_platforms  = localStorage.getItem(`${this.props.platforms}`) !== 'undefined' ? JSON.parse(localStorage.getItem(`${this.props.platforms}`)) : [];
                    //console.log(local_domain);

                    const local_tags  = localStorage.getItem(`${this.props.tags}`) !== 'undefined' ? JSON.parse(localStorage.getItem(`${this.props.tags}`)) : [];
                    //console.log(local_criteria);
                    // console.log(this.props.tags)
                    // console.log("asdsa",local_tags.value);
                    
                    this.props.fetchStrategy(
                        local_tags ? local_tags.value : tags[0].value,
                        this.props.projectId, 
                        local_platforms && local_platforms.length > 0 ? objectToArray(local_platforms) : objectToArray(platforms), 
                        null,
                        this.props.token,
                        0)

                    
                    this.setState({
                        
                        selectedTags: (this.state.selectedTags.length > 0 ? this.state.selectedTags :  (local_tags ? local_tags: tags[0])),
                        allTags: tags,

                        selectedPlatform: (this.state.selectedPlatform.length > 0 ? this.state.selectedPlatform : (local_platforms && local_platforms.length>0 ? local_platforms: platforms)),
                        allPlatform : platforms,

                    })
                }
            })
            .catch( err => {
                console.log(err);
                this.setState({loading:false})
            });    
    }

    componentDidMount() {
        
        this.fetchFilter();
    }

    componentDidUpdate(prevProps) {
        if(this.props.response != prevProps.response && this.state.allResponse.length <= 0)
        {
            this.handleresponse(this.props.response);
        }
    } 

    handleresponse =(resp) =>{
        console.log(resp)
        this.setState({allResponse: handleKeyValueresponse(resp), selectedResponse: handleKeyValueresponse(resp)})
    }

    selectTowChangeTags= (selectedTags) => {
        // console.log(objectToArray(this.state.selectedPlatform));
        this.setState({ selectedTags:selectedTags ?  selectedTags : [] , allResponse: []});
        this.props.fetchStrategy(selectedTags.value,this.props.projectId, objectToArray(this.state.selectedPlatform),objectToArray(this.state.selectedResponse), this.props.token, 1)

        localStorage.setItem("goldTags", JSON.stringify(selectedTags))
       
       
        // this.props.createTable(this.props.question, selectedCriteria ? selectedCriteria : [], this.state.selectedLevel,  this.state.selectedDomains, this.state.selectedTopics, this.props.answerType);
    }

    selectTowChangePlatform= (selectedPlatform) => {
        console.log(selectedPlatform);
        if(selectedPlatform){
            if(!selectedPlatform ||  selectedPlatform.length <= 0){
                selectedPlatform = [];
                selectedPlatform.push(this.state.allPlatform[0])
            }
            this.setState({ selectedPlatform:selectedPlatform ? selectedPlatform : []  });
            this.props.fetchStrategy(this.state.selectedTags.value,this.props.projectId, objectToArray(selectedPlatform),objectToArray(this.state.selectedResponse), this.props.token, 0)
        }
        
        // this.props.createTable(this.props.question, this.state.selectedCriteria, this.state.selectedLevel, selectedDomains ? selectedDomains : [], this.state.selectedTopics, this.props.answerType);
    }

    selectTowChangeResponses= (selectedResponse) => {
        console.log(selectedResponse);
        if(selectedResponse){
            
            if(!selectedResponse ||  selectedResponse.length <= 0){
                selectedResponse = [];
                selectedResponse.push(this.state.allResponse[0])
            }
            console.log(selectedResponse, this.state.allResponse)
            this.setState({ selectedResponse:selectedResponse ? selectedResponse : []  });
            this.props.fetchStrategy(this.state.selectedTags.value,this.props.projectId,objectToArray(this.state.selectedPlatform), objectToArray(selectedResponse), this.props.token, 0)
        }
        
        // this.props.createTable(this.props.question, this.state.selectedCriteria, this.state.selectedLevel, selectedDomains ? selectedDomains : [], this.state.selectedTopics, this.props.answerType);
    }

    render(){
        return (
            <div className="row">
                <div className="col-sm-12 col-lg-4">
                    <div className="page-title-box">
                        <label>Question Tag</label>
                        <MySelect
                            value={this.state.selectedTags}
                            onChange={this.selectTowChangeTags}
                            options={this.state.allTags}
                            components={animatedComponents}
                            allowSelectAll={false}
                            isMulti ={false}
                            // handleKeyDown={(e) => this.handleKeyDown(e,'allDomains', 'selectedDomains','domain')}
                            // ref={this.domain}
                        />
                    </div>
                </div>
                <div className="col-sm-12 col-lg-4">
                    <div className="page-title-box">
                        <label>Platform</label>
                        <MySelect
                            value={this.state.selectedPlatform}
                            onChange={this.selectTowChangePlatform}
                            options={this.state.allPlatform}
                            components={animatedComponents}
                            allowSelectAll={true}
                            isMulti ={true}
                            noMin={false}
                            // handleKeyDown={(e) => this.handleKeyDown(e,'allLevels', 'selectedLevel','level')}
                            // ref={this.level}
                        />
                    </div>
                </div>
                <div className="col-sm-12 col-lg-4">
                    <div className="page-title-box">
                        <label>Response</label>
                        <MySelect
                            value={this.state.selectedResponse}
                            onChange={this.selectTowChangeResponses}
                            options={this.state.allResponse}
                            components={animatedComponents}
                            allowSelectAll={true}
                            isMulti ={true}
                            noMin={false}
                            // handleKeyDown={(e) => this.handleKeyDown(e,'allLevels', 'selectedLevel','level')}
                            // ref={this.level}
                        />
                    </div>
                </div>
            </div>
        )
    }
}


const mapStateToProps = state => {
    return {
        token:state.auth.token,
    }
}

export default connect(mapStateToProps)(Filter);
