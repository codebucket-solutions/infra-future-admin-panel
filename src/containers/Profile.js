import React, { Component } from 'react';
import { MDBDataTable } from 'mdbreact';
import { connect } from 'react-redux';

import * as actions from '../store/actions/index';
import AUX from '../hoc/Aux_';
import axios from '../axios-details';

import ProfileModal from '../components/Modal/modalProfile';
import Spinner from '../components/Spinner/simpleSpinner';
import ModalConfirm from '../components/Modal/modalConfirm';
import { btnStyle } from '../util'

class Profile extends Component {


    constructor(props) {
        super(props);
        this.state = {
            columns: [
                {
                    label: 'Sr.No.',
                    field: 'sro',
                    sort: 'asc',
                    width: 150
                },
                {
                    label: 'Profile Name',
                    field: 'pname',
                    sort: 'asc',
                    width: 270
                },
                {
                    label: 'Client',
                    field: 'client',
                    sort: 'asc',
                    width: 200
                },
                {
                    label: 'Description',
                    field: 'desc',
                    sort: 'asc',
                    width: 100
                },
                {
                    label: 'Action',
                    field: 'action',
                    sort: 'asc',
                    width: 100
                },
            ],
            rows: [],
            rows1: [{}],
            data: [],
            modalTitle: '',
            toggle: false,
            loading: false,
            profileId: null,
            toggleDelete: false,
            message: null,
            error: null
        }
    }



    fetchProfile = () => {
        this.setState({ loading: true });
        const config = {
            headers: { Authorization: `Bearer ${this.props.token}` }
        };
        axios.get('/profile', config)
            .then(res => {
                //console.log(res.data)
                let profiles = [];
                if (res.data.status) {
                    for (let key in res.data.data.profiles) {
                        profiles.push({
                            sro: parseInt(key) + 1,
                            pname: res.data.data.profiles[key].profile_name,
                            client: res.data.data.profiles[key].f_name ? (res.data.data.profiles[key].f_name + " " + (res.data.data.profiles[key].l_name ? res.data.data.profiles[key].l_name : '')) : '',
                            desc: res.data.data.profiles[key].description,
                            action: <AUX>
                                <button
                                    title="Map questions to profile"
                                    onClick={() => this.handleMapping(res.data.data.profiles[key].profile_id)}
                                    className="btn btn-secondary">Map Questions</button>
                                &nbsp;
                                <button
                                    title="Edit a profile"
                                    onClick={() => this.editProfile(res.data.data.profiles[key].profile_id)}
                                    className="btn btn-success">Edit</button>
                                &nbsp;
                                <button
                                    title="Permanently delete a profile"
                                    onClick={() => this.HandleDeleteModal(res.data.data.profiles[key].profile_id)}
                                    className="btn btn-danger">Delete</button>
                            </AUX>
                        })
                    }

                }
                this.setState({ loading: false, rows: profiles })

            })
            .catch(err => {
                this.setState({ loading: false })
            });
    }

    componentDidMount() {
        this.fetchProfile();
    }

    handleMapping = (mappingId) => {
        this.props.doMapping(mappingId);
        this.props.history.push('/select_questions')
    }

    // open add profile
    openModal = () => {
        this.setState(prevState => {
            return { toggle: !prevState.toggle, modalTitle: "Add New Profile", profileId: null };
        })
    }

    // close profile modal
    closeModal = (update) => {
        if (update) {
            this.fetchProfile();
        }
        this.openModal();
    }

    // open edit modal
    editProfile = (profileId) => {
        this.setState(prevState => {
            return { toggle: !prevState.toggle, modalTitle: "Edit Profile", profileId: profileId };
        })
    }



    // open delete modal
    HandleDeleteModal = (profileId) => {
        this.setState(prevState => {
            return { toggleDelete: !prevState.toggleDelete, profileId: profileId };
        })
    }

    //close delete Modal
    closeDeleteModal = () => {
        this.setState(prevState => {
            return { toggleDelete: !prevState.toggleDelete };
        })
    }


    // delete profile
    handleProfileDelete = () => {
        //console.log(this.state);
        this.closeDeleteModal();
        this.setState({ loading: true, message: null, error: null });
        const data = {
            token: this.props.token,
            profile_id: this.state.profileId,
            user_name: this.props.name
        }
        axios.post('/profile/delete', data)
            .then(res => {
                if (res.data.status) {
                    this.setState({ loading: false, profileId: null, message: res.data.message })
                }
                else {
                    this.setState({ loading: false, profileId: null, error: res.data.message })
                }
                this.fetchProfile();

            })
            .catch(err => {
                this.setState({ loading: false, profileId: null, error: "Something went wrong. Please try again later." });

            })
    }

    render() {
        //console.log(this.state);
        return (
            <AUX>
                <div className="row">
                    <div className="col-sm-12">
                        <div className="page-title-box">
                            <h4 className="page-title">Profiles</h4>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card m-b-20">
                            <div className="card-body">
                                <h4 className="mt-0 header-title text-right">
                                    <button
                                        title="Create a new profile"
                                        className="btn btn-primary mb-3" style={{btnStyle}} onClick={this.openModal}>Add Profile</button>
                                </h4>
                                {this.state.loading ? <AUX><Spinner /><br /></AUX> : null}
                                <div style={{ overflow: 'auto' }}>
                                    <MDBDataTable
                                        bordered
                                        hover
                                        data={{ columns: this.state.columns, rows: this.state.rows }}
                                        info={this.state.rows.length > 0 ? true : false}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {
                    this.state.toggle
                        ? <ProfileModal modal={this.state.toggle} toggle={this.closeModal} profileId={this.state.profileId} title={this.state.modalTitle} />
                        : false
                }
                {
                    this.state.toggleDelete
                        ? <ModalConfirm modal={this.state.toggleDelete} toggle={this.closeDeleteModal} click={this.handleProfileDelete} content="Do you really want to delete this profile?" title="Delete profile" />
                        : false
                }
            </AUX>
        );
    }
}

const mapStatetoProps = state => {
    return {
        token: state.auth.token,
        name: state.auth.name,
    };
}

const mapDispatchToProps = dispatch => {
    return {
        doMapping: (mappingId) => dispatch(actions.doMapping(mappingId)),
    }
}

export default connect(mapStatetoProps, mapDispatchToProps)(Profile);