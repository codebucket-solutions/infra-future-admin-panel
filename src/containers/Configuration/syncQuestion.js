import React, { Component } from 'react';
import { connect } from 'react-redux';

import AUX from '../../hoc/Aux_';
import axios from '../../axios-details';


import Spinner from '../../components/Spinner/simpleSpinner';
import Alert from '../../components/Alert/common.js';
import ConfirmModal from '../../components/Modal/modalConfirm';
import { btnStyle } from '../../util';

class SyncQuestion extends Component {

    constructor(props) {
        super(props);
        this.state = {

            loading: false,
            error: null,
            message: null,

            projectId: null,
            toggleSync: false,
            type: "1",
            title: null,
            content: null,

        }
    }

    handleSyncModal = (e, status) => {
        const { type } = this.state;
        if (e)
            e.preventDefault();
        if (type === "1") {
            this.setState({ title: "Sync Questions from Gold", content: "All the question and the responses will we deleted. Do you want to continue?" })
        }
        else if (type === "2") {
            this.setState({ title: "Sync Rules from Gold", content: "All the previous rules will we deleted. Do you want to continue?" })
        }
        else if (type === "3") {
            this.setState({ title: "Sync Hierarchy", content: "All the previous will be deleted. Do you want to continue?" })
        }
        this.setState(prev => {
            return { toggleSync: !prev.toggleSync }
        })
        if (status) {
            this.handleSync();
        }
    }

    handleSync = () => {
        const token = {
            token: this.props.token,
            project_id: this.state.projectId
        }
        // console.log(token)
        this.setState({ loading: true });
        let path = '';
        if (this.state.type === "1") {
            path = "/questions/sync"
        }
        else if(this.state.type === "2") {
            path = "/questions/sync/rules"
        }
        else {
            path = "/questions/sync/hierarchy"
        }
        axios.post(path, token).then(res => {
            //console.log(res.data.data.questions);
            if (res.data.status) {
                this.setState({ loading: false, message: res.data.message, error: null });
            }
            else {
                this.setState({ loading: false, error: res.data.message, message: null });
            }
        }).catch(error => {
            console.log(error);
            this.setState({ loading: false, error: "Something went wrong. Please try again.", message: null });
        });
    }

    inputChangeHandler = (e, name) => {
        this.setState({ [name]: e.target.value })
    }

    render() {

        return (
            <AUX>
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card m-b-20" style={{ marginBottom: 10 }}>
                            <div className="card-body" >
                                <h6 className="page-title text-left" title="Sync Questions">Sync Question/Rules</h6>
                                {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} /> : null}
                                {this.state.message ? <Alert classes={"alert-success"} msg={this.state.message} /> : null}
                                {this.state.loading ? <AUX><Spinner /><br /></AUX> : null}

                                <br />
                                <form onSubmit={(e) => this.handleSyncModal(e)}>
                                    <div className="row">
                                        <div className="col-sm-12 col-lg-3 mb-3">
                                            <label>Enter Project Token</label>
                                        </div>
                                        <div className="col-sm-12 col-lg-4 mb-3">
                                            <input required
                                                className="form-control"
                                                placeholder="Enter Project Token"
                                                value={this.state.projectId}
                                                onChange={(e) => this.inputChangeHandler(e, "projectId")} />
                                        </div>
                                        <div className="col-sm-12 col-lg-4 mb-3">
                                            <select
                                                className="form-control"
                                                onChange={(event) => this.inputChangeHandler(event, "type")}
                                                value={this.state.type}>
                                                <option value="1">Questions </option>
                                                <option value="2">Rules</option>
                                                <option value="3">Hierarchy</option>
                                            </select>
                                        </div>

                                        <div className="col-sm-12 text-center">
                                            <button style={{ btnStyle }} type="submit" className="btn btn-primary waves-effect waves-light">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                {
                    this.state.toggleSync
                        ? <ConfirmModal modal={this.state.toggleSync} toggle={() => this.handleSyncModal(null, false)} click={() => this.handleSyncModal(null, true)} title={this.state.title} content={this.state.content} />
                        : false
                }
            </AUX>
        );
    }
}

const mapStateToProps = state => {
    return {
        token: state.auth.token
    }
}

export default connect(mapStateToProps)(SyncQuestion);
