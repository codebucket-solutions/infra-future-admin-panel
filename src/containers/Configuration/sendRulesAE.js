import React, { Component } from 'react';
import { connect } from 'react-redux';
import AUX from '../../hoc/Aux_';
import axios from '../../axios-details';
import Spinner from '../../components/Spinner/simpleSpinner';
import Alert from '../../components/Alert/common.js';
import MySelect from '../../components/ReactSelect';
import { objectToArray, handleKeyValue } from '../../shared/utility';
import AEComponent from '../../components/AEComponent';

import { btnStyle } from '../../util';

class SendRulesAE extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            error: null,
            message: null,
            AllAE: []
           
        }
    }

    componentDidMount() {
        let platform = process.env.REACT_APP_INSIGHT_PLATFORM;
        this.setState({ allTags: handleKeyValue(platform.split(',')) })
        this.fetchAPI()
    }

    fetchAPI = () =>{
        const token = {
            token: this.props.token,
        }
        this.setState({ loading: true });
        axios.get("/fetchAssessment/ae_info", token).then(res => {
            console.log(res.data);
           
            if(res.data.status){
                this.setState({
                    AllAE:res.data.data,
                    loading: false,
                })
            }
        }).catch(error => {
            console.log(error);
            this.setState({ loading: false, error: "Something went wrong. Please try again.", message: null });
        });
    }
   
    sendDataToAE = () => {
        const token = {
            token: this.props.token,
        }
        this.setState({ loading: true });
        axios.post("/fetchAssessment/rules", token).then(res => {
            //console.log(res.data.data.questions);
            if (res.data.status) {
                this.setState({ loading: false, message: res.data.message, error: null });
            }
            else {
                this.setState({ loading: false, error: res.data.message, message: null });
            }
        }).catch(error => {
            console.log(error);
            this.setState({ loading: false, error: "Something went wrong. Please try again.", message: null });
        });
    }

    handleSubmit = (apiData) =>{
        
        apiData = {
            apiData:apiData, 
            token: this.props.token,
            
        }
        console.log(apiData);
        this.setState({ loading: true });
        axios.post("/fetchAssessment/ae_info", apiData).then(res => {
            //console.log(res.data.data.questions);
            if (res.data.status) {
                this.setState({ loading: false, message: res.data.message, error: null });
            }
            else {
                this.setState({ loading: false, error: res.data.message, message: null });
            }
        }).catch(error => {
            console.log(error);
            this.setState({ loading: false, error: "Something went wrong. Please try again.", message: null });
        });
    }

    
    render() {
        console.log(this.state)
        return (
            <AUX>
                <div className="row" style={{marginBottom:50}}>
                    <div className="col-lg-12">
                        <div className="card" style={{ marginBottom: 10 }}>
                            <div className="card-body" >
                                <h6 className="page-title text-left" title="Sync Questions">AE Configuration</h6>
                                {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} /> : null}
                                {this.state.message ? <Alert classes={"alert-success"} msg={this.state.message} /> : null}
                                {this.state.loading ? <AUX><Spinner /><br /></AUX> : null}
                                
                                {
                                    this.state.AllAE && this.state.AllAE.length > 0
                                    ?
                                    <AEComponent
                                        allAE={this.state.AllAE}
                                        handleSubmit={this.handleSubmit}
                                    />
                                    : null
                                }
                                
                                
                                
                                <br />
                                <div className="row">
                                    <div className="col-sm-3 col-lg-3 mb-3">
                                        <label>Send Rules To AE</label>
                                    </div>
                                    <div className="col-sm-6 text-center">
                                        <button 
                                            style={{ btnStyle }} 
                                            onClick={() => this.sendDataToAE()}
                                            type="button" className="btn btn-primary waves-effect waves-light">Sync Rules With AE</button>
                                    </div>
                                    <div className="col-sm-3 col-lg-3 mb-3">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </AUX>
        );
    }
}

const mapStateToProps = state => {
    return {
        token: state.auth.token
    }
}

export default connect(mapStateToProps)(SendRulesAE);
