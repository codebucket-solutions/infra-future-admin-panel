import React, { useState } from 'react';
import { connect } from 'react-redux';
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import axios from '../../axios-details';
import Spinner from '../../components/Spinner/simpleSpinner';
import Alert from '../../components/Alert/common';
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';

class PresignedURLMailContent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            error: null,
            message: null,
            content: EditorState.createEmpty()
        }
    }

    fetchAPI = () => {
        const config = {
            token: this.props.token,
            type: "mail_content"
        };

        console.log(config)
        axios.post('/presigned_url/get_data_by_id', config)
            .then(res => {
                console.log(res.data);
                const contentBlock = htmlToDraft(res.data.data.value);
                if (contentBlock) {
                    const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
                    const editorState = EditorState.createWithContent(contentState);
                    this.setState({
                        content: editorState,
                    });
                }
                // this.setState({ content: res.data.data.value })

            })
            .catch(err => {
                console.error(err)
            });
    }

    componentDidMount() {
        this.fetchAPI();
    }

    submitUrlExpiryDate = (e) => {
        e.preventDefault();
        this.setState({ loading: true });
        const config = {
            token: this.props.token,
            type: "mail_content",
            content: draftToHtml(convertToRaw(this.state.content.getCurrentContent()))
        };

        console.log(config)
        axios.post('/presigned_url/update_data_by_id', config)
            .then(res => {
                console.log(res)
                if (res.data.status)
                    this.setState({ message: res.data.message, loading: false });
                else
                    this.setState({ error: res.data.message, loading: false });
            })
            .catch(err => {
                this.setState({ error: "Something went wrong. Please try again", loading: false });
            });

    }

    getSelectedDate = (e) => {
        this.setState({ content: e});
    }
    render() {
        return <>
            <div className="row">
                <div className="col-lg-12">
                    <div className="card" style={{marginBottom:10}}>
                        <div className="card-body">
                            <h6 className="page-title text-left">Url Mail Content</h6>
                            {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} /> : null}
                            {this.state.message ? <Alert classes={"alert-success"} msg={this.state.message} /> : null}
                            {this.state.loading ? <><Spinner /><br /></> : null}
                            <br />
                            <form onSubmit={(e) => this.submitUrlExpiryDate(e)}>
                                <div className="row">
                                    <div className="col-sm-3 col-lg-3 mb-3">
                                        <label>Mail Content</label>
                                    </div>
                                    <div className="col-sm-12 col-lg-9">
                                        <Editor
                                            editorState={this.state.content}
                                            toolbarClassName="toolbarClassName"
                                            wrapperClassName="editor-mail"
                                            editorClassName="editor-box"
                                            onEditorStateChange={this.getSelectedDate}
                                            className="editor-mail"
                                        />
                                        {/* <textarea rows="3" value={this.state.content} className="form-control" required onChange={(e) => this.getSelectedDate(e)} /> */}
                                    </div>
                                    <div className="col-sm-12 text-center" >
                                        <br />
                                        <button type="submit" className="btn btn-primary waves-effect waves-light">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </>
    }
}

const mapStateToProps = state => {
    return {
        token: state.auth.token
    }
}

export default connect(mapStateToProps)(PresignedURLMailContent);