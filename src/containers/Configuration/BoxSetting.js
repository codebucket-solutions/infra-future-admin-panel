import React, { Component } from 'react';
import { connect } from 'react-redux';
import { handleKeyValue, objectToArray } from '../../shared/utility'
import AUX from '../../hoc/Aux_';
import axios from '../../axios-details';
import MySelect from '../../components/ReactSelect';

import Spinner from '../../components/Spinner/simpleSpinner';
import Alert from '../../components/Alert/common.js';
import { btnStyle } from '../../util';

class BoxSettings extends Component {

    constructor(props) {
        super(props);
        this.state = {

            loading: false,
            error: null,
            message: null,

            allGroups: [1, 2, 3, 4],
            allBasicGraph: [],
            allGraphs: [],
            selectedGraph: [],

        }
    }

    fetchSetting = () => {
        this.setState({ loading: true });
        const config = {
            headers: { Authorization: `Bearer ${this.props.token}` }
        };
        axios.get('/import_setting/get_box', config)
            .then(res => {
                console.log(res.data)
                const { data } = res.data;
                let allBasicGraph = [];
                let selectedGraph = [];
                let allGraphs = [];
                let selected = [];
                if (res.data.status) {

                    data.forEach((d, i) => {
                        allBasicGraph.push(d.name)
                        allGraphs.push(d.name)

                        if (d.groups_struct) {

                            let _tempStruct = d.groups_struct.split(',');
                            console.log(_tempStruct);
                            _tempStruct.forEach(t => {
                                if (!selectedGraph[t]) {
                                    selectedGraph[t] = []
                                }
                                selectedGraph[t].push(d.name)
                            })
                        }
                    })

                    selectedGraph.forEach((d, i) => {
                        if (d) {
                            selected[i] = handleKeyValue(d)
                        }
                        else {
                            selected[i] = []
                        }

                    })
                    console.log(selected)

                }
                this.setState({
                    loading: false,
                    allBasicGraph: handleKeyValue(allBasicGraph),
                    selectedGraph: selected
                })

            })
            .catch(err => {
                console.log(err);
                this.setState({ loading: false })
            });
    }

    componentDidMount() {
        this.fetchSetting();
    }

    selectTowChange = (e, group) => {
        let { selectedGraph } = this.state;
        selectedGraph[group] = e;
        this.setState({ selectedGraph: selectedGraph });
    }

    //disable import
    submitHandler = (e) => {
        e.preventDefault();
        this.setState({ loading: false, error: null, message: null });

        let selected = []
        this.state.selectedGraph.forEach((d, i) => {
            if (d) {
                selected[i] = objectToArray(d)
            }
            else {
                selected[i] = []
            }

        })


        const data = {
            token: this.props.token,
            groups: selected,
        }
        console.log(data)
        axios.post('/import_setting/set_box', data)
            .then(res => {
                if (res.data.status) {
                    this.setState({ loading: false, message: res.data.message })
                    this.fetchSetting();
                }
                else {
                    this.setState({ loading: false, error: res.data.message })
                }
            })
            .catch(err => {
                this.setState({ loading: false, error: "Something went wrong. Please try again later." });
            })
    }

    render() {
        const { allBasicGraph, allAdvancedGraph, selectedGraph, allGroups } = this.state;
        return (
            <AUX>
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card m-b-20" style={{ marginBottom: '10px' }}>
                            <div className="card-body" >
                                <h6 className="page-title text-left" title="">Stats Box List Items</h6>
                                {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} /> : null}
                                {this.state.message ? <Alert classes={"alert-success"} msg={this.state.message} /> : null}
                                {this.state.loading ? <AUX><Spinner /><br /></AUX> : null}

                                <br />
                                <form onSubmit={this.submitHandler}>
                                    <div className="row">
                                        {
                                            allGroups.map((g, i) => {
                                                console.log(g)
                                                return (
                                                    <>
                                                        <div key={g + i} className="col-sm-12 col-lg-3 mb-3">
                                                            <label>{"Group " + allGroups[i]}</label>
                                                        </div>
                                                        <div key={g + i} className="col-sm-12 col-lg-9 mb-9">

                                                            <MySelect
                                                                value={selectedGraph[allGroups[i]]}
                                                                onChange={(e) => this.selectTowChange(e, allGroups[i])}
                                                                options={allBasicGraph}
                                                                isMulti={true}
                                                                noMin={true}
                                                                allowSelectAll={true}
                                                            />
                                                        </div>

                                                    </>
                                                )
                                            })
                                        }

                                        <div className="col-sm-12 text-center">
                                            <br />
                                            <button style={{ btnStyle }} type="submit" className="btn btn-primary waves-effect waves-light">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </AUX>
        );
    }
}

const mapStateToProps = state => {
    return {
        token: state.auth.token
    }
}

export default connect(mapStateToProps)(BoxSettings);
