import React, { Component } from 'react';
import { connect } from 'react-redux';
import AUX from '../../hoc/Aux_';
import axios from '../../axios-details';
import Spinner from '../../components/Spinner/simpleSpinner';
import Alert from '../../components/Alert/common.js';
import MySelect from '../../components/ReactSelect';
import { objectToArray, handleKeyValue } from '../../shared/utility'

import { btnStyle } from '../../util';

class SendRulesAE extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            error: null,
            message: null,

            allTags:[],
            selectedTags: [],
            assessmentTypes:'',
            api:'',
            allApi:[]
        }
    }

    componentDidMount() {
        let platform = process.env.REACT_APP_INSIGHT_PLATFORM;
        let _allApi = [
            {
                id:"api_old",
                api:process.env.REACT_APP_ASSESSMENT_API
            },
            {
                id:"api_generic",
                api:process.env.REACT_APP_ASSESSMENT_API_GENERIC
            }
        ]   
        this.fetchAPI(_allApi)
        this.setState({ allTags: handleKeyValue(platform.split(',')), allApi: _allApi })

    }

    fetchAPI = (allApi) =>{
        const token = {
            token: this.props.token,
        }
        this.setState({ loading: true });
        axios.get("/fetchAssessment/ae_info", token).then(res => {
            console.log(res.data);
            console.log(allApi);
            if(res.data.status){
                console.log(allApi.filter(a => a.id === res.data.data[0].type))
                this.setState({
                    api:allApi.filter(a => a.id === res.data.data[0].type)[0].id,
                    assessmentTypes:res.data.data[1].details,
                    selectedTags:handleKeyValue(res.data.data[2].details.split(',')),
                    loading: false,
                })
            }
        }).catch(error => {
            console.log(error);
            this.setState({ loading: false, error: "Something went wrong. Please try again.", message: null });
        });
    }
   
    sendDataToAE = () => {
        const token = {
            token: this.props.token,
        }
        this.setState({ loading: true });
        axios.post("/fetchAssessment/rules", token).then(res => {
            //console.log(res.data.data.questions);
            if (res.data.status) {
                this.setState({ loading: false, message: res.data.message, error: null });
            }
            else {
                this.setState({ loading: false, error: res.data.message, message: null });
            }
        }).catch(error => {
            console.log(error);
            this.setState({ loading: false, error: "Something went wrong. Please try again.", message: null });
        });
    }

    selectTowChangeFilter =(e) =>{
        this.setState({ selectedTags:e})
    }

    handleChange = (e, state) =>{
        console.log(e.target)
        this.setState({[state]: e.target.value})
    }

    handleSubmit = (e) =>{
        e.preventDefault();
        const token = {
            token: this.props.token,
            api:this.state.allApi.filter(a => a.id === this.state.api),
            assessment_types: this.state.assessmentTypes,
            cloud_platform:objectToArray(this.state.selectedTags)
        }
        console.log(token);
        this.setState({ loading: true });
        axios.post("/fetchAssessment/ae_info", token).then(res => {
            //console.log(res.data.data.questions);
            if (res.data.status) {
                this.setState({ loading: false, message: res.data.message, error: null });
            }
            else {
                this.setState({ loading: false, error: res.data.message, message: null });
            }
        }).catch(error => {
            console.log(error);
            this.setState({ loading: false, error: "Something went wrong. Please try again.", message: null });
        });
    }

    
    render() {
        console.log(this.state)
        return (
            <AUX>
                <div className="row" style={{marginBottom:50}}>
                    <div className="col-lg-12">
                        <div className="card" style={{ marginBottom: 10 }}>
                            <div className="card-body" >
                                <h6 className="page-title text-left" title="Sync Questions">AE Configuration</h6>
                                {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} /> : null}
                                {this.state.message ? <Alert classes={"alert-success"} msg={this.state.message} /> : null}
                                {this.state.loading ? <AUX><Spinner /><br /></AUX> : null}

                                
                                <form onSubmit={(e) => this.handleSubmit(e)}>
                                    <div className="row">
                                        <div className="col-sm-3 col-lg-3 mb-3">
                                            <label>AE API</label>
                                        </div>
                                        <div className="col-sm-9 text-center">
                                            <select
                                                className="form-control"
                                                onChange={(e) =>this.handleChange(e, "api")}
                                                value={this.state.api}
                                            >
                                                <option value=''>Select</option>
                                                {
                                                    this.state.allApi.map(a =>(
                                                        <option value={a.id}>{a.api}</option>
                                                    ))
                                                }
                                            </select>   
                                        </div>

                                        <div className="col-sm-3 col-lg-3 mb-3">
                                            <label>Assessment Input</label>
                                        </div>
                                        <div className="col-sm-9 text-center">
                                            <input 
                                                type="text"
                                                className="form-control"
                                                required
                                                onChange={(e) => this.handleChange(e,"assessmentTypes" )}
                                                value={this.state.assessmentTypes}

                                            />
                                        </div>

                                        <div className="col-sm-3 col-lg-3 mb-3">
                                            <label>Cloud Platform</label>
                                        </div>
                                        <div className="col-sm-9 text-center">
                                            <MySelect
                                                options={this.state.allTags}
                                                isMulti
                                                noMin = {true}
                                                onChange={(e) => this.selectTowChangeFilter(e)}
                                                allowSelectAll={true}
                                                value={this.state.selectedTags}
                                            
                                            />
                                        </div>
                                        
                                        <div className="col-sm-12 col-lg-13 mb-3 mt-2 text-center">
                                            <button 
                                                style={{ btnStyle }} 
                                                type="submit" 
                                                className="btn btn-primary waves-effect waves-light">Submit</button>
                                        </div>
                                    </div>
                                </form>
                                <br />
                                <div className="row">
                                    <div className="col-sm-3 col-lg-3 mb-3">
                                        <label>Send Rules To AE</label>
                                    </div>
                                    <div className="col-sm-6 text-center">
                                        <button 
                                            style={{ btnStyle }} 
                                            onClick={() => this.sendDataToAE()}
                                            type="button" className="btn btn-primary waves-effect waves-light">Sync Rules With AE</button>
                                    </div>
                                    <div className="col-sm-3 col-lg-3 mb-3">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </AUX>
        );
    }
}

const mapStateToProps = state => {
    return {
        token: state.auth.token
    }
}

export default connect(mapStateToProps)(SendRulesAE);
