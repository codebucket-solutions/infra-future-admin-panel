import React, { Component } from 'react';
import { connect } from 'react-redux';

import AUX from '../../hoc/Aux_';
import axios from '../../axios-details';
import MySelect from '../../components/ReactSelect';
import { handleKeyValueClient, objectToArray } from '../../shared/utility';

import Spinner from '../../components/Spinner/simpleSpinner';
import Alert from '../../components/Alert/common.js';
import { btnStyle } from '../../util';

class Configuration extends Component {

    constructor(props) {
        super(props);
        this.state = {

            loading: false,
            error: null,
            message: null,

            allClients: [],

            responseImport: [],
            appImport: [],

        }
    }

    fetchClients = () => {
        this.setState({ loading: true, error: null, message: null });
        const config = {
            headers: { Authorization: `Bearer ${this.props.token}` }
        };
        axios.get('/client', config)
            .then(res => {

                if (res.data.status) {

                    this.setState({ loading: false, allClients: handleKeyValueClient(res.data.data.clients) })
                    this.fetchSetting()
                }
                else {
                    this.setState({ loaded: false });
                }
            })
            .catch(err => {
                console.log(err);
                this.setState({ loading: false })
            });
    }

    fetchSetting = () => {
        this.setState({ loading: true });
        const config = {
            headers: { Authorization: `Bearer ${this.props.token}` }
        };
        axios.get('/import_setting/', config)
            .then(res => {
                // console.log(res.data.data);
                const { data } = res.data;
                if (res.data.status) {
                    this.setState({
                        appImport: handleKeyValueClient(data.app_import),
                        responseImport: handleKeyValueClient(data.response_import),
                    })
                }
                this.setState({ loading: false })
            })
            .catch(err => {
                this.setState({ loading: false })
            });
    }

    componentDidMount() {
        this.fetchClients();
    }

    selectTowChange = (e, state) => {
        this.setState({ [state]: e });
    }

    //disable import
    submitHandler = (e) => {
        e.preventDefault();
        this.setState({ loading: false, error: null, message: null });
        const data = {
            token: this.props.token,
            app_import: objectToArray(this.state.responseImport),
            response_import: objectToArray(this.state.appImport)
        }
        //console.log(data);
        axios.post('/import_setting/', data)
            .then(res => {
                //console.log(res.data);
                if (res.data.status) {
                    this.setState({ loading: false, message: res.data.message })
                }
                else {
                    this.setState({ loading: false, error: res.data.message })
                }
            })
            .catch(err => {
                this.setState({ loading: false, error: "Something went wrong. Please try again later." });
            })
    }

    render() {
        const { allClients, responseImport, appImport } = this.state;
        return (
            <AUX>
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card m-b-20" style={{ marginBottom: '10px' }}>
                            <div className="card-body" >
                                <h6 className="page-title text-left" title="Import settings to set restrictions to bulk import. These settings disable the option for bulk import of applications and responses using csv files in the user interface. The settings are applied to all users of the selected client. Users can continue to add applications using add application button under Applications.">Import Settings</h6>
                                {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} /> : null}
                                {this.state.message ? <Alert classes={"alert-success"} msg={this.state.message} /> : null}
                                {this.state.loading ? <AUX><Spinner /><br /></AUX> : null}

                                <br />
                                <form onSubmit={this.submitHandler}>
                                    <div className="row">
                                        <div className="col-sm-12 col-lg-3 mb-3">
                                            <label>Disable Application Import</label>
                                        </div>
                                        <div className="col-sm-12 col-lg-8 mb-3">
                                            <MySelect
                                                value={appImport}
                                                onChange={(e) => this.selectTowChange(e, "appImport")}
                                                options={allClients}
                                                allowSelectAll={true}
                                                isMulti={true}
                                                noMin={true}
                                            />
                                        </div>
                                        <div className="col-sm-12 col-lg-1 mb-3"></div>

                                        <div className="col-sm-12 col-lg-3 mb-3">
                                            <label>Disable Responses Import</label>
                                        </div>
                                        <div className="col-sm-12 col-lg-8 mb-3">
                                            <MySelect
                                                value={responseImport}
                                                onChange={(e) => this.selectTowChange(e, "responseImport")}
                                                options={allClients}
                                                allowSelectAll={true}
                                                isMulti={true}
                                                noMin={true}
                                            />
                                        </div>
                                        <div className="col-sm-12 col-lg-1 mb-3"></div>

                                        <div className="col-sm-12 text-center">
                                            <button style={{ btnStyle }} type="submit" className="btn btn-primary waves-effect waves-light">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </AUX>
        );
    }
}

const mapStateToProps = state => {
    return {
        token: state.auth.token
    }
}

export default connect(mapStateToProps)(Configuration);
