import React, { Component } from 'react';
import { connect } from 'react-redux';
import makeAnimated from "react-select/animated";
import AUX from '../../hoc/Aux_';
import axios from '../../axios-details';
import { handleKeyValueClient, objectToArray, handleKeyValue } from '../../shared/utility';
import MySelect from '../../components/ReactSelect';
import Spinner from '../../components/Spinner/simpleSpinner';
import Alert from '../../components/Alert/common.js';
import { btnStyle } from '../../util';
const animatedComponents = makeAnimated();

class SidebarConfig extends Component {

    constructor(props) {
        super(props);
        this.state = {

            loading: false,
            error: null,
            message: null,
            allClients: [],
            sidebar: null,
            selectedClients: []

        }
    }

    componentDidMount() {
        this.fetchClient();
    }

    fetchClient = () => {
        const config = {
            headers: { Authorization: `Bearer ${this.props.token}` }
        };
        //console.log(config)
        axios.get('/user/userclient', config)
            .then(res => {

                let clients = handleKeyValueClient(res.data.data.clients);
                //console.log(clients);
                this.setState({ allClients: clients })
                this.fetchSelectedClient();
            })
            .catch(err => {
                this.setState({ error: "Something went wrong." })
            });
    }

    fetchSelectedClient = () => {
        const token = {
            token: this.props.token
        }
        axios.post("/import_setting/get_sidebar", token)
            .then(res => {
                // console.log(res.data)
                if (res.data.status) {
                    let clients = handleKeyValueClient(res.data.data.value);
                    // console.log(clients);
                    this.setState({ selectedClients: clients })
                }
            })
            .catch(err => {
                console.log(err);
            })
    }

    handleSidebarSubmit = (e) => {
        e.preventDefault();
        const token = {
            token: this.props.token,
            value: objectToArray(this.state.selectedClients),
        }
        console.log(token)
        this.setState({ loading: true });
        axios.post("/import_setting/set_sidebar", token).then(res => {
            //console.log(res.data.data.questions);
            if (res.data.status) {
                this.setState({ loading: false, message: res.data.message, error: null });
            }
            else {
                this.setState({ loading: false, error: res.data.message, message: null });
            }
        }).catch(error => {
            console.log(error);
            this.setState({ loading: false, error: "Something went wrong. Please try again.", message: null });
        });
    }

    handleSidebar = (e) => {
        this.setState({ sidebar: e.target.value })
    }
    selectTowChange = (e) => {
        console.log(e);
        this.setState({ selectedClients: e })
    }

    render() {
        // console.log(this.state)
        return (
            <AUX>
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card" style={{ marginBottom: 10 }}>
                            <div className="card-body" >
                                <h6 className="page-title text-left" title="Sync Questions">Sidebar Configuration</h6>
                                {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} /> : null}
                                {this.state.message ? <Alert classes={"alert-success"} msg={this.state.message} /> : null}
                                {this.state.loading ? <AUX><Spinner /><br /></AUX> : null}

                                <br />
                                <form onSubmit={(e) => this.handleSidebarSubmit(e)}>
                                    <div className="row">
                                        <div className="col-sm-3 col-lg-3">
                                            <label>Disable Sidebar</label>
                                        </div>
                                        <div className="col-sm-12 col-lg-9 mb-3">

                                            <MySelect
                                                options={this.state.allClients}
                                                isMulti
                                                noMin={true}
                                                components={animatedComponents}
                                                onChange={(e) => this.selectTowChange(e)}
                                                allowSelectAll={false}
                                                value={this.state.selectedClients}

                                            />

                                        </div>

                                        <div className="col-sm-12 text-center">
                                            <button style={{ btnStyle }} type="submit" className="btn btn-primary waves-effect waves-light">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </AUX>
        );
    }
}

const mapStateToProps = state => {
    return {
        token: state.auth.token
    }
}

export default connect(mapStateToProps)(SidebarConfig);
