import React, { Component } from 'react';
import { connect } from 'react-redux';
import AUX from '../../hoc/Aux_';
import axios from '../../axios-details';
import Spinner from '../../components/Spinner/simpleSpinner';
import Alert from '../../components/Alert/common.js';
import MySelect from '../../components/ReactSelect';
import { objectToArray, handleKeyValue } from '../../shared/utility';
import AEComponent from '../../components/AEComponent';

import { btnStyle } from '../../util';

class SendRulesAE extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            error: null,
            message: null,
           
            ip:'',
            port:'',
            info:false,
            warning:false,
            errorS:false,
        }
    }

    componentDidMount() {
        this.fetchAPI()
    }

    fetchAPI = () =>{
        const config = {
            headers: { Authorization: `Bearer ${this.props.token}` }
        };
        this.setState({ loading: true });
        axios.get("/syslog_config", config).then(res => {
            console.log(res.data);
            let syslog_config = res.data.data.syslog_config;
            console.log(syslog_config)
            if(res.data.status){
                this.setState({
                    ip:syslog_config[0].ip,
                    port:syslog_config[0].port,
                    loading: false,
                    info:parseInt(syslog_config[0].info) >= 1 ? true : false,
                    warning:parseInt(syslog_config[0].warning) >= 1 ? true : false,
                    errorS:parseInt(syslog_config[0].error) >= 1 ? true : false,
                })
            }
        }).catch(error => {
            console.log(error);
            this.setState({ loading: false, error: "Something went wrong. Please try again.", message: null });
        });
    }


    handleSubmit = (e) =>{
        e.preventDefault();
        let apiData = {
            ip:this.state.ip,
            port:this.state.port, 
            info:this.state.info ? 1 : 0,
            warning:this.state.warning ? 1 : 0,
            error:this.state.errorS ? 1 : 0,
            token: this.props.token,
            
        }
        this.setState({ loading: true });
        axios.post("/syslog_config", apiData).then(res => {
            //console.log(res.data.data.questions);
            if (res.data.status) {
                this.setState({ loading: false, message: res.data.message, error: null });
            }
            else {
                this.setState({ loading: false, error: res.data.message, message: null });
            }
        }).catch(error => {
            console.log(error);
            this.setState({ loading: false, error: "Something went wrong. Please try again.", message: null });
        });
    }

    handleChange = (e, state) =>{
        this.setState({[state]: e.target.value})
    }

    handleCheck = (state) =>{
        this.setState({[state]: !this.state[state]})
    }

    
    render() {
        console.log(this.state)
        return (
            <AUX>
                <div className="row" style={{marginBottom:50}}>
                    <div className="col-lg-12">
                        <div className="card" style={{ marginBottom: 10 }}>
                            <div className="card-body" >
                                {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} /> : null}
                                {this.state.message ? <Alert classes={"alert-success"} msg={this.state.message} /> : null}
                                {this.state.loading ? <AUX><Spinner /><br /></AUX> : null}
                                
                                
                                
                                <br />
                                <form onSubmit={this.handleSubmit}>
                                    <div className="row">
                                        <div className="col-sm-12 col-lg-12 mb-3">
                                            <h6>Syslog Configuration</h6>
                                        </div>
                                        <div className="col-sm-3 col-lg-3 mb-3">
                                            <label>IP/Domain</label>
                                        </div>
                                        <div className="col-sm-8 text-center">
                                            <input  
                                                type="text"
                                                required
                                                className="form-control"
                                                onChange={(e) =>this.handleChange(e, "ip")}
                                                value={this.state.ip}
                                            />
                                        </div>
                                        <div className="col-sm-3 col-lg-3 mb-3">
                                            <label>Port</label>
                                        </div>
                                        <div className="col-sm-8 text-center">
                                            <input  
                                                type="text"
                                                required
                                                className="form-control"
                                                onChange={(e) =>this.handleChange(e, "port")}
                                                value={this.state.port}
                                            />
                                        </div>
                                        <div className="col-sm-3 col-lg-3 mb-3">
                                           
                                        </div>
                                        <div className="col-sm-1">
                                            <label>Info</label>
                                        </div>
                                        <div className="col-sm-1">
                                            <input  
                                                type="checkbox"
                                                
                                                style={{height:18}} 
                                                className="form-control"
                                                onChange={(e) =>this.handleCheck("info")}
                                                checked={this.state.info ? true : false}
                                            />
                                        </div>
                                        <div className="col-sm-1">
                                            <label>Warning</label>
                                        </div>
                                        <div className="col-sm-1">
                                            <input  
                                                type="checkbox"
                                                
                                                style={{height:18}} 
                                                className="form-control"
                                                onChange={(e) =>this.handleCheck( "warning")}
                                                checked={this.state.warning ? true : false}
                                            />
                                            
                                        </div>
                                        <div className="col-sm-1">
                                            <label>Error</label>
                                        </div>
                                        <div className="col-sm-1">
                                            <input 
                                                style={{height:18}} 
                                                type="checkbox"
                                                
                                                className="form-control"
                                                onChange={(e) =>this.handleCheck( "errorS")}
                                                checked={this.state.errorS ? true : false}
                                            />
                                        </div>

                                        <div className="col-sm-12 text-center mt-4">
                                            <button 
                                                style={{ btnStyle }} 
                                                type="submit" className="btn btn-primary waves-effect waves-light">Submit</button>
                                        </div>    
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </AUX>
        );
    }
}

const mapStateToProps = state => {
    return {
        token: state.auth.token
    }
}

export default connect(mapStateToProps)(SendRulesAE);
