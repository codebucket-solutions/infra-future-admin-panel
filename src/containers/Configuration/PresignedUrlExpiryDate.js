import React, { useState } from 'react';
import { connect } from 'react-redux';

import axios from '../../axios-details';
import Spinner from '../../components/Spinner/simpleSpinner';
import Alert from '../../components/Alert/common';
import { btnStyle } from '../../util';

class PresignedUrlExpiryDate extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            error: null,
            message: null,
            selectedDate: null
        }
    }

    fetchAPI = () => {
        const config = {
            token: this.props.token,
            type: "expiry_date"
        };

        console.log(config)
        axios.post('/presigned_url/get_data_by_id', config)
            .then(res => {
                console.log(res.data);
                this.setState({ selectedDate: res.data.data.value })

            })
            .catch(err => {
                console.error(err)
            });
    }



    componentDidMount() {
        this.fetchAPI();
    }

    submitUrlExpiryDate = (e) => {
        e.preventDefault();
        this.setState({ loading: true });
        const config = {
            token: this.props.token,
            type: "expiry_date",
            content: this.state.selectedDate
        };

        console.log(config)
        axios.post('/presigned_url/update_data_by_id', config)
            .then(res => {
                if (res.data.status)
                    this.setState({ message: res.data.message, loading: false });
                else
                    this.setState({ error: res.data.message, loading: false });
            })
            .catch(err => {
                this.setState({ error: "Something went wrong. Please try again", loading: false });
            });

    }

    getSelectedDate = (e) => {
        this.setState({ selectedDate: e.target.value });
    }

    render() {


        return <>
            <div className="row">
                <div className="col-lg-12">
                    <div className="card " style={{ marginBottom: 10 }}>
                        <div className="card-body">
                            <h6 className="page-title text-left">Url Expiry Date</h6>
                            {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} /> : null}
                            {this.state.message ? <Alert classes={"alert-success"} msg={this.state.message} /> : null}
                            {this.state.loading ? <><Spinner /><br /></> : null}

                            <br />
                            <form onSubmit={(e) => this.submitUrlExpiryDate(e)}>
                                <div className="row">
                                    <div className="col-sm-3 col-lg-3 ">
                                        <label>Select URL Expiry</label>
                                    </div>
                                    <div className="col-sm-12 col-lg-9 mb-3">
                                        <input type="date" className="form-control" value={this.state.selectedDate} required onChange={(e) => this.getSelectedDate(e)} />
                                    </div>
                                    <div className="col-sm-12 text-center" >
                                        <button type="submit" className="btn btn-primary waves-effect waves-light">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </>
    }
}

const mapStateToProps = state => {
    return {
        token: state.auth.token
    }
}
export default connect(mapStateToProps)(PresignedUrlExpiryDate);