import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from 'react-redux';

// import AUX from '../../hoc/Aux_';
import axios from '../../axios-details';

import Spinner from '../../components/Spinner/simpleSpinner';
import Alert from '../../components/Alert/common';
import Star from '../../components/Required/star';

class modalImport extends Component {

    constructor(props) {
        super(props);
        this.state={
          file:'',
          
          loading:false,
          message:null,
          error:null,

        };
    }

    fileChangeHandler = (event, controlName) =>{
      this.setState({[controlName]:event.target.files[0]});
    }

    closeModal = () =>{
      setTimeout(
        () => this.props.toggle(this.state.update)
        ,700);
    }

    submitHandler = (e) =>{
        e.preventDefault();
        //console.log(this.state);
        this.setState({loading:true,error:null, message: null});

        let formData = new FormData();     
        formData.append('csv_file', this.state.file);
        formData.append('token', this.props.token);
        console.log(this.state.file)
        const config = {     
            headers: { 'content-type': 'multipart/form-data',Authorization: `Bearer ${this.props.token}` }
        }
        console.log(config)
            // upload response of multiple apps of a user
        axios.post('/ahp/import', formData, config)
        .then(res=>{
            console.log(res.data);
            if(res.data.status){
                this.setState({loading:false,update:true, message:"Successfully uploaded all hierarchy."});
                this.closeModal();
            }
            else
                this.setState({loading:false,update:true,error:res.data.message});
                // this.closeModal();
        })
        .catch(err =>{
          console.log(err);
          this.setState({loading:false, error:"Something went wrong.Please try again"})
          // this.closeModal();
        })
              
    }
    
    render() {

        const { type, csvData } = this.state;;
        console.log(type);
        let body = (
        <div className="card-body">
            <form onSubmit={this.submitHandler}>
                {this.state.message ? <Alert classes={"alert-success"} msg={this.state.message} />: null}
                {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} />: null}
                {this.state.loading ?  <Spinner /> : null }
                <div className="row">
                  
                    <div className="col-sm-6 col-md-6 col-lg-6">
                        <div className="form-group">
                            <label htmlFor="name">Upload CSV<Star /></label>
                            <input type="file" required accept=".csv" onChange={(event) => this.fileChangeHandler(event,"file" )} name="file" id="file" className="" />
                        </div>
                    </div>

                    <div className="col-sm-6 col-md-6 col-lg-6">
                        <div className="form-group">
                            <br />
                            <button type="submit" className="btn btn-primary waves-effect waves-light">Upload</button>
                        </div>
                    </div>
                    {
                      type === 1 ? 
                      <div className="col-md-12 text-center" style={{fontSize: '16px',fontWeight: 700}}>
                        <a href='files/Import-Question.csv' style={{color: '#e66161'}} download><u>Download CSV Format to upload Criteria</u></a>
                      </div>
                    : 
                      <div className="col-md-12 text-center" style={{fontSize: '16px',fontWeight: 700}}>
                        
                        
                      </div>
                    }
                    
                </div>
            </form>
        </div>
    );
    
    return (
      <div>
        <Modal size="lg" isOpen={this.props.modal} toggle={() => this.props.toggle(null)} >
          <ModalHeader toggle={() => this.props.toggle(null)}>Upload CSV</ModalHeader>
          <ModalBody>
            {this.state.loading ?   <Spinner /> : body }
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={() => this.props.toggle(null)}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStatetoProps = state =>{
  return {
      token: state.auth.token,
      name:state.auth.name,
  };
}

export default connect(mapStatetoProps)(modalImport);
