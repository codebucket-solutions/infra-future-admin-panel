import React, { Component } from 'react';
import { connect } from 'react-redux';

import Spinner from '../../../components/Spinner/simpleSpinner';
import Alert from '../../../components/Alert/common';
import Star from '../../../components/Required/star';
import ModalLayout from '../../../components/ModalLayout';
import { allAHPCriteria } from '../../../shared/questions'
import { postAPI, getAPI } from '../../../APICall/index';
import { ADD_AHP_CRITERIA, FETCH_SINGLE_AHP_CRITERIA, EDIT_AHP_CRITERIA } from '../../../APICall/urls';

class ModalAddAHP extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      message: null,
      error: null,
      update: false,
      ahp:'',
      ahpType:allAHPCriteria,
      selectedAhpType:'ahp1'
    };
  }

  inputChangeHandler = (event, controlName) => {
    this.setState({ [controlName]: event.target.value });
  }

  udpateStateStatus = (loading, message, error) => {
    this.setState({loading: loading, message: message, error: error})
  }

  submitHandler = async(e) =>{
    this.udpateStateStatus(true)
    e.preventDefault();
    let ahpData;
    let postData = {
      name: this.state.ahp,
      type: this.state.selectedAhpType
    }

    if(this.props.ahpId){
      postData = {
        ...postData,
        ahp_id:this.props.ahpId
      }
      ahpData = await postAPI(EDIT_AHP_CRITERIA, postData);
    }
    else{
      ahpData = await postAPI(ADD_AHP_CRITERIA, postData);
    }
    
    if(ahpData.status){
      this.udpateStateStatus(false, ahpData.message);
      this.closeModal(true);
    }
    else{
      this.udpateStateStatus(false,null, ahpData.message);
      this.closeModal(false);
    }
    
  }

  closeModal = (status) =>{
    setTimeout(
      () => this.props.toggle(status)
      ,700);
  }

  componentDidMount() {
    if(this.props.ahpId)
      this.fetchAHP();
  }

  fetchAHP = async () =>{
    let ahpData = await getAPI(FETCH_SINGLE_AHP_CRITERIA+this.props.ahpId);
    console.log(ahpData.data)
    this.updateState(ahpData.data)
   
  }

  updateState = (ahpData) =>{
      this.setState({ahp:ahpData.name, selectedAhpType: ahpData.type})
  }
  
  render() {
    
    const { ahpType }  = this.state;

    let body = (
      <div className="card-body">
        <form onSubmit={this.submitHandler}>

          {this.state.message ? <Alert classes={"alert-success"} msg={this.state.message} /> : null}
          {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} /> : null}
          {this.state.loading ? <Spinner /> : null}

          <div className="row">
            <div className="col-sm-6 col-md-6 col-lg-6">
              <div className="form-group">
                <label htmlFor="name">Criteria<Star /></label>
                <input 
                  type="text" 
                  placeholder="Add Criteria"  
                  value={this.state.ahp} 
                  required 
                  onChange={(event) => this.inputChangeHandler(event, "ahp")} 
                  className="form-control" 
                />
              </div>
            </div>

            <div className="col-sm-6 col-md-6 col-lg-6">
              <div className="form-group">
                <label htmlFor="name">Criteria Type<Star /></label>
                <select
                  className="form-control"
                  value={this.state.selectedAhpType} 
                  required 
                  onChange={(event) => this.inputChangeHandler(event, "selectedAhpType")} 
                >
                  {
                    ahpType.map((a,i) =>(
                      <option value={a[0]}>{a[1]}</option>
                    ))
                  }
                </select>
                
              </div>
            </div>

            <div className="col-sm-12 col-md-12 col-lg-12 text-center">
              <div className="form-group">
                <br />
                <button type="submit" className="btn btn-primary waves-effect waves-light">Submit</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    );

    return (
      <div>
        <ModalLayout
          size="lg"
          isOpen={this.props.modal}
          toggle={() =>this.props.toggle()}
          title={this.props.title}
        >
          {this.state.loading ? <Spinner /> : body}
        </ModalLayout>
      </div>
    );
  }
}


export default ModalAddAHP;
