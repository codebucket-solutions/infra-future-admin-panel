import React, { Component } from 'react';
import { MDBDataTable } from 'mdbreact';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { CSVLink } from "react-csv";

import fileDownload from 'js-file-download';
import { allAHPCriteria } from '../../shared/questions'
import ModalAddAHP from './ModalAddAHP';
import Spinner from '../../components/Spinner/simpleSpinner';
import ConfirmModal from '../../components/Modal/modalConfirm.js';
import { getAPI, postAPI } from '../../APICall/index';
import { FETCH_ALL_AHP_CRITERIA ,DELETE_AHP_CRITERIA } from '../../APICall/urls';
import Alert  from '../../components/Alert/common';
import '../response.css';
import ModalImport from './modalImport';
import { ahpHeader } from '../../shared/questions';


let selectedAHP = new Set();

const AhpTitle = (id) =>{
    switch(id){
        case "ahp1":
            return {
                label:"AHP Criteria Level 1",
                id:1
            }
            break;
        case "ahp2":
            return {
                label:"AHP Criteria Level 2",
                id:2
            }
            break;
        case "ahp3":
            return {
                label:"AHP Criteria Level 3",
                id:3
            }
            break;
    }
}
class AhpHierarchy extends Component {

    constructor(props) {
        super(props);
        this.state = {
            columns: [
                {
                    label:  <label  className="container mb-4">
                            <input                                 
                                type="checkbox" 
                                value={"all"}
                                onClick={(event) => this.handleCheckboxAll(event, "all")}
                                checked={() => this.checkAllChech(null)}
                                />
                                <span className="checkmark"></span>
                            </label>,
                    field: 'sro',
                    width: 150
                },
                {
                    label: 'Name',
                    field: 'name',
                    sort: 'asc',
                    width: 270
                },
                {
                    label: 'Action',
                    field: 'action',
                    sort: 'asc',
                    width: 100
                },
            ],
            rows: [],
            error:false,
            message:false,

            ahpData:[],
            ahpToEdit:null,
            title:null,
            modalAddAHP:false,
            allAHPCriteria:allAHPCriteria,
            selectedAHPCriteria:"1",
            selectedType:0,
            allAHP:[],
            csvData: [],

            modalImport:false,
        }
    }

    componentDidMount() {
        this.fetchAHP();
    }


    handleCheckboxAll = (event) =>{
        let { allAHP, selectedAHPCriteria } = this.state
        // console.log(allAHP, selectedAHPCriteria)
        if(event.target.checked){
            selectedAHP.clear();
            allAHP.map((a, key) =>{
                if(selectedAHPCriteria === "1"){
                    selectedAHP.add(a.id.toString());
                }
                else{
                    if(selectedAHPCriteria === allAHP[key].type){
                        selectedAHP.add(a.id.toString());
                    }
                }
                
            })
        }
        else{
            selectedAHP.clear();
        }
        this.updateState(allAHP, selectedAHPCriteria);
    }

    checkAllChech = (selectedAHPCriteria) =>{
        // console.log(this.state.allAHP.length , selectedAHP.size)
        let count = 0;
        let _selectedAHPCriteria = selectedAHPCriteria ? selectedAHPCriteria : this.state.selectedAHPCriteria;
        this.state.allAHP.map((a, key) =>{
            
            if(_selectedAHPCriteria === "1"){
                count ++;
            }
            else{
                if(_selectedAHPCriteria === this.state.allAHP[key].type){
                    count ++;
                }
            }
            
        })

        if(selectedAHP.size >= count  )
            return true;
        else
            return false;
    }

    handleCheckbox = (event, id) =>{
        //console.log(event.target, quest_id)

        if(event.target.checked){
            selectedAHP.add(id.toString());
        }
        else{
            selectedAHP.delete(event.target.value)
        }
        
        this.updateState(this.state.allAHP, this.state.selectedAHPCriteria);
    }

    changeCheckBox = (selectedAHPCriteria) =>{
        let { columns } = this.state;
        columns[0].label = (
            <label  className="container mb-4">
                <input                                 
                    type="checkbox" 
                    value={"all"}
                    onClick={(event) => this.handleCheckboxAll(event, "all")}
                    checked={this.checkAllChech(selectedAHPCriteria)}
                />
                    <span className="checkmark"></span>
            </label>
        );
        this.setState({columns: columns});
    }

    fetchAHP = async() => {
        this.setState({ loading: true });
        let ahpData = await getAPI(FETCH_ALL_AHP_CRITERIA)
        // console.log(ahpData.data)
        if (ahpData.status) {
            this.updateState(ahpData.data, "1");    
            this.setState({ loading: false, ahpData: ahpData.data});  
        }
        else{
            this.setState({ loading: false, error: ahpData.message })
        }
    }

    updateState = (ahpData, filter) =>{
        let _ahpData = [];
        // console.log(ahpData)
        let active;
        let csvData = [];
        csvData.push(ahpHeader);
        for (let key in ahpData) {
            csvData.push([ahpData[key].name, AhpTitle(ahpData[key].type).id]);
            active = (
                <>
                    <button onClick={() => this.handleModalAddAHP(false,ahpData[key].id)}
                        className="btn btn-success">Edit</button> &nbsp;&nbsp;
                </>
            )
            if(filter === "1"){
                _ahpData.push({
                    sro: <>
                        <label key={key} className="container">
                                <input                                 
                                type="checkbox" 
                                value={ahpData[key].id}
                                onClick={(event) => this.handleCheckbox(event, ahpData[key].id)}
                                checked={ selectedAHP ? selectedAHP.has(ahpData[key].id.toString()) ? true : false : false}
                                />
                            <span className="checkmark"></span>
                        </label>
                    </>,
                    name: ahpData[key].name,
                    type:AhpTitle(ahpData[key].type).label,
                    action: active,
                })
            }
            else{
                // console.log(filter, ahpData[key].type)
                if(filter === ahpData[key].type){
                    _ahpData.push({
                        sro: <>
                            <label key={key} className="container">
                                    <input                                 
                                    type="checkbox" 
                                    value={ahpData[key].id}
                                    onClick={(event) => this.handleCheckbox(event, ahpData[key].id)}
                                    checked={ selectedAHP ? selectedAHP.has(ahpData[key].id.toString()) ? true : false : false}
                                    />
                                <span className="checkmark"></span>
                            </label>
                        </>,
                        name: ahpData[key].name,
                        type:AhpTitle(ahpData[key].type).label,
                        action: active,
                    })
                }
            }
            
        }
        this.setState({rows: _ahpData, allAHP: ahpData, csvData: csvData});
        this.changeCheckBox(filter);
    }

    handleModalAddAHP = (status, id) =>{
        // console.log(id)
        if(id)
            this.setState({ahpToEdit:id, title: "Edit Criteria"})
        else    
        this.setState({ahpToEdit:null, title: "Add New Criteria"})

        if(status)
            this.fetchAHP();

        this.setState({modalAddAHP: !this.state.modalAddAHP})
    }

    

    handleAHPDelete = () =>{
        this.setState({deleteModal: !this.state.deleteModal})
    }

    submitDelete = async () =>{

        let ahp = [...selectedAHP];
        const token = {
            token: this.props.token,
            ahp_id:  ahp,
        };

        if(ahp.length > 0){
            console.log(token);

            let ahpData = await postAPI(DELETE_AHP_CRITERIA, token)
            this.handleAHPDelete();
            if(ahpData.status){
                this.setState({message: ahpData.message, error:null});
                this.fetchAHP()            }
            else{
                this.setState({error: ahpData.message, message:null});
            }
            console.log(ahpData)
        }
        else{
            this.setState({message: null, error:"Please select atleast one criteria to delete."});
            this.handleAHPDelete();
        }
        
    }

    handleAHPFIlter = (e) =>{
        this.setState({selectedAHPCriteria: e.target.value});
        // console.log(e.target.value)
        this.updateState(this.state.ahpData, e.target.value)
    }

    handleModalImport = (update) =>{

        if(update){
            this.updateState(this.state.allAHP, this.state.selectedAHPCriteria);
        }

        this.setState({
            modalImport: !this.state.modalImport
        })
    }

    
    

    render() {
        // console.log(this.state.modalImport)
        // console.log(this.state.selectedAHPCriteria)
        let filename = "criteria"+Date.now()+".csv";

        const  { allAHPCriteria } = this.state; 
        return (
            <>
                <div className="row">
                    <div className="col-sm-12">
                        <div className="page-title-box">
                            <h4 className="page-title">Criteria</h4>
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                                <li className="breadcrumb-item active">Criteria</li>
                            </ol>
                            {/* <Tinycharts /> */}
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-12">
                        <div className="card m-b-20">
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-md-2"></div>
                                    <div className="col-md-5">
                                        <select
                                            className="form-control"
                                            value={this.state.selectedAHP}
                                            onChange={(e) => this.handleAHPFIlter(e)}
                                        >
                                            <option value={"1"}>All</option>
                                            {
                                                allAHPCriteria.map((a,i) =>(
                                                <option value={a[0]}>{a[1]}</option>
                                                ))
                                            }
                                        </select>

                                    </div>
                                    <div className="col-md-5">
                                        <h4 className="mt-0 header-title text-right">
                                            
                                            
                                            <button className="btn btn-primary mb-3" onClick={() => this.handleModalAddAHP()}>Add New Criteria</button>
                                            &nbsp;

                                            <button className="btn btn-secondary mb-3" onClick={() => this.handleModalImport()}>Import</button>&nbsp;

                                            <CSVLink
                                                className="btn btn-success mb-3"
                                                filename={filename} 
                                                data={this.state.csvData}
                                            >
                                                Export All
                                            </CSVLink>&nbsp;

                                            <button className="btn btn-danger mb-3" onClick={() => this.handleAHPDelete()}>Delete</button>
                                           
                                        </h4>
                                    </div>
                                </div>
                                
                                
                                {this.state.loading ? <><Spinner /><br /></> : null}
                                {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} />: null}
                                {this.state.message ? <Alert classes={"alert-success"} msg={this.state.message} />: null}
                                <div style={{ overflow: 'auto' }}>
                                    <MDBDataTable
                                        bordered
                                        hover
                                        data={{ columns: this.state.columns, rows: this.state.rows }}
                                        info={this.state.rows.length > 0 ? true : false}
                                    />
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                {
                    this.state.modalAddAHP 
                    ?
                    <ModalAddAHP 
                        modal={this.state.modalAddAHP} 
                        title={this.state.title}
                        ahpId={this.state.ahpToEdit}
                        toggle={this.handleModalAddAHP} /> 
                    : null
                }
                {
                    this.state.deleteModal
                    ? <ConfirmModal 
                        modal={this.state.deleteModal} 
                        toggle={() =>this.handleAHPDelete(null, null)} 
                        click={() => this.submitDelete()} 
                        content={"Do you really want to delete this criteria?"}  
                        title={"Delete Criteria"} />
                    : false
                }
                {
                    this.state.modalImport
                    ? <ModalImport 
                        modal={this.state.modalImport} 
                        toggle={this.handleModalImport} 
                    />
                    : false
                }
            </>
        );
    }
}

const mapStatetoProps = state => {
    return {
        token: state.auth.token,
    };
}

export default connect(mapStatetoProps)(AhpHierarchy);