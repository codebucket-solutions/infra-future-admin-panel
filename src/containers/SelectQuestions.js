import React , {Component } from 'react';
import AUX from '../hoc/Aux_';
import makeAnimated from "react-select/animated";
import {  Prompt } from 'react-router-dom';

import axios from '../axios-details';
import { MDBDataTable } from 'mdbreact';
import { connect } from 'react-redux';
import { handleKeyValue,objectToArray } from '../shared/utility';
import Alert  from '../components/Alert/common';

import MySelect from '../components/ReactSelect';
import QuestionModal from '../components/Modal/modalQuestions';
import ImportModal from '../components/Modal/modalImportQuestion';
import ConfirmModal from '../components/Modal/modalConfirm';
import Spinner from '../components/Spinner/simpleSpinner';
import './response.css';

let selectedQuestion = new Set();
const animatedComponents = makeAnimated();
class Questions extends Component{


    constructor(props){
        super(props);
        this.state = {
            questions: [],
            columns: [
                {
                    label:  <label  className="container">
                            <input                                 
                                type="checkbox" 
                                value={"all"}
                                onClick={(event) => this.handleCheckboxAll(event, "all")}
                                checked={() =>this.checkAllChech(null, null, null, null, 1)}
                                />
                                <span className="checkmark"></span>
                            </label>,
                    field: 'export',
                    sort: 'asc',
                },
                {
                    label: 'Questions',
                    field: 'question',
                    sort: 'asc',
                    width: 200
                },
                {
                    label: 'Description',
                    field: 'description',
                    sort: 'asc',
                    width: 200
                },
                {
                    label: 'Type',
                    field: 'type',
                    sort: 'asc',
                    width: 100
                },
                {
                    label: 'Responses',
                    field: 'option',
                    sort: 'asc',
                    width: 100
                },
            ],
            rows: [],
            toggle:false,
            loading:false,
            profileName:'',


            
            allCriteria:[],
            selectedCriteria:[],
            allLevels: [],
            selectedLevel:[],
            allDomains:[],
            selectedDomains:[],
            allTopics:[],
            selectedTopics:[],
            formIsHalfFilledOut:false,
            
        }
        this.domain = React.createRef();
        this.level = React.createRef();
        this.criteria = React.createRef();
        this.topic = React.createRef();
    }

    fetchQuestion = () =>{
        this.setState({ loading:true });
        const config = {
            headers: { Authorization: `Bearer ${this.props.token}` }
        };
        //console.log(this.props.mappingId);
        axios.get('/profile/map/'+this.props.mappingId, config)
            .then(response => {
                let res = response.data;
                console.log(res.data);
                selectedQuestion = new Set();
                res.data.questions.map(q =>{
                    if(q != ""){
                        selectedQuestion.add(q);
                        return q;
                    }
                    
                })
                if(response.data.status){
                    let criteria = handleKeyValue(res.data.criteria);
                    let levels = handleKeyValue(res.data.levels);
                    let domains = handleKeyValue(res.data.domains);
                    let topics = handleKeyValue(res.data.topics);
                    console.log(domains);
                    //console.log(this.state.selectedCriteria.length);

                    // get filter from localStorage
                    const local_domain  = JSON.parse(localStorage.getItem('admin_map_domain_filter'));
                    const local_criteria  = JSON.parse(localStorage.getItem('admin_map_criteria_filter'));
                    const local_level  = JSON.parse(localStorage.getItem('admin_map_level_filter'));
                    const local_topic  = JSON.parse(localStorage.getItem('admin_map_topic_filter'));
                    console.log(local_domain);
                    
                    
                    this.setState({
                        questions: res.data.data_obj,

                        selectedCriteria: (this.state.selectedCriteria.length > 0 ? this.state.selectedCriteria :  (local_criteria && local_criteria.length>0 ? local_criteria: criteria)),
                        allCriteria: criteria,

                        selectedLevel: (this.state.selectedLevel.length > 0 ? this.state.selectedLevel : (local_level && local_level.length>0 ? local_level: levels)),
                        allLevels : levels,

                        selectedDomains: (this.state.selectedDomains.length > 0 ? this.state.selectedDomains : (local_domain && local_domain.length>0 ? local_domain: domains)),
                        allDomains: domains,

                        allTopics: topics,
                        selectedTopics: (this.state.selectedTopics.length > 0 ? this.state.selectedTopics : (local_topic && local_topic.length>0 ? local_topic: topics)),

                        loading:false,
                        profileName: res.data.profile_name
                    })

                    //create table
                    this.createTable(
                        res.data.data_obj,
                        local_criteria && local_criteria.length > 0 ? local_criteria :criteria,
                        local_level && local_level.length > 0 ? local_level :levels,
                        local_domain && local_domain.length > 0 ? local_domain :domains,
                        local_topic && local_topic.length > 0 ? local_topic :topics
                    );

                }
                else{
                    this.setState({loading:false})
                }
            })
            .catch( err => {
                console.log(err);
                this.setState({loading:false})
            });    
    }

    createTable = (data_obj, criteria, levels, domains, topics) => {

        //set filter data to local storage
        localStorage.setItem('admin_map_domain_filter',JSON.stringify(domains));
        localStorage.setItem('admin_map_criteria_filter',JSON.stringify(criteria));
        localStorage.setItem('admin_map_level_filter',JSON.stringify(levels));
        localStorage.setItem('admin_map_topic_filter',JSON.stringify(topics));

        criteria=objectToArray(criteria);
        levels = objectToArray(levels);
        domains = objectToArray(domains);
        topics = objectToArray(topics);

        let fetchQuestions = [];
        for(let key in data_obj){
            
            if((criteria && criteria.includes(data_obj[key].ahp_criteria_lev_1)) && (levels && levels.includes(data_obj[key].levels)) && (domains && domains.includes(data_obj[key].category)) && (topics && topics.some(r=> data_obj[key].topics.indexOf(r) >= 0) ))
            {
               
                //console.log(data_obj);
                fetchQuestions.push({
                    export :  <AUX>
                    <label key={key} className="container">
                        <input                                 
                        type="checkbox" 
                        value={data_obj[key].quest_id}
                        onClick={(event) => this.handleCheckbox(event, data_obj[key].quest_id)}
                        checked={ selectedQuestion ? selectedQuestion.has(data_obj[key].quest_id.toString()) ? true : false : false}
                        />
                        <span className="checkmark"></span>
                    </label>
                    </AUX>,
                    question: data_obj[key].question,
                    description:data_obj[key].description,
                    type: data_obj[key].response_type,
                    option:this.getFetcheddata(data_obj[key].response_json, key),
                }) 
            }
        }
        //console.log(fetchQuestions);
        this.setState({rows: fetchQuestions});
        this.changeExport(criteria, levels, domains, topics);
    }

    changeExport = (criteria, levels, domains, topics) =>{
        let { columns } = this.state;
        columns[0].label = (
            <label  className="container">
                <input                                 
                    type="checkbox" 
                    value={"all"}
                    onClick={(event) => this.handleCheckboxAll(event, "all")}
                    checked={this.checkAllChech(criteria, levels, domains, topics, 2)}
                    />
                    <span className="checkmark"></span>
            </label>
        );
        this.setState({columns: columns});
    }

    checkAllChech(criteria, levels, domains, topics, type){
        //console.log(selectedQuestion);
        let {questions, selectedCriteria, selectedLevel, selectedDomains, selectedTopics} = this.state
        if(type === 1){
            criteria=objectToArray(selectedCriteria);
            levels = objectToArray(selectedLevel);
            domains = objectToArray(selectedDomains);
            topics = objectToArray(selectedTopics);
        }
        
        //console.log(levels);
        if(questions.length > 0){
            var ret = false;
            for(var i =0; i<questions.length; i++){
                if((criteria && criteria.includes(questions[i].ahp_criteria_lev_1)) && (levels && levels.includes(questions[i].levels)) && (domains && domains.includes(questions[i].category)) && (topics && topics.some(r=> questions[i].topics.indexOf(r) >= 0) ))
                {
                    ret =  selectedQuestion.has(questions[i].quest_id.toString()) ;
                    //console.log(questions[i].quest_id.toString()) ;
                    if(ret === false){
                        return false;
                    }
                }
            }
            return true;
        }
        else{
            return false;
        }
       
    }

    handleCheckbox = (event, quest_id) =>{
        //console.log(event.target, quest_id)
        if(event.target.checked){
            selectedQuestion.add(quest_id.toString());
        }
        else{
            selectedQuestion.delete(event.target.value)
        }
        this.setState({formIsHalfFilledOut:true});

        this.createTable(this.state.questions, this.state.selectedCriteria, this.state.selectedLevel,  this.state.selectedDomains, this.state.selectedTopics);
        

    }

    handleCheckboxAll = (event) =>{
        let {questions, selectedCriteria, selectedLevel, selectedDomains, selectedTopics} = this.state
        let criteria=objectToArray(selectedCriteria);
        let levels = objectToArray(selectedLevel);
        let domains = objectToArray(selectedDomains);
        let topics = objectToArray(selectedTopics);

        if(event.target.checked){
            
            questions.map((q,i)=>{
                if((criteria && criteria.includes(q.ahp_criteria_lev_1)) && (levels && levels.includes(q.levels)) && (domains && domains.includes(q.category)) && (topics && topics.some(r=> q.topics.indexOf(r) >= 0) ))
                {
                    selectedQuestion.add(q.quest_id.toString());
                }
                return q;
            })
            this.setState({checked:true});
        }
        else{
            questions.map((q,i)=>{
                if((criteria && criteria.includes(q.ahp_criteria_lev_1)) && (levels && levels.includes(q.levels)) && (domains && domains.includes(q.category)) && (topics && topics.some(r=> q.topics.indexOf(r) >= 0) ))
                {
                    selectedQuestion.delete(q.quest_id.toString());
                }
                return q;
            })
        }
        // this.checkAllChech(selectedQuestion);
        this.setState({formIsHalfFilledOut:true});
        this.createTable(this.state.questions, this.state.selectedCriteria, this.state.selectedLevel,  this.state.selectedDomains, this.state.selectedTopics);
    }

    getFetcheddata = (array, unique) =>{
        let content = [];
        //array = array.JSON.parse();
        //console.log(array);
        array.map((data, index) =>{ 
            //console.log(unique+index+1);
            //content.push( <AUX key={"'"+unique+"'"+index}><label >{data.choice}</label><br /></AUX> );
            return content.push( <p key={"'"+unique+"'"+index}>{data.choice}</p> );
        }) 
        return content; 
    }

    selectTowChangeCriteria= (selectedCriteria) => {
        this.setState({ selectedCriteria:selectedCriteria ?  selectedCriteria : [] });
        this.createTable(this.state.questions, selectedCriteria ? selectedCriteria : [], this.state.selectedLevel,  this.state.selectedDomains, this.state.selectedTopics);
    }

    selectTowChangeDomains= (selectedDomains) => {
        this.setState({ selectedDomains:selectedDomains ? selectedDomains : []  });
        this.createTable(this.state.questions, this.state.selectedCriteria, this.state.selectedLevel, selectedDomains ? selectedDomains : [], this.state.selectedTopics);
    }

    selectTowChangeLevel= (selectedLevel) => {
        this.setState({ selectedLevel:selectedLevel ? selectedLevel : [] });
        this.createTable(this.state.questions, this.state.selectedCriteria, selectedLevel ? selectedLevel : [], this.state.selectedDomains, this.state.selectedTopics);
        
    }

    selectTowChangeTopics= (selectedTopics) => {
        this.setState({ selectedTopics:selectedTopics ? selectedTopics : [] });
        this.createTable(this.state.questions, this.state.selectedCriteria,this.state.selectedLevel, this.state.selectedDomains, selectedTopics ? selectedTopics : []);
        
    }
    
    componentDidMount(){
        this.fetchQuestion();
    }

    handleQuestionsSelected = (event) =>{
        event.preventDefault();
        //console.log(this.state.selectedQuestion);
        this.setState({loading:true, message:null, error:null, formIsHalfFilledOut:false});
        const data = {
            token: this.props.token,
            profile_id: this.props.mappingId,
            questions: [...selectedQuestion],
        }
        //console.log(selectedQuestion.size);
        if(selectedQuestion.size > 0){
            //console.log(data);
            axios.post('/profile/map/', data)
            .then(res => {
                //console.log(res.data.data);
                if(res.data.status) {
                    this.setState({loading:false, isSubmitted:true, message:res.data.message});
                }
                else {
                    this.setState({loading:false, isSubmitted:true, error:res.data.message});
                }
            })
            .catch( err => {
                this.setState({loading:false, isSubmitted:true, error:"Something went wrong. Please try again."});
            });    
        }
        else{
            this.setState({loading:false, isSubmitted:false, error:"Please select at least one question."});
        }
        
    }

    // wild card search function
    handleKeyDown = (e,all, selected,myInput) => {
        let allFields  = this.state[all];
        let selectedFields = this.state[selected] ;
        
        if (e.key === 'Enter') {
            let string = e.target.value.substring(0, e.target.value.length - 1);
            allFields.map( a => {
                if(a.value.toLowerCase().includes(string.toLowerCase())){
                    selectedFields.push(a);
                }
                return a;
            })
            this.setState({[selected]:selectedFields});

            
            this[myInput].current.blur();
            this[myInput].current.focus();
            

            this.createTable(this.state.questions, this.state.selectedCriteria , this.state.selectedLevel,  this.state.selectedDomains, this.state.selectedTopics);
        }
    }

    render(){
        
        //console.log(selectedQuestion)

        return(
            <AUX>
                <Prompt
                    when={this.state.formIsHalfFilledOut}
                    message="Are you sure you want to leave?"
                />
                <div className="row">
                    <div className="col-sm-12">
                        <div className="page-title-box">
                        <h4 className="page-title">Map Question</h4>
                        </div>
                    </div>
                </div>

                <div className="row" style={{marginTop: '-28px'}}>
                    <div className="col-sm-12 col-lg-4">
                        <div className="page-title-box">
                            <label>Domains</label>
                            <MySelect
                                value={this.state.selectedDomains}
                                onChange={this.selectTowChangeDomains}
                                options={this.state.allDomains}
                                components={animatedComponents}
                                allowSelectAll={true}
                                isMulti ={true}

                                handleKeyDown={(e) => this.handleKeyDown(e,'allDomains', 'selectedDomains','domain')}
                                ref={this.domain}
                            />
                            </div>
                    </div>
                    <div className="col-sm-12 col-lg-4">
                        <div className="page-title-box">
                            <label>Level</label>
                            <MySelect
                                value={this.state.selectedLevel}
                                onChange={this.selectTowChangeLevel}
                                options={this.state.allLevels}
                                components={animatedComponents}
                                allowSelectAll={true}
                                isMulti ={true}

                                handleKeyDown={(e) => this.handleKeyDown(e,'allLevels', 'selectedLevel','level')}
                                ref={this.level}
                            />
                            </div>
                    </div>
                    <div className="col-sm-12 col-lg-4">
                        <div className="page-title-box">
                            <label>Criteria</label>
                            <MySelect
                                value={this.state.selectedCriteria}
                                onChange={this.selectTowChangeCriteria}
                                options={this.state.allCriteria}
                                components={animatedComponents}
                                allowSelectAll={true}
                                isMulti ={true}

                                handleKeyDown={(e) => this.handleKeyDown(e, 'allCriteria', 'selectedCriteria','criteria')}
                                ref={this.criteria}
                            />
                            </div>
                    </div>
                </div>
                <div className="row" style={{marginTop: '-28px'}}>
                    <div className="col-sm-12 col-lg-12" >
                        <div className="page-title-box">
                            <label>Topics</label>
                            <MySelect
                                value={this.state.selectedTopics}
                                components={animatedComponents}
                                onChange={this.selectTowChangeTopics}
                                options={this.state.allTopics}
                                allowSelectAll={true}
                                isMulti ={true}

                                handleKeyDown={(e) => this.handleKeyDown(e, 'allTopics', 'selectedTopics','topic')}
                                ref={this.topic}
                            />
                            </div>
                    </div>
                    
                </div>
                        
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card m-b-20">
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-md-6">
                                        <h4 className="mt-0 header-title ">
                                            {this.state.profileName+"("+selectedQuestion.size+")"}
                                        </h4>
                                    </div>
                                    <div className="col-md-6 text-right">
                                            <button     
                                                title="Add a new question"
                                                className="btn btn-primary mb-3" 
                                                onClick={(e) => this.handleQuestionsSelected(e)}>Submit</button>
                                    </div>
                                </div>
                                {this.state.message ? <Alert classes={"alert-success"} msg={this.state.message} />: null}
                                {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} />: null}
                                {this.state.loading ?  <Spinner /> : null }
                                <div style={{overflow:'auto'}}>
                                  <MDBDataTable
                                      bordered
                                      key={1}
                                      hover
                                      searching={false}
                                      data={{ columns: this.state.columns, rows:this.state.rows }}
                                      info={this.state.rows.length > 0 ? true : false}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {
                    this.state.toggle
                    ? <QuestionModal modal={this.state.toggle} toggle={this.closeModal} title={this.state.modalTitle} questionId={this.state.editQuestion} />
                    : false
                }
                {
                    this.state.toggleImport
                    ? <ImportModal modal={this.state.toggleImport} toggle={this.closeModalImport} />
                    : false
                }
                {
                    this.state.toggleConfirm
                    ? <ConfirmModal modal={this.state.toggleConfirm} toggle={this.closeDisableModal} click={this.disableQuestion} content={this.state.content}  title={this.state.title} />
                    : false
                }
            </AUX>
        );
    }
}

const mapStateToProps = state => {
    return {
        token:state.auth.token,
        mappingId : state.mapping.mappingId
    }
}

export default connect(mapStateToProps)(Questions);