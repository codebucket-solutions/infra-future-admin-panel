import React, { Component } from 'react';
import { connect } from 'react-redux';

import AUX from '../hoc/Aux_';
import axios from '../axios-details';
import MySelect from '../components/ReactSelect';
import { handleKeyValueMail, objectToArray } from '../shared/utility';

import Spinner from '../components/Spinner/simpleSpinner';
import Alert from '../components/Alert/common.js';
import ImportConfirmation from './Configuration/ImportSetting';
import SyncQuestion from './Configuration/syncQuestion';
import SidebarConfig from './Configuration/sidebarConfig';
import PresignedUrlExpiryDate from './Configuration/PresignedUrlExpiryDate';
import PresignedURLMailContent from './Configuration/PresignedURLMailContent';
import SendRulesAE  from './Configuration/sendRulesAE';
import SyslogConfig  from './Configuration/syslogConfig';


import { btnStyle } from '../util';

const pageTitleStyle = {
    fontSize: "20px"
}


class Configuration extends Component {

    constructor(props) {
        super(props);
        this.state = {

            loading: false,
            error: null,
            message: null,

            selectedEndpoint: [],
            allEndpoints: [],
            allMailTo: [],
        }
    }

    fetchSetting = () => {
        this.setState({ loading: true });
        const config = {
            headers: { Authorization: `Bearer ${this.props.token}` }
        };
        axios.get('/mail_setting/', config)
            .then(res => {

                //console.log(res.data.data);
                const { data } = res.data;
                if (res.data.status) {
                    //console.log(data);
                    let selectedEndpoint = [];
                    let allEndpoints = [];

                    data.all_endpoint.map((a, i) => {
                        //console.log(a)
                        let newEndpoint = [];
                        if (a.admin === 1) {
                            newEndpoint.push({ id: 1, item: "Admin" })
                        }
                        if (a.client === 1) {
                            newEndpoint.push({ id: 2, item: "Client" })
                        }
                        if (a.user === 1) {
                            newEndpoint.push({ id: 3, item: "User" })
                        }
                        //console.log(handleKeyValueMail(newEndpoint));
                        selectedEndpoint.push(handleKeyValueMail(newEndpoint));
                        allEndpoints.push(a.item);
                    })
                    //console.log(handleKeyValueMail(data.all_mail));
                    //console.log(handleKeyValueMail(data.all_mail));
                    this.setState({
                        selectedEndpoint: selectedEndpoint,
                        allMailTo: handleKeyValueMail(data.all_mail),
                        allEndpoints,
                    })
                }
                this.setState({ loading: false })

            })
            .catch(err => {
                this.setState({ loading: false })
            }

            );
    }

    componentDidMount() {
        this.fetchSetting();
    }

    selectTowChange = (e, i, id) => {
        let { selectedEndpoint } = this.state;
        selectedEndpoint[id] = e
        this.setState({ selectedEndpoint: selectedEndpoint });
    }

    changeToArray = (data) => {
        let arr = [];
        data.forEach((d, i) => {
            let temp, tempArr;
            temp = [];
            tempArr = [];
            tempArr = objectToArray(d);
            temp.push(tempArr.includes(1) ? 1 : 0);
            temp.push(tempArr.includes(2) ? 1 : 0);
            temp.push(tempArr.includes(3) ? 1 : 0);
            temp.push(i + 1);
            arr.push(temp);
        })
        // console.log(arr);
        return arr;
    }


    // delete profile
    submitHandler = (e) => {
        e.preventDefault();
        this.setState({ loading: false, error: null, message: null });
        window.scrollTo(0, 0)
        const data = {
            token: this.props.token,
            mail_to: this.changeToArray(this.state.selectedEndpoint),
        }
        //console.log(data);
        axios.post('/mail_setting/', data)
            .then(res => {
                //console.log(res.data);
                if (res.data.status) {
                    this.setState({ loading: false, message: res.data.message })
                }
                else {
                    this.setState({ loading: false, error: res.data.message })
                }
            })
            .catch(err => {
                this.setState({ loading: false, error: "Something went wrong. Please try again later." });

            })
    }


    render() {
        //console.log(this.state.allMailTo);
        const { selectedEndpoint, allEndpoints, allMailTo } = this.state;
        return (
            <AUX>
                <div className="row">
                    <div className="col-sm-12">
                        <div className="page-title-box">
                            <h4 className="page-title" style={pageTitleStyle}>Configuration</h4>

                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-12">
                        <div className="card mb-2" style={{ marginBottom: '20%' }}>
                            <div className="card-body" >
                                <h6 className="page-title " title="Mail configuration settings for sending notifications to groups of Admins, Users or Clients. These settings enable notifications for the selected user groups.">Mail Settings</h6>
                                {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} /> : null}
                                {this.state.message ? <Alert classes={"alert-success"} msg={this.state.message} /> : null}
                                {this.state.loading ? <AUX><Spinner /><br /></AUX> : null}
                                {/* <h4 className="mt-0 header-title text-left">
                                    Mail Setting
                                </h4> */}
                                <br />
                                <form onSubmit={this.submitHandler}>
                                    <div className="row">
                                        {
                                            selectedEndpoint && selectedEndpoint.map((s, i) => {
                                                return (
                                                    <>
                                                        <div className="col-sm-12 col-lg-3 mb-3">
                                                            <label>{allEndpoints[i]}</label>
                                                        </div>
                                                        <div className="col-sm-12 col-lg-4 mb-3">

                                                            <MySelect
                                                                value={selectedEndpoint[i]}
                                                                onChange={(e) => this.selectTowChange(e, "selectedMailTo", i)}
                                                                options={(i === 3 || i === 4) ? [allMailTo[0]] : allMailTo}
                                                                allowSelectAll={true}
                                                                isMulti={true}
                                                                noMin={true}
                                                            />
                                                        </div>
                                                        <div className="col-sm-12 col-lg-5 mb-3"></div>
                                                    </>
                                                )
                                            })
                                        }

                                        <div className="col-sm-12 text-center">
                                            <button style={{ btnStyle }} type="submit" className="btn btn-primary waves-effect waves-light">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <ImportConfirmation />
                <SyncQuestion />
                <SidebarConfig />
                <PresignedUrlExpiryDate />
                <PresignedURLMailContent />
                <SendRulesAE />
                <SyslogConfig />
            </AUX>
        );
    }
}

const mapStateToProps = state => {
    return {
        token: state.auth.token
    }
}

export default connect(mapStateToProps)(Configuration);
