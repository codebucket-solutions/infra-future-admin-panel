import React, { Component } from 'react';
import AUX from '../hoc/Aux_';
import makeAnimated from "react-select/animated";
import { Link } from 'react-router-dom';
import { CSVLink } from "react-csv";


import axios from '../axios-details';
import { MDBDataTable } from 'mdbreact';
import { connect } from 'react-redux';
import { handleKeyValue, objectToArray } from '../shared/utility';

import MySelect from '../components/ReactSelect';
import QuestionModal from '../components/Modal/modalQuestions';
import ImportModal from '../components/Modal/modalImportQuestion';
import ConfirmModal from '../components/Modal/modalConfirm';
import Spinner from '../components/Spinner/simpleSpinner';
import Alert from '../components/Alert/common';

import './response.css';
import { btnStyle } from '../util';

let csvData = [];
const header = ['quest_tag', 'ahp_criteria_lev_1', 'ahp_criteria_lev_2', 'ahp_criteria_lev_3', 'ahp_comment', 'seq_no', 'measure', 'source_response', 'question', 'answer_key', 'description', 'internal_notes', 'response_type', 'topics', 'levels', 'domains', 'response_json', 'allow_comments', 'response_keys', 'response_1', 'response_2', 'response_3', 'response_4', 'response_5', 'response_6', 'response_7', 'response_8', 'response_9', 'response_10', 'response_11', 'response_11', 'response_13', 'response_14', 'response_15', 'response_16', 'response_17', 'response_18', 'response_19', 'response_20']
const status = ['Inactive', 'Active'];
const animatedComponents = makeAnimated();

let selectedQuestion = new Set();
class Questions extends Component {


    constructor(props) {
        super(props);
        this.state = {
            columns: [
                {
                    label: <label className="container mb-4">
                        <input
                            type="checkbox"
                            value={"all"}
                            onClick={(event) => this.handleCheckboxAll(event, "all")}
                            checked={() => this.checkAllChech(null, null, null, null, 1)}
                        />
                        <span className="checkmark"></span>
                    </label>,
                    field: 'sro',
                    sort: 'asc',
                    width: 150
                },
                {
                    label: 'Questions',
                    field: 'question',
                    sort: 'asc',
                    width: 200
                },
                {
                    label: 'Description',
                    field: 'description',
                    sort: 'asc',
                    width: 200
                },
                {
                    label: 'Type',
                    field: 'type',
                    sort: 'asc',
                    width: 100
                },
                {
                    label: 'Responses',
                    field: 'option',
                    sort: 'asc',
                    width: 100
                },
                {
                    label: 'Status',
                    field: 'status',
                    sort: 'asc',
                    width: 100
                },
                {
                    label: 'Action',
                    field: 'action',
                    sort: 'asc',
                    width: 100
                }
            ],
            rows: [],
            toggle: false,
            loading: false,
            modalTitle: '',
            editQuestion: '',
            shouldBlockNavigation: true,
            toggleImport: false,
            toggleConfirm: false,
            questionId: null,
            content: null,
            title: null,
            deleteStatus: null,
            error: null,
            message: null,
            questions: [],
            allCriteria: [],
            selectedCriteria: [],
            allLevels: [],
            selectedLevel: [],
            allDomains: [],
            selectedDomains: [],
            allTopics: [],
            selectedTopics: [],
            toggleDelete: false,
            toggleSync: false,

        }

        this.domain = React.createRef();
        this.level = React.createRef();
        this.criteria = React.createRef();
        this.topic = React.createRef();
        this.csv = React.createRef();
    }

    handleCheckboxAll = (event) => {
        let { questions, selectedCriteria, selectedLevel, selectedDomains, selectedTopics } = this.state
        let criteria = objectToArray(selectedCriteria);
        let levels = objectToArray(selectedLevel);
        let domains = objectToArray(selectedDomains);
        let topics = objectToArray(selectedTopics);

        if (event.target.checked) {

            questions.map((q, i) => {
                if ((criteria && criteria.includes(q.ahp_criteria_lev_1)) && (levels && levels.includes(q.levels)) && (domains && domains.includes(q.category)) && (topics && topics.some(r => q.topics.indexOf(r) >= 0))) {
                    selectedQuestion.add(q.quest_id.toString());
                }
                return q;
            })
            this.setState({ checked: true });
        }
        else {
            questions.map((q, i) => {
                if ((criteria && criteria.includes(q.ahp_criteria_lev_1)) && (levels && levels.includes(q.levels)) && (domains && domains.includes(q.category)) && (topics && topics.some(r => q.topics.indexOf(r) >= 0))) {
                    selectedQuestion.delete(q.quest_id.toString());
                }
                return q;
            })
        }
        // this.checkAllChech(selectedQuestion);
        this.setState({ formIsHalfFilledOut: true });
        this.createTable(this.state.questions, this.state.selectedCriteria, this.state.selectedLevel, this.state.selectedDomains, this.state.selectedTopics);
    }

    handleCheckbox = (event, quest_id) => {
        //console.log(event.target, quest_id)
        if (event.target.checked) {
            selectedQuestion.add(quest_id.toString());
        }
        else {
            selectedQuestion.delete(event.target.value)
        }
        this.setState({ formIsHalfFilledOut: true });

        this.createTable(this.state.questions, this.state.selectedCriteria, this.state.selectedLevel, this.state.selectedDomains, this.state.selectedTopics);


    }

    checkAllChech(criteria, levels, domains, topics, type) {
        //console.log(selectedQuestion);
        let { questions, selectedCriteria, selectedLevel, selectedDomains, selectedTopics } = this.state
        if (type === 1) {
            criteria = objectToArray(selectedCriteria);
            levels = objectToArray(selectedLevel);
            domains = objectToArray(selectedDomains);
            topics = objectToArray(selectedTopics);
        }

        //console.log(levels);
        if (questions.length > 0) {
            var ret = false;
            for (var i = 0; i < questions.length; i++) {
                if ((criteria && criteria.includes(questions[i].ahp_criteria_lev_1)) && (levels && levels.includes(questions[i].levels)) && (domains && domains.includes(questions[i].category)) && (topics && topics.some(r => questions[i].topics.indexOf(r) >= 0))) {
                    ret = selectedQuestion.has(questions[i].quest_id.toString());
                    //console.log(questions[i].quest_id.toString()) ;
                    if (ret === false) {
                        return false;
                    }
                }
            }
            return true;
        }
        else {
            return false;
        }

    }

    changeCheckBox = (criteria, levels, domains, topics) => {
        let { columns } = this.state;
        columns[0].label = (
            <label className="container mb-4">
                <input
                    type="checkbox"
                    value={"all"}
                    onClick={(event) => this.handleCheckboxAll(event, "all")}
                    checked={this.checkAllChech(criteria, levels, domains, topics, 2)}
                />
                <span className="checkmark"></span>
            </label>
        );
        this.setState({ columns: columns });
    }

    // convert key value pair to string
    convertToString = (object) => {
        let obj = [];
        if (object) {
            for (var i = 0; i < object.length; i++) {
                obj.push(
                    object[i].value
                )
            }
        }
        return obj;
    }

    getFetcheddata = (array, unique) => {
        let content = [];
        //array = array.JSON.parse();
        //console.log(array);
        array.map((data, index) => {
            //console.log(unique+index+1);
            //content.push( <AUX key={"'"+unique+"'"+index}><label >{data.choice}</label><br /></AUX> );
            return content.push(<p key={"'" + unique + "'" + index}>{data.choice}</p>);
        })
        return content;
    }

    getFetchCSV = (array) => {
        var datas = array.map(function (item) {
            return item.choice;
        });
        //console.log(datas.toString());
        return datas.toString();
    }

    fetchQuestion = () => {
        this.setState({ loading: true });
        const config = {
            headers: { Authorization: `Bearer ${this.props.token}` }
        };
        axios.get('/questions', config)
            .then(response => {
                let res = response.data;
                console.log(response.data);
                if (response.data.status) {
                    let criteria = handleKeyValue(res.data.criteria);
                    let levels = handleKeyValue(res.data.levels);
                    let domains = handleKeyValue(res.data.domains);
                    let topics = handleKeyValue(res.data.topics);
                    //console.log(domains);
                    //console.log(this.state.selectedCriteria.length);

                    // get filter from localStorage
                    const local_domain = JSON.parse(localStorage.getItem('admin_question_domain_filter'));
                    const local_criteria = JSON.parse(localStorage.getItem('admin_question_criteria_filter'));
                    const local_level = JSON.parse(localStorage.getItem('admin_question_level_filter'));
                    const local_topic = JSON.parse(localStorage.getItem('admin_question_topic_filter'));

                    //create table
                    this.createTable(
                        res.data.qs,
                        local_criteria && local_criteria.length > 0 ? local_criteria : criteria,
                        local_level && local_level.length > 0 ? local_level : levels,
                        local_domain && local_domain.length > 0 ? local_domain : domains,
                        local_topic && local_topic.length > 0 ? local_topic : topics
                    );

                    //set state
                    this.setState({
                        questions: res.data.qs,

                        selectedCriteria: (this.state.selectedCriteria.length > 0 ? this.state.selectedCriteria : (local_criteria && local_criteria.length > 0 ? local_criteria : criteria)),
                        allCriteria: criteria,

                        selectedLevel: (this.state.selectedLevel.length > 0 ? this.state.selectedLevel : (local_level && local_level.length > 0 ? local_level : levels)),
                        allLevels: levels,

                        selectedDomains: (this.state.selectedDomains.length > 0 ? this.state.selectedDomains : (local_domain && local_domain.length > 0 ? local_domain : domains)),
                        allDomains: domains,

                        allTopics: topics,
                        selectedTopics: (this.state.selectedTopics.length > 0 ? this.state.selectedTopics : (local_topic && local_topic.length > 0 ? local_topic : topics)),

                        loading: false,
                    })

                }
                else {
                    this.setState({ loading: false })
                }
            })
            .catch(err => {
                this.setState({ loading: false })
            });
    }

    createTable = (data_obj, criteria, levels, domains, topics) => {

        //set filter data to local storage
        localStorage.setItem('admin_question_domain_filter', JSON.stringify(domains));
        localStorage.setItem('admin_question_criteria_filter', JSON.stringify(criteria));
        localStorage.setItem('admin_question_level_filter', JSON.stringify(levels));
        localStorage.setItem('admin_question_topic_filter', JSON.stringify(topics));

        criteria = objectToArray(criteria);
        levels = objectToArray(levels);
        domains = objectToArray(domains);
        topics = objectToArray(topics);
        //console.log(topics);
        // console.log(criteria);
        let fetchQuestions = [];
        for (let key in data_obj) {

            if ((criteria && criteria.includes(data_obj[key].ahp_criteria_lev_1)) && (levels && levels.includes(data_obj[key].levels)) && (domains && domains.includes(data_obj[key].category)) && (topics && topics.some(r => data_obj[key].topics.indexOf(r) >= 0))) {
                //console.log(domains.includes(data_obj[key].category));
                let qs = (<AUX>
                    <button
                        style={{btnStyle}}
                        title="Edit question details"
                        onClick={() => this.editQuestionHandler(data_obj[key].quest_id)} className="btn btn-primary">Edit</button> &nbsp;
                    <button
                        title="Disable the question"
                        onClick={() => this.deleteQuestionHandler(data_obj[key].quest_id, '0')} className="btn btn-danger">Disable</button>
                </AUX>);
                //console.log(qs);
                if (data_obj[key].active === '0') {
                    qs = (<AUX>
                        <button
                            style={{btnStyle}}
                            title="Edit question details"
                            onClick={() => this.editQuestionHandler(data_obj[key].quest_id)} className="btn btn-primary">Edit</button> &nbsp;
                        <button
                            title="Enable the question"
                            onClick={() => this.deleteQuestionHandler(data_obj[key].quest_id, '1')} className="btn btn-success">Activate</button>

                    </AUX>);
                }
                //console.log(qs);
                fetchQuestions.push({
                    sro: <AUX>
                        <label key={key} className="container">
                            <input
                                type="checkbox"
                                value={data_obj[key].quest_id}
                                onClick={(event) => this.handleCheckbox(event, data_obj[key].quest_id)}
                                checked={selectedQuestion ? selectedQuestion.has(data_obj[key].quest_id.toString()) ? true : false : false}
                            />
                            <span className="checkmark"></span>
                        </label>
                    </AUX>,
                    question: data_obj[key].question,
                    description: data_obj[key].description,
                    type: data_obj[key].response_type,
                    option: this.getFetcheddata(data_obj[key].response_json, key),
                    status: status[data_obj[key].active],
                    action: qs

                })
            }
        }
        //console.log(fetchQuestions);
        this.setState({ rows: fetchQuestions });
        this.changeCheckBox(criteria, levels, domains, topics);
    }

    selectTowChangeCriteria = (selectedCriteria) => {
        this.setState({ selectedCriteria: selectedCriteria ? selectedCriteria : [] });
        this.createTable(this.state.questions, selectedCriteria ? selectedCriteria : [], this.state.selectedLevel, this.state.selectedDomains, this.state.selectedTopics);
    }

    selectTowChangeDomains = (selectedDomains) => {
        this.setState({ selectedDomains: selectedDomains ? selectedDomains : [] });
        this.createTable(this.state.questions, this.state.selectedCriteria, this.state.selectedLevel, selectedDomains ? selectedDomains : [], this.state.selectedTopics);
    }

    selectTowChangeLevel = (selectedLevel) => {
        this.setState({ selectedLevel: selectedLevel ? selectedLevel : [] });
        this.createTable(this.state.questions, this.state.selectedCriteria, selectedLevel ? selectedLevel : [], this.state.selectedDomains, this.state.selectedTopics);

    }

    selectTowChangeTopics = (selectedTopics) => {
        this.setState({ selectedTopics: selectedTopics ? selectedTopics : [] });
        this.createTable(this.state.questions, this.state.selectedCriteria, this.state.selectedLevel, this.state.selectedDomains, selectedTopics ? selectedTopics : []);

    }
    exportCSV = () => {
        csvData = [];
        csvData.push(header)
        const token = {
            token: this.props.token
        };
        this.setState({ loading: true });
        axios.post("/questions/export", token).then(res => {
            //console.log(res.data.data.questions);
            for (let key in res.data.data.questions) {
                let columnArray = [];
                header.map((v, i) => {
                    //console.log(v)
                    if (v === 'quest_id') {
                        columnArray.push(parseInt(key) + 1)
                    }
                    else {
                        columnArray.push(res.data.data.questions[key][v] ? res.data.data.questions[key][v].toString().replace(/"/g, '""') : '')
                    }
                    return true;
                    //console.log(res.data.data.questions[key][v]);
                })
                csvData.push(columnArray);
            }
            this.setState({ loading: false });
            let btn = this.refs.csv;
            btn.link.click();
        }).catch(error => {
            //console.log(error);
        });
    }

    componentDidMount() {
        this.fetchQuestion();
    }

    addQuestionHandler = () => {
        this.setState(prevState => {
            return ({ toggle: !prevState.toggle, modalTitle: "Add New Question", editQuestion: null })
        })
    }

    editQuestionHandler = (id) => {
        this.setState(prevState => {
            return ({ toggle: !prevState.toggle, modalTitle: "Edit Question", editQuestion: id })
        })
    }

    closeModal = (update) => {
        //console.log(update);
        if (update) {
            this.fetchQuestion();
        }
        this.setState(prevState => {
            return ({ toggle: !prevState.toggle })
        })

    }

    handleimport = () => {
        this.setState(prevState => {
            return ({ toggleImport: !prevState.toggleImport })
        })
    }
    closeModalImport = (update) => {
        //console.log(update);
        if (update) {
            this.fetchQuestion();
        }
        this.setState(prevState => {
            return ({ toggleImport: !prevState.toggleImport })
        })

    }

    deleteQuestionHandler = (id, type) => {
        if (type === '0') {
            this.setState(prevState => {
                return ({ toggleConfirm: !prevState.toggleConfirm, questionId: id, content: "Do you really want to disable this question?", title: "Disable Question", deleteStatus: type })
            })
        } else {
            this.setState(prevState => {
                return ({ toggleConfirm: !prevState.toggleConfirm, questionId: id, content: "Do you  want to enable this question?", title: "Enable Question", deleteStatus: type })
            })
        }

    }

    closeDisableModal = () => {
        this.setState(prevState => {
            return ({ toggleConfirm: !prevState.toggleConfirm, questionId: null, content: null, title: null })
        })
    }

    disableQuestion = () => {
        const token = {
            token: this.props.token,
            quest_id: this.state.questionId,
            status: this.state.deleteStatus.toString(),
            user_name: this.props.name,
        };
        this.setState({ loading: true });
        console.log(token);
        axios.post("/questions/delete", token)
            .then(res => {
                //console.log(res.data);
                if (res.data.status) {
                    this.setState({ loading: false, message: res.data.message });
                }
                else {
                    this.setState({ loading: false, error: res.data.message });
                }
                this.closeDisableModal();
                this.fetchQuestion();
            })
            .catch(error => {
                this.setState({ loading: true, error: "Something went wrong. Please try again." });
            });

    }

    // wild card search function
    handleKeyDown = (e, all, selected, myInput) => {
        let allFields = this.state[all];
        let selectedFields = this.state[selected];

        if (e.key === 'Enter') {
            let string = e.target.value.substring(0, e.target.value.length - 1);
            allFields.map(a => {
                if (a.value.toLowerCase().includes(string.toLowerCase())) {
                    selectedFields.push(a);
                }
                return a;
            })
            this.setState({ [selected]: selectedFields });


            this[myInput].current.blur();
            this[myInput].current.focus();


            this.createTable(this.state.questions, this.state.selectedCriteria, this.state.selectedLevel, this.state.selectedDomains, this.state.selectedTopics);
        }
    }

    handleQuestionsToDelete = () => {
        //console.log(this.state.selectedQuestion);
        this.setState({ loading: true, message: null, error: null });
        const data = {
            token: this.props.token,
            quest_id: [...selectedQuestion],
            user_name: this.props.name,
        }
        console.log(data);
        axios.post('/questions/delete_questions/', data)
            .then(res => {
                console.log(res.data.data);
                this.closeDeleteModal(1);
                if (res.data.status) {
                    this.setState({ loading: false, isSubmitted: true, message: res.data.message });

                }
                else {
                    this.setState({ loading: false, isSubmitted: true, error: res.data.message });
                }
                this.fetchQuestion();
            })
            .catch(err => {
                this.closeDeleteModal(1);
                this.setState({ loading: false, isSubmitted: true, error: "Something went wrong. Please try again." });

            });

    }

    closeDeleteModal = (i) => {
        this.setState({ message: null, error: null });
        if (selectedQuestion.size > 0 || i === 1) {
            this.setState(prevState => {
                return ({ toggleDelete: !prevState.toggleDelete })
            })
        }
        else {
            this.setState({ loading: false, isSubmitted: false, error: "Please select at least one question." });
        }

    }

    render() {

        let filename = "question" + Date.now() + ".csv";

        return (
            <AUX>
                <div className="row">
                    <div className="col-sm-12">
                        <div className="page-title-box">
                            <h4 className="page-title">Questions</h4>
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                                <li className="breadcrumb-item active">Questions</li>
                            </ol>
                            {/* <Tinycharts /> */}
                        </div>
                    </div>
                </div>

                <div className="row" style={{ marginTop: '-28px' }}>
                    <div className="col-sm-12 col-lg-4">
                        <div className="page-title-box">
                            <label>Domains</label>
                            <MySelect
                                value={this.state.selectedDomains}
                                onChange={this.selectTowChangeDomains}
                                options={this.state.allDomains}
                                components={animatedComponents}
                                allowSelectAll={true}
                                isMulti={true}

                                handleKeyDown={(e) => this.handleKeyDown(e, 'allDomains', 'selectedDomains', 'domain')}
                                ref={this.domain}
                            />
                        </div>
                    </div>
                    <div className="col-sm-12 col-lg-4">
                        <div className="page-title-box">
                            <label>Level</label>
                            <MySelect
                                value={this.state.selectedLevel}
                                onChange={this.selectTowChangeLevel}
                                options={this.state.allLevels}
                                components={animatedComponents}
                                allowSelectAll={true}
                                isMulti={true}

                                handleKeyDown={(e) => this.handleKeyDown(e, 'allLevels', 'selectedLevel', 'level')}
                                ref={this.level}
                            />
                        </div>
                    </div>
                    <div className="col-sm-12 col-lg-4">
                        <div className="page-title-box">
                            <label>Criteria</label>
                            <MySelect
                                value={this.state.selectedCriteria}
                                onChange={this.selectTowChangeCriteria}
                                options={this.state.allCriteria}
                                components={animatedComponents}
                                allowSelectAll={true}
                                isMulti={true}

                                handleKeyDown={(e) => this.handleKeyDown(e, 'allCriteria', 'selectedCriteria', 'criteria')}
                                ref={this.criteria}
                            />
                        </div>
                    </div>
                    {/* <input ref={this.myInput} type="file"/> */}
                </div>
                <div className="row" style={{ marginTop: '-28px' }}>
                    <div className="col-sm-12 col-lg-12">
                        <div className="page-title-box">
                            <label>Topics</label>
                            <MySelect
                                value={this.state.selectedTopics}
                                components={animatedComponents}
                                onChange={this.selectTowChangeTopics}
                                options={this.state.allTopics}
                                allowSelectAll={true}
                                isMulti={true}

                                handleKeyDown={(e) => this.handleKeyDown(e, 'allTopics', 'selectedTopics', 'topic')}
                                ref={this.topic}
                            />
                        </div>
                    </div>
                </div>



                <div className="row">
                    <div className="col-lg-12">
                        <div className="card m-b-20">
                            <div className="card-body">
                                <h4 className="mt-0 header-title text-right">
                                    <button
                                        title="Add a new question"
                                        className="btn btn-primary mb-3"
                                        onClick={this.addQuestionHandler}
                                        style={{btnStyle}}
                                    >Add Question</button>&nbsp;
                                    <button
                                        title="Import new questions"
                                        className="btn btn-secondary mb-3" onClick={this.handleimport}>Import</button>&nbsp;
                                    <button
                                        title="Export all questions"
                                        className="btn btn-success mb-3" onClick={this.exportCSV}>Export All</button>
                                    <CSVLink
                                        ref="csv"
                                        filename={filename}
                                        data={csvData}
                                        style={{ display: 'none' }}
                                    >

                                    </CSVLink>

                                   &nbsp;
                                   <button
                                        title="Delete Questions"
                                        className="btn btn-danger mb-3" onClick={this.closeDeleteModal}>Delete</button>
                                    {/* <button className="btn btn-secondary mb-3" onClick={() => this.openCSVModal()}>Import</button> */}

                                </h4>
                                {this.state.loading ? <Spinner /> : null}
                                {this.state.message ? <Alert classes={"alert-success"} msg={this.state.message} /> : null}
                                {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} /> : null}
                                <div style={{ overflow: 'auto' }}>
                                    <MDBDataTable
                                        bordered
                                        data={{ columns: this.state.columns, rows: this.state.rows }}
                                        info={this.state.rows.length > 0 ? true : false}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {
                    this.state.toggle
                        ? <QuestionModal modal={this.state.toggle} toggle={this.closeModal} title={this.state.modalTitle} questionId={this.state.editQuestion} operationType={3} />
                        : false
                }
                {
                    this.state.toggleImport
                        ? <ImportModal modal={this.state.toggleImport} toggle={this.closeModalImport} />
                        : false
                }
                {
                    this.state.toggleConfirm
                        ? <ConfirmModal modal={this.state.toggleConfirm} toggle={this.closeDisableModal} click={this.disableQuestion} content={this.state.content} title={this.state.title} />
                        : false
                }
                {
                    this.state.toggleDelete
                        ? <ConfirmModal modal={this.state.toggleDelete} toggle={this.closeDeleteModal} click={this.handleQuestionsToDelete} title="Delete Question(s)" content="Do you really want to delete these question(s)?" />
                        : false
                }

            </AUX>
        );
    }
}

const mapStateToProps = state => {
    return {
        token: state.auth.token,
        name: state.auth.name,
    }
}

export default connect(mapStateToProps)(Questions);