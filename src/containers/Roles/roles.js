import React, { Component } from 'react';
import { MDBDataTable } from 'mdbreact';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import AUX from '../../hoc/Aux_';
import * as actions from '../../store/actions/index';

import ModalAddRoles from './modalAddRoles';
import Alert from '../../components/Alert/common';
import Spinner from '../../components/Spinner/simpleSpinner';
import { btnStyle } from '../../util';


class Profile extends Component {


    constructor(props) {
        super(props);
        this.state = {
            columns: [
                {
                    label: 'Sr.No.',
                    field: 'sro',
                    sort: 'asc',
                    width: 150
                },
                {
                    label: 'Roles Name',
                    field: 'role',
                    sort: 'asc',
                    width: 270
                },
                {
                    label: 'Number of Admin',
                    field: 'number',
                    sort: 'asc',
                    width: 200
                },
                {
                    label: 'Action',
                    field: 'action',
                    sort: 'asc',
                    width: 200
                }
            ],
            rows: [{
                sro:1,
                role:'Ouestion Admin',
                number:'1',
                action:<button type="button" className="btn btn-primary">Edit</button>

            }],
            data: [],
            toggle: false,
            loading: false,
            title: null,
            adminId: null,
            error: null,
            message: null,

            content: null,
            deactivateType: null,
        }
    }

    openModal = (adminId) => {
        let title = adminId ? 'Edit Roles' : 'Add Roles';
        this.setState(prevState => {
            return { toggle: !prevState.toggle, title: title, adminId: adminId ? adminId : null };
        })
    }

    

    fetchAdmin = () => {
        
    }

    componentDidMount() {
        this.fetchAdmin();
    }


    closeModal = (update) => {
        if (update) {
            this.fetchAdmin();
        }
        this.openModal();
    }

    handleDisable = () => {

    }

    render() {
        return (
            <AUX>
                <div className="row">
                    <div className="col-sm-12">
                        <div className="page-title-box">
                            <h4 className="page-title">All Roles</h4>
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                                <li className="breadcrumb-item active">All Roles</li>
                            </ol>
                            {/* <Tinycharts /> */}
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-12">
                        <div className="card m-b-20">
                            <div className="card-body">
                                <h4 className="mt-0 header-title text-right">
                                    <button
                                        style={{btnStyle}}
                                        title="Add new user and assign roles"
                                        className="btn btn-primary mb-3" onClick={() => this.openModal()}>Add Roles</button>
                                </h4>
                                {this.state.loading ? <AUX><Spinner /><br /></AUX> : null}
                                {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} /> : null}
                                <div style={{ overflow: 'auto' }}>
                                    <MDBDataTable
                                        bordered
                                        hover
                                        data={{ columns: this.state.columns, rows: this.state.rows }}
                                        info={this.state.rows.length > 0 ? true : false}
                                    />
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                {
                    this.state.toggle
                        ? <ModalAddRoles modal={this.state.toggle} toggle={this.closeModal} title={this.state.title} adminId={this.state.adminId} />
                        : false
                }
            </AUX>
        );
    }
}

const mapStatetoProps = state => {
    return {
        token: state.auth.token,
    };
}

const mapDispatchToProps = dispatch => {
    return {
        doMapping: (mappingId) => dispatch(actions.doMapping(mappingId)),
    }
}

export default connect(mapStatetoProps, mapDispatchToProps)(Profile);