import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from 'react-redux';

import Spinner from '../../components/Spinner/simpleSpinner';
import Alert from '../../components/Alert/common';
import Star from '../../components/Required/star';
import MySelect from '../../components/ReactSelect/index';
import { getAPI } from '../../APICall/index';
import { EXPORT_ALL_ROLES } from '../../APICall/urls';

class ChangeUser extends Component {

    constructor(props) {
        super(props);
        this.state = {
            client: '',
            options: [{ label: "Question roles", value: 1 }, { label: "Admin roles", value: 2 }],
            roles: [],
            loading: false,
            message: null,
            error: null,
            update: false,

            allRoles: [],
        };
    }

    componentDidMount() {
        this.fetchAllRoles();
    }

    fetchAllRoles = async () => {
        let allRoles = await getAPI(EXPORT_ALL_ROLES);
        let _allRoles = [];
        if (allRoles.status) {
            let roles = allRoles.data.roles;
            roles.forEach((r, i) => {
                console.log(r);
                let _allScope = r.roles.split(",");
                let _scope = [];
                _allScope.forEach((s, j) => {
                    _scope.push({ "name": s, "eligible": false })
                })
                _allRoles.push({ "name": r.role_name, "scope": _scope })

            })
            console.log(_allRoles)
            this.setState({ allRoles: _allRoles })
        }
    }

    inputChangeHandler = (event, i, j, scope) => {
        console.log(i,j, scope)
        let _allRoles = [...this.state.allRoles];
        console.log(_allRoles,_allRoles[i].scope[j]);
        _allRoles[i].scope[j].eligible = !scope.eligible;
        console.log(_allRoles);
        this.setState({ allRoles: _allRoles })


    }

    closeModal = () => {
        setTimeout(
            () => this.props.toggle(this.state.update)
            , 700);
    }

    submitHandler = (e) => {
        e.preventDefault();
        //console.log(this.state);
    }

    render() {
        //console.log(this.state.options);

        const { allRoles } = this.state; 

        let body = (
            <div className="card-body">
                <form onSubmit={this.submitHandler}>
                    {this.state.message ? <Alert classes={"alert-success"} msg={this.state.message} /> : null}
                    {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} /> : null}

                    <div className="row">
                        <div className="col-sm-12">
                            <div className="form-group">
                                <label htmlFor="client">Role Name<Star /></label>
                                <input
                                    type="text"
                                    placeholder="Enter Role Name"
                                    className="form-control" />
                            </div>
                        </div>
                        <div className="col-sm-12">
                            <div className="form-group">
                                <label htmlFor="client">Select Roles<Star /></label>
                                
                            </div>
                        </div>
                    </div>
                    
                        {
                            allRoles && allRoles.map((a,i) =>(
                                <div className="row">
                                    <div className="col-md-2">
                                        {a.name}
                                    </div>
                                    {
                                        a.scope.map((s,j) =>(
                                            <div className="col-md-2">
                                            <div className="form-group">
                                                
                                                <input
                                                    type="checkbox"
                                                    onChange={(e) => this.inputChangeHandler(e, i, j, s)}
                                                    checked={s.eligible ? true : false}
                                                />
                                                <label htmlFor="client">&nbsp;{s.name}</label>
                                            </div>
                                            </div>
                                        ))
                                    }
                                    
                                </div>
                            ))
                        }
                            <button type="submit" className="btn btn-primary waves-effect waves-light">Submit</button>
                </form>
            </div>
        );

        return (
            <div>
                <Modal size="xl" isOpen={this.props.modal} toggle={() => this.props.toggle(this.state.update)} >
                    <ModalHeader toggle={() => this.props.toggle(this.state.update)}>Change User/Client</ModalHeader>
                    <ModalBody>
                        {this.state.loading ? <Spinner /> : body}
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={() => this.props.toggle(this.state.update)}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

const mapStatetoProps = state => {
    return {
        token: state.auth.token,
    };
}

export default connect(mapStatetoProps)(ChangeUser);
