import { createStore ,combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';

import UIreducer from './store/reducers/reducer';
import Tinyreducer from './store/reducers/TinychartReducer';
import AuthReducer from './store/reducers/auth';
import MappingReducer from './store/reducers/mapping';
import assessment from './store/reducers/assessment';


const rootReducer = combineReducers({
    ui_red: UIreducer,
    tiny_red: Tinyreducer,
    auth: AuthReducer,
    mapping: MappingReducer,
    assessment: assessment,
});

const composeEnhancers = process.env.NODE_ENV === 'development' ?  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : null || compose;

export const store = createStore(rootReducer, composeEnhancers(
    applyMiddleware(thunk)
));

