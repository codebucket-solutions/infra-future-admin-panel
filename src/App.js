import React, { Component, Suspense } from 'react';
import './App.css';
import { Route, Switch } from 'react-router-dom';
// import mainbuilder from './containers/mainbuilder/mainbuilder';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import './App.css';

import * as actions from './store/actions/index';


import Layout from './components/Layout/Layout';
import GraphConfiguration from './containers/GraphConfigration/GraphConfiguration';

//Pages

const Dashboard = React.lazy(() => {
  return import('./containers/Dashboard/Dashboard');
});

const Profile = React.lazy(() => {
  return import('./containers/Profile');
});

const SelectQuestions = React.lazy(() => {
  return import('./containers/SelectQuestions');
});

const Questions = React.lazy(() => {
  return import('./containers/Questions');
});

const Rules = React.lazy(() => {
  return import('./containers/Rules/gold');
})

const Strategy = React.lazy(() => {
  return import('./containers/Strategy');
})

const PresignedURL = React.lazy(() => {
  return import('./containers/PresignedURL/PresignedURL');
});

const UrlLandingPage = React.lazy(() => {
  return import('./containers/PresignedURL/UrlLandingPage/index');
})

const AllApps = React.lazy(() => {
  return import('./containers/AllApps');
});

const Responses = React.lazy(() => {
  return import('./containers/Response.js')
})

const Admin = React.lazy(() => {
  return import('./containers/Admin')
})

const Clients = React.lazy(() => {
  return import('./containers/Clients')
})

const Users = React.lazy(() => {
  return import('./containers/Users')
})

const Insights = React.lazy(() => {
  return import('./containers/Insights.js');
})

const Configuration = React.lazy(() => {
  return import('./containers/Configuration.js');
})

const GrapgConfiguration = React.lazy(() => {
  return import('./containers/GraphConfigration/GraphConfiguration.js')
})

// const Response = React.lazy(() =>{
//   return import('./containers/Response');
// });

const Login = React.lazy(() => {
  return import('./containers/Auth/Login.js');
});

const ForgotPassword = React.lazy(() => {
  return import('./containers/Auth/ForgotPassword');
});


const Logout = React.lazy(() => {
  return import('./containers/Auth/Logout');
})

//AHP
const HierarchyTree = React.lazy(() => {
  return import('./containers/AhpHierarchy/d3Tree');
})
const AHP = React.lazy(() => {
  return import('./containers/AhpHierarchy/index');
})

const AllCriteria = React.lazy(() => {
  return import('./containers/AHPSection/index')
})

//BSP section
const MapService = React.lazy(() => {
  return import('./containers/BSP/MapService/mapService');
})

const AllServices = React.lazy(() => {
  return import('./containers/BSP/AllService/AllService');
})

//Roles
const Roles = React.lazy(() =>{
  return import('./containers/Roles/roles');
})

//BSP
const BS_Capability = React.lazy(() => {
  return import('./containers/BSP/BusinessCapability/capability')
})

const BS_Services = React.lazy(() => {
  return import('./containers/BSP/BusinessServices/businessServices')
})

const BS_Server = React.lazy(() => {
  return import('./containers/BSP/Server/server')
})

const Services = React.lazy(() => {
  return import('./containers/BSP/Services/services')
})

class App extends Component {


  componentDidMount() {
    this.props.onTryLogin();
    this.props.checkAssessment();
    this.props.checkMapping();
  }

  render() {
    //console.log(this.props.isAuth);
    let layout = (
      <Layout topbar={false} sidebar={false} footer={false} isloginpage={true}>
        <Suspense fallback={<p>Loading....</p>}>
          <Switch>
            <Route path="/forgot_password" render={(props) => <ForgotPassword  {...props} />} />
            <Route path="/" render={(props) => <Login  {...props} />} />
          </Switch>
        </Suspense>
      </Layout>
    );
    if (this.props.isAuth) {
      if (parseInt(this.props.accessLevel) === 1) {
        // profile User
        layout = (
          <Layout topbar={true} sidebar={true} footer={true} isloginpage={false}>
            <Suspense fallback={<p>Loading....</p>}>
              <Switch>
                <Route path='/logout' render={(props) => <Logout {...props} />} />

                <Route path='/select_questions' render={(props) => <SelectQuestions  {...props} />} />

                <Route path='/' render={(props) => <Profile  {...props} />} />
              </Switch>
            </Suspense>
          </Layout>);
      }
      else if (parseInt(this.props.accessLevel) === 2) {
        // Question User
        layout = (
          <Layout topbar={true} sidebar={true} footer={true} isloginpage={false}>
            <Suspense fallback={<p>Loading....</p>}>
              <Switch>
                <Route path='/logout' render={(props) => <Logout {...props} />} />

                <Route path='/' render={(props) => <Questions  {...props} />} />
              </Switch>
            </Suspense>
          </Layout>);
      }
      else {
        // super admin
        // access level 0 or 3
        layout = (
          <Layout topbar={true} sidebar={true} footer={true} isloginpage={false}>
            <Suspense fallback={<p>Loading....</p>}>
              <Switch>
                <Route path='/logout' render={(props) => <Logout {...props} />} />

                <Route path='/profile' render={(props) => <Profile  {...props} />} />
                <Route path='/questions' render={(props) => <Questions  {...props} />} />
                <Route path='/rules' render={(props) => <Rules  {...props} />} />
                <Route path='/strategy' render={(props) => <Strategy  {...props} />} />

                <Route path='/apps' render={(props) => <AllApps  {...props} />} />
                <Route path='/responses' render={(props) => <Responses  {...props} />} />

                <Route path='/clients' render={(props) => <Clients  {...props} />} />
                <Route path='/users' render={(props) => <Users  {...props} />} />

                <Route path='/presignedurl' render={(props) => <PresignedURL  {...props} />} />

                <Route path='/hierarchy_tree' render={(props) => <HierarchyTree  {...props} />} />
                <Route path='/hierarchy' render={(props) => <AHP  {...props} />} />  

                <Route path='/all_ahp' render={(props) => <AllCriteria  {...props} />} />
                <Route path='/urllandingpage' render={(props) => <UrlLandingPage {...props} />} />
                {/* <Route path="/urllandingpage" component={ UrlLandingPage} /> */}

                <Route path='/sub_admin' render={(props) => <Admin  {...props} />} />

                <Route path='/select_questions' render={(props) => <SelectQuestions  {...props} />} />

                <Route path='/insights' render={(props) => <Insights  {...props} />} />
              
                <Route path='/basic-configuration' render={(props) => <Configuration  {...props} />} />
                <Route path='/graph-configuration' render={(props) => <GraphConfiguration  {...props} />} />
                <Route path='/capability' render={(props) => <BS_Capability  {...props} />} />
                <Route path='/businessServices' render={(props) => <BS_Services  {...props} />} />
                <Route path='/server' render={(props) => <BS_Server  {...props} />} />
                <Route path='/services' render={(props) => <Services  {...props} />} />

                <Route path='/roles' render={(props) => <Roles  {...props} />} />


                {/* <Route path='/d3' render={(props) => <D3  {...props} />} /> */}
                {/* <Route path='/ahp_hierarchy' render={(props) => <AHP  {...props} />} /> */}

                {/* BSP */}
                <Route path='/map-services' render={(props) => <MapService {...props} />} />
                <Route path='/all-services' render={(props) => <AllServices {...props} />} />


                <Route path='/' render={(props) => <Dashboard {...props} />} />

              </Switch>
            </Suspense>
          </Layout>);
      }

    }
    return (
      <div className="App">
        {layout}
      </div>
    );
  }
}
const mapStatetoProps = state => {
  return {
    isAuth: state.auth.token,
    accessLevel: state.auth.accessLevel,
  };
}

const mapDispatchToProps = dispatch => {
  return {
    onTryLogin: () => dispatch(actions.authCheckStatus()),
    checkMapping: () => dispatch(actions.checkMapping()),
    checkAssessment: () => dispatch(actions.checkAssessment())
  }
}

export default withRouter(connect(mapStatetoProps, mapDispatchToProps)(App));
