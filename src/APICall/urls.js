


//AHP Criteria API URLS
export const FETCH_ALL_AHP_CRITERIA = '/ahp/getAll';
export const FETCH_SINGLE_AHP_CRITERIA = '/ahp/fetch/';
export const ADD_AHP_CRITERIA = '/ahp/add';
export const EDIT_AHP_CRITERIA = '/ahp/edit';
export const DELETE_AHP_CRITERIA = '/ahp/delete';

//Projects
export const FETCH_ALL_PROJECT = '/project';


//Roles
export const EXPORT_ALL_ROLES = '/all_roles';
export const EXPORT_ALL_ROLE_INFO = '/roles';
export const EXPORT_EACH_ROLE_INFO = '/roles';
export const EXPORT_BASIC_ROLE_INFO = '/roles/basic';
export const ADD_NEW_ROLES = '/roles/add';
export const UPDATE_ROLES = '/roles/edit';

//BSP
//Business Capability;

export const FETCH_BUSINESS_CAPABILITIES = '/business_capability'





