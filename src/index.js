import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter } from 'react-router-dom';
import {store} from './store'



import { Provider } from 'react-redux';

//cssTransform
import './public/plugins/magnific-popup/magnific-popup.css';
import './public/fullcalendar/css/fullcalendar.min.css';
import './public/assets/css/bootstrap.min.css';
import './public/assets/css/metismenu.min.css';
import './public/assets/css/icons.css';
import './public/assets/css/style.css';


import 'jquery/dist/jquery.min.js'
import 'bootstrap/dist/js/bootstrap.min.js'



const app = (
    <Provider store={store}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>
);

ReactDOM.render(app, document.getElementById('root'));

serviceWorker.unregister();
