export const specialCharacters = (str) =>{
    return /[~`!#@$%&*+=\-_[\]\\';,/{}|\\":<>]/g.test(str);
}

export const btnStyle = {
    backgroundColor: "#8075c1"
}

export const sidebarStyle = {
    backgroundColor: "#ffffff"
}

export const sidebarStyleClassic = {
    backgroundColor: "#000000"
}

export const sidebarTextColor = {
    color: "#000000"
}
export const sidebarTextColorClassic = {
    color: "#ffffff"
}

export const btnColor = "#000b1a"
