export const TreeMapping = (nextParentNode, node) =>{
    if(nextParentNode.type === "ahp1"){
        if(node.type === "ahp2" || node.type === "tag"){
            return true;
        }
        else{
            return false;
        }
    }
    else if(nextParentNode.type === "ahp2"){
        if(node.type === "tag"){
            return true;
        }
        else{
            return false;
        }
    }
    else if(nextParentNode.type === "tag"){
        return false;
    }
    else
        return false;
}