export const header = ['quest_tag', 'ahp_criteria_lev_1', 'ahp_criteria_lev_2', 'ahp_criteria_lev_3', 'ahp_comment', 'seq_no', 'measure', 'source_response', 'question', 'answer_key', 'description', 'internal_notes', 'response_type', 'topics', 'levels', 'domains', 'response_json', 'allow_comments', 'response_keys', 'response_1', 'response_2', 'response_3', 'response_4', 'response_5', 'response_6', 'response_7', 'response_8', 'response_9', 'response_10', 'response_11','response_11', 'response_13','response_14', 'response_15','response_16', 'response_17','response_18', 'response_19','response_20']

export const strategyHeader = ['Name', 'Input Type', 'Value', 'Type', 'AE Header', 'Order Number'];

export const allAHPCriteria = [["ahp1","AHP Criteria Level 1"],["ahp2","AHP Criteria Level 2"],["ahp3","AHP Criteria Level 3"]];

export const ahpHeader = ['Name', 'Criteria'];