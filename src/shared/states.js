export const generateInput = (currentState, inputField) =>{
    inputField && inputField.map(i =>{
        let attribute = '';
        if(i.data_type === "checkbox"){
            attribute = []
        }
        currentState = {
            ...currentState,
            [i.attributes_name]: attribute
        }
    })
    return currentState;
}

export const handleRadio = (inputData, allData) => {
    let _finalData = ''
    console.log(allData, inputData)
    if (allData === '') {
        _finalData = inputData;
    }
    else {
        _finalData = inputData
    }
    return _finalData;
}

export const handleCheckbox = (inputData, allData, checked) => {
    if(checked) {
        allData.push(inputData);
    }
    else {
        let dataIndex = allData.indexOf(inputData)
        allData.splice(dataIndex, 1);
    }
    return allData;
}