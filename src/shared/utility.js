import React , {Component } from 'react';
export const updateObj = (oldObj, updatedProperties) =>{
    return{
        ...oldObj,
        ...updatedProperties
    }
};

export const checkValidity = (value, rules) => {
    let isValid = true;
    if (!rules) {
        return true;
    }
    
    if (rules.required) {
        isValid = value.trim() !== '' && isValid;
    }

    if (rules.minLength) {
        isValid = value.length >= rules.minLength && isValid
    }

    if (rules.maxLength) {
        isValid = value.length <= rules.maxLength && isValid
    }

    if (rules.isEmail) {
        const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
        isValid = pattern.test(value) && isValid
    }

    if (rules.isNumeric) {
        const pattern = /^\d+$/;
        isValid = pattern.test(value) && isValid
    }

    return isValid;
}

//convert array to key value pair
export const handleKeyValue = (arrayData) =>{
    // console.log(arrayData);
    let object = [];
    for(let key in arrayData){
        object.push({
            value:arrayData[key],
            label : arrayData[key]
        })
    }
    // console.log(object);
    return object;
}

//convert array to key value pair
export const handleKeyValueProfile = (arrayData) =>{
    //console.log(arrayData);
    let object = [];
    for(let key in arrayData){
        //console.log(arrayData[key])
        object.push({
            value:arrayData[key].profile_id,
            label : arrayData[key].profile_name
        })
    }
    // console.log(object);
    return object;
}

//convert array to key value pair for mail
export const handleKeyValueMail = (arrayData) =>{
    //console.log(arrayData);
    let object = [];
    for(let key in arrayData){
        //console.log(arrayData[key])
        object.push({
            value:arrayData[key].id,
            label : arrayData[key].item
        })
    }
    // console.log(object);
    return object;
}

export const handleProfile = (profiles, profileId) =>{
    let let_pro = [];
    //console.log(profileId, profiles);
    
    if(profileId){
        profileId = profileId.split(',');
        //console.log(profileId)
        for(var i in profileId){
            for(var j in profiles){
                //console.log(profileId[i])
                if(parseInt(profiles[j].value) === parseInt(profileId[i])){
                    //console.log(profileId)
                    let_pro.push(profiles[j]);
                }
            }
        }
    }
    
    return let_pro;
}

export const handleKeyValueClient = (arrayData) =>{
    // console.log(arrayData);+(option && option.l_name ? option.l_name : '')
    let object = [];
    for(let key in arrayData){
        object.push({
            value:arrayData[key].clnt_id,
            label : arrayData[key].f_name+" "+(arrayData[key].l_name ? arrayData[key].l_name: ''),
        })
    }
    // console.log(object);
    return object;
}

export const handleKeyValueUser = (arrayData) =>{
    // console.log(arrayData);
    let object = [];
    if(arrayData){
        for(let key in arrayData){
            object.push({
                value:arrayData[key].user_id,
                label : arrayData[key].f_name+" "+arrayData[key].l_name,
            })
        }
    }
    // console.log(object);
    return object;
}

 //convert array to key value pair
export const handleKeyValueApps = (arrayData) => {
    //console.log(arrayData);
    let object = [];
    for (let key in arrayData) {
      object.push({
        value: arrayData[key].app_id,
        label: arrayData[key].app_name
      })
    }
    // console.log(object);
    return object;
  }

//convert object to array
export const objectToArray = (obj) =>{
    let new_obj = [];
    // console.log(obj);
    obj && obj.map( o =>{
        // console.log(o)
        if(o)
            new_obj.push(o.value)
    });
    return new_obj;
}

// convert key value pair to string
export const convertToString = (object) =>{
    let  obj = [];
    if(object){
        for(var i=0; i<object.length; i++) {
            obj.push(
                object[i].value
            )
        }
    }
    return obj;
}

export const getFetchData = (array, unique) =>{
    let content = [];
    //array = array.JSON.parse();
    //console.log(array);
    array.map((data, index) =>{ 
        //console.log(unique+index+1);
        //content.push( <AUX key={"'"+unique+"'"+index}><label >{data.choice}</label><br /></AUX> );
        return content.push( <p key={"'"+unique+"'"+index}>{data.choice}</p> );
    }) 
    return content; 
}

export const handleKeyValueresponse = (arrayData) =>{
    // console.log(arrayData);
    let object = [];
    if(arrayData){
        for(let key in arrayData){
            object.push({
                value:arrayData[key].choice,
                label : arrayData[key].choice,
            })
        }
    }
    // console.log(object);
    return object;
}



export const handleKeyValueresponseBasic = (arrayData) =>{
    // console.log(arrayData);
    let object = [];
    if(arrayData){
        for(let key in arrayData){
            object.push({
                choice:arrayData[key],
            })
        }
    }
    // console.log(object);
    return object;
}

export const strategySchemaMapping = (slug) =>{
    if(slug.trim() === 'score'){
        return "Score"
    }
    else if(slug.trim() === 'dropdown'){
        return "Dropdown";
    }
}

export const generateGraph =(data, selectedApps) =>{
    if (data && data.length > 0) {
        let key = Object.keys(data[0]);
        let _selectedApps = [];
        if (selectedApps && selectedApps.length > 0) {
            data.map(d => {
                let apps = d.APPS ? d.APPS : (d.Apps ? d.Apps : (d.apps ? d.apps : null))
                let selected = selectedApps.filter(app => app === apps)
                if (selected.length > 0) {
                    _selectedApps.push(d)
                }
            })
        }
        else {
            _selectedApps = [...data];
            if (data.length > 10) {
                _selectedApps = data.slice(0, 10).map(i => {
                    return i;
                })
            }
        }
        return { selectedData: _selectedApps, dataKey: key[0], data: data }
    }
    else {
        return { selectedData: [], dataKey:[], data: [] }}
}