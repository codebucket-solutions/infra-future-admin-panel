export const SIDE_BAR = 'side_bar';
export const TOP_BAR = 'top_bar';
export const LOGINPAGE = 'loginpage';
export const REG_PAGE = 'reg_page';
export const SWITCHTOREG = 'switch_reg';
export const SWITCHTOLOGIN = 'switch_login';
export const RECOVER_PASS = 'recover_pass';
export const FOOTER = 'footer';

export const AUTH_START = 'AUTH_START';
export const AUTH_SUCCESS = 'AUTH_SUCCESS';
export const AUTH_FAILED = 'AUTH_FAILED';
export const INIT_LOGOUT = 'INIT_LOGOUT';
export const LOGOUT = 'LOGOUT';

export const SET_AUTH_REDIRECT_PATH = 'SET_AUTH_REDIRECT_PATH';
export const UPDATE_PROFILE_MAPPING = 'UPDATE_PROFILE_MAPPING';
export const APP_RESPONSE = 'APP_RESPONSE';

export const UPDATE_ASSESSMENT_FILTER = 'UPDATE_ASSESSMENT_FILTER';

export const DASHBOARD_NAVIGATION_ACTION = 'DASHBOARD_NAVIGATION_ACTION';
export const THEME_SELECTION_ACTION = "THEME_SELECTION_ACTION"
