import * as actionTypes from '../action.js';


export const checkAssessment = () =>{
    return dispatch =>{
        const admin_assessment_basic_filter  = JSON.parse(localStorage.getItem('admin_assessment_basic_filter'));
        const admin_assessment_a1_filter  = JSON.parse(localStorage.getItem('admin_assessment_a1_filter'));
        const admin_assessment_a2_filter  = JSON.parse(localStorage.getItem('admin_assessment_a2_filter'));
        const admin_cust_filter  = JSON.parse(localStorage.getItem('admin_cust_filter'));
        //console.log(admin_assessment_filter, admin_cust_filter);
        if(admin_assessment_basic_filter || admin_cust_filter || admin_assessment_a1_filter || admin_assessment_a2_filter)
            dispatch(updateAssessmentFilter(
                admin_assessment_basic_filter ? admin_assessment_basic_filter : [], 
                admin_assessment_a1_filter ? admin_assessment_a1_filter : [],
                admin_assessment_a2_filter ? admin_assessment_a2_filter : [], 
                admin_cust_filter ? admin_cust_filter : [] ));
    }
}

//update assessment filter
export const updateAssessmentFilter = (assessmentBasicFilter, assessmentA1Filter, assessmentA2Filter, customerFilter) =>{
    return {
        type:actionTypes.UPDATE_ASSESSMENT_FILTER,
        assessmentBasicFilter:assessmentBasicFilter,
        assessmentA1Filter:assessmentA1Filter,
        assessmentA2Filter:assessmentA2Filter,
        customerFilter: customerFilter
    }
}

//update local storage for assessment filter
export const assessmentFilter = (assessmentBasicFilter, assessmentA1Filter, assessmentA2Filter, customerFilter) => {
    
    return dispatch =>{
        
        localStorage.setItem('admin_assessment_basic_filter',JSON.stringify(assessmentBasicFilter ? assessmentBasicFilter : []));
        localStorage.setItem('admin_assessment_a1_filter',JSON.stringify(assessmentA1Filter ? assessmentA1Filter : []));
        localStorage.setItem('admin_assessment_a2_filter',JSON.stringify(assessmentA2Filter ? assessmentA2Filter : []));
        localStorage.setItem('admin_cust_filter',JSON.stringify(customerFilter ? customerFilter :[]));

        dispatch(updateAssessmentFilter(assessmentBasicFilter ? assessmentBasicFilter : [],
            assessmentA1Filter ? assessmentA1Filter : [],
            assessmentA2Filter ? assessmentA2Filter : [],
             customerFilter ? customerFilter: []));
    }
}