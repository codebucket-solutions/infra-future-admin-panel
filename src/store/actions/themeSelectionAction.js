import * as types from '../action';

export const selectthemeAction = (isClassic) => {
    console.log(isClassic)
    return {
        type: types.THEME_SELECTION_ACTION,
        payload : isClassic
    }
}