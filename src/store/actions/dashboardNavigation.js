import * as types from '../action';

export const dashboardNavigation = (route) => {
    return {
        type: types.DASHBOARD_NAVIGATION_ACTION,
        payload: route
    }
}