import * as actionTypes from '../action.js';
import axios from '../../axios-details';

export const authStart = () =>{
    return {
        type:actionTypes.AUTH_START
    };
};

export const authSuccess = (token, name, accessLevel, footer) =>{
    return {
        type:actionTypes.AUTH_SUCCESS,
        token:token,
        name:name,
        accessLevel:accessLevel,
        footer:footer
    }
}

export const authFailure = (error) =>{
    return {
        type:actionTypes.AUTH_FAILED,
        error:error
    }
}

export const logout=() =>{
    localStorage.removeItem('adminToken');
    localStorage.removeItem('name');
    localStorage.removeItem('accessLevel');
    localStorage.removeItem('footer');
    return {
        type:actionTypes.INIT_LOGOUT
    }
}


export const auth = (email,password) =>{
    return dispatch =>{
        dispatch(authStart());
        const authData={
            email:email,
            password:password,
        }
        // console.log(authData);
        axios.post('/login',authData)
        .then(res => {
            if(res.data.status){
                //console.log(res.data);
                //save to local
                localStorage.setItem('adminToken',res.data.data.token);
                localStorage.setItem('name',res.data.data.name);
                localStorage.setItem('accessLevel',res.data.data.access_level);
                localStorage.setItem('footer',res.data.data.footer);
                //dispatch

                dispatch(authSuccess(res.data.data.token, res.data.data.name, res.data.data.access_level,res.data.data.footer));
            }
            else{
                dispatch(authFailure(res.data.message));
            }
            //console.log(res.data);
        }).catch(err =>{
            //console.log(err);
            dispatch(authFailure("Server Error"));
        });
    };
};

export const checkToken = (token, name, accessLevel, footer) =>{
    return dispatch =>{
        dispatch(authStart());
        const authData={
            token:token,
        }
        //console.log(authData);
        axios.post('/login/check_login',authData)
        .then(res => {
            //console.log(res.data);
            if(res.data.status){
                //dispatch

                dispatch(authSuccess(token, name, accessLevel, footer));
            }
            else{
                //console.log("asdasdsad")
                dispatch(logout());
            }
        }).catch(err =>{
            console.log(err);
            dispatch(logout());
        });
    };
};

export const setAuthRedirect= (path) =>{
    return{
        type:actionTypes.SET_AUTH_REDIRECT_PATH,
        path:path
    }
}

export const authCheckStatus= () =>{
    return dispatch =>{
        const token = localStorage.getItem('adminToken');
        const name =localStorage.getItem('name'); 
        const accessLevel =localStorage.getItem('accessLevel');
        const footer =localStorage.getItem('footer'); 

        

        if(!token){
            dispatch(logout());
        }
        else{
            dispatch(checkToken(token, name, accessLevel, footer));
        }
    }
}