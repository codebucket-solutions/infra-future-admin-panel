import * as actionTypes from '../action';
const initialState = {
    top_bar: true,
    side_bar: true,
    loginpage: false,
    isRegpage: false,
    recover_pass: false,
    footer: true,
    activeTab: null,
    isClassic : localStorage.getItem("selectedTheme")=== null ? true : (localStorage.getItem("selectedTheme") === "true" ? true : false)

}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.TOP_BAR:
            return {
                ...state,
                top_bar: !state.top_bar
            };
        case actionTypes.SIDE_BAR:
            return {
                ...state,
                side_bar: !state.side_bar
            };
        case actionTypes.LOGINPAGE:
            return {
                ...state,
                loginpage: !state.loginpage
            };
        case actionTypes.REG_PAGE:
            return {
                ...state,
                isRegpage: !state.isRegpage
            };
        case actionTypes.SWITCHTOLOGIN:
            return {
                ...state,
                isRegpage: !state.isRegpage,

            };
        case actionTypes.SWITCHTOREG:
            return {
                ...state,
                isRegpage: !state.isRegpage
            };
        case actionTypes.RECOVER_PASS:
            return {
                ...state,
                recover_pass: !state.recover_pass
            };
        case actionTypes.FOOTER:
            return {
                ...state,
                footer: !state.footer
            };
        case actionTypes.DASHBOARD_NAVIGATION_ACTION:
            return { ...state, activeTab: action.payload }
            case actionTypes.THEME_SELECTION_ACTION:
                return {...state, isClassic : action.payload }
        default:
            return state;
    }

}

export default reducer;