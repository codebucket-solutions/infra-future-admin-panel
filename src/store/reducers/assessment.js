import * as actionTypes from '../action.js';
import { updateObj } from '../../shared/utility';

const initialState ={
    customerFilter:[],
    assessmentBasicFilter:[],
    assessmentA1Filter:[],
    assessmentA2Filter:[],
}

const reducer = (state = initialState, action) =>{
    switch (action.type){
        case actionTypes.UPDATE_ASSESSMENT_FILTER:
            return updateObj(state, {assessmentBasicFilter:action.assessmentBasicFilter,assessmentA1Filter:action.assessmentA1Filter,assessmentA2Filter:action.assessmentA2Filter, customerFilter: action.customerFilter});
        default:
            return state;
    }
}

export default reducer;