import React from 'react';
import { withRouter } from 'react-router-dom';
import './DashboardCardComp.css';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { dashboardNavigation } from '../../store/actions/dashboardNavigation';


const data = [
    {

        'icon': "mdi mdi-clipboard-outline",
        'title': "Users",
        'desc': "Add, Clients, Users ",
        'color': "blue"
    },
    {
        'icon': "mdi mdi-album",
        'title': "Questions",
        'desc': "Add, Review, and Update Assessment Questions",
        'color': "blue"
    }, {
        'icon': "mdi mdi-calendar-check",
        'title': "Assessment Profiles",
        'desc': "Add, Update Assessment Profiles",
        'color': "green"
    }, {
        'icon': "mdi mdi-buffer",
        'title': "Applications",
        'desc': "Import Applications to Assess",
        'color': "green"
    },
    {
        'icon': "mdi mdi-web",
        'title': "Pre-Signed URL’s",
        'desc': "Create, Send Surveys to Clients and Users",
        'color': "green"
    }
]

class DashboardCardComp extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            activeTab: null
        }
    }

    navigateToParentComponent = (e, screen) => {
        return screen === "Users" ? this.navigateToScreen("users")
            : screen === "Questions" ? this.navigateToScreen("questions")
                : screen === "Assessment Profiles" ? this.navigateToScreen("profile")
                    : screen === "Applications" ? this.navigateToScreen("apps")
                        : screen === "Pre-Signed URL’s" ? this.navigateToScreen('presignedurl')
                            : null
    }

    navigateToScreen = (route) => {
        this.setState({ activeTab: route })
        this.props.updateActiveTab(route)
        this.props.history.push("/" + route)

    }

    render() {
        return <div className="row">
            {data.map((obj) => {
                return <div onClick={(e) => this.navigateToParentComponent(e, obj.title)} className="col card-container">
                    <div className="card custom" >
                        <div className="card-body-wrapper">
                            <div className="icon-container" style={{ color: `${obj.color}` }}><i className={obj.icon}></i></div>
                            <div>
                                <div className="text-info">{obj.title}</div>
                                <div className="fs-6">{obj.desc}</div>
                            </div>
                        </div>

                    </div>
                </div>
            })}
        </div>
    }
}

const mapStateToProps = (state) => {
    console.log(state)
    return {
        data: state
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateActiveTab: (activeTab) => dispatch(dashboardNavigation(activeTab))
    }
}

export default compose(
    withRouter,
    connect(mapStateToProps, mapDispatchToProps))(DashboardCardComp);

