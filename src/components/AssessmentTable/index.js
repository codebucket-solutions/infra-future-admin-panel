import React , {Component } from 'react';
import { MDBDataTable } from 'mdbreact';
import { CSVLink } from "react-csv";
import AUX from '../../hoc/Aux_';
import equal from 'fast-deep-equal';
// ModalConfirm from '../components/Modal/modalConfirm';

class AssessmentTable extends Component{
    constructor(props){
        super(props);
        this.state = {
            columns: [],
            rows: [],
            csvData:[],
            data:[],
        }
        this.ref = ""
    }

    toTitleCase(str) {
        return str.replace(
            /\w\S*/g,
            function(txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            }
        );
    }

    exportExcel() {  
        if(this.ref)   {
            //console.log(this.ref)
            this.ref.link.click()
        }   
            
    }

    fetchProfile = (data) =>{
        let csvData= [];
        //console.log(data);
        let columns = [];
        
        if(data.length <= 0)
            return;
        let temp_const =  Object.keys(data[0]);
        //console.log(temp_const);
        csvData.push(temp_const);
        temp_const.forEach( item => {
            columns.push({
                label:this.toTitleCase(item),
                field:item,
            })
        })
        for(let i in data){
            csvData.push(Object.values(data[i]));
        }
        //console.log(columns);
        this.setState({columns: columns, rows: data, csvData: csvData})
        

    }

    componentDidMount(){
        this.setState({data: this.props.data})
        this.fetchProfile(this.props.data);
    }

    componentDidUpdate(prevProps) {
        if(!equal(this.props.data, prevProps.data)){
            //console.log(this.props.data)
            this.fetchProfile(this.props.data);
        }
    }

    handleMapping (id){
        this.props.updatequestions(id);
        this.props.history.push('/questions')
        
    }

    handleModal = (id) => {
        this.setState(prevState => {
            return {toggle: !prevState.toggle, profileId: id}
        })
    }

    render(){
        let filename = this.props.title+Date.now()+".csv";
        return(
            <AUX>
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card m-b-20">
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-md-6">
                                        <h4 className="mt-0 header-title"><strong>{this.props.title}</strong></h4>
                                    </div>
                                    <div className="col-md-6 text-right">
                                        <CSVLink
                                            ref={(ref) => this.ref = ref}
                                            className="btn btn-primary btn-lg"
                                            filename={filename} 
                                            data={this.state.csvData}
                                            asyncOnClick={true}
                                            title="Export assessment result"
                                        >Export
                                        </CSVLink>
                                    </div>
                                </div>
                                <div style={{overflow:'auto'}}>
                                  <MDBDataTable
                                      bordered
                                      hover
                                      data={{ columns: this.state.columns, rows:this.state.rows }}
                                      info={this.state.rows.length > 0 ? true : false}
                                      
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </AUX>
        );
    }
}
  
export default AssessmentTable;