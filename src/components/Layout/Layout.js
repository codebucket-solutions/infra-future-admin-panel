import React from 'react';
import Aux from '../../hoc/Aux_';
import Topchart from '../Topchart/Topchart';
import Leftsidebar from '../Leftsidebar/Leftsidebar';
import Footer from '../Footer/Footer';

const layout = (props) => {
    const style = { marginLeft: 0 };
    //console.log(parseInt(process.env.REACT_APP_KAIBUR.trim()));
    return (
        <Aux>
            {!props.isloginpage ?
                <div id="wrapper" style={{ color: "white !important" }}>
                    {
                        parseInt(process.env.REACT_APP_KAIBUR.trim()) === 0 ?
                            <>
                                {props.topbar ? <Topchart /> : null}
                                {props.sidebar ? <Leftsidebar /> : null}
                            </>
                            : null
                    }

                    <main>
                        <div className="content-page" style={(parseInt(process.env.REACT_APP_KAIBUR.trim()) === 0) ? {} : style} >
                            <div className="content">
                                <div className="container-fluid">
                                    {props.children}
                                </div>
                            </div>
                        </div>
                    </main>
                    {props.footer ? <Footer /> : null}
                </div> : props.children}
        </Aux>
    );
}
export default layout;