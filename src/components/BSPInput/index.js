import React from "react";
import  { capitalizeFirstLetter } from "../../shared/stringManipulation"
const AddBusiness = (props) => {
    const getFunctions = () => {
        let fields = "";
        if (
            props.data_type == "text" ||
            props.data_type == "number" ||
            props.data_type == "email"
        ) {
            fields = (
                <div className="form-group">
                <label htmlFor="mobile">{capitalizeFirstLetter(props.name)}</label>
                <input
                    value={props.value}
                    onChange={(e) => props.handleInputChange(e, props.name,props.data_type, true)}
                    type={props.data_type}
                    className="form-control"
                    placeholder={"Enter "+ capitalizeFirstLetter(props.name)}
                    required={props.isRequired == "yes" ? true : false}
                />
                </div>
            );
        }
        else if (props.data_type === "dropdown") {
            let dropdown = props.options.split(",");

            let arDropdown =
                dropdown &&
                dropdown.map((m, i) => (
                    <option key={i} value={m}>
                        {m}
                    </option>
                ));
            fields = (
                <div className="form-group">
                <label htmlFor="mobile">{capitalizeFirstLetter(props.name)}</label>
                <select
                    value={props.value && props.value}
                    onChange={(e) => props.handleInputChange(e, props.name,props.data_type, true)}
                    required={props.isRequired == "yes" ? true : false}
                    className="form-control"
                >
                    <option value="0">--select--</option>
                    {arDropdown}
                </select>
                </div>
            );
        }
        else if (props.data_type == "checkbox") {
            let checkbox = props.options.split(",");

            let fields =
                checkbox &&
                checkbox.map((m, i) => (
                    <>
                    <input
                        type="checkbox"
                        id={m+i}
                        name={props.name}
                        className="pt-3"
                        value={m.trim()}
                        onChange={(e) => props.handleInputChange(e, props.name,props.data_type, true)}
                        required={props.isRequired == "yes" ? true : false}
                    />
                    <label htmlFor={m+i} className="ml-3"> {capitalizeFirstLetter(m)} </label>
                    <br />
                    </>
                ));
                return (
                    <div className="form-group">
                        <label htmlFor="mobile">{capitalizeFirstLetter(props.name)}</label>
                        <br />
                        {fields}
                    </div>
                )
        }

        else if (props.data_type == "radio") {
            let radio = props.options.split(",");

            let fields =
                radio &&
                radio.map((m, i) => (
                    <>
                    <input
                        type="radio"
                        id={m+i}
                        value={m.trim()}
                        name={props.name}
                        onChange={(e) => props.handleInputChange(e, props.name,props.data_type, true)}
                        required={props.isRequired == "yes" ? true : false}
                    />
                        <label className="ml-3" htmlFor={m+i}> {capitalizeFirstLetter(m)} </label>
                        <br />
                    </>
                ));
            
                return (
                    <div className="form-group">
                        <label htmlFor="mobile">{capitalizeFirstLetter(props.name)}</label>
                        <br />
                        {fields}
                    </div>
                )
        }
        // console.log(fields);

        return fields;
    }
    return <>{getFunctions()}</>;
};

export default AddBusiness;
