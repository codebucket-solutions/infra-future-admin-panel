import React from 'react';

const StrategyTable = (props) =>{
    
    let input = '';
    if(props.type === "score" || props.type === "input_type"){
        input  =(
            <input style={{width:'160px'}} maxlength="1" value={props.value} onChange={props.handleInputChange} type="number" className="form-control" />
        );
    }
    else if(props.type === "dropdown" || props.type === "Boolean"){
        
        let dropdown = props.dropdown.split(",");
        
        let arDropdown = dropdown && dropdown.map((m, i) =>(
            <option key={i} value={m.toLowerCase().trim()}>{m}</option>
        ))
        //console.log(dropdown);
        input =(
            <select style={{width:'160px'}} value={props.value && props.value.toLowerCase().trim()} onChange={props.handleInputChange} className="form-control">
                <option value='0'>--select--</option>
                {arDropdown}
            </select>
        )
    }
    

    return(
        <>
        {input}
        </>
    )
}

export default StrategyTable;