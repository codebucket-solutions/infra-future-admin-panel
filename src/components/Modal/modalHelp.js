import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from 'react-redux';

class ModalApp extends Component {

    constructor(props) {
        super(props);
        this.state={

      };
    }
    render() {

        const data = (
        <div>
            <h5 style={{fontWeight: 'bold', marginBottom:'2em', textAlign:'center'}}>Self Service Portal for Application Assessment</h5>
            <p>The Self-Service Portal (SSP) is a web application that assists an organization in the overall application assessment process including manage a list of applications to be assessed, manage assessment questions and gather responses for the assessment questions using surveys. The web application provides an administrator to setup assessment questions with the potential responses and allows gathering of responses, so an assessment can be completed.</p>
            <p>The following are the main components of the SSP application:</p>
            <ol>
                <li>Applications &ndash; Applications are basic assessment units. Responses are gathered for applications and are used for assessments</li>
                <li>Question &ndash; Assessment questions are information that is expected to be gathered for each of the applications. Each of the assessment question in turn has attributes including Question type, Responses, Domains, Topics, and Criteria</li>
                <li>Client &ndash; A client is an entity that is responding to assessment questions associated with their applications. A registered and licensed client can login and interact with the portal. A set of users can eb associated to a client</li>
                <li>Profiles &ndash; Profile is a logical entity that associates a set of questions to a client and an application. A profile allows an administrator to narrow down the list of questions that are required to be answered by a client for an application</li>
                <li>User, Role &ndash; Administrative users and their associated roles that can login to the portal and perform various actions</li>
                <li>Admin Portal &ndash; Browser based interface for assessment administrators</li>
                <li>User Portal (also referred to as Client Portal) - Browser based interface for non-administrative users</li>
                <li>Insights &ndash; The web application provides basic insights for all the data that is gathered. The insights are provided by displaying appropriate graphs, charts and tables.</li>
                <li>Advanced Insights - The web application can be integrated with an add-on application referred to as the Assessment Engine to provide additional insights</li>
                <li>Configuration &ndash; Configuration settings allow administrators to set various settings including notifications, enable and disable bulk import options</li>
            </ol>
            <p>The portal provides the following main functionalities:</p>
            <ol>
                <li>Add or Import assessment questions</li>
                <li>Add and manage profiles</li>
                <li>Add, Delete and Edit administrators, clients and users</li>
                <li>Add or Import a list of applications to be assessed</li>
                <li>Enter or gather responses to assessment questions</li>
                <li>View, or export all assessment responses</li>
                <li>Gain Insights and track status of surveys</li>
            </ol>
            <p>Please contact support for a detailed help file.</p>
        </div>
        );

        return (
        <div>
            <Modal isOpen={this.props.modal} size="xl" toggle={this.props.toggle} >
            <ModalHeader toggle={this.props.toggle}>Help</ModalHeader>
            <ModalBody>
                {data}
            </ModalBody>
            <ModalFooter>
                <Button color="secondary" onClick={this.props.toggle}>Cancel</Button>
            </ModalFooter>
            </Modal>
        </div>
        );
    }
}

const mapStateToProps = state =>{
  return {
      token : state.auth.token
  }
}

export default connect(mapStateToProps)(ModalApp);

