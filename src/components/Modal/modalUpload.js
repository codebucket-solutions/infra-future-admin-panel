import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from 'react-redux';

import AUX from '../../hoc/Aux_';
import axios from '../../axios-details';

import Spinner from '../../components/Spinner/simpleSpinner';
import Alert from '../../components/Alert/common';
import Star from '../../components/Required/star';

class ModalCSV extends Component {

  constructor(props) {
    super(props);
    this.state = {
      type: '',
      client: '',
      file: '',
      clientOptions: [''],
      profileOptions: [''],
      loading: false,
      message: null,
      error: null,
      update: false,
      data: 0,
      error_data: {},
    };
  }

  inputChangeHandler = (event, controlName) => {
    this.setState({ [controlName]: event.target.value });
  }

  fileChangeHandler = (event, controlName) => {
    this.setState({ [controlName]: event.target.files[0] });
  }

  closeModal = () => {
    setTimeout(
      () => this.props.toggle(this.state.update)
      , 2000);
  }

  fetchUsers = () => {
    this.setState({ loading: true });
    const config = {
      headers: { Authorization: `Bearer ${this.props.token}` }
    };
    axios.get('/user/userclient', config)
      .then(res => {
        //console.log(res.data)
        if (res.data.status) {
          this.setState({ loading: false, clientOptions: res.data.data.clients })
        }
      })
      .catch(err => {
        this.setState({ loading: false })
      });
  }


  componentDidMount() {
    this.fetchUsers();
  }


  submitHandler = (e) => {
    e.preventDefault();
    //console.log(this.state);
    this.setState({ loading: true, error: null, message: null, error_data: {} });
    const { type, client } = this.state;

    let formData = new FormData();
    formData.append('csv_file', this.state.file);
    formData.append('user_id', client);

    const config = {
      headers: { 'content-type': 'multipart/form-data', Authorization: `Bearer ${this.props.token}` }
    }
    if (type === "1" && client) {
      // upload response of multiple apps of a user
      axios.post('/response/import', formData, config)
        .then(res => {
          //console.log(res.data.data);
          if (res.data.status) {
            this.setState({ loading: false, update: true, data: res.data.data.newApp, message: res.data.message });
            this.closeModal();
          }
          else
            this.setState({ loading: false, update: true, data: res.data.data.newApp, error: res.data.message, error_data: res.data.data.incorrect });
          // this.closeModal();
        })
        .catch(err => {
          //console.log(err);
          this.setState({ loading: false, error: "Something went wrong.Please try again" })
          // this.closeModal();
        })
    }
    else if (type === "2" && client) {
      formData.append('user_name', this.props.name);
      // upload apps of a user
      axios.post('/apps/import', formData, config)
        .then(res => {
          //console.log(res.data);
          if (res.data.status) {
            this.setState({ loading: false, update: true, message: res.data.message });
            this.closeModal();
          }
          else
            this.setState({ loading: false, update: true, error: res.data.message });
          // this.closeModal();
        })
        .catch(err => {
          //console.log(err);
          this.setState({ loading: false, error: "Something went wrong.Please try again" })
          // this.closeModal();
        })
    }
    else {
      this.setState({ loading: false, error: "Please select import type and client name to proceed" })
    }
  }




  render() {
    let arr = [];
    if (this.state.error_data) {
      for (var loop in this.state.error_data) {
        arr.push(<Alert classes={"alert-danger"} msg={loop + " - " + this.state.error_data[loop].toString()} />)
      }
    }

    let toShow = '';
    if (this.state.type === '1') {
      toShow = (
        <AUX>
          <div className="col-md-12 text-center" style={{ fontSize: '16px', fontWeight: 700 }}>
            <a href='files/Responses-App.csv' style={{ color: '#e66161' }} download><u>Download CSV Format to upload response of apps of a client</u></a>
          </div>
        </AUX>
      )
    }
    else if (this.state.type === '2') {
      toShow = (
        <div className="col-md-12 text-center" style={{ fontSize: '16px', fontWeight: 700 }}>
          <a href='files/Apps-Import.csv' style={{ color: '#e66161' }} download><u>Download CSV Format to upload apps</u></a>
        </div>
      )
    }

    let body = (
      <div className="card-body">
        <form onSubmit={this.submitHandler}>
          {this.state.message ? <Alert classes={"alert-success"} msg={this.state.message} /> : null}
          {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} /> : null}
          {parseInt(this.state.data) > 0 ? <Alert classes={"alert-danger"} msg={"There are " + this.state.data + " new apps.Please update the mandatory details."} /> : null}

          {
            arr.length > 0 ? arr.map((q, i) => {
              return (<div key={i}>{q}</div>)
            }) : null
          }

          {this.state.loading ? <Spinner /> : null}

          <div className="row">

            <div className="col-sm-6 col-md-6 col-lg-6">
              <div className="form-group">
                <label htmlFor="type">Import Type<Star /></label>
                <select
                  required
                  className="form-control"
                  onChange={(event) => this.inputChangeHandler(event, "type")}
                  value={this.state.type}>
                  <option>Select</option>
                  <option value="1">Response Import</option>
                  <option value="2">App Import</option>
                </select>
              </div>
            </div>

            <div className="col-sm-6 col-md-6 col-lg-6">
              <div className="form-group">
                <label htmlFor="client">Client Name<Star /></label>
                <select
                  required
                  className="form-control"
                  onChange={(event) => this.inputChangeHandler(event, "client")}
                  value={this.state.client}>
                  <option>Select</option>
                  {
                    this.state.clientOptions.map((option, index) => {
                      return (<option key={index} value={option.clnt_id}>{option.f_name + " " + (option.l_name ? option.l_name : '')}</option>)
                    })
                  }
                </select>
              </div>
            </div>
            <div className="col-sm-6 col-md-6 col-lg-6">
              <div className="form-group">
                <label htmlFor="name">Upload CSV<Star /></label>
                <input type="file" required accept=".csv" onChange={(event) => this.fileChangeHandler(event, "file")} name="file" id="file" className="" />
              </div>
            </div>
            <div className="col-sm-6 col-md-6 col-lg-6">
              <div className="form-group">
                <br />
                <button type="submit" className="btn btn-primary waves-effect waves-light">Upload</button>
              </div>
            </div>
          </div>
          <div className="row">
            {toShow}
          </div>
        </form>
      </div>
    );

    return (
      <div>
        <Modal size="lg" isOpen={this.props.modal} toggle={() => this.props.toggle(this.state.update)} >
          <ModalHeader toggle={() => this.props.toggle(this.state.update)}>Upload CSV</ModalHeader>
          <ModalBody>
            {this.state.loading ? <Spinner /> : body}
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={() => this.props.toggle(this.state.update)}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStatetoProps = state => {
  return {
    token: state.auth.token,
    name: state.auth.name,
  };
}

export default connect(mapStatetoProps)(ModalCSV);
