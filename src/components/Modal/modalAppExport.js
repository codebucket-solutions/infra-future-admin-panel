import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from 'react-redux';

import axios from '../../axios-details';
import Spinner from '../../components/Spinner/simpleSpinner';
import Alert from '../../components/Alert/common';
import Star from '../../components/Required/star';


class ChangeUser extends Component {

    constructor(props) {
        super(props);
        this.state={
          csv:1,
        };
    }

    toExport =(e) =>{
      let id = this.state.csv
      // console.log(id)
      id = parseInt(id);
      if(id === 1){
        this.props.exportCSV();
      }
      else if(id === 2){
        this.props.exportCSVPer();
      }
      else if(id === 3){
        this.props.exportCSVAdvance();
      } 
      this.closeModal(); 
    }

    inputChangeHandler = (e) =>{
      this.setState({csv:e.target.value})
    }
    
    closeModal = () =>{
      setTimeout(
        () => this.props.toggle()
        ,400);
    }

    
    
    render() {
        //console.log(this.state.options);
        let body = (
        <div className="card-body">
                <div className="row">
                    <div className="col-sm-9">
                      <div className="form-group">
                            <label htmlFor="type">Export Type<Star /></label>
                            <select 
                            required
                              className="form-control" 
                              onChange={(event) => this.inputChangeHandler(event)}
                              value={this.state.csv}
                              value={this.state.type}>
                                  <option value="1">App Details and Responses</option>
                                  <option value="2">App Details with Survey Status</option>
                                  <option value="3">AE Apps responses </option>
                            </select>
                        </div>
                    </div>
                    <div className="col-sm-3 mt-4">
                        <button type="submit" onClick={this.toExport}  className="btn btn-primary waves-effect waves-light ">Submit</button>
                    </div>
                </div>
        </div>
    );
    
    return (
      <div>
        <Modal isOpen={this.props.modal} toggle={() => this.props.toggle()} >
        <ModalHeader toggle={() => this.props.toggle()}>{this.props.title}</ModalHeader>
          <ModalBody>
            {body}
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={() => this.props.toggle()}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStatetoProps = state =>{
  return {
      token: state.auth.token,
  };
}

export default connect(mapStatetoProps)(ChangeUser);
