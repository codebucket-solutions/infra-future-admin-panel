import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import axios from '../../axios-details';
import { connect } from 'react-redux';
import makeAnimated from "react-select/animated";
import { objectToArray, handleKeyValueProfile, handleProfile } from '../../shared/utility';


import Alert from '../Alert/common';
import SimpleSpinner from '../../components/Spinner/simpleSpinner';
import Star from '../../components/Required/star';
import MySelect from '../ReactSelect/index';

const animatedComponents = makeAnimated();
class ModalApp extends Component {

    constructor(props) {
        super(props);
        this.state = {
            profile: [],
            allProfile: [],
            message: null,
            error: null,
            loading: false,
            update: false,
        };
    }

    fetchAPI = () => {
        this.setState({ loading: true, error: null, message: null });
        //console.log(this.props);
        const config = {
            headers: { Authorization: `Bearer ${this.props.token}` }
        };
        axios.get('/profile/apps/' + this.props.appId, config)
            .then(res => {
                let { profiles } = res.data.data;
                let { profile_id } = res.data.data;

                profiles = handleKeyValueProfile(profiles);
                profile_id = handleProfile(profiles, profile_id);
                //console.log(profile_id);
                if (res.data.status) {
                    this.setState({ loading: false, allProfile: profiles, profile: profile_id })
                }
            })
            .catch(err => {
                this.setState({ loading: false })
            });
    }

    selectTowChange = (profile) => {
        this.setState({ profile: profile });
    }

    componentDidMount() {
        this.fetchAPI();
    }



    submitHandler = (e) => {
        e.preventDefault();
        //console.log(this.state.profile);
        this.setState({ loading: true, error: null, message: null })
        let aId = this.props.appId;
        const profileData = {
            token: this.props.token,
            profiles: objectToArray(this.state.profile),
            app_id: aId,
        }
        //console.log(profileData);
        axios.post('/apps/attach', profileData)
            .then(res => {
                //console.log(res.data);
                if (res.data.status) {
                    this.setState({ loading: false, message: res.data.message })
                }
                else {
                    this.setState({ loading: false, error: res.data.message })
                }
                this.closeModal();
            })
            .catch(err => {
                this.setState({ loading: false, error: "Something went wrong. Please try again." })
                this.closeModal();
            })

    }

    inputChangeHandler = (event, controlName) => {
        //console.log(controlName);
        this.setState({ [controlName]: event.target.value });
    }

    closeModal = () => {
        setTimeout(
            () => this.props.toggle(null, null)
            , 700);
    }



    render() {
        let body = (
            <div className="card-body">
                <form onSubmit={this.submitHandler}>
                    {this.state.error ? <Alert msg={this.state.error} classes="alert-danger" /> : null}
                    {this.state.message ? <Alert msg={this.state.message} classes="alert-success" /> : null}
                    {this.state.loading ? <SimpleSpinner /> : null}
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="form-group">
                                <label htmlFor="name">App Name<Star /></label>
                                <input
                                    type="text"
                                    className="form-control"
                                    required
                                    disabled
                                    value={this.props.appName ? this.props.appName : null}
                                    onChange={(event) => this.inputChangeHandler(event, "name")}
                                    placeholder="Enter App Name" />
                            </div>
                            <div className="form-group">
                                <label htmlFor="l3">Profile<Star /></label>
                                <MySelect
                                    value={this.state.profile}
                                    components={animatedComponents}
                                    onChange={this.selectTowChange}
                                    options={this.state.allProfile}
                                    allowSelectAll={true}
                                    isMulti={true}
                                    noMin={true}
                                />

                            </div>
                            <button style={{ backgroundColor: "#003c8f" }} type="submit" className="btn btn-primary waves-effect waves-light">Save Changes</button>
                        </div>
                    </div>
                </form>
            </div>
        );

        return (
            <div>
                <Modal isOpen={this.props.modal} toggle={() => this.props.toggle(this.state.update)} >
                    <ModalHeader toggle={() => this.props.toggle(this.state.update)}>{this.props.title}</ModalHeader>
                    <ModalBody>
                        {body}
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={() => this.props.toggle(this.state.update)}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}
const mapStateToProps = state => {
    return {
        token: state.auth.token
    }
}

export default connect(mapStateToProps)(ModalApp);

