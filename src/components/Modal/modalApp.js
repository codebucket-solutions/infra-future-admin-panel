import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import TagsInput from 'react-tagsinput';
import axios from '../../axios-details';
import './react-tagsinput.css';
import { connect } from 'react-redux';

import Alert from '../Alert/common';
import SimpleSpinner from '../../components/Spinner/simpleSpinner';
import Star from '../../components/Required/star';

class ModalApp extends Component {

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      service: '',
      businessOwner: '',
      appOwner: '',
      tags: [],
      focused: false,
      input: '',
      option: null,
      message: null,
      error: null,
      loading: false,
      update: false,
    };
    this.handleChange = this.handleChange.bind(this);
  }

  fetchAPI = () => {
    this.setState({ loading: true, error: null, message: null });
    const config = {
      headers: { Authorization: `Bearer ${this.props.token}` }
    };
    axios.get('/apps/fetch/' + this.props.appId, config)
      .then(res => {
        let { app } = res.data.data;
        //console.log(res.data.data);
        if (res.data.status) {
          this.setState({
            loading: false,
            name: app.app_name,
            service: app.b_service ? app.b_service : '',
            businessOwner: app.b_owner ? app.b_owner : '',
            appOwner: app.app_owner ? app.app_owner : '',
            tags: app.tags ? app.tags.split(",") : []
          })
        }
      })
      .catch(err => {
        this.setState({ loading: false })
        //console.log(err);
      });
  }

  componentDidMount() {
    if (this.props.appId) {
      this.fetchAPI();
    }

  }

  handleChange(tags) {
    this.setState({ tags })
  }


  submitHandler = (e) => {
    e.preventDefault();
    this.setState({ loading: true, error: null, message: null })
    const appData = {
      token: this.props.token,
      app_name: this.state.name,
      b_service: this.state.service,
      b_owner: this.state.businessOwner,
      app_owner: this.state.appOwner,
      tags: this.state.tags.toString()
    }
    if (this.props.appId) {
      appData.app_id = this.props.appId;
      axios.put('/apps/edit', appData)
        .then(res => {
          //console.log(res.data);
          if (res.data.status) {
            this.setState({ loading: false, update: true, message: res.data.message })
          }
          else {
            this.setState({ loading: false, error: res.data.message })
          }
          this.closeModal(true);
        })
        .catch(err => {
          this.closeModal(true);
          this.setState({ loading: false, error: "Something went wrong. Please try again." })
        });
    }
    else {
      appData.user_name = this.props.name;
      //console.log(appData);
      axios.post('/apps', appData)
        .then(res => {
          if (res.data.status) {
            this.setState({ loading: false, update: true, message: res.data.message })

          }
          else {
            this.setState({ loading: false, error: res.data.message })
          }
          this.closeModal(true);

        })
        .catch(err => {
          this.setState({ loading: false, error: "Something went wrong. Please try again." })
          this.closeModal()
        });
    }
  }

  inputChangeHandler = (event, controlName) => {
    //console.log(controlName);
    this.setState({ [controlName]: event.target.value });
  }

  closeModal = (status) => {
    setTimeout(
      () => this.props.toggle(null, status)
      , 700);
  }

  render() {
    // console.log(this.state);
    let body = (
      <div className="card-body">
        <form onSubmit={this.submitHandler}>
          {this.state.error ? <Alert msg={this.state.error} classes="alert-danger" /> : null}
          {this.state.message ? <Alert msg={this.state.message} classes="alert-success" /> : null}
          {this.state.loading ? <SimpleSpinner /> : null}
          <div className="row">
            <div className="col-sm-12">
              <div className="form-group">
                <label htmlFor="name">App Name<Star /></label>
                <input
                  type="text"
                  className="form-control"
                  required
                  value={this.state.name}
                  onChange={(event) => this.inputChangeHandler(event, "name")}
                  placeholder="Enter App Name" />
              </div>
              <div className="form-group">
                <label htmlFor="email">Business Service<Star /></label>
                <input
                  type="text"
                  required
                  className="form-control"
                  value={this.state.service}
                  onChange={(event) => this.inputChangeHandler(event, "service")}
                  placeholder="Enter Business Service" />
              </div>
              <div className="form-group">
                <label htmlFor="mobile">Business Owner<Star /></label>
                <input
                  required
                  type="text"
                  className="form-control"
                  value={this.state.businessOwner}
                  onChange={(event) => this.inputChangeHandler(event, "businessOwner")}
                  placeholder="Enter Business Owner"
                />
              </div>
              <div className="form-group">
                <label htmlFor="mobile">App Owner<Star /></label>
                <input
                  required
                  type="text"
                  className="form-control"
                  value={this.state.appOwner}
                  onChange={(event) => this.inputChangeHandler(event, "appOwner")}
                  placeholder="Enter App Owner"
                />
              </div>
              <div className="form-group">
                <label htmlFor="tags">Tags<Star /></label>
                <TagsInput value={this.state.tags} onChange={this.handleChange} />
                <span style={{ color: '#ff5858' }}>Hit enter to add a tag</span>
              </div>
              <button style={{ backgroundColor: "#003c8f" }} type="submit" className="btn btn-primary waves-effect waves-light">Submit</button>
            </div>
          </div>
        </form>
      </div>
    );

    return (
      <div>
        <Modal isOpen={this.props.modal} toggle={() => this.props.toggle(null, this.state.update)} >
          <ModalHeader toggle={() => this.props.toggle(null, this.state.update)}>{this.props.title}</ModalHeader>
          <ModalBody>
            {body}
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={() => this.props.toggle(null, this.state.update)}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    token: state.auth.token,
    name: state.auth.name
  }
}

export default connect(mapStateToProps)(ModalApp);

