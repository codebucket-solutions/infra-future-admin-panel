import React, { Component } from 'react';
import AUX from '../../hoc/Aux_';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

import axios from '../../axios-details';
import { connect } from 'react-redux';
import Alert from '../Alert/common';
import Spinner from '../Spinner/simpleSpinner';
import Star from '../../components/Required/star';

class modalQuestions extends Component {

    constructor(props) {
        super(props);
        this.state={
          tag:'',
          l1:'',
          l2: '',
          l3: '',
          ahp1:[],
          ahp2:[],
          ahp3:[],
          domain:'',
          topic: '',
          question: '',
          description: '',
          question_type: '',

          ahp_comment:'',
          measure:'', 
          seq_no:1, 
          source_response: '',
          levels: '',
          internal_notes: '', 
          answer_key:'',
          response_keys:'',
          allow_comments:'',
          userId:0,

          response:[''],
          loading:false,
          message:null,
          error:null,
          status:false,
          update:false,
          isEmpty:true,

          fetchQuestion: '/questions/',
          addQuestion: '/questions/add',
          editQuestion: 'questions/edit',
        
        };
    }

    // check variable is Empty
    notEmpty = (sValue) =>{
      if (sValue === "" || sValue === null || sValue === "undefined")
      {
          return false;
      }
      return true;
    }

    handleAPIPath = (id) =>{
      if(id === 1 || id === 2){
        this.setState({fetchQuestion:'/questions/', addQuestion:'/questions/add', editQuestion:'/questions/edit'})
      }
      else if(id === 3){
        this.setState({fetchQuestion:'/questions/', addQuestion:'/questions/add', editQuestion:'/questions/edit'})
      }
    }

    noEmptyArray = (array) =>{
      if(parseInt(array.length) === 1 && array[0] === ''){
        return false;
      }
      else{
        return true;
      }
    }

    fields_empty = () =>{
      if(this.notEmpty(this.state.tag) && this.notEmpty(this.state.l1) && this.notEmpty(this.state.l2) && this.notEmpty(this.state.domain) && this.notEmpty(this.state.topic)&& this.notEmpty(this.state.question)&& this.notEmpty(this.state.description)&& this.notEmpty(this.state.question_type) && this.noEmptyArray(this.state.response)){
        this.setState({isEmpty: false})
      }
      else{
        this.setState({isEmpty: true})
      }
    }
     

    fetchAHP = () => {
      this.setState({ loading:true, error: null, message: null });
      const token = {
          token: this.props.token
      };
      axios.post('/questions/ahp',token)
          .then(res => {
              
              let ahp = res.data.data;
              console.log(ahp)
              if(res.data.status){
                //console.log(response);
                this.setState({ loading:false, ahp1:ahp.l1, ahp2:ahp.l2, ahp3:ahp.l3})
              }
          })
          .catch( err => {
              this.setState({loading:false})
          });  
    }

    fetchQuestion = () =>{
      this.setState({ loading:true, error: null, message: null });
      const config = {
          headers: { Authorization: `Bearer ${this.props.token}` }
      };
      axios.get(this.state.fetchQuestion+this.props.questionId, config,)
          .then(res => {
              console.log(res.data)
              if(res.data.status){
                let response = [];
                res.data.data.qs.response_json.map( resData =>{
                  return response.push(resData.choice);
                })
                //console.log(response);
                this.setState({ loading:false, tag: res.data.data.qs.quest_tag, l1: res.data.data.qs.ahp_criteria_lev_1, l2: res.data.data.qs.ahp_criteria_lev_2, l3: res.data.data.qs.ahp_criteria_lev_3, domain: res.data.data.qs.domains, topic: res.data.data.qs.topics, question: res.data.data.qs.question, description: res.data.data.qs.description, question_type: res.data.data.qs.response_type, response: response,
                ahp_comment: res.data.data.qs.ahp_comment,
                measure: res.data.data.qs.measure,
                seq_no: res.data.data.qs.seq_no,
                source_response: res.data.data.qs.source_response,
                answer_key: res.data.data.qs.answer_key,
                internal_notes: res.data.data.qs.internal_notes,
                levels: res.data.data.qs.levels,
                allow_comments: res.data.data.qs.allow_comments,
                response_keys: res.data.data.qs.response_keys,
                user_id: this.props.customerId ? this.props.customerId : 0
                
                })
              }
          })
          .catch( err => {
              this.setState({loading:false})
          });    
  }

  closeModal = () =>{
    setTimeout(
      () => this.props.toggle(this.state.update)
      ,700);
  }

  componentDidMount(){
    this.handleAPIPath(this.props.operationType)
    this.fetchAHP();
    //console.log(this.props);
    if(this.props.questionId)
      this.fetchQuestion();
  }

  //  ---------- Add response input field
  addResponse = e => {
    e.preventDefault()
    let response = this.state.response.concat([''])
    this.setState({
      response
    })
    this.fields_empty();
  }
  ///-------------delet response input field
  handleDelete = i => e => {
    e.preventDefault()
    let response = [
      ...this.state.response.slice(0, i),
      ...this.state.response.slice(i + 1)
    ]
    this.setState({
      response
    })
    this.fields_empty();
  }

  // --------on change input for response
  handleText = i => e => {
    let response = [...this.state.response]
    response[i] = e.target.value
    this.setState({
      response
    })
    this.fields_empty();
  }

  // ----------on input change--------------------------------

  inputChangeHandler = (event, controlName) =>{
    //console.log(controlName); 
    this.setState({[controlName]:event.target.value});
    this.fields_empty();
  }
  
    submitHandler = (e) =>{
      e.preventDefault();

      let question = {
        token: this.props.token,
        quest_tag: this.state.tag,
        ahp_criteria_lev_1: this.state.l1,
        ahp_criteria_lev_2: this.state.l2,
        ahp_criteria_lev_3: this.state.l3,
        question: this.state.question,
        description: this.state.description,
        response_type: this.state.question_type,
        topics: this.state.topic,
        domains: this.state.domain,
        response: this.state.response,
        ahp_comment: this.state.ahp_comment,
        measure: this.state.measure,
        seq_no: this.state.seq_no,
        source_response: this.state.source_response,
        answer_key: this.state.answer_key,
        internal_notes: this.state.internal_notes,
        levels: this.state.levels,
        response_keys: this.state.response_keys,
        allow_comments: this.state.allow_comments,
        user_id: this.props.customerId ? this.props.customerId : 0
      }
      //console.log(question);
      this.setState({ loading:true, error: null, message: null });

      if(this.props.questionId){
        question.quest_id = this.props.questionId
        // console.log(question);
        // console.log(this.state);
          axios.post(this.state.editQuestion, question)
              .then(res => {
                  //console.log(res.data)
                  if(res.data.status){
                    this.setState({ loading:false,update:true, message:res.data.message})
                  }
                  else{
                    this.setState({ loading:false, error:res.data.message})
                  }
                  this.closeModal();
              })
              .catch( err => {
                  this.setState({loading:false, error:"Something went wrong. Please try again."})
                  this.closeModal();
              });  
      }
      else{
        axios.post(this.state.addQuestion, question)
            .then(res => {
                //console.log(res.data)
                if(res.data.status){
                  this.setState({ loading:false,update:true, message:res.data.message})
                  
                }
                else{
                  this.setState({ loading:false, error:res.data.message})
                }
                this.closeModal();
            })
            .catch( err => {
                this.setState({loading:false, error:"Something went wrong. Please try again."})
                this.closeModal();
            });  
      }
      
      
    }

    render() {
    //console.log(this.state);
        let body = (
        <div className="card-body">
            <form onSubmit={this.submitHandler}>
                {this.state.message ? <Alert classes={"alert-success"} msg={this.state.message} />: null}
                {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} />: null}
                {this.state.loading ?  <Spinner /> : null }
                <ul className="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                  <li className="nav-item">
                      <a className="nav-link active" data-toggle="tab" href="#basic" role="tab">Basic</a>
                  </li>
                  <li className="nav-item">
                      <a className="nav-link" data-toggle="tab" href="#questions" role="tab">Questions</a>
                  </li>   
                  <li className="nav-item">
                      <a className="nav-link" data-toggle="tab" href="#additional" role="tab">Additional</a>
                  </li>                      
                </ul>
        
                <div className="tab-content">
                    <div className="tab-pane active p-3" id="basic" role="tabpanel">
                        
                        <div className="row">
                          <div className="col-sm-12">
                            <div className="form-group">
                                <label htmlFor="tag">Tag<Star /></label>
                                <input 
                                    type="text" 
                                    className="form-control" 
                                    required 
                                    onChange={(event) => this.inputChangeHandler(event,"tag" )}
                                    value={this.state.tag}
                                    placeholder="Enter Tag" />
                            </div>
                            <div className="form-group">
                                <label htmlFor="l1">APH Criteria Level 1<Star /></label>
                                <select 
                                required
                                  className="form-control" 
                                  onChange={(event) => this.inputChangeHandler(event,"l1" )}
                                  value={this.state.l1}>
                                    <option>Select</option>
                                    {
                                      this.state.ahp1.map((ahp, index) => {
                                        return (<option key={index} value={ahp.id}>{ahp.name}</option>)
                                      })
                                    }
                                </select>
                            </div>
                            <div className="form-group">
                                <label htmlFor="l2">APH Criteria Level 2<Star /></label>
                                <select 
                                required
                                  className="form-control" 
                                  onChange={(event) => this.inputChangeHandler(event,"l2" )}
                                  value={this.state.l2}>
                                    <option>Select</option>
                                    {
                                      this.state.ahp2.map((ahp, index) => {
                                        return (<option key={index} value={ahp.id}>{ahp.name}</option>)
                                      })
                                    }
                                </select>
                                
                            </div>
                            <div className="form-group">
                                <label htmlFor="l3">APH Criteria Level 3</label>
                                <select 
                                  required
                                  className="form-control" 
                                  onChange={(event) => this.inputChangeHandler(event,"l3" )}
                                  value={this.state.l3}>
                                    <option>Select</option>
                                    {
                                      this.state.ahp3.map((ahp, index) => {
                                        return (<option key={index} value={ahp.id}>{ahp.name}</option>)
                                      })
                                    }
                                </select>
                            </div>
                            <div className="form-group">
                                <label htmlFor="domain">Domain<Star /></label>
                                <input 
                                    required 
                                    type="text" 
                                    className="form-control" 
                                    onChange={(event) => this.inputChangeHandler(event,"domain" )}
                                    value={this.state.domain}
                                    placeholder="Enter Domain"
                                    />
                            </div>
                            <div className="form-group">
                                <label htmlFor="topic">Topic<Star /></label>
                                <input 
                                    required 
                                    type="text" 
                                    className="form-control"
                                    onChange={(event) => this.inputChangeHandler(event,"topic" )}
                                    value={this.state.topic} 
                                    placeholder="Enter Topic"
                                    />
                            </div>
                          </div>
                        </div>  
                    </div>


                    <div className="tab-pane p-3" id="questions" role="tabpanel">
                      <div className="row">
                        <div className="col-sm-12">
                            <div className="form-group">
                                <label htmlFor="topic">Question<Star /></label>
                                <input 
                                    required 
                                    type="text" 
                                    className="form-control" 
                                    onChange={(event) => this.inputChangeHandler(event,"question" )}
                                    value={this.state.question}
                                    placeholder="Enter Question"
                                    />
                            </div>
                            <div className="form-group">
                                <label htmlFor="topic">Description<Star /></label>
                                <input 
                                    required 
                                    type="text" 
                                    className="form-control" 
                                    onChange={(event) => this.inputChangeHandler(event,"description" )}
                                    value={this.state.description}
                                    placeholder="Enter Description"
                                    />
                            </div>
                            <div className="form-group">
                                <label htmlFor="type">Question Type<Star /></label>
                                <select className="form-control"  onChange={(event) => this.inputChangeHandler(event,"question_type" )} value={this.state.question_type}>
                                    <option value="">Select</option>
                                    <option value="mcq-single">MCQ Single</option>
                                    <option value="mcq-multiple">MCQ Multiple</option>
                                </select>
                            </div>
                            
                                {this.state.response.map((response, index) => (
                                <span key={index}>
                                  <div className="form-group">
                                <label htmlFor="topic">Response {index+1}<Star /></label>
                                    <div className="row">
                                      <div className="col-md-10 col-sm-10 col-xs-10">
                                        <input
                                          type="text"
                                          className="form-control"
                                          placeholder="Enter Response"
                                          onChange={this.handleText(index)}
                                          value={response}
                                        />
                                      </div>
                                      <div className="col-md-2 col-sm-2 col-xs-2">
                                        <button className="btn btn-md btn-danger waves-effect waves-light" onClick={this.handleDelete(index)}>X</button>
                                      </div>
                                    </div>
                                  </div>
                                </span>
                              ))}
                              <button className="btn btn-md btn-success waves-effect waves-light" style={{float:"right"}} onClick={this.addResponse}>Add Response</button>
                              <br />
                              
                          </div>
                      </div>
                    </div>

                    <div className="tab-pane p-3" id="additional" role="tabpanel">
                        
                        <div className="row">
                          <div className="col-sm-12">
                            <div className="form-group">
                                <label htmlFor="comment">AHP Comment</label>
                                <input 
                                    type="text" 
                                    className="form-control"  
                                    onChange={(event) => this.inputChangeHandler(event,"ahp_comment" )}
                                    value={this.state.ahp_comment}
                                    placeholder="Enter AHP Comment" />
                            </div>
                            <div className="form-group">
                                <label htmlFor="seq_no">Sequence Number</label>
                                <input 
                                    type="number" 
                                    className="form-control"  
                                    onChange={(event) => this.inputChangeHandler(event,"seq_no" )}
                                    value={this.state.seq_no}
                                    placeholder="Enter Sequence Number" />
                            </div>
                            <div className="form-group">
                                <label htmlFor="measure">Measure</label>
                                <input 
                                    type="text" 
                                    className="form-control"  
                                    onChange={(event) => this.inputChangeHandler(event,"measure" )}
                                    value={this.state.measure}
                                    placeholder="Enter Measure" />
                            </div>
                            <div className="form-group">
                                <label htmlFor="source_response">Source Response</label>
                                <input 
                                    type="text" 
                                    className="form-control"  
                                    onChange={(event) => this.inputChangeHandler(event,"source_response" )}
                                    value={this.state.source_response}
                                    placeholder="Enter Source Response" />
                            </div>
                            <div className="form-group">
                                <label htmlFor="answer_key">Answer Key</label>
                                <input 
                                    type="text" 
                                    className="form-control"  
                                    onChange={(event) => this.inputChangeHandler(event,"answer_key" )}
                                    value={this.state.answer_key}
                                    placeholder="Enter Answer Key" />
                            </div>
                            <div className="form-group">
                                <label htmlFor="internal_notes">Internal Notes</label>
                                <input 
                                    type="text" 
                                    className="form-control"  
                                    onChange={(event) => this.inputChangeHandler(event,"internal_notes" )}
                                    value={this.state.internal_notes}
                                    placeholder="Enter Internal Notes" />
                            </div>
                            <div className="form-group">
                                <label htmlFor="internal_notes">Response Keys</label>
                                <input 
                                    type="text" 
                                    className="form-control"  
                                    onChange={(event) => this.inputChangeHandler(event,"response_keys" )}
                                    value={this.state.response_keys}
                                    placeholder="Enter Response Keys" />
                            </div>
                            <div className="form-group">
                                <label htmlFor="internal_notes">Allow Comments</label>
                                <select 
                                  className="form-control" 
                                  onChange={(event) => this.inputChangeHandler(event,"allow_comments" )}
                                  value={this.state.allow_comments}>
                                    <option value='1'>Yes</option>
                                    <option value='0'>No</option>
                                </select>
                                {/* <input 
                                    type="text" 
                                    className="form-control"  
                                    onChange={(event) => this.inputChangeHandler(event,"allow_comments" )}
                                    value={this.state.allow_comments}
                                    placeholder="Enter Allow Comments(0- false, 1- true)" /> */}
                            </div>
                            <div className="form-group">
                                <label htmlFor="levels">Levels</label>
                                <input 
                                    type="text" 
                                    className="form-control"  
                                    onChange={(event) => this.inputChangeHandler(event,"levels" )}
                                    value={this.state.levels}
                                    placeholder="Enter Levels" />
                            </div>

                          </div>
                        </div>  
                    </div>


                    <button type="submit"  disabled={this.state.isEmpty} className="btn btn-primary waves-effect waves-light">Submit</button>
                </div>
                
            </form>
        </div>
    );
    
    return (
      <div>
        <Modal isOpen={this.props.modal} toggle={() => this.props.toggle(this.state.update)} >
          <ModalHeader toggle={() => this.props.toggle(this.state.update)}>{this.props.title}</ModalHeader>
          <ModalBody>
            {this.state.loading ? <AUX><br /><br /><div className="spinner"></div><br /><br /></AUX> : body }
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={() => this.props.toggle(this.state.update)}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
      token:state.auth.token
  }
}

export default connect(mapStateToProps)(modalQuestions);
