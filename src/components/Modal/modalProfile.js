import React, { Component } from 'react';
import makeAnimated from "react-select/animated";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from 'react-redux';

import MySelect from '../ReactSelect';
import axios from '../../axios-details';
import Spinner from '../../components/Spinner/simpleSpinner';
import Alert from '../../components/Alert/common';
import Star from '../../components/Required/star';

const animatedComponents = makeAnimated();
class ModalApp extends Component {

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      desc: '',
      client: '',
      options: ['A', 'B'],
      allApps: [],
      selectedApps: [],
      loading: false,
      message: null,
      error: null,
      update: false,
    };
  }

  fetchUsers = () => {
    this.setState({ loading: true, error: null, message: null });
    const config = {
      headers: { Authorization: `Bearer ${this.props.token}` }
    };
    axios.get('/user/userclient', config)
      .then(res => {
        //console.log(res.data)
        if (res.data.status) {
          this.setState({ loading: false, options: res.data.data.clients })
        }
        else {
          this.setState({ loaded: false });
        }
      })
      .catch(err => {
        this.setState({ loading: false })
      });
  }

  fetchProfile = (profileId) => {
    this.setState({ loading: true });
    const config = {
      headers: { Authorization: `Bearer ${this.props.token}` }
    };
    axios.get('/profile/fetch/' + profileId, config)
      .then(res => {
        //console.log(res.data)
        let profile = res.data.data.profile;
        if (res.data.status) {
          this.setState({ loading: false, name: profile.profile_name, desc: profile.description, client: profile.client_id })
        }
        // fetching all details once the attached client is retrieved
        if (this.props.profileId) {
          this.fetchApps(profile.client_id);
          this.fetchAppsAttached();
        }
      })
      .catch(err => {
        this.setState({ loading: false })
      });
  }

  fetchAppsAttached = () => {
    const config = {
      headers: { Authorization: `Bearer ${this.props.token}` }
    };
    axios.get('/apps/profile/' + this.props.profileId, config)
      .then(res => {
        //console.log(res.data);
        let selectedApps = this.handleKeyValue(res.data.data.apps);
        this.setState({ loading: false, selectedApps: selectedApps })
      })
      .catch(err => {
        this.setState({ loading: false })
      });
  }

  fetchApps = (user_id) => {
    const config = {
      headers: { Authorization: `Bearer ${this.props.token}` }
    };
    axios.get('/apps/apps/' + user_id, config)
      .then(res => {
        //console.log(res.data);
        let allApps = this.handleKeyValue(res.data.data.apps);
        this.setState({ loading: false, allApps: allApps })
      })
      .catch(err => {
        this.setState({ loading: false })
      });
  }

  //convert array to key value pair
  handleKeyValue = (arrayData) => {
    //console.log(arrayData);
    let object = [];
    for (let key in arrayData) {
      object.push({
        value: arrayData[key].app_id,
        label: arrayData[key].app_name
      })
    }
    // console.log(object);
    return object;
  }

  // convert key value pair to string
  convertToString = (object) => {
    let obj = [];
    if (object) {
      for (var i = 0; i < object.length; i++) {
        obj.push(
          object[i].value
        )
      }
    }
    return obj;
  }

  componentDidMount() {
    //console.log(this.props.profileId);
    this.fetchUsers();
    if (this.props.profileId) {
      this.fetchProfile(this.props.profileId);
    }
  }

  inputChangeHandler = (event, controlName) => {
    this.setState({ [controlName]: event.target.value });
    if (controlName === 'client') {
      this.setState({ selectedApps: [] })
      this.fetchApps(event.target.value)

    }
  }

  closeModal = () => {
    setTimeout(
      () => this.props.toggle(this.state.update)
      , 700);
  }

  selectTowChange = (selectedApps) => {
    this.setState({ selectedApps: selectedApps });
  }
  submitHandler = (e) => {
    e.preventDefault();
    //console.log(this.state);
    const profile = {
      token: this.props.token,
      profile_name: this.state.name,
      description: this.state.desc,
      client_id: this.state.client,
      apps: this.convertToString(this.state.selectedApps),
    }
    //console.log(profile);
    if (this.props.profileId) {
      profile.profile_id = this.props.profileId
      axios.post('/profile/edit', profile)
        .then(res => {
          //console.log(res.data);
          if (res.data.status) {
            this.setState({ loading: false, update: true, message: res.data.message });
          }
          else {
            this.setState({ loading: false, error: res.data.message });
          }
          this.closeModal();
        }).catch(err => {
          this.setState({ loading: false, error: "Something went wrong Please try again" });
          this.closeModal();
        })
    }
    else {
      profile.user_name = this.props.name
      //console.log(profile);
      axios.post('/profile/add', profile)
        .then(res => {
          //console.log(res.data);
          if (res.data.status) {
            this.setState({ loading: false, update: true, message: res.data.message });
          }
          else {
            this.setState({ loading: false, error: res.data.message });
          }
          this.closeModal();
        }).catch(err => {
          this.setState({ loading: false, error: "Something went wrong Please try again" });
          this.closeModal();
        })
    }

  }




  render() {
    //console.log(this.state.options);
    let body = (
      <div className="card-body">
        <form onSubmit={this.submitHandler}>
          {this.state.message ? <Alert classes={"alert-success"} msg={this.state.message} /> : null}
          {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} /> : null}

          <div className="row">
            <div className="col-sm-12">
              <div className="form-group">
                <label htmlFor="name">Profile Name<Star /></label>
                <input
                  type="text"
                  className="form-control"
                  required
                  onChange={(event) => this.inputChangeHandler(event, "name")}
                  value={this.state.name}
                  placeholder="Enter Profile Name" />
              </div>
              <div className="form-group">
                <label htmlFor="desc">Description<Star /></label>
                <input
                  type="text"
                  required
                  className="form-control"
                  onChange={(event) => this.inputChangeHandler(event, "desc")}
                  value={this.state.desc}
                  placeholder="Enter Description" />
              </div>
              <div className="form-group">
                <label htmlFor="client">User Name<Star /></label>
                <select
                  className="form-control"
                  onChange={(event) => this.inputChangeHandler(event, "client")}
                  value={this.state.client}>
                  <option>Select</option>
                  {
                    this.state.options.map((option, index) => {
                      if (option.active === '1')
                        return (<option key={index} value={option.clnt_id}>{option.f_name + " " + (option && option.l_name ? option.l_name : '')}</option>)

                      return option;
                    })
                  }
                </select>
              </div>
              <div className="form-group">
                <label htmlFor="language">Apps</label>
                <MySelect
                  value={this.state.selectedApps}
                  components={animatedComponents}
                  onChange={this.selectTowChange}
                  options={this.state.allApps}
                  allowSelectAll={true}
                  isMulti={true}
                  noMin={true}
                />
              </div>

              <button type="submit" className="btn btn-primary waves-effect waves-light">Submit</button>
            </div>
          </div>
        </form>
      </div>
    );

    return (
      <div>
        <Modal isOpen={this.props.modal} toggle={() => this.props.toggle(this.state.update)} >
          <ModalHeader toggle={() => this.props.toggle(this.state.update)}>{this.props.title}</ModalHeader>
          <ModalBody>
            {this.state.loading ? <Spinner /> : body}
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={() => this.props.toggle(this.state.update)}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStatetoProps = state => {
  return {
    token: state.auth.token,
    name: state.auth.name,
  };
}

export default connect(mapStatetoProps)(ModalApp);
