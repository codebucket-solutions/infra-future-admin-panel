import React, { Component } from 'react';
import AUX from '../../hoc/Aux_';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from 'react-redux';
import { specialCharacters } from '../../Utils/specialCharater';
import axios from '../../axios-details';
import Spinner from '../../components/Spinner/simpleSpinner';
import Alert from '../../components/Alert/common';
import Star from '../../components/Required/star';


class ModalApp extends Component {

    constructor(props) {
        super(props);
        this.state={
          name:'',
          username:'',
          password:'',
          type:null,
          loading:false,
          message:null,
          error:null,
          update:false,
          incorrectPassword:false,
        };
    }
    
    //fetch Admin details from API
    fetchAdmin = () =>{
      this.setState({ loading:true, error: null, message: null});
      const config = {
          headers: { Authorization: `Bearer ${this.props.token}` }
      };
      axios.get('/admin/fetch/'+this.props.adminId, config)
          .then(res => {
              //console.log(res.data)
              if(res.data.status){
                let profile = res.data.data.profile;
                this.setState({ loading:false, name:profile.name, username: profile.username, type:profile.access_level})
              }
          })
          .catch( err => {
              this.setState({loading:false})
          });    
    }
    
    componentDidMount(){
        if(this.props.adminId)
            this.fetchAdmin();
    }

    //close modal function
    closeModal = () =>{
      setTimeout(
        () => this.props.toggle(this.state.update)
        ,700);
    }

    //function to update input values
    inputChangeHandler = (event, controlName) =>{
      this.setState({[controlName]:event.target.value});
    }

    //on submit function
    submitHandler = (e) =>{
      e.preventDefault();
      // console.log(this.state);
      
      // if admin type is not empty
      if(this.state.type){
        this.setState({loading:true, error:null, message:null, incorrectPassword:false});
        const profile = {
          token: this.props.token,
          name: this.state.name,
          username:this.state.username,
          access_level:this.state.type,
        }
        
        // Edit Admin details
        if(this.props.adminId){
          profile.admin_id = this.props.adminId;
          axios.post('/admin/edit', profile)
          .then(res=>{
            //console.log(res.data);
            if(res.data.status){
              
              this.setState({loading:false, update:true, message:res.data.message});
              this.closeModal();
            }
            else {
              this.setState({loading:false, error:res.data.message});
            } 
    
          }).catch(err =>{
              this.setState({loading:false, error:"Something went wrong Please try again"});
              //this.closeModal();
          })
        }
        // add new admin
        else{
          // check weather the  password is complex or not
          //console.log(this.state.password);
          if(this.state.password.length >= 6 && specialCharacters(this.state.password)){
            profile.password  = this.state.password;
            //console.log(profile);
            axios.post('/admin/add', profile)
            .then(res=>{
                //console.log(res.data);
                if(res.data.status){
                  this.setState({loading:false, update:true, message:res.data.message});
                  this.closeModal();
                }  
                else {
                  this.setState({loading:false, error:res.data.message});
                }
                
                    
            }).catch(err =>{
                this.setState({loading:false, error:"Something went wrong Please try again"});
                //this.closeModal();
            })
          }
          else{
            //console.log(this.state.password);
            this.setState({incorrectPassword: true, loading:false})
          }
        }
      }
      else{
        this.setState({ error:"Please select a admin type."});
      }
    }
    
    render() {
        let body = (
        <div className="card-body">
            <form onSubmit={this.submitHandler}>
                {this.state.message ? <Alert classes={"alert-success"} msg={this.state.message} />: null}
                {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} />: null}
                {this.state.loading ?  <Spinner /> : null }
                <div className="row">
                    <div className="col-sm-12">
                        <div className="form-group">
                            <label htmlFor="name">Name<Star /></label>
                            <input 
                                type="text" 
                                className="form-control" 
                                required 
                                onChange={(event) => this.inputChangeHandler(event,"name" )}
                                value={this.state.name}
                                placeholder="Enter Name" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="desc">Username<Star /></label>
                            <input 
                                type="email"
                                required 
                                className="form-control" 
                                onChange={(event) => this.inputChangeHandler(event,"username" )}
                                value={this.state.username}
                                placeholder="Enter Email (eg:test@gmail.com)" />
                        </div>
                        {
                          this.props.adminId ? null : 
                          <div className="form-group">
                            <label htmlFor="desc">Password<Star /></label>
                            <input 
                                type="text"
                                required 
                                className="form-control" 
                                onChange={(event) => this.inputChangeHandler(event,"password" )}
                                value={this.state.password}
                                placeholder="Enter Password" />
                            <span className={this.state.incorrectPassword ? "passwordStyle" : null}>At least 6 characters having one special character.</span>
                        </div>
                        }
                        
                        <div className="form-group">
                            <label htmlFor="type">Admin Type<Star /></label>
                            <select className="form-control" required  onChange={(event) => this.inputChangeHandler(event,"type" )} value={this.state.type}>
                                <option value="">Select</option>
                                <option value="3">Super Admin</option>
                                <option value="1">Profile Admin</option>
                                <option value="2">Question Admin</option>
                            </select>
                        </div>
                        <button type="submit" className="btn btn-primary waves-effect waves-light">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    );
    
    return (
      <div>
        <Modal isOpen={this.props.modal} toggle={() => this.props.toggle(this.state.update)} >
          <ModalHeader toggle={() => this.props.toggle(this.state.update)}>{this.props.title}</ModalHeader>
          <ModalBody>
            {this.state.loading ? <AUX><br /><br /><Spinner /><br /><br /></AUX> : body }
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={() => this.props.toggle(this.state.update)}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStatetoProps = state =>{
  return {
      token: state.auth.token,
  };
}

export default connect(mapStatetoProps)(ModalApp);
