import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import './react-tagsinput.css';
import { connect } from 'react-redux';
import { objectToArray, handleKeyValue } from '../../shared/utility'
import MySelect from '../ReactSelect';
import makeAnimated from "react-select/animated";
import Alert from '../Alert/common';
import SimpleSpinner from '../../components/Spinner/simpleSpinner';

const animatedComponents = makeAnimated();
class ModalSelectInsight extends Component {

    constructor(props) {
        super(props);
        this.state={
            error: null,
            message: null,
            loading: false,
            allTags:[],
            selectedTags: []
        };
    }

    componentDidMount() {
        let platform = process.env.REACT_APP_INSIGHT_PLATFORM;
        this.setState({ allTags: handleKeyValue(platform.split(','))})
    }

    submitHandler = (e) =>{
        e.preventDefault();
        let array = objectToArray(this.state.selectedTags);
        if(array.length > 0){
            console.log(array);
            this.props.toggle(array.toString());
        }
        else{
            this.setState({error: "Please select at least one platform"})
        }
        
    }
    selectTowChangeFilter= (selectedTags) => {
        this.setState({selectedTags: selectedTags})
    }

    render() {
        // console.log(this.state);
        let body = (
        <div className="card-body">
            <form onSubmit={this.submitHandler}>
                {this.state.error ? <Alert msg={this.state.error} classes="alert-danger" /> : null }
                {this.state.message ? <Alert msg={this.state.message} classes="alert-success" /> : null }
                {this.state.loading ?  <SimpleSpinner /> : null }
                <br />
                <div className="row">
                    <div className="col-sm-12">
                        <MySelect
                            options={this.state.allTags}
                            isMulti
                            noMin = {true}
                            components={animatedComponents}
                            onChange={(e) => this.selectTowChangeFilter(e)}
                            allowSelectAll={true}
                            value={this.state.selectedTags}
                           
                        />
                    </div>
                    <div className="col-sm-12 mt-3">
                        <button type="submit" className="btn btn-primary waves-effect waves-light">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    );
    
    return (
      <div>
        <Modal isOpen={this.props.modal} toggle={() => this.props.toggle(null)} >
          <ModalHeader toggle={() => this.props.toggle(null)}>Select Platform</ModalHeader>
          <ModalBody>
            {body}
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={() => this.props.toggle(null)}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state =>{
  return {
      token : state.auth.token,
      name: state.auth.name
  }
}

export default connect(mapStateToProps)(ModalSelectInsight);

