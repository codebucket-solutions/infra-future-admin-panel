import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from 'react-redux';
import { specialCharacters } from '../../Utils/specialCharater';
import countryCode from '../../shared/countryCode.json';

import axios from '../../axios-details';
import Spinner from '../Spinner/simpleSpinner';
import Alert from '../Alert/common';
import Star from '../Required/star';


class ModalApp extends Component {

    constructor(props) {
        super(props);
        this.state={
          f_name:'',
          l_name:'',
          company:'',
          address1:'',
          address2:'',
          country:'+1',
          phone:'',
          email:'',
          dialCode:'',
          password:'',
          message:null,
          error:null,
          update:false,
          incorrectPassword:false,
        };
    }

    fetchClient = () =>{
      this.setState({ loading:true, error: null, message: null});
      const config = {
          headers: { Authorization: `Bearer ${this.props.token}` }
      };
      axios.get('/user/'+this.props.clientId, config)
          .then(res => {
              //console.log(res.data)
              if(res.data.status){
                let client = res.data.data.client;
                let phone = client.phone.split('-');
        
                this.setState({ loading:false, f_name:client.f_name, l_name:client.l_name, address1:client.address_1,address2:client.address_2,country:client.country,company:client.company, email:client.email, phone:parseInt(phone[1]), dialCode:phone[0] })
              }
          })
          .catch( err => {
              this.setState({loading:false})
          });    
    }
    
    componentDidMount(){
        if(this.props.clientId)
            this.fetchClient();
    }

    closeModal = () =>{
      setTimeout(
        () => this.props.toggle(this.state.update)
        ,700);
    }

    inputChangeHandler = (event, controlName) =>{
      this.setState({[controlName]:event.target.value});
    }

  
    submitHandler = (e) =>{
      e.preventDefault();
      //console.log(this.state);
      if((this.state.password.length >= 6 && specialCharacters(this.state.password)) || this.props.clientId){
        this.setState({loading:true, error:null, message:null, incorrectPassword:false});
        let client = {
            token: this.props.token,
            f_name: this.state.f_name,
            l_name: this.state.l_name,
            company: this.state.company,
            phone: this.state.dialCode+"-"+this.state.phone,
            address_1: this.state.address1,
            address_2: this.state.address2,
            country:this.state.country,
        }
        //console.log(client);
        if(this.props.clientId){
          client.user_id = this.props.clientId;
          axios.put('/user/edit', client)
          .then(res=>{
              //console.log(res.data);
              if(res.data.status)
                  this.setState({loading:false, update:true, message:res.data.message});
              else {
                this.setState({loading:false, error:res.data.message});
              } 
                  this.closeModal();
          }).catch(err =>{
              this.setState({loading:false, error:"Something went wrong Please try again"});
              this.closeModal();
          })
        }
        else{
          client ={
            ...client,
            email: this.state.email,
            password:this.state.password,
          }
          //console.log(client);
          axios.post('/user/add', client)
          .then(res=>{
              //console.log(res.data);
              if(res.data.status)
                  this.setState({loading:false, update:true, message:res.data.message});
              else {
                this.setState({loading:false, error:res.data.message});
              }
              this.closeModal();
                  
          }).catch(err =>{
              this.setState({loading:false, error:"Something went wrong Please try again"});
              this.closeModal();
          })
        }
      }
      else{
        this.setState({incorrectPassword: true})
      }
      
      
    }
    
    render() {
        // get dial code
        var byDialCode = countryCode.slice(0);
        byDialCode.sort(function(a,b) {
            return a.dial_code - b.dial_code;
        });

        //get country name
        var byCountry = countryCode.slice(0);
        byCountry.sort(function(a,b) {
            return a.name - b.name;
        });
        let body = (
        <div className="card-body">
            <form onSubmit={this.submitHandler}>
                {this.state.message ? <Alert classes={"alert-success"} msg={this.state.message} />: null}
                {this.state.error ? <Alert classes={"alert-danger"} msg={this.state.error} />: null}
                {this.state.loading ?  <Spinner /> : null }
                <div className="row">
                    <div className="col-sm-12">
                        <div className="form-group">
                            <label htmlFor="name">First Name<Star /></label>
                            <input 
                                type="text" 
                                className="form-control" 
                                required 
                                onChange={(event) => this.inputChangeHandler(event,"f_name" )}
                                value={this.state.f_name}
                                placeholder="First Name" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="name">Last Name<Star /></label>
                            <input 
                                type="text" 
                                className="form-control" 
                                required 
                                onChange={(event) => this.inputChangeHandler(event,"l_name" )}
                                value={this.state.l_name}
                                placeholder="Last Name" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="name">Company Name<Star /></label>
                            <input 
                                type="text" 
                                className="form-control" 
                                required 
                                onChange={(event) => this.inputChangeHandler(event,"company" )}
                                value={this.state.company}
                                placeholder="Enter Company Name" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="desc">Email<Star /></label>
                            <input 
                                type="email"
                                required 
                                disabled = {this.props.clientId ? true : false}
                                className="form-control" 
                                onChange={(event) => this.inputChangeHandler(event,"email" )}
                                value={this.state.email}
                                placeholder="Enter Email (eg:test@gmail.com)" />
                        </div>
                        <div className="form-group row">
                          <div className="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
                                <label htmlFor="l3">Code</label>
                                <select 
                                required
                                className="form-control" 
                                onChange={(event) => this.inputChangeHandler(event,"dialCode" )}
                                value={this.state.dialCode  ? this.state.dialCode : '' }>
                                  {byDialCode.map((c,i) =>{
                                      return (<option key={i} value={c.dial_code}>{c.dial_code}</option>)
                                  })}
                                </select>
                            </div>
                            <div className="col-lg-9 col-md-9 col-sm-12 col-xs-12 ">
                              <label htmlFor="phone">Contact Number<Star /></label>
                              <input 
                                  type="number"
                                  required 
                                  className="form-control" 
                                  onChange={(event) => this.inputChangeHandler(event,"phone" )}
                                  value={this.state.phone}
                                  placeholder="Enter Contact Number" />
                            </div>
                        </div>
                        {
                          this.props.clientId ? null : 
                          <div className="form-group">
                            <label htmlFor="desc">Password<Star /></label>
                            <input 
                                type="text"
                                required 
                                className="form-control" 
                                onChange={(event) => this.inputChangeHandler(event,"password" )}
                                value={this.state.password}
                                placeholder="Enter Password" />
                            <span className={this.state.incorrectPassword ? "passwordStyle" : null}>At least 6 characters having one special character.</span>
                        </div>
                        }
                        <div className="form-group">
                            <label htmlFor="mobile">Address1</label>
                            <input 
                                type="text"
                                className="form-control" 
                                onChange={(event) => this.inputChangeHandler(event,"address1" )}
                                value={this.state.address1}
                                placeholder="Enter Address 1" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="mobile">Address2</label>
                            <input 
                                className="form-control" 
                                onChange={(event) => this.inputChangeHandler(event,"address2" )}
                                value={this.state.address2}
                                placeholder="Enter Address2" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="mobile">Country</label>
                            <select 
                                required
                                className="form-control" 
                                onChange={(event) => this.inputChangeHandler(event,"country" )}
                                value={this.state.country  ? this.state.country : '' }>
                                    <option >Select Country</option>
                                    {byCountry.map((c,i) =>{
                                        return (<option key={i} value={c.name}>{c.name}</option>)
                                    })}
                                
                              </select>
                            
                        </div>
                    
                        <button type="submit" className="btn btn-primary waves-effect waves-light">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    );
    
    return (
      <div>
        <Modal isOpen={this.props.modal} toggle={() => this.props.toggle(this.state.update)} >
          <ModalHeader toggle={() => this.props.toggle(this.state.update)}>{this.props.title}</ModalHeader>
          <ModalBody>
            {this.state.loading ? <Spinner /> : body }
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={() => this.props.toggle(this.state.update)}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStatetoProps = state =>{
  return {
      token: state.auth.token,
  };
}

export default connect(mapStatetoProps)(ModalApp);
