
import React from 'react';
import { Link } from 'react-router-dom';
import AUX from '../../hoc/Aux_';

import HelpModal from '../Modal/modalHelp';
import ContactModal from '../Modal/modalContact';
import { btnStyle } from '../../util';
import './Topchart.css';
import { connect} from 'react-redux'
import { selectthemeAction } from '../../store/actions/themeSelectionAction';

const topBarLeftStyle = {
    width: "70%",
    display: "flex",
    alignItems: "center",
    backgroundColor: "#3d4977",
    textAlign: "start",
}

const topBarLeftStyleClassic = {
   ...topBarLeftStyle,
    backgroundColor: "#ffffff",
}

const navBarStyle = {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    backgroundColor: "#3d4977",
}
const navBarStyleClassic = {
    ...navBarStyle,
    backgroundColor : "#ffffff"
}

const styleIcon = {
    display: "block",
    color: 'white',
    fontSize: '30px',
    marginLeft: "18px",
    marginRight: "4%",
    cursor: "pointer"
}
const styleIconClassic = {
    ...styleIcon,
    color: 'black',
}

const titleStyle = {
    color : "#ffffff"
}
const titleStyleClassic = {
    color : "#000000"
}

const headerIconStyle = {
    
}

const logoStyle = {
    width: "80%"
}

class topchart extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            open: true,
            toggle: false,
            toggleContact: false,
            isClassicTheme: this.props.isClassic
        }
    }

    check = () => {
        if (this.state.open) {
            document.body.classList.add('enlarged');
        }
        else {
            document.body.classList.remove('enlarged');
        }
        this.setState(prevState => {
            return { open: !prevState.open }
        })
    }

    toggleModal = (id) => {
        if (id === 1) {
            this.setState(prevState => {
                return { toggle: !prevState.toggle }
            })
        }
        else if (id === 2) {
            this.setState(prevState => {
                return { toggleContact: !prevState.toggleContact }
            })
        }

    }
    changeClientTheme = () => {
        console.log(localStorage.getItem("selectedTheme"))
        // console.log(this.state.isClassicTheme, this.props.isClassic)
        // this.setState({ isClassicTheme: !this.state.isClassicTheme })
        localStorage.setItem("selectedTheme", !this.props.isClassic);

        this.props.updateTheme(!this.props.isClassic)
        
    }
    changeClientTheme = () => {
        console.log(localStorage.getItem("selectedTheme"))
        // console.log(this.state.isClassicTheme, this.props.isClassic)
        // this.setState({ isClassicTheme: !this.state.isClassicTheme })
        localStorage.setItem("selectedTheme", !this.props.isClassic);

        this.props.updateTheme(!this.props.isClassic)
        
    }
    render() {
        return (
            <AUX>
                <div className="topbar">
                    <div style={this.props.isClassic ? topBarLeftStyle : topBarLeftStyleClassic}  className="topbar-left">
                        <Link to="/" className="logo logo-custom" style={{  textTransform: 'none', lineHeight: 1.5, color: 'white' }}>
                        <i style={this.props.isClassic ?styleIcon : styleIconClassic} className="mdi mdi-apps"></i>
                        <p style={this.props.isClassic  ?titleStyle : titleStyleClassic}>Assesment Portal {process.env.REACT_APP_WEBSITE_SUBTYPE}</p>
                        </Link>
                    </div>
                    <nav className="navbar-custom" style={this.props.isClassic ? navBarStyle : navBarStyleClassic}>


                        {/* <ul className="navbar-right d-flex list-inline float-right mb-0">
                            <li >
                                <button style={{ border: '1px solid' }} onClick={() => this.toggleModal(2)} className="btn btn-primary w-xs waves-effect waves-light mt-3" style={{ btnStyle }}>Contact Us</button>&nbsp;
                        </li>
                            <li >
                                <button style={{ border: '1px solid' }} onClick={() => this.toggleModal(1)} className="btn btn-light w-xs waves-effect waves-light mt-3">Help</button>
                            </li>
                            <li className="dropdown notification-list">
                                <div className="dropdown notification-list nav-pro-img">
                                    <button className="btn btn-link dropdown-toggle nav-link arrow-none waves-effect nav-user" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false">
                                        <img src={`${process.env.PUBLIC_URL}/assets/images/users/user.png`} alt="user" className="rounded-circle" style={{ height: 32, width: 32, marginTop: '-7px' }} />
                                    </button>
                                    <div className="dropdown-menu dropdown-menu-right profile-dropdown ">
                                        <Link to="/logout">
                                            <button className="btn btn-link dropdown-item text-danger" ><i className="mdi mdi-power text-danger"></i> Logout</button>
                                        </Link>
                                    </div>
                                </div>
                            </li>

                        </ul> */}

                    <div className="header-icon-container">
                            <li title="Change theme" onClick={this.changeClientTheme} style={headerIconStyle}><i style={this.props.isClassic   ?styleIcon : styleIconClassic} className="mdi mdi-theme-light-dark"></i></li>

                            <li title="Contact us" style={headerIconStyle} onClick={() => this.toggleModal(2)}><i style={this.props.isClassic  ?styleIcon : styleIconClassic} className="mdi mdi-contact-mail"></i></li>

                            <li title="Help"  onClick={() => this.toggleModal(1)}><i style={this.props.isClassic   ?styleIcon : styleIconClassic} className="mdi mdi-comment-question-outline"></i></li>

                            <li className="dropdown notification-list">
                                <div className="dropdown notification-list nav-pro-img">
                                    <i 
                                        data-toggle="dropdown" 
                                        aria-haspopup="false" aria-expanded="false" style={this.props.isClassic  ? {...styleIcon,marginTop: -5,marginLeft: 13} : {...styleIconClassic, marginTop: -5,marginLeft: 13}}  className="mdi mdi-account">    
                                    </i>
                                    <div className="dropdown-menu dropdown-menu-right profile-dropdown ">
                                        <button className="btn btn-link dropdown-item" ><i className="mdi mdi-account-circle m-r-5"></i><strong>{this.props.name}</strong></button>
                                        <Link to="/logout" >
                                            <button className="btn btn-link dropdown-item text-danger" ><i className="mdi mdi-power text-danger"></i> Logout</button>
                                        </Link>
                                    </div>
                                </div>
                            </li>
                        </div>

                        {/* <ul className="list-inline menu-left mb-0">
                            <li className="float-left" style={appTitle}>
                                <button className="button-menu-mobile open-left waves-effect" onClick={this.check}>
                                    <i className="mdi mdi-menu"></i>

                                </button>
                                <p className="font-weight-bold" style={headerParagraphStyle}>{process.env.REACT_APP_WEBSITE_NAME}</p>
                            </li>
                        </ul> */}

                    </nav>

                </div>
                {
                    this.state.toggle ? <HelpModal modal={this.state.toggle} toggle={() => this.toggleModal(1)} /> : null
                }
                {
                    this.state.toggleContact ? <ContactModal modal={this.state.toggleContact} toggle={() => this.toggleModal(2)} /> : null
                }
            </AUX>
        )
    }
}
const mapDispatchToProps = dispatch => {
    return {
        updateTheme : (theme) => dispatch(selectthemeAction(theme))
    }
}

const mapStateToProps = state => {
    console.log(state)
    return {
        name: state.auth.name,
        isClassic : state.ui_red.isClassic
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(topchart);
