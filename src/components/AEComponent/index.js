import React, { Component } from 'react';
import { btnStyle } from '../../util';
import MySelect from '../../components/ReactSelect';
import { objectToArray, handleKeyValue } from '../../shared/utility';

class AEComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            allTags:[],
            selectedTagsCustom: [],
            assessmentTypesCustom:'',
            apiCustom:'',
            apiAssessmentSelected:"",

            selectedTagsDynamic: [],
            assessmentTypesDynamic:'',
            apiDynamic:'',

            apiRulesCustom:'',
            apiRulesDynamic:'',
            apiRulesSelected:""
        }
    }

    componentDidMount() {
        let platform = process.env.REACT_APP_INSIGHT_PLATFORM;
        console.log(this.props.allAE)
        this.props.allAE.map((a,i) =>{
            if(a.name === "api1_cloud_platform"){
                this.setState({selectedTagsCustom: handleKeyValue(a.details.split(','))})
            }
            else if(a.name === "api2_cloud_platform"){
                this.setState({selectedTagsDynamic: handleKeyValue(a.details.split(','))})
            }
            
        
            this.setState({[a.name]: a.details})
        })

        this.setState({allTags: handleKeyValue(platform.split(','))})
    }


    selectTowChangeFilter =(e, state) =>{
        this.setState({ [state]:e})
    }

    handleChange = (e, state) =>{
        console.log(e.target)
        this.setState({[state]: e.target.value})
    }

    handleSubmit = (e) =>{
        e.preventDefault();
        const apiData = {
            apiCustom: this.state.apiCustom,
            assessmentTypesCustom: this.state.assessmentTypesCustom,
            api1_cloud_platform: objectToArray(this.state.selectedTagsCustom).toString(),
            apiDynamic: this.state.apiDynamic,
            assessmentTypesDynamic: this.state.assessmentTypesDynamic,
            api2_cloud_platform: objectToArray(this.state.selectedTagsDynamic).toString(),
            apiAssessmentSelected:this.state.apiAssessmentSelected,
            apiRulesCustom:this.state.apiRulesCustom,
            apiRulesDynamic:this.state.apiRulesDynamic,
            apiRulesSelected:this.state.apiRulesSelected,

            // api:this.state.allApi.filter(a => a.id === this.state.api),
            // assessment_types: this.state.assessmentTypes,
            // cloud_platform:objectToArray(this.state.selectedTags)
        }
        this.props.handleSubmit(apiData)
    }

    render() {
        return (
            <form onSubmit={(e) => this.handleSubmit(e)}>
                <div className="row">
                    <div className="col-sm-12 col-lg-13 mb-3">
                        <h6>AE Assessment1(Custom)</h6>
                    </div>
                    <div className="col-sm-3 col-lg-3 mb-3">
                        <label>API URL</label>
                    </div>
                    <div className="col-sm-8 text-center">
                        <input  
                            type="text"
                            className="form-control"
                            onChange={(e) =>this.handleChange(e, "apiCustom")}
                            value={this.state.apiCustom}
                        />
                    </div>
                    <div className="col-sm-1 text-center">
                        <input  
                            type="radio"
                            name="apiAssessmentSelected"
                            value="custom"
                            checked={this.state.apiAssessmentSelected === "custom" ? true : false  }
                            className="form-control"
                            onChange={(e) =>this.handleChange(e, "apiAssessmentSelected")}
                        />
                    </div>

                    <div className="col-sm-3 col-lg-3 mb-3">
                        <label>Assessment Input</label>
                    </div>
                    <div className="col-sm-9 text-center">
                        <input 
                            type="text"
                            className="form-control"
                            required
                            onChange={(e) => this.handleChange(e,"assessmentTypesCustom" )}
                            value={this.state.assessmentTypesCustom}
                        />
                    </div>

                    <div className="col-sm-3 col-lg-3 mb-3">
                        <label>Cloud Platform</label>
                    </div>
                    <div className="col-sm-9 text-center">
                        <MySelect
                            options={this.state.allTags}
                            isMulti
                            noMin = {true}
                            onChange={(e) => this.selectTowChangeFilter(e, "selectedTagsCustom")}
                            allowSelectAll={true}
                            value={this.state.selectedTagsCustom}
                        
                        />
                    </div>
                </div>

                <div className="row">
                    <div className="col-sm-12 col-lg-13 mb-3">
                        <h6>AE Assessment2(Dynamic)</h6>
                    </div>
                    <div className="col-sm-3 col-lg-3 mb-3">
                        <label>API URL</label>
                    </div>
                    <div className="col-sm-8 text-center">
                        <input  
                            type="text"
                            className="form-control"
                            onChange={(e) =>this.handleChange(e, "apiDynamic")}
                            value={this.state.apiDynamic}
                        />
                    </div>
                    <div className="col-sm-1 text-center">
                        <input  
                            type="radio"
                            name="apiAssessmentSelected"
                            value="dynamic"
                            checked={this.state.apiAssessmentSelected === "dynamic" ? true : false  }
                            className="form-control"
                            onChange={(e) =>this.handleChange(e, "apiAssessmentSelected")}
                        />
                    </div>

                    <div className="col-sm-3 col-lg-3 mb-3">
                        <label>Assessment Input</label>
                    </div>
                    <div className="col-sm-9 text-center">
                        <input 
                            type="text"
                            className="form-control"
                            required
                            onChange={(e) => this.handleChange(e,"assessmentTypesDynamic" )}
                            value={this.state.assessmentTypesDynamic}
                        />
                    </div>

                    <div className="col-sm-3 col-lg-3 mb-3">
                        <label>Cloud Platform</label>
                    </div>
                    <div className="col-sm-9 text-center">
                        <MySelect
                            options={this.state.allTags}
                            isMulti
                            noMin = {true}
                            onChange={(e) => this.selectTowChangeFilter(e, "selectedTagsDynamic")}
                            allowSelectAll={true}
                            value={this.state.selectedTagsDynamic}
                        />
                    </div>
                </div>
                {/* Rules */}
                <div className="row">
                    <div className="col-sm-12 col-lg-12 mb-3">
                            <h6>AE Rules1(Custom)</h6>
                    </div>
                    <div className="col-sm-3 col-lg-3 mb-3">
                        <label>API URL</label>
                    </div>
                    <div className="col-sm-8 text-center">
                        <input  
                            type="text"
                            className="form-control"
                            onChange={(e) =>this.handleChange(e, "apiRulesCustom")}
                            value={this.state.apiRulesCustom}
                        />
                    </div>
                    <div className="col-sm-1 text-center">
                        <input  
                            type="radio"
                            name="apiRulesSelected"
                            value="custom"
                            checked={this.state.apiRulesSelected === "custom" ? true : false  }
                            className="form-control"
                            onChange={(e) =>this.handleChange(e, "apiRulesSelected")}
                        />
                    </div>
                    <div className="col-sm-12 col-lg-12 mb-3">
                        <h6>AE Rules2(Dynamic)</h6>
                    </div>
                    <div className="col-sm-3 col-lg-3 mb-3">
                        <label>API URL</label>
                    </div>
                    <div className="col-sm-8 text-center">
                        <input  
                            type="text"
                            className="form-control"
                            onChange={(e) =>this.handleChange(e, "apiRulesDynamic")}
                            value={this.state.apiRulesDynamic}
                        />
                    </div>
                    <div className="col-sm-1 text-center">
                        <input  
                            type="radio"
                            name="apiRulesSelected"
                            value="dynamic"
                            checked={this.state.apiRulesSelected === "dynamic" ? true : false  }
                            className="form-control"
                            onChange={(e) =>this.handleChange(e, "apiRulesSelected")}
                        />
                    </div>
                    <div className="col-sm-12 col-lg-13 mb-3 mt-2 text-center">
                        <button 
                            style={{ btnStyle }} 
                            type="submit" 
                            className="btn btn-primary waves-effect waves-light">Submit</button>
                    </div>
                </div>
            </form>
        )
    }
}

export default AEComponent;