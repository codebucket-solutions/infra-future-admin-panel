import React from 'react'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

const ModalLayout = (props) =>{
    return(
        <Modal size={props.size} isOpen={props.isOpen} toggle={() => props.toggle()} >
          <ModalHeader toggle={() => props.toggle()}>{props.title}</ModalHeader>
          <ModalBody>
            {props.children}
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={() => props.toggle()}>Cancel</Button>
          </ModalFooter>
        </Modal>
    )
}

export default ModalLayout;