import React from 'react';
import { connect } from 'react-redux';

//import classes from './Layout.css';
const footer = ( props ) => (
    <footer className="footer">
    ©  2020 appmodz private limited V1.13
    {/* {props.footer} */}
</footer>  
);

const mapStateToProps = state =>{
    return {
        footer : state.auth.footer
    }
}

export default connect(mapStateToProps)(footer);
