import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Scrollbars } from 'react-custom-scrollbars';
import { connect } from 'react-redux';
import { compose } from 'redux';

import AUX from '../../hoc/Aux_';
import './Leftsidebar.css';
import { dashboardNavigation } from '../../store/actions/dashboardNavigation';
import { sidebarStyle, sidebarStyleClassic, sidebarTextColor,sidebarTextColorClassic } from '../../Utils/specialCharater';

import SimpleBar from 'simplebar-react';

const appTitle = {
    display: "flex",
    alignItems: "center",
    cursor: "pointer",
    fontSize: "40px",
    marginLeft: "18px",
    marginTop:9,
    marginBottom: "9px"
}

const headerParagraphStyle = {
    margin: "0px 0px 6px 0px",
    padding: "0px",
    fontSize: "18px",
    backgroundColor: "none",
    color: "white"
}

const sideBarStyleD = {
    backgroundColor : "#ffffff"
}

const sideBarStyleDClassic = {
    backgroundColor : "#000000"
}

 const iconStyle = {
    color : "black"
}

const iconStyleClassic = {
    color : "white",
}


class leftsidebar extends Component {


    
    constructor(props) {
        super(props);

        this.state = {
            Tab: '', SubTab: '', MoreTab: '',open : true
        };

    }
    setActiveTab = (tab, subtab, moretab, e) => {
        console.log("tab",tab)
        console.log("tab",subtab)
        this.props.updateActivetab(tab)
    }

    componentDidMount() {
        console.log(this.props.location)
        this.props.updateActivetab(this.props.location.pathname.replace('/', ''))
    }

    check = () => {
        if (this.state.open) {
            document.body.classList.add('enlarged');
        }
        else {
            document.body.classList.remove('enlarged');
        }
        this.setState(prevState => {
            return { open: !prevState.open }
        })
    }

    render() {
        console.log(this.props.activeTab)
        let links = (
            <AUX>
                <li className={this.state.open ? `title-main` : `title-main open`} style={this.props.isClassic ? sidebarTextColor : sidebarTextColorClassic}>{ this.state.open ? `Dashboard` : null}</li>
                <li>
                    <Link to='/' className={this.props.activeTab === '' ? 'waves-effect active' : 'waves-effect'} onClick={this.setActiveTab.bind(this, '', '', '')} >
                        <i style={this.props.isClassic  ? iconStyle : iconStyleClassic}  className={`mdi mdi-buffer`}></i><span style={this.props.isClassic  ? sidebarTextColor : sidebarTextColorClassic} >Dashboard</span></Link>
                </li>
                <li className={this.state.open ? `title-main` : `title-main open`} style={this.props.isClassic  ? sidebarTextColor : sidebarTextColorClassic}>{ this.state.open ? `Project Profile` : null}</li>
                <li>
                    <Link to='/questions' className={this.props.activeTab === 'questions' ? 'waves-effect active' : 'waves-effect'} onClick={this.setActiveTab.bind(this, 'questions', '', '')} >
                        <i title = "Questions" style={this.props.isClassic  ? iconStyle : iconStyleClassic}  className="mdi mdi-calendar-check"></i><span style={this.props.isClassic  ? sidebarTextColor : sidebarTextColorClassic} > Questions </span></Link>
                </li>
                <li>
                    <Link to='/rules' className={this.props.activeTab === 'rules' ? 'waves-effect active' : 'waves-effect'} onClick={this.setActiveTab.bind(this, 'rules', '', '')} >
                        <i title = "Rules" style={this.props.isClassic  ? iconStyle : iconStyleClassic}  className="mdi mdi-routes"></i><span style={this.props.isClassic  ? sidebarTextColor : sidebarTextColorClassic} > Rules </span></Link>
                </li>
                <li>
                    <Link to='/apps' className={this.props.activeTab === 'apps' ? 'waves-effect active' : 'waves-effect'} onClick={this.setActiveTab.bind(this, 'apps', '', '')} >
                        <i title = "Applications" style={this.props.isClassic  ? iconStyle : iconStyleClassic}  className="mdi mdi-apps"></i><span style={this.props.isClassic  ? sidebarTextColor : sidebarTextColorClassic} > Applications </span></Link>
                </li>
                <li>
                    <Link to='/profile' className={this.props.activeTab === 'profile' ? 'waves-effect active' : 'waves-effect'} onClick={this.setActiveTab.bind(this, 'profile', '', '')} >
                        <i title = "Assessment Profiles" style={this.props.isClassic  ? iconStyle : iconStyleClassic}  className="mdi mdi-clipboard-outline"></i><span style={this.props.isClassic  ? sidebarTextColor : sidebarTextColorClassic} > Assessment Profiles </span></Link>
                </li>
   
   
                    {/* BSP */}
                    <li className={this.state.open ? `title-main` : `title-main open`} style={this.props.isClassic  ? sidebarTextColor : sidebarTextColorClassic}>{ this.state.open ? `BSP` : null}</li>
                                <li>
                        <Link to='/capability' className={this.props.activeTab === 'capability' ? 'waves-effect active' : 'waves-effect'} onClick={this.setActiveTab.bind(this, 'capability', '', '')} >
                            <i title = "Buisness Capability" style={this.props.isClassic  ? iconStyle : iconStyleClassic}  className="mdi mdi-cube"></i><span style={this.props.isClassic  ? sidebarTextColor : sidebarTextColorClassic} > Buisness Capablity </span></Link>

                    </li>
                    <li>
                        <Link to='/businessServices' className={this.props.activeTab === 'businessServices' ? 'waves-effect active' : 'waves-effect'} onClick={this.setActiveTab.bind(this, 'businessServices', '', '')} >
                            <i title = "Buisness Services" style={this.props.isClassic  ? iconStyle : iconStyleClassic}  className="mdi mdi-settings"></i><span style={this.props.isClassic  ? sidebarTextColor : sidebarTextColorClassic} > Buisness Service </span></Link>

                    </li>
                    <li>
                        <Link to='/server' className={this.props.activeTab === 'server' ? 'waves-effect active' : 'waves-effect'} onClick={this.setActiveTab.bind(this, 'server', '', '')} >
                            <i title = "Servers" style={this.props.isClassic  ? iconStyle : iconStyleClassic}  className="mdi mdi-server"></i><span style={this.props.isClassic  ? sidebarTextColor : sidebarTextColorClassic} > Server </span></Link>

                    </li>
                    <li>
                        <Link to='/services' className={this.props.activeTab === 'services' ? 'waves-effect active' : 'waves-effect'} onClick={this.setActiveTab.bind(this, 'services', '', '')} >
                            <i title = "Services" style={this.props.isClassic  ? iconStyle : iconStyleClassic}  className="mdi mdi-television-guide "></i><span style={this.props.isClassic  ? sidebarTextColor : sidebarTextColorClassic} > Services </span></Link>

                    </li>
                    {/* BSP */}


                <li className={this.state.open ? `title-main` : `title-main open`} style={this.props.isClassic  ? sidebarTextColor : sidebarTextColorClassic}>{ this.state.open ? `Administration` : null}</li>
                <li>
                    <Link to="/presignedurl" className={this.props.activeTab === "presignedurl" ? 'waves-effect active' : 'waves-effect '} onClick={this.setActiveTab.bind(this, "presignedurl", '', '')}>
                        <i title = "Presigned URL Surveys" style={this.props.isClassic  ? iconStyle : iconStyleClassic}  className="mdi mdi-clipboard-outline"></i><span style={this.props.isClassic  ? sidebarTextColor : sidebarTextColorClassic} >Presigned URL Surveys</span></Link>
                </li>
                <li>
                    <Link to="/hierarchy" className={this.state.Tab === "hierarchy" ? 'waves-effect active' : 'waves-effect'} onClick={this.setActiveTab.bind(this, "hierarchy", '', '')}>
                        <i style={this.props.isClassic  ? iconStyle : iconStyleClassic}  className="mdi mdi-file-tree"></i><span label="Hierarchy" style={this.props.isClassic  ? sidebarTextColor : sidebarTextColorClassic} >Hierarchy</span></Link>
                </li>
                <li>
                    <Link to="/all_ahp" className={this.props.activeTab === "all_ahp" ? 'waves-effect active' : 'waves-effect '} onClick={this.setActiveTab.bind(this, "all_ahp", '', '')}>
                        <i title = "Criteria" style={this.props.isClassic  ? iconStyle : iconStyleClassic}  className="mdi mdi-clipboard-outline"></i>
                            <span style={this.props.isClassic  ? sidebarTextColor : sidebarTextColorClassic} >Criteria</span>
                    </Link>
                </li>
                <li>
                    <Link to='/sub_admin' className={this.props.activeTab === 'sub_admin' ? 'waves-effect active' : 'waves-effect'} onClick={this.setActiveTab.bind(this, 'sub_admin', '', '')} >
                        <i title = "Admins & Roles" style={this.props.isClassic  ? iconStyle : iconStyleClassic}  className="mdi mdi-voice"></i><span style={this.props.isClassic  ? sidebarTextColor : sidebarTextColorClassic} > Admins & Roles </span></Link>
                </li>
                <li>
                    <Link to='/clients' className={this.props.activeTab === 'clients' ? 'waves-effect active' : 'waves-effect'} onClick={this.setActiveTab.bind(this, 'clients', '', '')} >
                        <i title = "Client" style={this.props.isClassic  ? iconStyle : iconStyleClassic}  className="mdi mdi-account-network"></i><span style={this.props.isClassic  ? sidebarTextColor : sidebarTextColorClassic} > Clients </span></Link>
                </li>
                <li>
                    <Link to='/users' className={this.props.activeTab === 'users' ? 'waves-effect active' : 'waves-effect'} onClick={this.setActiveTab.bind(this, 'users', '', '')} >
                        <i title = "Users" style={this.props.isClassic  ? iconStyle : iconStyleClassic}  className="mdi mdi-account"></i><span style={this.props.isClassic  ? sidebarTextColor : sidebarTextColorClassic} > Users </span></Link>

                </li>

                <li className={this.state.open ? `title-main` : `title-main open`} style={this.props.isClassic  ? sidebarTextColor : sidebarTextColorClassic}>{ this.state.open ? `Configuration` : null}</li>
                <li>
                    <Link to='/basic-configuration' className={this.props.activeTab === 'basic-configuration' ? 'waves-effect active' : 'waves-effect'} onClick={this.setActiveTab.bind(this, 'basic-configuration', '', '')} >
                        <i title = "Basic" style={this.props.isClassic  ? iconStyle : iconStyleClassic}  className="mdi mdi-settings"></i><span style={this.props.isClassic  ? sidebarTextColor : sidebarTextColorClassic} > Basic </span></Link>
                </li>
                <li>
                    <Link to='/graph-configuration' className={this.props.activeTab === 'graph-configuration' ? 'waves-effect active' : 'waves-effect'} onClick={this.setActiveTab.bind(this, 'graph-configuration', '', '')} >
                        <i title = "Graph" style={this.props.isClassic  ? iconStyle : iconStyleClassic}  className="mdi mdi-settings"></i><span style={this.props.isClassic  ? sidebarTextColor : sidebarTextColorClassic} > Graph </span></Link>
                </li>
                <li className={this.state.open ? `title-main` : `title-main open`} style={this.props.isClassic  ? sidebarTextColor : sidebarTextColorClassic}>Insight</li>
                <li>
                    <Link to='/insights' className={this.props.activeTab === 'insights' ? 'waves-effect active' : 'waves-effect'} onClick={this.setActiveTab.bind(this, 'insights', '', '')} >
                        <i title = "Insights" style={this.props.isClassic  ? iconStyle : iconStyleClassic}  className="mdi mdi-buffer"></i><span style={this.props.isClassic  ? sidebarTextColor : sidebarTextColorClassic} > Insights </span></Link>
                </li>
        </AUX>
        )
        if (parseInt(this.props.accessLevel) === 1) {
            links = (
                <AUX>
                    <li className="menu-title" style={this.props.isClassic  ? sidebarTextColor : sidebarTextColorClassic}>Users</li>
                    <li>
                        <Link to='/' className={this.state.Tab === 'profile' ? 'waves-effect active' : 'waves-effect'} onClick={this.setActiveTab.bind(this, 'profile', '', '')} >
                            <i style={this.props.isClassic  ? iconStyle : iconStyleClassic}  className="mdi mdi-clipboard-outline"></i><span style={this.props.isClassic  ? sidebarTextColor : sidebarTextColorClassic} > Assessment Profiles </span></Link>
                    </li>
                </AUX>
            );
        }
        else if (parseInt(this.props.accessLevel) === 2) {
            links = (
                <AUX>
                    <li className="menu-title" style={this.props.isClassic  ? sidebarTextColor : sidebarTextColorClassic}>Users</li>
                    <li>
                        <Link to='/' className={this.state.Tab === 'questions' ? 'waves-effect active' : 'waves-effect'} onClick={this.setActiveTab.bind(this, 'questions', '', '')} >
                            <i style={this.props.isClassic  ? iconStyle : iconStyleClassic}  className="mdi mdi-calendar-check"></i><span style={this.props.isClassic  ? sidebarTextColor : sidebarTextColorClassic} > Questions </span></Link>
                    </li>
                </AUX>
            );

        }
        return (
            <div className="left side-menu" 
                

                style={this.props.isClassic  ? {...sideBarStyleD,
                    overflowY: "scroll",overflowX:"hidden"} : {...sideBarStyleDClassic,overflowY: "scroll", overflowX : "hidden"}}
            >

                {/* <Scrollbars style={{ height: 800, Color: 'red', overflowY: "scroll" }} > */}
                <div id="remove-scroll">
                    <div id="sidebar-menu">
                        <ul className="metismenu" id="side-menu">
                        <li style={appTitle} onClick={this.check}>
                                    <i style={this.props.isClassic  ? iconStyle : iconStyleClassic}  style={this.props.isClassic  ? {...iconStyle, fontSize:31} : {...iconStyleClassic,fontSize:31}} className="mdi mdi-menu menu-icon"></i>
                                </li>
                            {links}
                        </ul>
                    </div>
                    <div className="clearfix"></div>
                </div>
                {/* </Scrollbars> */}

            </div>

        );
    }
}


const mapStateToProps = state => {
    return {
        token: state.auth.token,
        accessLevel: state.auth.accessLevel,
        activeTab: state.ui_red.activeTab,
        isClassic : state.ui_red.isClassic

    }
}

const mapDispatchToProps = dispatch => {
    return {
        updateActivetab: (activetab) => dispatch(dashboardNavigation(activetab))
    }
}



export default compose(
    withRouter,
    connect(mapStateToProps, mapDispatchToProps))(leftsidebar)