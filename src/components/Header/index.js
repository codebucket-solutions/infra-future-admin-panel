import React from 'react';
import { Link } from 'react-router-dom';


const Header = (props) =>(
    <div className="row">
        <div className="col-sm-12">
            <div className="page-title-box">
                <h4 className="page-title">{props.title}</h4>
                <ol className="breadcrumb">
                    <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                    <li className="breadcrumb-item active">{props.title}</li>
                </ol>
            </div>
        </div>
    </div>
)

export default Header;