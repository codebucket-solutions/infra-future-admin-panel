import React from 'react';
import { Link } from 'react-router-dom';


const commonNav = (props) => (
    <nav className="navbar-custom" style={{ marginLeft: '0px' }}>
        <ul className="navbar-left d-flex list-inline float-left mb-0">
            <li>
                <Link to="/" className="logo logo-admin mt-4 mb-2" style={{ textTransform: 'none' }}>
                    <div className="pt-3 pl-3">
                        <span className="text-muted"> {process.env.REACT_APP_WEBSITE_NAME}</span>
                    </div>

                </Link>
            </li>
        </ul>
        {/* <ul className="navbar-right d-flex list-inline float-right pt-3 pl-3">
            <li>
                <Link to="/register" style={{border: "1px solid #1e61cd"}} className="btn btn-light w-md waves-effect waves-light" >Sign Up</Link>
            </li>
        </ul>
        <ul className="navbar-right d-flex list-inline float-right pt-4 pl-3">
            <li className="dropdown notification-list d-none d-sm-block">
                Don't have an account?
            </li>
        </ul> */}
    </nav>
)

export default commonNav;